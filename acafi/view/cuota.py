from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import date, datetime, timedelta

from acafi.models import cuota, fondo, indicador, serie, eventoInversion, fondoInversion
from acafi.deploy import convertir

#sacamos la variacion_valor_libro desde el dia actual hacia adelante y hacia atras
def get_variacion(c, dia_actual, dias_antes=10, dias_despues=10 ):
    
    
    lista_dias_antes = []
    lista_dias_despues = []

    if dia_actual != '' and dia_actual != None:
        dia_actual = datetime.strptime(dia_actual, '%Y-%m-%d')
    else:
        dia_actual = date.today()

    s = c.serie
    
    cuot = cuota.objects.get(serie = s, fecha= dia_actual)
    fin = cuot.valor_libro

    for x in range(1, dias_antes+1):
        lista_dias_antes += [dia_actual - timedelta(days=x)]
    
    for x in range(1, dias_despues+1):
        lista_dias_despues += [dia_actual + timedelta(days=x)]
    

    tot_lista = []
    
    for fecha in lista_dias_antes:
        aux ={}
        aux["f_inicio"] = str(fecha+ timedelta(days=1))

        try:
            c2 = cuota.objects.get(serie =s, fecha=fecha)
            ini = c2.valor_libro
            variacion = fin - ini
            aux["variacion"] = float(variacion)

            fin = c2.valor_libro
        except cuota.DoesNotExist:
            ini = 0
            variacion = 0
            aux["variacion"] = float(variacion)
            fin = c.valor_libro
        
        tot_lista.append(aux)

    ini = c.valor_libro
    for fecha in lista_dias_despues:
        aux ={}
        aux["f_inicio"] = fecha

        try:
            c2 = cuota.objects.get(serie =s, fecha=fecha)
            fin = c2.valor_libro
            variacion = fin - ini

            #aux["ini"] = float(ini)
            #aux["fin"] = float(fin)
            aux["variacion"] = float(variacion)
            ini = c2.valor_libro
        except cuota.DoesNotExist:
            fin = 0
            variacion = fin - ini
            #aux["ini"] = float(ini)
            #aux["fin"] = float(fin)
            aux["variacion"] = float(variacion)            
        
        tot_lista.append(aux)
        # de este total hay que sacar el menor valor

        tot_lista = list( sorted(tot_lista, key=lambda valor: valor['variacion']) )

    return tot_lista[0]
            


        #final menos inicial



    lista = cuota.objects.filter(fecha=ayer)

#lista todas las series
def listar_cuotas(request):
    

    url = ''
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    filter_fecha= request.GET.get('filter_fecha')
    filter_moneda= request.GET.get('filter_moneda')

    fondos = fondo.objects.all()
        

    lista = []
    
    monedas = cuota.objects.order_by().values_list('moneda', flat=True).distinct()


    lista = cuota.objects.all()
    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        #lista = cuota.objects.all()

        lista = lista.filter(serie__fondo__runsvs__contains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    
    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(serie__nombre=filter_name)
        url += 'filter_name=' + filter_name + '&'

    if filter_fecha != None and filter_fecha != '' and filter_fecha != 'None':
        lista = lista.filter(fecha=filter_fecha)

        url += 'filter_fecha=' + filter_fecha + '&'


    if filter_moneda != None and filter_moneda != '' and filter_moneda != 'None':
        lista = lista.filter(moneda=filter_moneda)
        url += 'filter_moneda=' + filter_moneda + '&'

    page = request.GET.get('page')
    if page is None:
        page = 1

    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    


    for l in lista:
        if l.moneda == 'PROM':
            origen = 'dolar'
        elif l.moneda == 'EUR':
            origen = 'euro'
        else:
            origen = 'Peso Chileno'
        l.valor_libro= [l.valor_libro, convertir(origen, 'Peso Chileno', l.valor_libro, fecha= l.fecha)]
        l.valor_economico= [l.valor_economico, convertir(origen, 'Peso Chileno', l.valor_economico, fecha= l.fecha)]
        l.patrimonio_neto= [l.patrimonio_neto, convertir(origen, 'Peso Chileno', l.patrimonio_neto, fecha= l.fecha)]
        l.activo_total= [l.activo_total, convertir(origen, 'Peso Chileno', l.activo_total, fecha= l.fecha)]

        #print(origen)
        
        if origen == 'dolar':
            valor_moneda = indicador.objects.filter(fecha__lt=l.fecha, dolar__gt=0).last()
            valor_moneda = valor_moneda.dolar  
        elif origen == 'euro':
            valor_moneda = indicador.objects.filter(fecha__lte=l.fecha, euro__gt=0).last()
            valor_moneda = valor_moneda.euro 
        elif origen == 'uf':
            valor_moneda = indicador.objects.filter(fecha__lte=l.fecha, uf__gt=0).last()
            valor_moneda = valor_moneda.uf 
        else:
            valor_moneda = 1

        l.moneda = [l.moneda, valor_moneda]

    
    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name, 'filter_fecha': filter_fecha, 'filter_moneda': filter_moneda}
    context = {"lista": lista, 'url': url, 'fondos' : fondos, 'monedas' : monedas, "count":count }

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'cuota/lista.html', merged_dict)

#lista todas las series
def listar_cuotas_div(request):
    

    url = ''
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    filter_fecha= request.GET.get('filter_fecha')
    filter_moneda= request.GET.get('filter_moneda')


    
    lista = []
    monedas = cuota.objects.order_by().values_list('moneda', flat=True).distinct()
    
    ayer = datetime.now() - timedelta(days=1)
    lista = cuota.objects.order_by('-fecha')#.filter(fecha=ayer)#.values('serie__fondo', 'serie').distinct()



    hoy = date.today()
    fondos = fondo.objects.filter(vigente=True)
    '''for f in fondos:
        series = serie.objects.filter(fondo = f)
        for s in series:
            lista.append(get_cuota(s, hoy))
    '''
    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        #lista = cuota.objects.all()

        lista = lista.filter(serie__fondo__runsvs__contains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    
    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(serie__nombre=filter_name)
        url += 'filter_name=' + filter_name + '&'

    if filter_fecha != None and filter_fecha != '' and filter_fecha != 'None':
        lista = lista.filter(fecha=filter_fecha)

        url += 'filter_fecha=' + filter_fecha + '&'


    if filter_moneda != None and filter_moneda != '' and filter_moneda != 'None':
        lista = lista.filter(moneda=filter_moneda)
        url += 'filter_moneda=' + filter_moneda + '&'

    orden = request.GET.get('orden')
    if orden == 'asc':
        lista = lista.order_by('fecha')


    page = request.GET.get('page')
    if page is None:
        page = 1

   
    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    
    for l in lista:
        '''
        try:
            finv = fondoInversion.objects.filter(serie=l.serie).first()
            if finv == None:
                dividendo = 0
            else:
                e = eventoInversion.objects.get(fondo_inversion= finv.cfi, fecha_limite=l.fecha, tipo ="DV")    
                dividendo = e.valor
        except (eventoInversion.DoesNotExist, fondoInversion.DoesNotExist):
            dividendo = 0
        '''
        try:
            dividendo = 0
            finv = fondoInversion.objects.filter(serie=l.serie)
            for f in finv:
                try:
                    e = eventoInversion.objects.get(fondo_inversion= f.cfi, fecha_limite=l.fecha, tipo ="DV")
                    dividendo += float(e.valor)
                except eventoInversion.DoesNotExist:
                    dividendo += 0
                
    
        except fondoInversion.DoesNotExist:
            dividendo = 0



        try:
            l.valor_libro = str("(("+str(l.valor_libro)+"+"+ str(dividendo)+') /'+str(l.cuota_anterior().valor_libro)+")-1")+" = "+str(l.rentab())
        except (TypeError, ZeroDivisionError, AttributeError):
            l.valor_libro = str('0') 

        try:
            l.indice = str( l.cuota_anterior().indice ) +"*(1+ rent ) ="+str(l.indice)
        except AttributeError:
            pass
    #for l in lista:
    #    l.valor_libro = l.serie

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name, 'filter_fecha': filter_fecha, 'filter_moneda': filter_moneda}
    context = {"lista": lista,  'url': url, 'fondos' : fondos, 'monedas' : monedas, 'count' : count }

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'cuota/lista_div.html', merged_dict)

