import pdfkit

from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from acafi.models import  administrador

#lista de administradores
def listar_administradores(request):
    lista = administrador.objects.all()

    url = ''
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    filter_vigente = request.GET.get('filter_vigente')
    filter_fi = request.GET.get('filter_fi')

    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        lista = lista.filter(rut__icontains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(razon_social__icontains=filter_name)
        url += 'filter_name=' + filter_name + '&'

    if filter_vigente != None and filter_vigente != '' and filter_vigente != 'None':
        lista = lista.filter(vigente=filter_vigente)
        url += 'filter_vigente=' + filter_vigente + '&'

    if filter_fi != None and filter_fi != '' and filter_fi != 'None':
        lista = lista.filter(fecha_inscripcion=filter_fi)
        url += 'filter_fi=' + filter_fi + '&'

    page = request.GET.get('page')
    if page is None:
        page = 1

    
    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name, 'filter_vigente': filter_vigente, "filter_fi":filter_fi}
    context = {"lista": lista, "count": count, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'administrador/lista.html', merged_dict)

def crear_pdf(request):
    pdf = pdfkit.from_url('http://18.216.50.249/perfil/fondo/factsheet/7001-7/2017/', False)
    response = HttpResponse(pdf,content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="7001-7.pdf"'

    pass