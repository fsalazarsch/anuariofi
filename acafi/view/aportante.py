from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from acafi.models import  aportante

#lista de aportantes
def listar_aportantes(request):
    lista = aportante.objects.all()

    url = ''
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    filter_serie = request.GET.get('filter_serie')

    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        lista = lista.filter(rut__icontains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(nombre__icontains=filter_name)
        url += 'filter_name=' + filter_name + '&'

    if filter_serie != None and filter_serie != '' and filter_serie != 'None':
        lista = lista.filter(tipo_persona__icontains=filter_serie)
        url += 'filter_serie=' + filter_serie + '&'

    page = request.GET.get('page')
    if page is None:
        page = 1

    
    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name, 'filter_serie': filter_serie}
    context = {"lista": lista, "count": count,  'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'aportante/lista.html', merged_dict)

