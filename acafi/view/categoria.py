from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.urls import reverse
from acafi.models import categoria

#lista de categorias
def listar_categorias(request):
    if request.user.is_staff or request.user.is_superuser:

        lista = categoria.objects.all()
        aux = lista

        url = ''
        filter_rut = request.GET.get('filter_rut')
        filter_name = request.GET.get('filter_name')
        filter_vigencia = request.GET.get('filter_vigencia')
        filter_tipo_fondo = request.GET.get('filter_tipo_fondo')
        filter_categoria = request.GET.get('filter_categoria')

        if filter_rut != None and filter_rut != '' and filter_rut != 'None':
            lista = lista.filter(indice__icontains=filter_rut)
            url += 'filter_rut=' + filter_rut + '&'

        if filter_name != None and filter_name != '' and filter_name != 'None':
            lista = lista.filter(nombre__icontains=filter_name)
            url += 'filter_name=' + filter_name + '&'

        if filter_categoria != None and filter_categoria != '' and filter_categoria != 'None':
            lista = lista.filter(categoria_madre__indice=filter_categoria)
            url += 'filter_categoria=' + filter_categoria + '&'

        page = request.GET.get('page')
        if page is None:
            page = 1


        count = lista.count()
        paginator = Paginator(lista, 15)
        lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

        try:
            lista = paginator.page(page)
        except PageNotAnInteger:
            lista = paginator.page(1)
        except EmptyPage:
            lista = paginator.page(paginator.num_pages)

        filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name,
                   'filter_categoria': filter_categoria}
        context = {"lista": lista, "aux": aux, "count": count, 'url': url}

        merged_dict = dict(list(filtros.items()) + list(context.items()))
        return render(request, 'categoria/lista.html', merged_dict)

    else:
        response = 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))
