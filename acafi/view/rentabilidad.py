from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from acafi.models import rent

#lista todos los paises
def listar_rentabilidades(request):
    lista = rent.objects.all()

    
    url = ''
    filter_periodo = request.GET.get('filter_periodo')
    filter_rut = request.GET.get('filter_rut')
    filter_vc = request.GET.get('filter_vc')
    if filter_vc != None:
        filter_vc = filter_vc.replace('.','')
        filter_vc = filter_vc.replace(',','.')

    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        lista = lista.filter(fondo__runsvs__icontains=filter_rut) | lista.filter(fondo__nombre__icontains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__icontains=filter_periodo)
        url += 'filter_periodo=' + filter_periodo + '&'

    
    page = request.GET.get('page')
    if page is None:
        page = 1


    count = lista.count()

    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]


    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_periodo': filter_periodo, 'filter_rut': filter_rut, 'filter_vc': filter_vc}
    context = {"lista": lista, "count": count,  'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'rentabilidad/lista.html', merged_dict)

