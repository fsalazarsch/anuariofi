from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from acafi.models import divisa

#lista todas las divisas
def listar_divisas(request):
    lista = divisa.objects.all()

    url = ''
    filter_codigo = request.GET.get('filter_codigo')
    filter_nombre = request.GET.get('filter_nombre')
    filter_nombresvs = request.GET.get('filter_nombresvs')
    

    if filter_codigo != None and filter_codigo != '' and filter_codigo != 'None':
        lista = lista.filter(divisa_id__icontains=filter_codigo)
        url += 'filter_codigo=' + filter_codigo + '&'

    if filter_nombre != None and filter_nombre != '' and filter_nombre != 'None':
        lista = lista.filter(nombre__icontains=filter_nombre)
        url += 'filter_nombre=' + filter_nombre + '&'

    if filter_nombresvs != None and filter_nombresvs != '' and filter_nombresvs != 'None':
        lista = lista.filter(alias_svs__icontains=filter_nombresvs)
        url += 'filter_nombresvs=' + filter_nombresvs + '&'

    
    page = request.GET.get('page')
    if page is None:
        page = 1

    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_codigo': filter_codigo, 'filter_nombre': filter_nombre, 'filter_nombresvs': filter_nombresvs}
    context = {"lista": lista, "count": count, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'divisa/lista.html', merged_dict)

