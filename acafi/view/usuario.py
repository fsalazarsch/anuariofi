import json, datetime, time, requests, uuid, sendgrid, os, pyexcel, sys

from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.urls import reverse
from django.conf import settings
from django.contrib.auth.models import User

from acafi.forms import Usuario_form, Formimportarlista
from acafi.models import fondo, fondo_ext, categoria, aportante, pais, administrador, perfil

from sendgrid.helpers.mail import *

#Envia un mail usando sendgrid
def enviar_mail(titulo, cuerpo, mail):
    apikey = "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"
    sg = sendgrid.SendGridAPIClient(apikey="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")
    from_email = Email("no-reply@acafi.cl")
    to_email = Email(mail)
    content = Content("text/html", cuerpo)
    mail = Mail(from_email, titulo, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())
    
#crea una contraseña para el usuario
def activar_usuario(request, token): 
    try:
        p = perfil.objects.get(token = token)
        if p.token == '':
            response = '2'
        else:
            u = p.usuario
            u.set_password(u.first_name+u.last_name)
            u.save()
            p.token =''
            p.save()
            response = '1'

            cuerpo = "Felicidades:<br> Ha activado correctamente su usuario: <br>"
            cuerpo +="<b>Credenciales de acceso</b><br>"
            cuerpo += "<b>Nombre:</b> "+str(u.username)+"<br>"
            cuerpo += "<b>Password:</b> "+str(u.first_name+u.last_name)+"<br>"
            cuerpo += "(Le recomendamos cambiar su contraseña)"
            enviar_mail('Sistema ACAFI', cuerpo, u.email)

    except ObjectDoesNotExist:

        response = '2'


    
    return HttpResponseRedirect('/login/?response='+response)

#lista usuarios no administradores
def listar_usuarios(request, response=None):
    if request.user.is_authenticated and request.user.is_superuser:
        lista = User.objects.filter(is_staff = 0)
       
        
        url = ''
        filter_name = request.GET.get('filter_name')
        filter_email = request.GET.get('filter_email')
        filter_activo = request.GET.get('filter_activo')
        filter_staff = request.GET.get('filter_staff')
        filter_fechaingreso = request.GET.get('filter_fechaingreso')

        if filter_name != None and filter_name != '' and filter_name != 'None':
            lista = lista.filter(username__icontains= filter_name)
            url +='filter_name='+filter_name+'&'
        
        if filter_email != None and filter_email != '' and filter_email != 'None':
            lista = lista.filter(email__icontains = filter_email)
            url +='filter_email='+filter_email+'&'

        if filter_activo != None and filter_activo != '' and filter_activo != 'None':
            lista = lista.filter(is_active = filter_activo)
            url +='filter_activo='+filter_activo+'&'

        if filter_staff != None and filter_staff != '' and filter_staff != 'None':
            lista = lista.filter(is_superuser = filter_staff)
            url +='filter_staff='+filter_staff+'&'

        if filter_fechaingreso != None and filter_fechaingreso != '' and filter_fechaingreso != 'None':
            lista = lista.filter(date_joined__gte = datetime.datetime.strptime(filter_fechaingreso, "%Y-%m-%d").date())
            url +='filter_fechaingreso='+filter_fechaingreso+'&'

        page = request.GET.get('page')
        if page is None:
            page = 1
    
        count = lista.count()
        paginator = Paginator(lista, 15)
        lista = lista[(15*(int(page)-1)): 15*(int(page)-1)+15]
        
        
        try:
            lista = paginator.page(page)
        except PageNotAnInteger:
            lista = paginator.page(1)
        except EmptyPage:
            lista = paginator.page(paginator.num_pages)


        filtros = {'response': response, 'page' : page, 'filter_email' : filter_email, 'filter_name' : filter_name, 'filter_activo' : filter_activo, 'filter_staff' : filter_staff, 'filter_fechaingreso' :filter_fechaingreso}
        context = {"lista" : lista, "count" : count, 'url':url }

        merged_dict = dict( list(filtros.items()) + list(context.items()))
        return render(request, 'usuarios/lista.html', merged_dict)

    else:
        response= 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

#creacion / modificacion de usuario
def index_usuario(request, id=None):
    flag = 0
    user = request.user
    if user.is_staff or user.is_superuser:
        flag = 2
    if  user.is_staff == 0 and user.is_superuser ==0:
        if id != None and int(user.id) == int(id):
            flag = 1

    if flag >= 1:
        
        if request.method == "POST": #formulario con fondo no asignado
            form = Usuario_form(request.POST)
            if id != None:
                u = User.objects.get(pk = id)
                oldpwd = u.password

                if (request.POST.get('password') != oldpwd):
                    u.set_password(request.POST.get('password'))
                response = 3
                cuerpo = "Se ha modificado el usuario "+str(id)+" con los siguientes parámetros:<br>"
                cuerpo += "Password: "+request.POST.get('password')+"<br>"
                cuerpo2 = ''

            else:
                u = User()
                u.datetime = datetime.datetime.now()
                response = 2
                token = uuid.uuid4().hex 
                url = settings.URL_BASE+"activar_usuario/"+token      
                cuerpo = "Se ha creado un nuevo usuario con los siguientes parámetros:<br>"
                cuerpo2 = "Bienvenido, ahora debe activar su cuenta haciendo click <a href='"+url+"'>aqui</a>, o ir a la siguiente dirección:<br>"+url
                

            u.first_name = request.POST.get('nombre')
            u.last_name = request.POST.get('apellido')
            u.username = u.first_name+' '+u.last_name
            u.email = request.POST.get('email')
            u.save()
            if id == None:
                id = User.objects.latest('id')

            cuerpo += "Nombre de usuario: "+request.POST.get('nombre')+" "+request.POST.get('apellido')+"<br>"
            cuerpo += "Email: "+request.POST.get('email')+"<br>"


            #CREAMOS EL PERFIL
            if response == 2:
                try:
                    p = perfil.objects.get(pk = id)
                    p.usuario = u
                    p.id_usercreador = request.user
                    p.nivel_acceso = 10
                    p.token = token

                except ObjectDoesNotExist:
                    p = perfil(usuario = u, id_usercreador = request.user, nivel_acceso = 10, token= token)
                    p.save()

                p.save()
            
            if request.POST.get('fondo'):
                f = fondo.objects.get(pk = request.POST.get('fondo'))
                f.user = u
                f.save()
            enviar_mail('Sistema ACAFI', cuerpo, settings.EMAIL_HOST_USER)
            if cuerpo2 != '':
                enviar_mail('Sistema ACAFI', cuerpo2, u.email)
            if response ==3:
                enviar_mail('Sistema ACAFI', cuerpo, u.email)
                return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

            return HttpResponseRedirect(reverse('usuarios',  kwargs={"response": response}))

        else:
            try:
                u = User.objects.get(pk = id)
                form = Usuario_form(initial={'username' : u.username, 'nombre' : u.first_name, 'apellido' : u.last_name, 'email' : u.email, 'password' : u.password})
            except ObjectDoesNotExist:
                form = Usuario_form()
                u = User()
            
            return render(request, 'usuarios/index.html', {'form' : form, 'id' : id, 'u' : u})

    else:
        response= 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

#importa usuarios desde un .xls, .ods. ó csv
def importar_usuarios(request):


    if request.method == "POST":
        form = Formimportarlista(request.POST, request.FILES)
        user = request.user
        filename = request.FILES['archivo'].name
        extension = filename.split(".")[-1]
        # Obtain the file extension and content
        # pass a tuple instead of a file name
        content = request.FILES['archivo'].read()
        #if sys.version_info[0] > 2:
            # in order to support python 3
            # have to decode bytes to str
        #    content = content.decode('utf-8')
        sheet = pyexcel.get_array(file_type=extension, file_content=content)
        # then use it as usual
        #sheet.name_columns_by_row(0)
        # respond with a json
        


        #sheet = pyexcel.get_sheet(file_name=request.FILES['archivo'].filename)
        #sheet = sheet.get_array()
        
        i = len(sheet[0])
        th = sheet[0]
        del sheet[0]
            
        
        for fila in range(len(sheet)):
            for col in range(len(sheet[0])):
        #        if type(sheet[fila][col]) != types.UnicodeType:
                sheet[fila][col] = str(sheet[fila][col])
        
        respuesta=''
        for s in sheet:
            if s[0] != '' and s[0] != None: #empresa
                i =1
                try:
                    while s[i] != '' and s[i] != None and s[i+1] != '' and s[i+1] != None: #username o email, nombre
                        #guardamos username y perfil nuevo
                        try:
                            u = User.objects.get(username=str(s[i+1]))
                            respuesta += str('El usuario '+str(u.username)+' ya existe<br>')
                        except User.DoesNotExist:
                            u = User(username=str(s[i+1]), email=str(s[i+1]), first_name=str(s[i]) )
                            u.set_password('acafi2018')
                            u.save()
                            respuesta += str('Se ha creado el usuario '+str(u.username)+'<br>')

                        adm = administrador.objects.get(razon_social__icontains= str(s[0]))
                        
                        try:
                            p = perfil.objects.get(usuario = u)                    
                            respuesta += str('Ya se ha asignado el usuario '+str(u.username)+' a la administradora '+str(adm.razon_social)+'<br>')

                        except perfil.DoesNotExist:
                            p = perfil(usuario= u, nivel_acceso=10, id_usercreador=request.user, administrador=adm)
                            p.save()
                            respuesta += str('Se ha asignado el usuario '+str(u.username)+' a la administradora '+str(adm.razon_social)+'<br>')
                        respuesta += str('<hr>')

                        i += 2
                except IndexError:
                    pass

           #response = 1
        return render(request, 'usuarios/importar.html', {'form': form, 'usuario': request.user, "sheet" : sheet, "i":i, "respuesta" : respuesta})

    else:
        form = Formimportarlista()
        return render(request, 'usuarios/importar.html', {'form': form, 'usuario': request.user})
