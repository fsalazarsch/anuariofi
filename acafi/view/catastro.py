from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.contrib.auth.models import User
from acafi.models import fondo, perfil, administrador, divisa, categoria, fondo_ext, movimiento, exportacion, inversion, periodo, rent, fondo_corfo, resumen, resultado, composicion, sector, pais, indicador
from acafi.view.usuario import enviar_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.safestring import mark_safe
from django.contrib.auth import authenticate, login
from django.template import loader
from django.db.models import Count, Avg, Sum, Q
from django.conf import settings
import json, csv, requests, decimal, pyexcel, datetime
from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from io import BytesIO
from openpyxl import Workbook, load_workbook

from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.styles import colors, fills
from openpyxl.cell import Cell

from django.db.models.functions import Substr
from acafi.deploy import escribir_log

def monto_real(fondo, periodo, origen, destino, monto):
    aux = periodo.split('-')
    try:
        moneda_real = origen.split(' de ')[1]
        if moneda_real == '':
            moneda_real = 'mxn'
    except IndexError:
        fe = fondo_ext.objects.get(runsvs = fondo.runsvs)
        moneda_real =  fe.divisa.divisa_id 

    if moneda_real.lower() == destino.lower():
        return monto
        
    else:
        i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
        nueva_fecha = i.fecha + timedelta(days=1)
        i = indicador.objects.get(pk= nueva_fecha)
            
        if moneda_real.lower() == 'pesos':
            if destino.lower() != 'pesos':
                while i._meta.get_field( destino.lower()).value_from_object(i) == None:
                    nueva_fecha = i.fecha + timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                return monto/i._meta.get_field( destino.lower()).value_from_object(i)*1000
                
        if destino.lower() == 'pesos':
            if moneda_real.lower() != 'pesos':
                while i._meta.get_field( moneda_real.lower()).value_from_object(i) == None:
                    nueva_fecha = i.fecha + timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                return float(monto*i._meta.get_field( moneda_real.lower()).value_from_object(i) )
                

        if moneda_real.lower() != 'pesos' and destino.lower() != 'pesos':
            while i._meta.get_field( moneda_real.lower()).value_from_object(i) == None or i._meta.get_field( destino.lower()).value_from_object(i) == None:
                nueva_fecha = i.fecha + timedelta(days=1)
                i = indicador.objects.get(pk= nueva_fecha)
            return float(monto*i._meta.get_field( moneda_real.lower())/i._meta.get_field( destino.lower()))
    


def get_sector(stri):
    try:
        s = sector.objects.get(pk=stri)
        return s.nombre
    except sector.DoesNotExist:
        return '---'

def fecha_tope(periodo):
    period = periodo.split('-')
    if int(period[0]) == 12:
        fech= str(int(period[1])+1)+"-01-01"
    else:
        fech= str(int(period[1]))+"-"+str(int(period[0])+1)+"-01"

    datetime_obj = datetime.strptime(fech, '%Y-%m-%d')
    return datetime_obj

def fecha_minima(periodo):
    period = periodo.split('-')
    fech= str(int(period[1]))+"-"+str(int(period[0])-2)+"-01"
    datetime_obj = datetime.strptime(fech, '%Y-%m-%d')
    return datetime_obj

def descargar_excel():
    peraux = filter_periodo.split("-")

    mes_inicio = int(peraux[0])-2
    anio = int(peraux[1])

    if int(peraux[0]) != 12:
        mes_termino = int(peraux[0])+1
    else:
        mes_termino = 1
        anio += 1

    if mes_inicio < 10:
        mes_inicio = "0"+str(mes_inicio)

    fecha_desde = "01/"+str(mes_inicio)+"/"+str(anio)

    if mes_termino < 10:
        mes_termino = "0"+str(mes_termino)

    fecha_hasta = "01/"+str(mes_termino)+"/"+str(anio)

    url="http://www.cmfchile.cl/institucional/inc/clasificaciones_asignadas_excel_fcorte_2.php?clasificadora=0&tipo_emisor=0&emisor=0&tipo_instrumento=0&fecha_desde="+str(fecha_desde)+"&fecha_hasta="+str(fecha_hasta)+"&fecha_corte="+str(fecha_hasta)+"&insc_emisor=0&viginst=0"
    hdr = {'User-Agent': 'Mozilla/5.0'}
    r = requests.post(url, headers=hdr)
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    output = open(ruta+"riesgos.xlsx", 'wb')
    output.write(r.content)
    output.close()

def get_filter_periodo():
    return "12-2018"

filter_periodo = get_filter_periodo()

def get_lista():
    fecha_top = fecha_tope( get_filter_periodo() )
    #select * from acafi_fondo where 
    #    (termino_operaciones >= "2018-10-01" or termino_operaciones = null);
    lista = fondo.objects.filter(Q(termino_operaciones__gte= fecha_top) | Q( termino_operaciones__isnull=True))
    #    inicio_operaciones < "2018-10-01" and inicio_operaciones  is not null and 
    lista = lista.filter(inicio_operaciones__lt = fecha_top, inicio_operaciones__isnull=False).order_by('runsvs')

    aux = lista

    for l in lista:
        if not movimiento.objects.filter(fondo = l, periodo = get_filter_periodo()).exists():
            aux = aux.exclude(runsvs=l.runsvs)

    return aux

def get_nuevos_fondos():
    fecha_top = fecha_tope( get_filter_periodo() )
    fecha_min = fecha_minima( get_filter_periodo() )

    lista = fondo.objects.filter(inicio_operaciones__lt = fecha_top, inicio_operaciones__isnull=False).order_by('runsvs')
    lista = lista.filter(inicio_operaciones__gte= fecha_min)
    return lista

def estados_financieros(request):#hoja1
    def fondo_gris(ws, stri, bold=False):
        bggris = colors.Color(rgb='f8f8f8')
        my_fill = fills.PatternFill(patternType='solid', fgColor=bggris)
        fuente = Font(name= 'Calibri', bold=bold)

        ws[stri].fill = my_fill
        ws[stri].font = fuente

    #filter_periodo = get_filter_periodo()

    #wb = load_workbook(ruta+'hoja9.xlsx')
    #wb = Workbook()
    #ws = wb.create_sheet("1-EEFF", 0) # insert at first position

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    try:
        wb = load_workbook(ruta+'precatastro'+filter_periodo+'.xlsx')
    except FileNotFoundError:
        wb = Workbook()
        wb.save(ruta+'precatastro'+filter_periodo+'.xlsx')
    
    if "1-EEFF" not in wb.sheetnames:
        ws = wb.create_sheet("1-EEFF")
    else:
        ws = wb["1-EEFF"]


    lista = get_lista()
    
    ws['B1'] = "ESTADO DE SITUACION FINANCIERA"
    ws['B7'] = "ACTIVO"
    ws['B8'] = "Activo Corriente"
    ws['B19'] = "Activo no Corriente"
    ws['B32'] = "PASIVO"
    ws['B33'] = "Pasivo Corriente"
    ws['B44'] = "Pasivo no Corriente"
    ws['B53'] = "PATRIMONIO NETO"

    ws['C9'] = "Efectivo y efectivo equivalente (+)"
    ws['C10'] = "Activos financieros a valor razonable con efecto en resultados (+)"
    ws['C11'] = "Activos financieros a valor razonable con efecto en otros resultados integrales (+)"
    ws['C12'] = "Activos financieros a valor razonable con efecto en resultados entregados en garantía (+)"
    ws['C13'] = "Activos financieros a costo amortizado (+)"
    ws['C14'] = "Cuentas y documentos por cobrar por operaciones (+)"
    ws['C15'] = "Otros documentos y cuentas por cobrar (+)"
    ws['C16'] = "Otros activos (+)"
    ws['C17'] = "Total Activo Corriente (+)"

    ws['C20'] = "Activos financieros a valor razonable con efecto en resultados (+)"
    ws['C21'] = "Activos financieros a valor razonable con efecto en otros resultados integrales (+)"
    ws['C22'] = "Activos financieros a costo amortizado (+)"
    ws['C23'] = "Cuentas y documentos por cobrar por operaciones (+)"
    ws['C24'] = "Otros documentos y cuentas por cobrar (+)"
    ws['C25'] = "Inversiones valorizadas por el método de la participación (+)"
    ws['C26'] = "Propiedades de Inversión (+)"
    ws['C27'] = "Otros activos (+)"
    ws['C28'] = "Total Activo No Corriente (+)"
    ws['C30'] = "Total Activo (+)"

    ws['C34'] = "Pasivos financieros a valor razonable con efecto en resultados (+)"
    ws['C35'] = "Préstamos (+)"
    ws['C36'] = "Otros Pasivos Financieros (+)"
    ws['C37'] = "Cuentas y documentos por pagar por operaciones (+)"
    ws['C38'] = "Remuneraciones sociedad administradora (+)"
    ws['C39'] = "Otros documentos y cuentas por pagar (+)"
    ws['C40'] = "Ingresos anticipados (+)"
    ws['C41'] = "Otros pasivos (+)"
    ws['C42'] = "Total Pasivo Corriente (+)"

    ws['C45'] = "Préstamos (+)"
    ws['C46'] = "Otros Pasivos Financieros (+)"
    ws['C47'] = "Cuentas y documentos por pagar por operaciones (+)"
    ws['C48'] = "Otros documentos y cuentas por pagar (+)"
    ws['C49'] = "Ingresos anticipados (+)"
    ws['C50'] = "Otros pasivos (+)"
    ws['C51'] = "Total Pasivo No Corriente (+)"

    ws['C54'] = "Aportes (+)"
    ws['C55'] = "Otras Reservas (+)"
    ws['C56'] = "Resultados Acumulados (+ ó -)"
    ws['C57'] = "Resultado del ejercicio (+ ó -)"
    ws['C58'] = "Dividendos provisorios (-)"
    ws['C59'] = "Total Patrimonio Neto (+ ó -)"
    ws['C61'] = "Total Pasivo (+)"

    ws['B66'] = "INGRESOS/ PERDIDAS DE LA OPERACION"
    ws['B80'] = "GASTOS"
    ws['B94'] = "Otros resultados integrales"

    ws['C67'] = "Intereses y reajustes (+)"
    ws['C68'] = "Ingresos por dividendos (+)"
    ws['C69'] = "Diferencias de cambio netas sobre activos financieros a costo amortizado (+ ó -)"
    ws['C70'] = "Diferencias de cambio netas sobre efectivo y efectivo equivalente (+ ó -)"
    ws['C71'] = "Cambios netos en valor razonable de activos financieros y pasivos financieros a valor razonable con efecto en resultados (+ ó -)"
    ws['C72'] = "Resultado en venta de instrumentos financieros (+ ó -)"
    ws['C73'] = "Resultados por venta de inmuebles (+)"
    ws['C74'] = "Ingreso por arriendo de bienes raíces (+)"
    ws['C75'] = "Variaciones en valor razonable de propiedades de inversión (+ ó -)"
    ws['C76'] = "Resultado en inversiones valorizadas por el método de la participación (+ ó -)"
    ws['C77'] = "Otros (+ ó -)"
    ws['C78'] = "Total ingresos/(pérdidas) netos de la operación (+ ó -)"

    ws['C81'] = "Depreciaciones (-)"
    ws['C82'] = "Remuneración del Comité de Vigilancia (-)"
    ws['C83'] = "Comisión de administración (-)"
    ws['C84'] = "Honorarios por custodia y admistración (-)"
    ws['C85'] = "Costos de transacción (-)"
    ws['C86'] = "Otros gastos de operación (-)"
    ws['C87'] = "Total gastos de operación (-)"
    ws['C88'] = "Utilidad/(pérdida) de la operación (+ ó -)"
    ws['C89'] = "Costos financieros (-)"
    ws['C90'] = "Utilidad/(pérdida) antes de impuesto (+ ó -)"
    ws['C91'] = "Impuesto a las ganancias por inversiones en el exterior (-)"
    ws['C92'] = "Resultado del ejercicio (+ ó -)"

    ws['C95'] = "Cobertura de Flujo de Caja (+)"
    ws['C96'] = "Ajustes por Conversión (+ ó -)"
    ws['C97'] = "Ajustes provenientes de inversiones valorizadas por el método de la participación (+ ó -)"
    ws['C98'] = "Otros Ajustes al Patrimonio Neto (+ ó -)"
    ws['C99'] = "Total de otros resultados integrales (+ ó -)"
    ws['C101'] = "Total Resultado Integral (+ ó -)"

    for i in range (1, 102):
        fondo_gris(ws, 'B'+str(i), True)
        fondo_gris(ws, 'C'+str(i))

    wb.save(ruta+'precatastro_eeff'+filter_periodo+'.xlsx')
    
    col = 5
    fila = 4
    for l in lista:
        fila = 4
        print(l.runsvs) 
        if ws.cell(row=fila, column=col).value == None:
            
            print(str(fila)+":"+str(col)+":"+l.runsvs)
            
            ws.cell(row=fila, column=col, value=l.runsvs)
            fuente = Font(name= 'Calibri', bold=True)
            ws.cell(row=fila, column=col).font= fuente

            #if filter_periodo == None:
            #    filter_periodo = "06-2018"
            
            movs = movimiento.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="AC-")
            fila = 9
            for m in movs:
                ws.cell(row=fila, column=col, value=m.monto)
                ws.cell(row=fila, column=col).number_format = '#,##0'
                fila+=1

            movs = movimiento.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="ANC-")
            fila = 20
            for m in movs:
                ws.cell(row=fila, column=col, value=m.monto)
                ws.cell(row=fila, column=col).number_format = '#,##0'
                fila+=1

            movs = movimiento.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="PC-")
            fila = 34
            for m in movs:
                ws.cell(row=fila, column=col, value=m.monto)
                ws.cell(row=fila, column=col).number_format = '#,##0'
                fila+=1

            movs = movimiento.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="PNC-")
            fila = 45
            for m in movs:
                ws.cell(row=fila, column=col, value=m.monto)
                ws.cell(row=fila, column=col).number_format = '#,##0'
                fila+=1

            movs = movimiento.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="PN-")
            fila = 54
            for m in movs:
                ws.cell(row=fila, column=col, value=m.monto)
                ws.cell(row=fila, column=col).number_format = '#,##0'
                fila+=1

            filas = [17, 28, 30, 42, 51, 59, 61]
            index = 0
            movs = movimiento.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="TOTAL")
            for m in movs:
                ws.cell(row=filas[index], column=col, value=m.monto)
                ws.cell(row=filas[index], column=col).number_format = '#,##0'
                index += 1        


            movs = resultado.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="IPDLO-")
            fila = 67
            for m in movs:
                ws.cell(row=fila, column=col, value=m.monto)
                ws.cell(row=fila, column=col).number_format = '#,##0'
                fila+=1        

            try:
                movs = resultado.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="G-")
                movs = movs.exclude(etiqueta_svs__endswith="costos financieros")
                ws.cell(row=89, column=col, value= resultado.objects.get(fondo = l, periodo = filter_periodo, etiqueta_svs="G-costos financieros").monto )
                ws.cell(row=89, column=col).number_format = '#,##0'

                movs = movs.exclude(etiqueta_svs__endswith="impuesto a las ganancias por inversiones en el exterior")
                ws.cell(row=91, column=col, value= resultado.objects.get(fondo = l, periodo = filter_periodo, etiqueta_svs="G-impuesto a las ganancias por inversiones en el exterior").monto )
                ws.cell(row=91, column=col).number_format = '#,##0'
            except resultado.DoesNotExist:
                ws.cell(row=89, column=col, value= 0)
                ws.cell(row=91, column=col, value= 0)
                

            fila = 81
            for m in movs:
                ws.cell(row=fila, column=col, value=m.monto)
                ws.cell(row=fila, column=col).number_format = '#,##0'

                fila+=1     

            movs = resultado.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="ORI-")
            fila = 95
            for m in movs:
                ws.cell(row=fila, column=col, value=m.monto)
                ws.cell(row=fila, column=col).number_format = '#,##0'
                fila+=1

            filas = [78, 87, 99, 101]
            index = 0
            movs = resultado.objects.filter(fondo = l, periodo = filter_periodo, etiqueta_svs__startswith="TOTAL")
            for m in movs:
                ws.cell(row=filas[index], column=col, value=m.monto)
                ws.cell(row=filas[index], column=col).number_format = '#,##0'

                index += 1  
            try:
                ws.cell(row=88, column=col, value= resultado.objects.get(fondo = l, periodo = filter_periodo, etiqueta_svs__icontains="PERDIDA DE LA OPERACION").monto )
                ws.cell(row=88, column=col).number_format = '#,##0'
            except resultado.DoesNotExist:
                ws.cell(row=88, column=col, value= 0)
                
            try:
                ws.cell(row=90, column=col, value= resultado.objects.get(fondo = l, periodo = filter_periodo, etiqueta_svs__icontains="PERDIDA ANTES DE IMPUESTO").monto )
                ws.cell(row=90, column=col).number_format = '#,##0'
            except resultado.DoesNotExist:
                ws.cell(row=90, column=col, value= 0)

            try:
                ws.cell(row=92, column=col, value= resultado.objects.get(fondo = l, periodo = filter_periodo, etiqueta_svs__icontains="RESULTADO DEL EJERCICIO").monto )
                ws.cell(row=92, column=col).number_format = '#,##0'
            except resultado.DoesNotExist:
                ws.cell(row=92, column=col, value= 0)

        col+=1
        wb.save(ruta+'precatastro_eeff'+filter_periodo+'.xlsx')


        #writer.writerow([l.runsvs, l.nombre, vigente, admin, validado, categ, estr, comision, comentario, divisa, codigo_bolsa, bloomberg, isin, bolsa_offshore, portafolio_manager, riesgo_feller_rate, riesgo_comision, riesgo_fitch_ratings, riesgo_humphreys, riesgo_icr ])
        #i += 1
        #print(str(i)+" "+l.periodo+" "+l.fondo.runsvs)

    #ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_eeff'+filter_periodo+'.xlsx')

    return HttpResponseRedirect('/perfil/')

def parametros_iniciales(request):#hoja2
    
    #filter_periodo = get_filter_periodo()

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    try:
        wb = load_workbook(ruta+'parametros_iniciales'+filter_periodo+'.xlsx')
    except FileNotFoundError:
        wb = Workbook()
        ws = wb.active
        ws.title = "2-Parametros Iniciales"
        wb.save(ruta+'parametros_iniciales'+filter_periodo+'.xlsx')
    
    #if "2-Parametros Iniciales" not in wb.sheetnames:
    #    ws = wb.create_sheet("2-Parametros Iniciales")
    #else:
    #    ws = wb["2-Parametros Iniciales"]

    #wb = Workbook()
    #ws = wb.create_sheet("2-Parametros Iniciales", 0) # insert at first position

    ws['B2'] = "Activos Administrados por Administradora y Tipo de Fondo"
    ws['C2'] = "RUT Admin"
    ws['D2'] = "Administradora"
    ws['E2'] = "Rut Fondo"
    ws['F2'] = "Clase"
    ws['G2'] = "Rescatable/no Rescatable"
    ws['H2'] = "Moneda"
    ws['I2'] = "Activos"
    ws['J2'] = "Aportes"
    ws['K2'] = "Reparto Patrimonio"
    ws['L2'] = "Reparto Dividendos"
    ws['M2'] = "Patrimonio Neto"
    ws['N2'] = "Valores Cuota"
    ws['O2'] = "Fecha inicio"
    ws['P2'] = "Fecha Termino"    

    lista = get_lista()

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    col = 2
    fila = 6
    for l in lista:
        print(l.runsvs)
        ws.cell(row=fila, column=col, value=l.nombre)
        ws.cell(row=fila, column=col+1, value=l.admin.rut)
        ws.cell(row=fila, column=col+2, value=l.admin.razon_social)
        ws.cell(row=fila, column=col+3, value=l.runsvs)
        if l.categoria == None:
            categorianombre = '---'
        else:
            categorianombre = l.categoria.indice
        ws.cell(row=fila, column=col+4, value=categorianombre)
        if l.tipo_inversion == 0:
            tipo_inversion = "No Rescatable"
        if l.tipo_inversion == 1:
            tipo_inversion = "Rescatable"
        ws.cell(row=fila, column=col+5, value=tipo_inversion)
        
        try:
            m = movimiento.objects.get(fondo=l, periodo=filter_periodo, etiqueta_svs="Total Activo")
            ws.cell(row=fila, column=col+6, value=m.moneda)

            ws.cell(row=fila, column=col+7, value=m.monto)
        except movimiento.DoesNotExist:
            ws.cell(row=fila, column=col+6, value='')
            ws.cell(row=fila, column=col+7, value=0)

        '''
        try:
            m = movimiento.objects.get(fondo=l, periodo=filter_periodo, etiqueta_svs__icontains="Aportes")
            ws.cell(row=fila, column=col+8, value=m.monto)
        except movimiento.DoesNotExist:
            ws.cell(row=fila, column=col+8, value=0)
        '''
        
        p = periodo.objects.get(periodo=filter_periodo, fondo=l)
        aux = json.loads(p.datos)
        ws.cell(row=fila, column=col+8, value=aux['APORTES'])
        ws.cell(row=fila, column=col+9, value=aux['REPARTOS DE PATRIMONIOS'])
        ws.cell(row=fila, column=col+10, value=aux['REPARTOS DE DIVIDENDOS'])

        try:
            m = movimiento.objects.get(fondo=l, periodo=filter_periodo, etiqueta_svs__icontains="Total Patrimonio Neto")
            ws.cell(row=fila, column=col+11, value=m.monto)
            ws.cell(row=fila, column=col+12, value=str(float(p.valor_obs)))
        except movimiento.DoesNotExist:
            ws.cell(row=fila, column=col+11, value=0)
            ws.cell(row=fila, column=col+12, value=0)
        
        ws.cell(row=fila, column=col+13, value=l.inicio_operaciones)
        if l.termino_operaciones:
            ws.cell(row=fila, column=col+14, value=l.termino_operaciones)  
        else:
            ws.cell(row=fila, column=col+14, value="---") 

        wb.save(ruta+'parametros_iniciales'+filter_periodo+'.xlsx')
        #Valor Libro de la Cuota
        fila += 1
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'parametros_iniciales'+filter_periodo+'.xlsx')

    return HttpResponseRedirect('/perfil/')

def nuevos_fondos(request):#hoja3

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    try:
        wb = load_workbook(ruta+'nuevos_fondos'+filter_periodo+'.xlsx')
    except FileNotFoundError:
        wb = Workbook()
        ws = wb.active
        ws.title = "3-Nuevos Fondos Trimestre"
        wb.save(ruta+'nuevos_fondos'+filter_periodo+'.xlsx')


    ws['C2'] = "Rut Fondo"
    ws['D2'] = "Nombre Fondo"
    ws['E2'] = "Rescatable/no Rescatable"
    ws['F2'] = "Incio operaciones"
    ws['G2'] = "Clasificacion acafi"
    ws['H2'] = "Administradora"
    ws['I2'] = "Tiene datos en la eeff"
    
    lista = get_nuevos_fondos()
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    col = 3
    fila = 3
    for l in lista:
        print(l.runsvs)
        
        ws.cell(row=fila, column=col, value=l.runsvs)
        ws.cell(row=fila, column=col+1, value=l.nombre)

        if l.tipo_inversion == 0:
            tipo_inversion = "No Rescatable"
        if l.tipo_inversion == 1:
            tipo_inversion = "Rescatable"
        ws.cell(row=fila, column=col+2, value=tipo_inversion)
        ws.cell(row=fila, column=col+3, value=l.inicio_operaciones)

        if l.categoria == None:
            categorianombre = '---'
        else:
            categorianombre = l.categoria.indice
        ws.cell(row=fila, column=col+4, value=categorianombre)
        ws.cell(row=fila, column=col+5, value=l.admin.razon_social)

        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:
            res = "Si"
        else:
            res = "No"
        ws.cell(row=fila, column=col+6, value=res)
 
        #Valor Libro de la Cuota
        fila += 1
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'nuevos_fondos'+filter_periodo+'.xlsx')

    return HttpResponseRedirect('/perfil/')


def cartera_inversiones1(request):#hoja4

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    try:
        wb = load_workbook(filename = ruta+'precatastro_cartera_inversiones_'+filter_periodo+'.xlsx')
    except FileNotFoundError:
        wb = Workbook()
        wb.save(ruta+'precatastro_cartera_inversiones_'+filter_periodo+'.xlsx')

    #wb = Workbook()
    ws = wb.active
    ws.title = "4-Cartera de Inversiones"

    arr = ["Acciones de sociedades anónimas abiertas", "Derechos preferentes de suscripción de acciones de sociedades anónimas abiertas", "Cuotas de fondos mutuos", "Cuotas de fondos de inversión", "Certificados de depósitos de valores (CDV)", "Títulos que representen productos", "Otros títulos de renta variable", "Depósitos a plazo y otros títulos de bancos e instituciones financieras", "Cartera de créditos o de cobranzas", "Títulos emitidos o garantizados por Estados o Bancos Centrales", "Otros títulos de deuda", "Acciones no registradas", "Cuotas de fondos de inversión privados", "Títulos de deuda no registrados", "Bienes raíces", "Proyectos en desarrollo", "Deuda de operaciones de leasing", "Acciones de sociedades anónimas inmobiliarias y concesionarias", "Otras inversiones"]
    largo = len(arr)
    col = 8
    fila = 6
    for i in range(2):
        for a in arr:
            ws.cell(row=fila, column=col, value=a)
            col += 1
        
    ws['H5'] = "Nacional"
    ws['AA5'] = "Extranjero"
    #ws['AT5'] = "Total"

    lista = get_lista()

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    #lista = lista.filter(pk__gte="8000-9")

    for l in lista:
        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:

            if  ws.cell(row=fila+1, column=col).value == None:
                print(str(col)+" "+str(fila)+l.runsvs)

                col = 2
                
                ws.cell(row=fila+1, column=col, value=l.nombre)
                ws.cell(row=fila+1, column=col+1, value=l.admin.rut)
                ws.cell(row=fila+1, column=col+2, value=l.admin.razon_social)
                ws.cell(row=fila+1, column=col+3, value=l.runsvs)
                if l.categoria == None:
                    categorianombre = '---'
                else:
                    categorianombre = l.categoria.indice
                ws.cell(row=fila+1, column=col+4, value=categorianombre)
                if l.tipo_inversion == 0:
                    tipo_inversion = "No Rescatable"
                if l.tipo_inversion == 1:
                    tipo_inversion = "Rescatable"
                fila += 1
        wb.save(ruta+'precatastro_cartera_inversiones_'+filter_periodo+'.xlsx')
    
    fila = 7
    
    for l in lista:
        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:
            print(l.runsvs)

            col = 8


            p = periodo.objects.get(fondo=l, periodo=filter_periodo)
            for a in arr:
                if ws.cell(row=fila, column=col).value != '' or ws.cell(row=fila, column=col+largo+1).value != '':
                    try:
                        r = resumen.objects.get(periodo=p, descripcion=a)
                        rnacional = r.nacional
                        rextranjero = r.extranjero
                    except resumen.DoesNotExist:
                        rnacional = 0
                        rextranjero = 0

                    ws.cell(row=fila, column=col, value=rnacional)
                    ws.cell(row=fila, column=col+largo, value=rextranjero)
                
                col += 1

            wb.save(ruta+'precatastro_cartera_inversiones_'+filter_periodo+'.xlsx')
            fila += 1
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_cartera_inversiones_.xlsx')

    return HttpResponseRedirect('/perfil/')

def cartera_inversiones(request):#hoja4.1

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    try:
        wb = load_workbook(filename = ruta+'precatastro_cartera_inversiones_'+filter_periodo+'.xlsx')
    except FileNotFoundError:
        wb = Workbook()
        wb.save(ruta+'precatastro_cartera_inversiones_'+filter_periodo+'.xlsx')

    #wb = Workbook()
    ws = wb.active
    ws.title = "4-Cartera de Inversiones (CLP)"

    arr = ["Acciones de sociedades anónimas abiertas", "Derechos preferentes de suscripción de acciones de sociedades anónimas abiertas", "Cuotas de fondos mutuos", "Cuotas de fondos de inversión", "Certificados de depósitos de valores (CDV)", "Títulos que representen productos", "Otros títulos de renta variable", "Depósitos a plazo y otros títulos de bancos e instituciones financieras", "Cartera de créditos o de cobranzas", "Títulos emitidos o garantizados por Estados o Bancos Centrales", "Otros títulos de deuda", "Acciones no registradas", "Cuotas de fondos de inversión privados", "Títulos de deuda no registrados", "Bienes raíces", "Proyectos en desarrollo", "Deuda de operaciones de leasing", "Acciones de sociedades anónimas inmobiliarias y concesionarias", "Otras inversiones"]
    largo = len(arr)
    col = 8
    fila = 6
    for i in range(2):
        for a in arr:
            ws.cell(row=fila, column=col, value=a)
            col += 1
        
    ws['H5'] = "Nacional"
    ws['AA5'] = "Extranjero"
    #ws['AT5'] = "Total"

    lista = get_lista()

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    #lista = lista.filter(pk__gte="8000-9")

    for l in lista:
        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:

            if  ws.cell(row=fila+1, column=col).value == None:
                print(str(col)+" "+str(fila)+l.runsvs)

                col = 2
                
                ws.cell(row=fila+1, column=col, value=l.nombre)
                ws.cell(row=fila+1, column=col+1, value=l.admin.rut)
                ws.cell(row=fila+1, column=col+2, value=l.admin.razon_social)
                ws.cell(row=fila+1, column=col+3, value=l.runsvs)
                if l.categoria == None:
                    categorianombre = '---'
                else:
                    categorianombre = l.categoria.indice
                ws.cell(row=fila+1, column=col+4, value=categorianombre)
                if l.tipo_inversion == 0:
                    tipo_inversion = "No Rescatable"
                if l.tipo_inversion == 1:
                    tipo_inversion = "Rescatable"
                fila += 1
        wb.save(ruta+'precatastro_cartera_inversiones_'+filter_periodo+'.xlsx')
    
    fila = 7
    
    for l in lista:
        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:
            print(l.runsvs)
            origen = resultado.objects.filter(periodo=filter_periodo, fondo=l).first().moneda
            #ORIGEN

            col = 8


            p = periodo.objects.get(fondo=l, periodo=filter_periodo)        
            for a in arr:
                if ws.cell(row=fila, column=col).value != '' or ws.cell(row=fila, column=col+largo+1).value != '':
                    try:
                        r = resumen.objects.get(periodo=p, descripcion=a)
                        rnacional = monto_real(l, filter_periodo, origen, 'pesos', r.nacional) 
                        rextranjero = monto_real(l, filter_periodo, origen, 'pesos', r.extranjero) 
                    except resumen.DoesNotExist:
                        rnacional = 0
                        rextranjero = 0

                    ws.cell(row=fila, column=col, value=rnacional)
                    ws.cell(row=fila, column=col+largo, value=rextranjero)
                
                col += 1

            wb.save(ruta+'precatastro_cartera_inversiones_'+filter_periodo+'.xlsx')
            fila += 1
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_cartera_inversiones_.xlsx')

    return HttpResponseRedirect('/perfil/')


def acciones_nacionales(request):#hoja5
    
    wb = Workbook()
    ws = wb.create_sheet("5- Acciones Nac.x Sector", 0) # insert at first position

    ws['F4'] = "Clasificacion"
    ws['G4'] = "Nemotécnico"
    ws['H4'] = "Rut Emisor"
    ws['I4'] = "Cod. País"
    ws['J4'] = "Tipo Instrumento"
    ws['K4'] = "Fecha Vencimiento"
    ws['L4'] = "Situación del Instr."
    ws['M4'] = "Clasif. De Riesgo"
    ws['N4'] = "Grupo Empresarial"
    ws['O4'] = "Cant Unidades"
    ws['P4'] = "Tipo Unidades"
    ws['Q4'] = "TIR, Valor par o Precio"
    ws['R4'] = "Código Valolización"
    ws['S4'] = "Base tasa"
    ws['T4'] = "Tipo de interés"
    ws['U4'] = "Valolización al Cierre"
    ws['V4'] = "Cód. Moneda Liquidación"
    ws['W4'] = "Cód. Pais Transacción"
    ws['X4'] = "Capital del Emisor"
    ws['Y4'] = "Tot. Activo del Emisor"
    ws['Z4'] = "Tot. Activo del Fondo"

    lista = get_lista()

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    col = 1
    fila = 6
    for l in lista:
        print(l.runsvs)
        
        ws.cell(row=fila, column=col, value=l.nombre)
        ws.cell(row=fila, column=col+1, value=l.admin.rut)
        ws.cell(row=fila, column=col+2, value=l.admin.razon_social)
        ws.cell(row=fila, column=col+3, value=l.runsvs)
        if l.categoria == None:
            categorianombre = '---'
        else:
            categorianombre = l.categoria.nombre
        ws.cell(row=fila, column=col+4, value=categorianombre)

        
        acciones = inversion.objects.filter(fondo = l, periodo=filter_periodo)
        for a in acciones:
            if a.tipo_instrumento == "ACC":
                ws.cell(row=fila, column=col, value=l.nombre)
                ws.cell(row=fila, column=col+1, value=l.admin.rut)
                ws.cell(row=fila, column=col+2, value=l.admin.razon_social)
                ws.cell(row=fila, column=col+3, value=l.runsvs)
                if l.categoria == None:
                    categorianombre = '---'
                else:
                    categorianombre = l.categoria.nombre
                ws.cell(row=fila, column=col+4, value=categorianombre)

                ws.cell(row=fila, column=col+5, value=get_sector(a.nemotecnico))
                ws.cell(row=fila, column=col+6, value=a.nemotecnico)
                ws.cell(row=fila, column=col+7, value=a.rut_emisor)
                ws.cell(row=fila, column=col+8, value=a.cod_pais)
                ws.cell(row=fila, column=col+9, value=a.instrumento)
                ws.cell(row=fila, column=col+10, value=a.fecha_vencimiento)
                ws.cell(row=fila, column=col+11, value=a.situacion_instrumento)
                ws.cell(row=fila, column=col+12, value=a.clasificacion_riesgo)
                ws.cell(row=fila, column=col+13, value=a.grupo_empresarial)
                ws.cell(row=fila, column=col+14, value=a.cant_unidades)
                ws.cell(row=fila, column=col+15, value=a.tipo_unidades)
                ws.cell(row=fila, column=col+16, value=a.tir_precio)
                ws.cell(row=fila, column=col+17, value=a.codigo_valolizacion)
                ws.cell(row=fila, column=col+18, value=a.tasa)
                ws.cell(row=fila, column=col+19, value=a.tipo_interes)
                ws.cell(row=fila, column=col+20, value=a.valolizacion_cierre)
                ws.cell(row=fila, column=col+21, value=a.cod_moneda_liq)
                ws.cell(row=fila, column=col+22, value=a.cod_pais_transaccion)
                ws.cell(row=fila, column=col+23, value=a.cap_emisor)
                ws.cell(row=fila, column=col+24, value=a.act_emisor)
                ws.cell(row=fila, column=col+25, value=a.activo_fondo)


                fila += 1
        
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_hoja5.xlsx')

    return HttpResponseRedirect('/perfil/')

def cuotas_aportantes(request):#hoja6
    
    wb = Workbook()
    ws = wb.create_sheet("6-Cuotas y Aportantes", 0) # insert at first position

    ws['B4'] = "Activos Administrados por Administradora y Tipo de Fondo"
    ws['C4'] = "RUT Admin"
    ws['D4'] = "Administradora"
    ws['E4'] = "Rut Fondo"
    ws['F4'] = "Clase"
    ws['G4'] = "Rescatable/no Rescatable"
    ws['H4'] = "Nº Aportantes"
    ws['I4'] = "Nº Cuotas emitidas"
    ws['J4'] = "Nº Cuotas pagadas"
    
    ws.cell(row=3, column=11, value="Nombre")
    ws.cell(row=3, column=11+12, value="Rut")
    ws.cell(row=3, column=11+24, value="persona")
    ws.cell(row=3, column=11+36, value="propiedad")

    for j in range(4):
        i=0
        for i in range(12):
            ws.cell(row=4, column=j*12+i+11, value=str(i+1))


    #lista  = fondo.objects.filter(vigente = True)
    lista = get_lista()

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    col = 2
    fila = 6
    for l in lista:
        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:
            print(l.runsvs)
            
            ws.cell(row=fila, column=col, value=l.nombre)
            ws.cell(row=fila, column=col+1, value=l.admin.rut)
            ws.cell(row=fila, column=col+2, value=l.admin.razon_social)
            ws.cell(row=fila, column=col+3, value=l.runsvs)
            if l.categoria == None:
                categorianombre = '---'
            else:
                categorianombre = l.categoria.nombre
            ws.cell(row=fila, column=col+4, value=categorianombre)
            if l.tipo_inversion == 0:
                tipo_inversion = "No Rescatable"
            if l.tipo_inversion == 1:
                tipo_inversion = "Rescatable"
            ws.cell(row=fila, column=col+5, value=tipo_inversion)

            p = periodo.objects.get(periodo=filter_periodo, fondo=l)
            ws.cell(row=fila, column=col+6, value=p.total_aportantes)

            aux = json.loads(p.datos)
            ws.cell(row=fila, column=col+7, value=aux['CUOTAS EMITIDAS'])
            ws.cell(row=fila, column=col+8, value=aux['CUOTAS PAGADAS'])

            periodos = periodo.objects.filter(periodo=filter_periodo, fondo=l)[:1]
            composiciones = composicion.objects.filter(periodo=periodos).order_by('-porc_propiedad')[:12]
            colaux = col+9
            for comp in composiciones:
                ws.cell(row=fila, column=colaux, value=comp.aportante.nombre)
                ws.cell(row=fila, column=colaux+12, value=comp.aportante.rut)
                ws.cell(row=fila, column=colaux+24, value=comp.aportante.get_persona())
                ws.cell(row=fila, column=colaux+36, value=comp.porc_propiedad)
                colaux += 1
            fila += 1
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_aportantes_'+str(filter_periodo)+'.xlsx')

    return HttpResponseRedirect('/perfil/')

def clasif_riesgo(request):#hoja7
    #descargar_excel()
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    my_array = pyexcel.get_array(file_name=ruta+"riesgos.xlsx")
    del my_array[0]
    for fila in my_array:
        if fila[3] != '':
            try:
                f = fondo_ext.objects.get(runsvs__startswith=str(fila[3]))
                
                try:
                    riesg = json.loads(f.riesgo)
                except json.JSONDecodeError:
                    riesg = {'felier_rate': '', 'fitch_ratings': '', 'comision': '', 'icr': '', 'humphreys': ''}


                riesg['felier_rate'] = fila[10]
                riesg['fitch_ratings'] = fila[12]
                riesg['icr'] = fila[14]
                riesg['humphreys'] = fila[8]
                
                f.riesg = json.dumps(riesg)
                f.save()            
                
                #print(str(fila[3])+":   "+fila[12]+"--"+ fila[8] +"--"+ fila[10] +"--"+ fila[14])
            
            except fondo_ext.DoesNotExist:
                pass

    #ahora a crear el excel

    wb = Workbook()
    ws = wb.create_sheet("7-Clasificaciones Riesgo", 0) # insert at first position

    ws['C3'] = "Clasificación Riesgo"
    ws['D3'] = "RUT Fondo"
    ws['E3'] = "Clase"
    ws['F3'] = "Fitch Chile Clasificadora de Riesgo Ltda."
    ws['G3'] = "Clasificadora de Riesgo Humphreys Ltda."
    ws['H3'] = "Feller Rate Clasificadora de Riesgo Ltda."
    ws['I3'] = "ICR Compañía Clasificadora de Riesgo Ltda."
    


    #lista  = fondo.objects.filter(vigente = True)
    lista = fondo.objects.filter(inicio_operaciones__lte = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    col = 3
    fila = 4
    for l in lista:
        print(l.runsvs)
        
        ws.cell(row=fila, column=col, value=l.nombre)
        ws.cell(row=fila, column=col+1, value=l.runsvs)
        if l.categoria == None:
            categorianombre = '---'
        else:
            categorianombre = l.categoria.indice
        ws.cell(row=fila, column=col+2, value=categorianombre)

        f = fondo_ext.objects.get(runsvs=l.runsvs)
                
        try:
            riesg = json.loads(f.riesgo)
            feller= riesg['felier_rate']
            fitch = riesg['fitch_ratings']
            icr = riesg['icr']
            humphreys = riesg['humphreys']

        except json.JSONDecodeError:
            feller = ''
            fitch=''
            icr =''
            humphreys=''


        ws.cell(row=fila, column=col+3, value=fitch)
        ws.cell(row=fila, column=col+4, value=humphreys)
        ws.cell(row=fila, column=col+5, value=feller)
        ws.cell(row=fila, column=col+6, value=icr)

        fila += 1   

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_hoja7.xlsx')

    return HttpResponseRedirect('/perfil/')

def inv_moneda(request):

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    try:
        wb = load_workbook(ruta+'precatastro_hoja9.xlsx')
    except FileNotFoundError:
        wb = Workbook()
        wb.save(ruta+'precatastro_hoja9.xlsx')
    
    ws = wb.active
    ws.title = "9-Inversion Moneda"
    #ws = wb.create_sheet("6-Inversion Moneda", 0) # insert at first position

    ws['B4'] = "Activos Administrados por Administradora y Tipo de Fondo"
    ws['C4'] = "RUT Admin"
    ws['D4'] = "Administradora"
    ws['E4'] = "Rut Fondo"
    ws['F4'] = "Clase"
    ws['G4'] = "Rescatable/no Rescatable"

    
    distinct = inversion.objects.values('moneda').annotate(name_count=Count('moneda')).filter(name_count__gte=1)#codigo moneda liquidación
    distinct2 = inversion.objects.values('tipo_unidades').annotate(name_count=Count('tipo_unidades')).filter(name_count__gte=1)
    
    #records = inversion.objects.filter(moneda__in=[item['moneda'] for item in distinct])

    lista = get_lista()

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    col = 2

    colaux = col+5
    ws.cell(row=3, column=colaux, value='moneda') 
    for d in distinct:
        #ws.cell(row=4, column=colaux, value=divisa.objects.filter( Q(alias_svs=d['moneda']) | Q(pk=d['moneda']) ).first().nombre)
        ws.cell(row=4, column=colaux, value=d['moneda']) 
        colaux += 1

    ws.cell(row=3, column=colaux, value='tipo unidades') 
    for d2 in distinct2:
        #try:
        #    ws.cell(row=4, column=colaux, value=divisa.objects.filter( Q(alias_svs=d2['tipo_unidades']) | Q(pk=d2['tipo_unidades']) ).first().nombre)
        #except AttributeError:
        #    pass
        ws.cell(row=4, column=colaux, value=d2['tipo_unidades']) 
        
        colaux += 1

    fila = 6
    for l in lista:
        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:
            if ws.cell(row=fila, column=col).value == None:
                print(l.runsvs)
                ws.cell(row=fila, column=col, value=l.nombre)
                ws.cell(row=fila, column=col+1, value=l.admin.rut)
                ws.cell(row=fila, column=col+2, value=l.admin.razon_social)
                ws.cell(row=fila, column=col+3, value=l.runsvs)
                if l.categoria == None:
                    categorianombre = '---'
                else:
                    categorianombre = l.categoria.indice
                ws.cell(row=fila, column=col+4, value=categorianombre)
                if l.tipo_inversion == 0:
                    tipo_inversion = "No Rescatable"
                if l.tipo_inversion == 1:
                    tipo_inversion = "Rescatable"

                colaux = col+5
                for d in distinct:
                    if ws.cell(row=fila, column=colaux).value == None:
                        try:
                            tot = inversion.objects.filter(fondo=l, periodo=filter_periodo, moneda=d['moneda']).aggregate(Sum('porcentaje'))
                            ws.cell(row=fila, column=colaux, value=float(tot['porcentaje__sum']))
                        except TypeError:
                            pass
                    colaux += 1

                for d2 in distinct2:
                    if ws.cell(row=fila, column=colaux).value == None:
                        try:
                            tot = inversion.objects.filter(fondo=l, periodo=filter_periodo, tipo_unidades=d2['tipo_unidades']).aggregate(Sum('porcentaje'))
                            ws.cell(row=fila, column=colaux, value=float(tot['porcentaje__sum']))
                        except TypeError:
                            pass
                    colaux += 1


            wb.save(ruta+'precatastro_inversion_moneda_'+str(filter_periodo)+'.xlsx')
            #Valor Libro de la Cuota
            fila += 1
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_inversion_moneda_'+str(filter_periodo)+'.xlsx')

    return HttpResponseRedirect('/perfil/')

def inv_pais(request):

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    try:
        wb = load_workbook(ruta+'precatastro_inversion_pais_'+str(filter_periodo)+'.xlsx')
    except FileNotFoundError:
        wb = Workbook()
        wb.save(ruta+'precatastro_inversion_pais_'+str(filter_periodo)+'.xlsx')
    
    ws = wb.active
    ws.title = "8-Inversion Pais"
    #ws = wb.create_sheet("6-Inversion Moneda", 0) # insert at first position

    ws['B4'] = "Activos Administrados por Administradora y Tipo de Fondo"
    ws['C4'] = "RUT Admin"
    ws['D4'] = "Administradora"
    ws['E4'] = "Rut Fondo"
    ws['F4'] = "Clase"
    ws['G4'] = "Rescatable/no Rescatable"
    ws['G3'] = "Pais"
    ws['BZ3'] = "Código Pais Transacción"

    
    distinct = inversion.objects.values('pais').annotate(name_count=Count('pais')).filter(name_count__gte=1)
    distinct2 = inversion.objects.values('cod_pais_transaccion').annotate(name_count=Count('cod_pais_transaccion')).filter(name_count__gte=1)
    
    #records = inversion.objects.filter(moneda__in=[item['moneda'] for item in distinct])

    lista = get_lista()
    
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    col = 2

    colaux = col+5
    for d in distinct:
        ws.cell(row=4, column=colaux, value=pais.objects.get(pk=d['pais']).nombre)
        colaux += 1

    for d2 in distinct2:
        ws.cell(row=4, column=colaux, value=pais.objects.get(pk=d2['cod_pais_transaccion']).nombre)
        colaux += 1

    fila = 6
    for l in lista:
        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:
            if ws.cell(row=fila, column=col).value == None:
                print(l.runsvs)
                ws.cell(row=fila, column=col, value=l.nombre)
                ws.cell(row=fila, column=col+1, value=l.admin.rut)
                ws.cell(row=fila, column=col+2, value=l.admin.razon_social)
                ws.cell(row=fila, column=col+3, value=l.runsvs)
                if l.categoria == None:
                    categorianombre = '---'
                else:
                    categorianombre = l.categoria.indice
                ws.cell(row=fila, column=col+4, value=categorianombre)
                if l.tipo_inversion == 0:
                    tipo_inversion = "No Rescatable"
                if l.tipo_inversion == 1:
                    tipo_inversion = "Rescatable"

                colaux = col+5
                for d in distinct:
                    if ws.cell(row=fila, column=colaux).value == None:
                        try:
                            tot = inversion.objects.filter(fondo=l, periodo=filter_periodo, pais=d['pais']).aggregate(Sum('porcentaje'))
                            ws.cell(row=fila, column=colaux, value=float(tot['porcentaje__sum']))
                        except TypeError:
                            pass
                    colaux += 1

                for d2 in distinct2:
                    if ws.cell(row=fila, column=colaux).value == None:
                        try:
                            tot = inversion.objects.filter(fondo=l, periodo=filter_periodo, cod_pais_transaccion=d2['cod_pais_transaccion']).aggregate(Sum('porcentaje'))
                            ws.cell(row=fila, column=colaux, value=float(tot['porcentaje__sum']))
                        except TypeError:
                            pass
                    colaux += 1


            wb.save(ruta+'precatastro_inversion_pais_'+str(filter_periodo)+'.xlsx')
            #Valor Libro de la Cuota
            fila += 1
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_inversion_pais_'+str(filter_periodo)+'.xlsx')

    return HttpResponseRedirect('/perfil/')

def get_lista_activos(request):
    lista = get_lista()
    fecha_top = fecha_tope( get_filter_periodo() )
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    
    try:
        wb = load_workbook(ruta+'precatastro_hoja10.xlsx')
    except FileNotFoundError:
        wb = Workbook()
        wb.save(ruta+'precatastro_hoja10.xlsx')    

    ws = wb.active
    ws.title = "10.- Lista de Fondos activos"

    ws['B1'] = "Nombre"
    ws['C1'] = "Run CMF"
    ws['D1'] = "Inicio operaciones"
    ws['E1'] = "Termino operaciones"

    lista = fondo.objects.filter(Q(termino_operaciones__gte= fecha_top) | Q( termino_operaciones__isnull=True))
    lista = lista.filter(inicio_operaciones__lt = fecha_top, inicio_operaciones__isnull=False).order_by('runsvs')
    #lista de fondos que la fecha de termino sea mayor o igual a fecha de tope o sea nula.
    #y fecha de inicio operaciones sea menor a la fecha de tope y no sea nula

    col = 2
    fila = 3
    for l in lista:
        print(l.runsvs)
        
        ws.cell(row=fila, column=col, value=l.nombre)
        ws.cell(row=fila, column=col+1, value=l.runsvs)
        ws.cell(row=fila, column=col+2, value=l.inicio_operaciones)
        ws.cell(row=fila, column=col+3, value=l.termino_operaciones)
        
        fila += 1   

    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'precatastro_hoja10.xlsx')

    return HttpResponseRedirect('/perfil/')

