from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from acafi.models import pais

#lista todos los paises
def listar_paises(request):
    lista = pais.objects.all()

    url = ''
    filter_codigo = request.GET.get('filter_codigo')
    filter_nombre = request.GET.get('filter_nombre')
    

    if filter_codigo != None and filter_codigo != '' and filter_codigo != 'None':
        lista = lista.filter(codigo__icontains=filter_codigo)
        url += 'filter_codigo=' + filter_codigo + '&'

    if filter_nombre != None and filter_nombre != '' and filter_nombre != 'None':
        lista = lista.filter(nombre__icontains=filter_nombre)
        url += 'filter_nombre=' + filter_nombre + '&'

    
    page = request.GET.get('page')
    if page is None:
        page = 1

    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_codigo': filter_codigo, 'filter_nombre': filter_nombre}
    context = {"lista": lista, "count": count, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'pais/lista.html', merged_dict)

