from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.contrib.auth.models import User
from acafi.forms import Corfo_form, Bug_form, Inversiones_corfo_form, Formimportarlista
from acafi.models import fondo_corfo, perfil, administrador, divisa, categoria
from acafi.view.usuario import enviar_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.safestring import mark_safe
from django.contrib.auth import authenticate, login
from django.template import loader
from django.db.models import Avg, Sum
import json, csv, pyexcel
from django.core.exceptions import ValidationError

#convierte un string en flotante, si no es numero retorna 0
def conv_float(s):
    if s == None or s =='':
        return 0
    else:
        return float(s)

#edita fondos_corfo
def index_fondo_corfo(request, runsvs=None):
    user = request.user
    flag = 0
    try:
        acceso = perfil.objects.get(pk=user).nivel_acceso
        administradora = perfil.objects.get(pk=user).administrador
        if fondo_corfo.objects.get(pk=runsvs).admin == administradora:
            flag = 1
    except perfil.DoesNotExist:
        acceso = 0
    if user.is_staff or user.is_superuser or acceso >= 75:
        flag = 2
    #if fondo_corfo.objects.get(pk=runsvs).user == user or fondo_corfo.objects.get(pk=runsvs).user == user:
    #    flag = 1

        # perfil = profile.objects.get(user_id = request.user.id)
    if flag >= 1:
        if request.method == "POST":
            form = Corfo_form(request.POST)
            form3 = Inversiones_corfo_form(request.POST)

            rut = request.POST.get('rut')
            fc = fondo_corfo(rut=rut)

            fc.periodo = request.POST.get('periodo')
            fc.nombre = request.POST.get('nombre')
            fc.admin = administrador.objects.get(pk=request.POST.get('admin'))
            fc.codigo_bolsa = request.POST.get('codigo_bolsa')
            fc.bloomberg = request.POST.get('bloomberg')
            fc.isin = request.POST.get('isin')
            fc.moneda = divisa.objects.get(pk=request.POST.get('moneda'))
            fc.tipo_fondo = categoria.objects.get(pk=request.POST.get('tipo_fondo'))
            
            fc.tipo_linea = request.POST.get('tipo_linea')
            fc.area_interes = request.POST.get('area_interes')
            fc.patrimonio = conv_float(request.POST.get('patrimonio'))
            fc.portfolio_manager = request.POST.get('portfolio_manager')

            fc.estrategia = request.POST.get('estrategia')
            fc.comision = request.POST.get('comision')
            fc.comentario = request.POST.get('comentario')
            fc.save()


            cifra_monto_us = conv_float(request.POST.get('cifra_monto_us'))
            cifra_monto_uf = conv_float(request.POST.get('cifra_monto_uf'))

            cifra_lca_us = conv_float(request.POST.get('cifra_lca_us'))
            cifra_lca_uf = conv_float(request.POST.get('cifra_lca_uf'))

            cifra_dc_us = conv_float(request.POST.get('cifra_dc_us'))
            cifra_dc_uf = conv_float(request.POST.get('cifra_dc_uf'))

            cifra_dp_us = conv_float(request.POST.get('cifra_dp_us'))
            cifra_dp_uf = conv_float(request.POST.get('cifra_dp_uf'))

            cifra_ne = conv_float(request.POST.get('cifra_ne'))
            cifra_estado = request.POST.get('cifra_estado')
            
            cifra = {}

            cifra['cifra_monto_us'] = cifra_monto_us
            cifra['cifra_monto_uf'] = cifra_monto_uf
            cifra['cifra_lca_us'] = cifra_lca_us
            cifra['cifra_lca_uf'] = cifra_lca_uf
            cifra['cifra_dc_us'] = cifra_dc_us
            cifra['cifra_dc_uf'] = cifra_dc_uf
            cifra['cifra_dp_us'] = cifra_dp_us
            cifra['cifra_dp_uf'] = cifra_dp_uf
            cifra['cifra_ne'] = cifra_ne
            cifra['cifra_estado'] = cifra_estado
            fc.cifras = json.dumps(cifra)
            fc.save()



            inversiones = {}
            inversion_empresa_1 = request.POST.get('inversion_empresa_1')
            inversion_monto_1 = conv_float(request.POST.get('inversion_monto_1'))
            inversion_empresa_2 = request.POST.get('inversion_empresa_2')
            inversion_monto_2 = conv_float(request.POST.get('inversion_monto_2'))
            inversion_empresa_3 = request.POST.get('inversion_empresa_3')
            inversion_monto_3 = conv_float(request.POST.get('inversion_monto_3'))
            inversion_empresa_4 = request.POST.get('inversion_empresa_4')
            inversion_monto_4 = conv_float(request.POST.get('inversion_monto_4'))
            inversion_empresa_5 = request.POST.get('inversion_empresa_5')
            inversion_monto_5 = conv_float(request.POST.get('inversion_monto_5'))
            inversion_empresa_6 = request.POST.get('inversion_empresa_6')
            inversion_monto_6 = conv_float(request.POST.get('inversion_monto_6'))
            inversion_empresa_7 = request.POST.get('inversion_empresa_7')
            inversion_monto_7 = conv_float(request.POST.get('inversion_monto_7'))
            inversion_empresa_8 = request.POST.get('inversion_empresa_8')
            inversion_monto_8 = conv_float(request.POST.get('inversion_monto_8'))
            inversion_empresa_9 = request.POST.get('inversion_empresa_9')
            inversion_monto_9 = conv_float(request.POST.get('inversion_monto_9'))
            inversion_empresa_10 = request.POST.get('inversion_empresa_10')
            inversion_monto_10 = conv_float(request.POST.get('inversion_monto_10'))
            inversion_empresa_11 = request.POST.get('inversion_empresa_11')
            inversion_monto_11 = conv_float(request.POST.get('inversion_monto_11'))
            inversion_empresa_12 = request.POST.get('inversion_empresa_12')
            inversion_monto_12 = conv_float(request.POST.get('inversion_monto_12'))
    
            inversiones['inversion_empresa_1'] = inversion_empresa_1
            inversiones['inversion_monto_1'] = inversion_monto_1
            inversiones['inversion_empresa_2'] = inversion_empresa_2
            inversiones['inversion_monto_2'] = inversion_monto_2
            inversiones['inversion_empresa_3'] = inversion_empresa_3
            inversiones['inversion_monto_3'] = inversion_monto_3
            inversiones['inversion_empresa_4'] = inversion_empresa_4
            inversiones['inversion_monto_4'] = inversion_monto_4
            inversiones['inversion_empresa_5'] = inversion_empresa_5
            inversiones['inversion_monto_5'] = inversion_monto_5
            inversiones['inversion_empresa_6'] = inversion_empresa_6
            inversiones['inversion_monto_6'] = inversion_monto_6
            inversiones['inversion_empresa_7'] = inversion_empresa_7
            inversiones['inversion_monto_7'] = inversion_monto_7
            inversiones['inversion_empresa_8'] = inversion_empresa_8
            inversiones['inversion_monto_8'] = inversion_monto_8
            inversiones['inversion_empresa_9'] = inversion_empresa_9
            inversiones['inversion_monto_9'] = inversion_monto_9
            inversiones['inversion_empresa_10'] = inversion_empresa_10
            inversiones['inversion_monto_10'] = inversion_monto_10
            inversiones['inversion_empresa_11'] = inversion_empresa_11
            inversiones['inversion_monto_11'] = inversion_monto_11
            inversiones['inversion_empresa_12'] = inversion_empresa_12
            inversiones['inversion_monto_12'] = inversion_monto_12

            fc.inversiones = json.dumps(inversiones)
            fc.save()


            try:
                fc.apertura_linea = request.POST.get('apertura_linea')
                fc.save()
            except ValidationError:
                pass

            try:
                fc.fecha_vencimiento = request.POST.get('fecha_vencimiento')
                fc.save()
            except ValidationError:
                pass

            response = 2
            return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))
    
        else:
            if runsvs:
                #'admin': fc.admin, , 'moneda': fc.moneda, tipo_fondo
                try:
                    fc = fondo_corfo.objects.get(rut=runsvs)



                    cifra = json.loads(fc.cifras)
                    if 'cifra_monto_us' not  in cifra:
                        cifra['cifra_monto_us'] = 0
                    if 'cifra_monto_uf' not  in cifra:
                        cifra['cifra_monto_uf'] = 0
                    if 'cifra_lca_us' not  in cifra:
                        cifra['cifra_lca_us'] = 0
                    if 'cifra_lca_uf' not  in cifra:
                        cifra['cifra_lca_uf'] = 0

                    if 'cifra_dc_us' not  in cifra:
                        cifra['cifra_dc_us'] = 0
                    if 'cifra_dc_uf' not  in cifra:
                        cifra['cifra_dc_uf'] = 0

                    if 'cifra_dp_us' not  in cifra:
                        cifra['cifra_dp_us'] = 0
                    if 'cifra_dp_uf' not  in cifra:
                        cifra['cifra_dp_uf'] = 0
                    if 'cifra_estado' not  in cifra:
                        cifra['cifra_estado'] = 0

                    admin = fc.admin
                    if admin == None:
                        arut = None
                    else:
                        arut = admin.rut

                    moneda = fc.moneda
                    form = Corfo_form(initial={'rut': fc.rut, 'nombre' : fc.nombre, 'codigo_bolsa' : fc.codigo_bolsa, 'bloomberg' : fc.bloomberg, 'isin': fc.isin, 'apertura_linea' : fc.apertura_linea, 'fecha_vencimiento' : fc.fecha_vencimiento, 'tipo_linea' : fc.tipo_linea, 'area_interes' : fc.area_interes, 'portfolio_manager' : fc.portfolio_manager, 'patrimonio' : fc.patrimonio, 'comision' : fc.comision, 'comentario' : fc.comentario, 'estrategia' : fc.estrategia,\
                     'admin': arut, 'moneda' : moneda.divisa_id, 'tipo_fondo' : fc.tipo_fondo.indice, 'cifra_estado' : cifra['cifra_estado'], \
                     'cifra_monto_us' : cifra['cifra_monto_us'], 'cifra_monto_uf' : cifra['cifra_monto_uf'], 'cifra_lca_us' : cifra['cifra_lca_us'], 'cifra_lca_uf' : cifra['cifra_lca_uf'], 'cifra_dc_us' : cifra['cifra_dc_us'], 'cifra_dc_uf' : cifra['cifra_dc_uf'], 'cifra_dp_us' : cifra['cifra_dp_us'], 'cifra_dp_uf' : cifra['cifra_dp_uf']\
                       })
                    try:
                        inversion = json.loads(fc.inversiones)
                    except TypeError:
                        inversion = json.loads('{}')
                    if 'inversion_empresa_1' not in inversion:
                        inversion['inversion_empresa_1'] = ''
                    if 'inversion_empresa_2' not in inversion:
                        inversion['inversion_empresa_2'] = ''
                    if 'inversion_empresa_3' not in inversion:
                        inversion['inversion_empresa_3'] = ''
                    if 'inversion_empresa_4' not in inversion:
                        inversion['inversion_empresa_4'] = ''
                    if 'inversion_empresa_5' not in inversion:
                        inversion['inversion_empresa_5'] = ''
                    if 'inversion_empresa_6' not in inversion:
                        inversion['inversion_empresa_6'] = ''
                    if 'inversion_empresa_7' not in inversion:
                        inversion['inversion_empresa_7'] = ''
                    if 'inversion_empresa_8' not in inversion:
                        inversion['inversion_empresa_8'] = ''
                    if 'inversion_empresa_9' not in inversion:
                        inversion['inversion_empresa_9'] = ''
                    if 'inversion_empresa_10' not in inversion:
                        inversion['inversion_empresa_10'] = ''
                    if 'inversion_empresa_11' not in inversion:
                        inversion['inversion_empresa_11'] = ''
                    if 'inversion_empresa_12' not in inversion:
                        inversion['inversion_empresa_12'] = ''

                    if 'inversion_monto_1' not in inversion:
                        inversion['inversion_monto_1'] = 0
                    if 'inversion_monto_2' not in inversion:
                        inversion['inversion_monto_2'] = 0
                    if 'inversion_monto_3' not in inversion:
                        inversion['inversion_monto_3'] = 0
                    if 'inversion_monto_4' not in inversion:
                        inversion['inversion_monto_4'] = 0
                    if 'inversion_monto_5' not in inversion:
                        inversion['inversion_monto_5'] = 0
                    if 'inversion_monto_6' not in inversion:
                        inversion['inversion_monto_6'] = 0
                    if 'inversion_monto_7' not in inversion:
                        inversion['inversion_monto_7'] = 0
                    if 'inversion_monto_8' not in inversion:
                        inversion['inversion_monto_8'] = 0
                    if 'inversion_monto_9' not in inversion:
                        inversion['inversion_monto_9'] = 0
                    if 'inversion_monto_10' not in inversion:
                        inversion['inversion_monto_10'] = 0
                    if 'inversion_monto_11' not in inversion:
                        inversion['inversion_monto_11'] = 0
                    if 'inversion_monto_12' not in inversion:
                        inversion['inversion_monto_12'] = 0

                    form3 = Inversiones_corfo_form(initial={'inversion_empresa_1' : inversion['inversion_empresa_1'], 'inversion_monto_1' : inversion['inversion_monto_1'], \
                        'inversion_empresa_2' : inversion['inversion_empresa_2'], 'inversion_monto_2' : inversion['inversion_monto_2'], \
                        'inversion_empresa_3' : inversion['inversion_empresa_3'], 'inversion_monto_3' : inversion['inversion_monto_3'], \
                        'inversion_empresa_4' : inversion['inversion_empresa_4'], 'inversion_monto_4' : inversion['inversion_monto_4'], \
                        'inversion_empresa_5' : inversion['inversion_empresa_5'], 'inversion_monto_5' : inversion['inversion_monto_5'], \
                        'inversion_empresa_6' : inversion['inversion_empresa_6'], 'inversion_monto_6' : inversion['inversion_monto_6'], \
                        'inversion_empresa_7' : inversion['inversion_empresa_7'], 'inversion_monto_7' : inversion['inversion_monto_7'], \
                        'inversion_empresa_8' : inversion['inversion_empresa_8'], 'inversion_monto_8' : inversion['inversion_monto_8'], \
                        'inversion_empresa_9' : inversion['inversion_empresa_9'], 'inversion_monto_9' : inversion['inversion_monto_9'], \
                        'inversion_empresa_10' : inversion['inversion_empresa_10'], 'inversion_monto_10' : inversion['inversion_monto_10'], \
                        'inversion_empresa_11' : inversion['inversion_empresa_11'], 'inversion_monto_11' : inversion['inversion_monto_11'], \
                        'inversion_empresa_12' : inversion['inversion_empresa_12'], 'inversion_monto_12' : inversion['inversion_monto_12']} )

                    form2 = Bug_form(initial={'fondo': runsvs})
                except fondo_corfo.DoesNotExist:
                    response = 3
                    return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))                    
            else:
                form = Corfo_form()
                form2 = Bug_form()
                form3 = Inversiones_corfo_form()
        
            return render(request, 'fondo_corfo/index.html',{'form': form, 'form2': form2, 'form3' : form3, 'runsvs': runsvs,'range': range(10)})

    else:
        response = 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

#lista de fondos_corfo
def listar_fondos_corfo(request):
    user = request.user
    if user == None:
        response = 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

    lista = fondo_corfo.objects.all()
    if request.user.is_staff == 0 and request.user.is_superuser == 0 and perfil.objects.get(pk=request.user).nivel_acceso < 75:
        administradora = perfil.objects.get(pk=user).administrador
        lista = lista.filter(admin=administradora)
     
    url = ''
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    filter_vigencia = request.GET.get('filter_vigencia')
    filter_categoria = request.GET.get('filter_categoria')

    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        lista = lista.filter(rut__icontains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(nombre__icontains=filter_name)
        url += 'filter_name=' + filter_name + '&'

    if filter_vigencia != None and filter_vigencia != '' and filter_vigencia != 'None':
        lista = lista.filter(cifras__contains=filter_vigencia)
        url += 'filter_vigencia=' + filter_vigencia + '&'

    if filter_categoria != None and filter_categoria != '' and filter_categoria != 'None':
        lista = lista.filter(categoria__indice__icontains=filter_categoria)
        url += 'filter_categoria=' + filter_categoria + '&'

    page = request.GET.get('page')
    if page is None:
        page = 1

    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]
    categorias = categoria.objects.filter(seleccionable=1)

    '''for l in lista:
        if l.vigente == True:
            l.vigente = 'V'
        else:
            l.vigente = 'NV'
    '''

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name,
                   'filter_vigencia': filter_vigencia, 'filter_categoria': filter_categoria}
    context = {"lista_fondos": lista, "count": count, "lista": lista, 'lista_categorias': categorias,
                   'url': url, 'user': request.user}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'fondo_corfo/lista_fondos.html', merged_dict)

#despliega fatsheet_corfo
def factsheet_corfo(request, id, anio=None, paginador=None):
    f = fondo_corfo.objects.get(pk=id)
    if anio == None:
        anio = datetime.datetime.year

    if f.patrimonio != None:
        pt = int(f.patrimonio)/1000000
    else:
        pt = 0

    cifras = json.loads(f.cifras)
    tot = int(cifras['cifra_monto_uf'])
    if tot == 0:
        tot = 100
    if int(cifras['cifra_dc_uf']) == 0 or (tot - int(cifras['cifra_dc_uf'])) < 0:
        suma = 0
    else:
        suma = tot - int(cifras['cifra_dc_uf'])
    tabla_inversiones = []
    if f.inversiones != None:
        inversiones = json.loads(f.inversiones)
        #inversiones =  str(total)+" / "+str(inversiones2['inversion_empresa_1'])
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_1'], int(inversiones['inversion_monto_1']), float(inversiones['inversion_monto_1']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_2'], int(inversiones['inversion_monto_2']), float(inversiones['inversion_monto_2']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_3'], int(inversiones['inversion_monto_3']), float(inversiones['inversion_monto_3']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_4'], int(inversiones['inversion_monto_4']), float(inversiones['inversion_monto_4']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_5'], int(inversiones['inversion_monto_5']), float(inversiones['inversion_monto_5']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_6'], int(inversiones['inversion_monto_6']), float(inversiones['inversion_monto_6']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_7'], int(inversiones['inversion_monto_7']), float(inversiones['inversion_monto_7']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_8'], int(inversiones['inversion_monto_8']), float(inversiones['inversion_monto_8']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_9'], int(inversiones['inversion_monto_9']), float(inversiones['inversion_monto_9']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_10'], int(inversiones['inversion_monto_10']), float(inversiones['inversion_monto_10']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_11'], int(inversiones['inversion_monto_11']), float(inversiones['inversion_monto_11']) *100/tot ])
        except KeyError:
            pass
        try:
            tabla_inversiones.append([ inversiones['inversion_empresa_12'], int(inversiones['inversion_monto_12']), float(inversiones['inversion_monto_12']) *100/tot ])
        except KeyError:
            pass
        #tabla_inversiones = zip(*tabla_inversiones)
        tabla_inversiones.sort(key=lambda x: x[1], reverse=True)
    else:
        inversiones = "{}"

   

    return render(request, "factsheets/hojacorfo.html", {"id" : id, "fondo" : f, "yyyy" : anio, "pt" :pt, "cifras" : cifras, "tabla_inversiones" : tabla_inversiones, "inversiones" : inversiones,  "range" : range(12), "paginador" : paginador, "sum" : suma})

def import_corfo(request):
    if request.method == "POST":
        form = Formimportarlista(request.POST, request.FILES)
        user = request.user
        filename = request.FILES['archivo'].name
        extension = filename.split(".")[-1]
        content = request.FILES['archivo'].read()
        
        sheet = pyexcel.get_array(file_type=extension, file_content=content)
        #sheet = sheet.get_array()
        
        #i = len(sheet[0])
        th = sheet[0]
        del sheet[0]
            
        
        for fila in range(len(sheet)):
            for col in range(len(sheet[0])):
        #        if type(sheet[fila][col]) != types.UnicodeType:
                sheet[fila][col] = str(sheet[fila][col])
        
        respuesta=''
        for s in sheet:
            if s[0] != '' and s[0] != None: #corfo
                i =1
                try:
                    co = fondo_corfo.objects.get(pk= s[0])

                    try:
                        adm = administrador.objects.get(razon_social= str(s[3]))
                        co.admin = adm
                    except administrador.DoesNotExist:
                        pass
                    if s[4] != '':
                        co.estrategia = str(s[4])
                    if s[5] != '':
                        co.comision = str(s[5])
                    if s[6] != '':
                        co.area_interes = str(s[6])

                    inversion = {}
                    if s[7] != '' and s[8] != '':
                        inversion['inversion_empresa_1'] = s[7]
                        inversion['inversion_monto_1'] = s[8]

                    if s[9] != '' and s[10] != '':
                        inversion['inversion_empresa_2'] = s[9]
                        inversion['inversion_monto_2'] = s[10]
                    if s[11] != '' and s[12] != '':
                        inversion['inversion_empresa_3'] = s[11]
                        inversion['inversion_monto_3'] = s[12]
                    if s[13] != '' and s[14] != '':
                        inversion['inversion_empresa_4'] = s[13]
                        inversion['inversion_monto_4'] = s[14]
                    if s[15] != '' and s[16] != '':
                        inversion['inversion_empresa_5'] = s[15]
                        inversion['inversion_monto_5'] = s[16]
                    if s[17] != '' and s[18] != '':
                        inversion['inversion_empresa_6'] = s[17]
                        inversion['inversion_monto_6'] = s[18]
                    if s[19] != '' and s[20] != '':
                        inversion['inversion_empresa_7'] = s[19]
                        inversion['inversion_monto_7'] = s[20]
                    if s[21] != '' and s[22] != '':
                        inversion['inversion_empresa_8'] = s[21]
                        inversion['inversion_monto_8'] = s[22]
                    if s[23] != '' and s[24] != '':
                        inversion['inversion_empresa_9'] = s[23]
                        inversion['inversion_monto_9'] = s[24]
                    if s[25] != '' and s[26] != '':
                        inversion['inversion_empresa_10'] = s[25]
                        inversion['inversion_monto_10'] = s[26]

                    if s[27] != '' and s[28] != '':
                        inversion['inversion_empresa_11'] = s[27]
                        inversion['inversion_monto_11'] = s[28]
                    if s[29] != '' and s[30] != '':
                        inversion['inversion_empresa_12'] = s[29]
                        inversion['inversion_monto_12'] = s[30]

                    co.inversiones = json.dumps(inversion)

                    co.save()

                    respuesta += "El fondo "+str(s[0])+" se ha insertado "+str('<hr>')

                    i += 1
                except IndexError:
                    pass

           #response = 1
        return render(request, 'fondo_corfo/importar.html', {'form': form, 'usuario': request.user, "sheet" : sheet, "i":i, "respuesta" : respuesta})

    else:
        form = Formimportarlista()
        return render(request, 'fondo_corfo/importar.html', {'form': form, 'usuario': request.user})
