import json, datetime
from acafi.models import movimiento, fondo, hoja_catastro, categoria, administrador, resultado
from acafi.deploy import escribir_log, forfecha, valor_divisa
from acafi.view.catastros.generar.lista_fondos import lista_vigentes, lista_primaria


etiquetas_svs_activos =[\
    'AC-Efectivo y efectivo equivalente',\
    'AC-Activos financieros a valor razonable con efecto en resultados',\
    'AC-Activos financieros a valor razonable con efecto en otros resultados integrales',\
    'AC-Activos financieros a valor razonable con efecto en resultados entregados en garantía',\
    'AC-Activos financieros a costo amortizado',\
    'AC-Cuentas y documentos por cobrar por operaciones',\
    'AC-Otros documentos y cuentas por cobrar',\
    'AC-Otros activos',\
    'Total Activo Corriente',\
    '', 'Activo no Corriente',\
    'ANC-Activos financieros a valor razonable con efecto en resultados',\
    'ANC-Activos financieros a valor razonable con efecto en otros resultados integrales',\
    'ANC-Activos financieros a costo amortizado',\
    'ANC-Cuentas y documentos por cobrar por operaciones',\
    'ANC-Otros documentos y cuentas por cobrar',\
    'ANC-Inversiones valorizadas por el método de la participación',\
    'ANC-Propiedades de Inversión',\
    'ANC-Otros activos',\
    'Total Activo No Corriente',\
    '',\
    'Total Activo',\
    '',\
    'PASIVO', 'Pasivo Corriente',\
    'PC-Pasivos financieros a valor razonable con efecto en resultados',\
    'PC-Préstamos',\
    'PC-Otros Pasivos Financieros',\
    'PC-Cuentas y documentos por pagar por operaciones',\
    'PC-Remuneraciones sociedad administradora',\
    'PC-Otros documentos y cuentas por pagar',\
    'PC-Ingresos anticipados',\
    'PC-Otros pasivos',\
    'Total Pasivo Corriente',\
    '', 'Pasivo No Corriente',\
    'PNC-Préstamos',\
    'PNC-Otros Pasivos Financieros',\
    'PNC-Cuentas y documentos por pagar por operaciones',\
    'PNC-Otros documentos y cuentas por pagar',\
    'PNC-Ingresos anticipados',\
    'PNC-Otros pasivos',\
    'Total Pasivo No Corriente',\
    '', 'PATRIMONIO NETO',\
    'PN-Aportes', \
    'PN-Otras Reservas', \
    'PN-Resultados Acumulados  o', \
    'PN-Resultado del ejercicio  o', \
    'PN-Dividendos provisorios', \
    'Total Patrimonio Neto  o', \
    '',\
    'Total Pasivo'\
    ]
etiquetas_svs =[\
    'IPDLO-Intereses y reajustes',\
    'IPDLO-Ingresos por dividendos',\
    'IPDLO-Diferencias de cambio netas sobre activos financieros a costo amortizado  o',\
    'IPDLO-Diferencias de cambio netas sobre efectivo y efectivo equivalente  o',\
    'IPDLO-Cambios netos en valor razonable de activos financieros y pasivos financieros a valor razonable con efecto en resultados  o',\
    'IPDLO-Resultado en venta de instrumentos financieros  o',\
    'IPDLO-Resultados por venta de inmuebles',\
    'IPDLO-Ingreso por arriendo de bienes raíces',\
    'IPDLO-Variaciones en valor razonable de propiedades de inversión  o',\
    'IPDLO-Resultado en inversiones valorizadas por el método de la participación  o',\
    'IPDLO-Otros  o',\
    'Total ingresospérdidas netos de la operación  o',\
    '','GASTOS',\
    'G-Depreciaciones',\
    'G-Remuneración del Comité de Vigilancia',\
    'G-Comisión de administración',\
    'G-Honorarios por custodia y admistración',\
    'G-Costos de transacción',\
    'G-Otros gastos de operación',\
    'Total gastos de operación',\
    '',\
    'Utilidadpérdida de la operación  o',\
    '',\
    'G-Costos financieros',\
    '',\
    'Utilidadpérdida antes de impuesto  o',\
    '',\
    'G-Impuesto a las ganancias por inversiones en el exterior',\
    '',\
    'Resultado del ejercicio  o',\
    '','',\
    'ORI-Cobertura de Flujo de Caja',\
    'ORI-Ajustes por Conversión  o',\
    'ORI-Ajustes provenientes de inversiones valorizadas por el método de la participación  o',\
    'ORI-Otros Ajustes al Patrimonio Neto  o',\
    'Total de otros resultados integrales  o',\
    '',\
    'Total Resultado Integral  o',
    ]
def filtrado_movs(movimientos):

    res = []

    for m in etiquetas_svs_activos:
        
        if m == '':
            res.append('')
        else:
            try:
                mov = movimientos.get(etiqueta_svs__iendswith =m)
                res.append(mov.monto_real('pesos'))
            except movimiento.DoesNotExist:
                res.append('')
    return res

def filtrado_resultados_integrales(resultados):
    
    res = []
    for m in etiquetas_svs:
        if m == '':
            res.append('')
        else:
            try:
                mov = resultados.get(etiqueta_svs__iendswith =m)
                res.append(mov.monto_real('pesos'))
            except resultado.DoesNotExist:
                res.append('')
    return res

def tiene_eeff(l, period):
    if movimiento.objects.filter(fondo = l, periodo = period).exists():
        return True
    else:
        return False


 


def eeff(period):
    print(period)
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    aux = period.split('-')   
    lis = lista_vigentes(lista_primaria(period), period)
    cont = lis.count()
    try:
        contador = len(eval('['+hc.eeff+']')[0])
        res = eval(hc.eeff)
    except (IndexError, TypeError):
        contador = 0

    i = 0
    j = 0
    if contador == 0:
        aux = ['DATOS FONDO','RUN Fondo =>','Nombre Fondo','Administradora','R.U.T. Administradora','Clase Fondo','','ESTADO DE SITUACIÓN FINANCIERA al '+str(period)+' (Millones de pesos)','RUN Fondo =>','ACTIVO','Activo Corriente']
        aux2 = ['','','ESTADO DE RESULTADOS INTEGRALES (Miles de pesos)','RUN Fondo','INGRESOS/ PERDIDAS DE LA OPERACION']
        res.append(aux+etiquetas_svs_activos+aux2+etiquetas_svs)
        for l in lis:

            if l.categoria == '' or l.categoria== None:
                lcategoriaindice = ''
            else:
                lcategoriaindice = l.categoria.indice
            aux = ['', l.runsvs.split('-')[0], l.nombre, l.admin.razon_social, l.admin.rut , lcategoriaindice ,'','', l.runsvs.split('-')[0],'','']
        
            if i != contador:
                i += 1
                pass
            else:
                movs = movimiento.objects.filter(periodo=period, fondo=l)
                aux +=  ( map(lambda x: x/1000 if type(x) is not str  else x, filtrado_movs(movs) ))
                aux += (['','','','',''])
                movs = resultado.objects.filter(periodo=period, fondo=l)
                aux +=  ( map(lambda x: x/1000 if x != '' else '', filtrado_resultados_integrales(movs) ))
                    
            res.append(aux)
                
            hc.eeff = json.dumps(res)
            hc.save()

        aux = []
        aux2 = []
        aux3 = []

        for j in range(0, cont+14):
            if j < 13:
                stri = ''
                stri2 = ''
                stri3 = ''
            else:
                stri = '=SUM(B'+str(j+1)+':'+str(get_column_letter(lis.count()+1))+str(j+1)+')'
                stri2 = '='+str(get_column_letter(lis.count()+2))+str(j+1)+'/'+get_column_letter(lis.count()+3)+'11'
                stri3 = '='+str(get_column_letter(lis.count()+2))+str(j+1)+'/'+get_column_letter(lis.count()+4)+'11'
                
            aux.append( stri )
            aux2.append( stri2 )
            aux3.append( stri3 )

        res.append(aux)
        res.append(aux2)
        res.append(aux3)

        hc.eeff = json.dumps(res)
        hc.save()

