import json, datetime
from acafi.models import movimiento, fondo, hoja_catastro
from acafi.deploy import escribir_log
from django.db.models import Q




def lista_vigentes(lista, period):
    aux = period.split('-')

    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))

    for l in lista:
        if not movimiento.objects.filter(fondo = l, periodo = period).exists():
            pass
            
    return lista
def lista_primaria(period):
    aux = period.split('-')
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista_total = lista.exclude(categoria =0).count()

    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]


    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista_fondos_nuevos = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
    lista_fondos_nuevos = lista_fondos_nuevos.filter(inicio_operaciones__month__gte=int(aux[0])-2)
    return lista


def lista_fondos_vigentes(period):
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    lista = lista_primaria(period)
    lista = lista_vigentes(lista, period)
    res.append(['Rut administradora', 'Nombre administradora', 'Nombre Fondo', 'Rut Fondo',  'Rescat/No Rescat', 'Clasificación Acafi', 'Nemotécnicos', 'Inicio operaciones', 'Término Operaciones', 'Link Sitio CMF', 'EEFF', 'TOTAL ACTIVO', 'TOTAL ACTIVO CORRIENTE', 'TOTAL ACTIVO NO CORRIENTE',  'TOTAL PASIVO', 'TOTAL PASIVO CORRIENTE', 'TOTAL PASIVO NO CORRIENTE', 'TOTAL PATRIMONIO NETO'])
    for l in lista:
        print(l.runsvs)
        if l.categoria == None:
            categ = 0
        else:
            categ = l.categoria.indice
        l.inicio_operaciones = l.inicio_operaciones.strftime('%d-%m-%Y')
        if l.termino_operaciones != None:
            l.termino_operaciones = l.termino_operaciones.strftime('%d-%m-%Y')
        else:
            l.termino_operaciones = ''

        if l.tipo_inversion == 1:
            l.tipo_inversion = 'R'
        else:
            l.tipo_inversion = 'NR'

        if l.nemotecnicos() == None:
            lnemotecnicos = ''
        else:
            lnemotecnicos = l.nemotecnicos()

        aux = []
        movs = movimiento.objects.filter(fondo = l, periodo = period, etiqueta_svs__startswith='TOTAL').order_by('etiqueta_svs')
        if len(movs) > 0:
            eeff = 'Si'
        else:
            eeff = 'No'
        for m in movs:
            aux.append(m.monto_real('pesos'))
        res.append([ l.admin.rut, l.admin.razon_social, l.nombre, l.runsvs, l.tipo_inversion, categ,  lnemotecnicos, l.inicio_operaciones, l.termino_operaciones, l.urlsvs, eeff] + aux) 
        


    hc.lista_fondos = json.dumps(res)
    hc.save()
    escribir_log( "Lista de fondos vigentes creados <br>" , "Generacion catastro")

def lista_de_fondos_nuevos(period):
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    lista = lista_primaria(period)
    lista = lista_vigentes(lista, period)
    aux = period.split('-')
    #lista = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
    lista = lista.filter(inicio_operaciones__month__lte=aux[0], inicio_operaciones__year=aux[1])
    lista = lista.filter(inicio_operaciones__month__gte=int(aux[0])-2)
    res.append(['Rut administradora', 'Nombre administradora', 'Nombre Fondo', 'Rut Fondo',  'Rescat/No Rescat', 'Clasificación Acafi', 'Nemotécnicos', 'Inicio operaciones', 'Término Operaciones', 'Link Sitio CMF', 'EEFF', 'TOTAL ACTIVO', 'TOTAL ACTIVO CORRIENTE', 'TOTAL ACTIVO NO CORRIENTE',  'TOTAL PASIVO', 'TOTAL PASIVO CORRIENTE', 'TOTAL PASIVO NO CORRIENTE', 'TOTAL PATRIMONIO NETO'])
    
    for l in lista:
        print(l.runsvs)
        if l.categoria == None:
            categ = 0
        else:
            categ = l.categoria.indice
        l.inicio_operaciones = l.inicio_operaciones.strftime('%d-%m-%Y')
        if l.termino_operaciones != None:
            l.termino_operaciones = l.termino_operaciones.strftime('%d-%m-%Y')
        else:
            l.termino_operaciones = ''

        if l.tipo_inversion == 1:
            l.tipo_inversion = 'R'
        else:
            l.tipo_inversion = 'NR'

        if l.nemotecnicos() == None:
            lnemotecnicos = ''
        else:
            lnemotecnicos = l.nemotecnicos()

        aux = []
        movs = movimiento.objects.filter(fondo = l, periodo = period, etiqueta_svs__startswith='TOTAL').order_by('etiqueta_svs')
        if len(movs) > 0:
            eeff = 'Si'
        else:
            eeff = 'No'
        for m in movs:
            aux.append(m.monto_real('pesos'))
        res.append([ l.admin.rut, l.admin.razon_social, l.nombre, l.runsvs, l.tipo_inversion, categ,  lnemotecnicos, l.inicio_operaciones, l.termino_operaciones, l.urlsvs, eeff] + aux) 
        
    hc.lista_fondos_nuevos = json.dumps(res)
    hc.save()
