import json, datetime
from acafi.models import movimiento, fondo, hoja_catastro, categoria, administrador
from acafi.deploy import escribir_log, forfecha, valor_divisa


def lista_primaria(period):
    aux = period.split('-')
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista_total = lista.exclude(categoria =0).count()

    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]


    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista_fondos_nuevos = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
    lista_fondos_nuevos = lista_fondos_nuevos.filter(inicio_operaciones__month__gte=int(aux[0])-2)
    return lista


'''
def lista_vigentes(lista, period):
    aux = period.split('-')

    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))

    for l in lista:
        if not movimiento.objects.filter(fondo = l, periodo = period).exists():
            pass      
    return lista

def lista_fondos_vigentes(period):
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    lista = lista_primaria(period)
    lista = lista_vigentes(lista, period)
    res.append([ 'Run CMF', 'Nemotécnicos', 'Administrador', 'Nombre fondo', 'Clasificación Acafi', 'Rescat/No Rescat', 'Inicio operaciones', 'Término Operaciones'])
    for l in lista:
        print(l.runsvs)
        if l.categoria == None:
            categ = 0
        else:
            categ = l.categoria.indice
        l.inicio_operaciones = l.inicio_operaciones.strftime('%d-%m-%Y')
        if l.termino_operaciones != None:
            l.termino_operaciones = l.termino_operaciones.strftime('%d-%m-%Y')
        else:
            l.termino_operaciones = ''

        if l.tipo_inversion == 1:
            l.tipo_inversion = 'R'
        else:
            l.tipo_inversion = 'NR'

        if l.nemotecnicos() == None:
            lnemotecnicos = ''
        else:
            lnemotecnicos = l.nemotecnicos()

        res.append([ l.runsvs, lnemotecnicos, l.admin.razon_social, l.nombre, categ, l.tipo_inversion, l.inicio_operaciones, l.termino_operaciones])

    hc.lista_fondos = json.dumps(res)
    hc.save()
    escribir_log( "Lista de fondos vigentes creados <br>" , "Generacion catastro")

def lista_de_fondos_nuevos(period):
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    lista = lista_primaria(period)
    lista = lista_vigentes(lista, period)
    aux = period.split('-')
    lista = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
    lista = lista.filter(inicio_operaciones__month__gte=int(aux[0])-2)

    res.append([ 'Run CMF', 'Nemotécnicos', 'Administrador', 'Nombre fondo', 'Clasificación Acafi', 'Rescat/No Rescat', 'Inicio operaciones', 'Término Operaciones'])
    for l in lista:
        if l.categoria == None:
            categ = 0
        else:
            categ = l.categoria.indice
            l.inicio_operaciones = l.inicio_operaciones.strftime('%d-%m-%Y')
        if l.termino_operaciones != None:
            l.termino_operaciones = l.termino_operaciones.strftime('%d-%m-%Y')
        else:
            l.termino_operaciones = ''

        if l.tipo_inversion == 1:
            l.tipo_inversion = 'R'
        else:
            l.tipo_inversion = 'NR'

        if l.nemotecnicos() == None:
            lnemotecnicos = ''
        else:
            lnemotecnicos = l.nemotecnicos()

        res.append([ l.runsvs, lnemotecnicos, l.admin.razon_social, l.nombre, categ, l.tipo_inversion, str(l.inicio_operaciones), str(l.termino_operaciones)])

    hc.lista_fondos_nuevos = json.dumps(res)
    hc.save()
'''

def activos_fondos_nuevos(period):
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    aux = period.split('-')      
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
    lista = lista.filter(inicio_operaciones__month__gte=int(aux[0])-2)

    res.append(['','','','','','','','', 'Fecha', forfecha(period)])
    res.append(['','','','','','','','', 'Valor Dólar', valor_divisa(period, 'dolar')])
    res.append(['','','','','','','','', 'Valor UF', valor_divisa(period, 'uf')])
    res.append([''])
    res.append(['','Nuevos Fondos que presentaron Estados Financieros a '+str(forfecha(period))])

    res.append(['', 'Administradora', 'Rut Admin', 'Fondo', 'Rut Fondo', 'Clase', 'Moneda', 'Rescate', 'Mill. de $', 'Miles de US$'])
    i = 7
    for l in lista:
        if l.categoria == None:
            categ = 0
        else:
            categ = l.categoria.indice
            
        if l.tipo_inversion == 1:
            l.tipo_inversion = 'R'
        else:
            l.tipo_inversion = 'NR'

        mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
        if 'dolar' in mov.moneda.lower() or 'dólar' in mov.moneda.lower():
            mon = 'US$'
        elif 'euro' in mov.moneda.lower():
            mon = 'Euro'
        else:
            mon = '$'

        monto = mov.monto
        if mon == '$':
            monto_dolar = float(mov.monto_real('dolar'))/1000000
        else:
            monto_dolar = float(mov.monto_real('dolar'))/1000

        
        res.append(['', l.admin.razon_social, l.admin.rut , l.nombre, l.runsvs.split('-')[0], categ, mon, l.tipo_inversion, float(mov.monto_real('pesos'))/1000, '=I'+str(i)+'/J2'])
        i += 1
    hc.fondos_nuevos = json.dumps(res)
    hc.save()

def activos_total_activos(period):
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    aux = period.split('-')      
    
    lista = lista_primaria(period)
    
    categorias = (
        ("Fondos de Deuda",  "Renta Fija/Deuda") ,\
        ("Fondos Accionarios",  "Renta Variable/Acciones") ,\
        ("Fondos Mixtos",  "Mixtos Deuda/Acciones") ,\
        ("Otros Fondos Mixtos",  "Otros (ETFs,etc)") ,\
        ("Fondos Inmobiliarios",  "Inmobiliarios") ,\

        ("_Fondos de Capital Privado",  "Capital Privado") ,\
        ("_Fondo de Fondos",  "Fondo de Fondos") ,\
        ("_Fondos de Deuda Privada",  "Deuda Privada") ,\
        ("_Fondos de Infraestructura y Energia",  "Infraestructura") ,\
        ("_Otros Activos Alternativos",  "Otros") )
    
    fila = [forfecha(period)]
    for key,value in categorias:
        suma = 0

        if key[0] == '_':
            key = key.replace('_', '')
            filtrado_lista = lista.filter(categoria__nombre__startswith="Activos Alternativos", categoria__nombre__icontains=value)

        else:
            filtrado_lista = lista.filter(categoria__nombre__icontains=value)

        #headers += [key, '']

        for l in filtrado_lista:
            print(l.runsvs)
            try:
                mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
                suma += float(mov.monto_real('pesos'))/1000
            except movimiento.DoesNotExist:
                pass
        fila += [suma, '']
    
    #res.append(headers)    
    res.append(fila)
    #escribir_log(''.join(fila), "total_activos")
    #la estructura y los totales en la generación
    hc.total_activos = json.dumps(res)
    hc.save()

def activos_por_fondo(period):
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    lis = fondo.objects.filter(inicio_operaciones__isnull=False)
    lis = lis.exclude(categoria =0).order_by('categoria')
    cont = lis.count()
            
    try:                
        contador = len(eval('['+hc.activos_fondo+']')[0])
        res = eval(hc.activos_fondo)
    except (IndexError, TypeError):
        contador = 0

    i = 0
    j = 0

    categs = categoria.objects.exclude(pk=0)
    for c in categs.order_by('indice'):
        for l in lis.filter(categoria=c):
            fila_pivot += 1
            if i != contador:
                i += 1
                pass
            else:
                print(l.runsvs.split('-')[0])
                fila = {}
                try:
                    mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
                    fila[l.runsvs.split('-')[0]] = [float(mov.monto_real('pesos'))/1000]

                except (movimiento.DoesNotExist, KeyError):
                    fila[l.runsvs.split('-')[0]] = [''] 

                res.append(fila)
                        

        hc.activos_fondo = json.dumps(res)
        hc.save()

def activo_por_afi(period):
    res = []
    hc = hoja_catastro.objects.get(pk=period)
    aux = period.split('-')
    lista = lista_primaria(period)

    try:                
        contador = len(eval('['+hc.activo_por_afi+']')[0])
        res = eval(hc.activo_por_afi)
    except (IndexError, TypeError):
        contador = 0

    i = 0
    j = 0

    if contador == 0:
        #res.append(['','','','','','','',''])
        header = ['','','','','', '','','','','','', '', 'Fecha', forfecha(period)]
        res.append(header)
        header = ['','','','','', '','','','','','', '','Valor Dólar', valor_divisa(period, 'dolar')]
        res.append(header)
        header = ['','','','','','','','','','','', '','Valor UF', valor_divisa(period, 'uf')]
        res.append(header)
        res.append(['',''])
        res.append(['','(Millones de pesos)'])
        res.append(['',''])
        res.append(['RUT','ADMINISTRADORA','','Fondos de Deuda', 'Fondos Accionarios', 'Fondos Mixtos', 'Otros Fondos Mixtos',\
                 'Fondos Inmobiliarios', 'Fondos de Capital Privado', 'Fondo de Fondos', '_Fondos de Deuda Privada', '_Otros Activos Alternativos', 'Totales', 'Participación'])
        res.append(['',''])
        
        categorias = (
            ("Fondos de Deuda",  "Renta Fija/Deuda") ,\
            ("Fondos Accionarios",  "Renta Variable/Acciones") ,\
            ("Fondos Mixtos",  "Mixtos Deuda/Acciones") ,\
            ("Otros Fondos Mixtos",  "Otros (ETFs,etc)") ,\
            ("Fondos Inmobiliarios",  "Inmobiliarios") ,\

            ("_Fondos de Capital Privado",  "Capital Privado") ,\
            ("_Fondo de Fondos",  "Fondo de Fondos") ,\
            ("_Fondos de Deuda Privada",  "Deuda Privada") ,\
            ("_Fondos de Infraestructura y Energia",  "Infraestructura") ,\
            ("_Otros Activos Alternativos",  "Otros") )    
        #categorias = ['11', '12', '13', '14', '10', '20','30','40']

        for a in administrador.objects.filter().order_by('razon_social'):
            if a.tiene_fondos():
                print(a.tiene_fondos())
                fila = [a.rut, a.razon_social,'']
                for key,value in categorias:
                    print("\n"+value+"\n")
                    suma = 0
                    if key[0] == '_':
                        key = key.replace('_', '')
                        filtrado_lista = lista.filter(categoria__nombre__startswith="Activos Alternativos", categoria__nombre__icontains=value, admin=a)

                    else:
                        filtrado_lista = lista.filter(categoria__nombre__icontains=value, admin=a)
                    for l in filtrado_lista:
                        print(l.runsvs)
                        try:
                            mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
                            suma += float(mov.monto_real('pesos'))/1000
                        except movimiento.DoesNotExist:
                            pass
                    fila += [suma]
            
                res.append(fila)

        hc.activo_por_afi = json.dumps(res)        
        hc.save()

