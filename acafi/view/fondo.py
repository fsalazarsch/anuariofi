from django.shortcuts import render, redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from acafi.forms import Fondo_ext_form, Fondo_asignar, Bug_form, Fondo_exportar_adm, Formimportarlista
from django.db.models.functions import Substr
from django.db.models import Avg, Sum, Q
from django.utils.safestring import mark_safe
from django.urls import reverse
from acafi.models import fondo, perfil, fondo_ext, periodo, inversion, pais, movimiento, sector, composicion, divisa, resumen, categoria, administrador, rent, detalle_fondo, cuota, serie, hoja_catastro, resultado, indicador, transaccion_cuota, nemo_inversion
import json, datetime, zipfile, os, pwd, grp, pyexcel, requests
from acafi.deploy import escribir_log, getperiodosanteriores, get_periodos
from django.views.generic import View
from pyexcel_xls import save_data
from openpyxl.utils import get_column_letter
from openpyxl import load_workbook, Workbook

from openpyxl.chart import Reference, Series, BarChart3D, PieChart3D
from openpyxl.styles import Font, Color, Fill, PatternFill, Alignment

import pyexcel as pe
from io import BytesIO, StringIO
from django.conf import settings
from acafi.view.graficos import generar_json
from pathlib import Path
from weasyprint import HTML, CSS
from acafi.deploy import get_paginador
from acafi.view.serie import dias_entre

from acafi.view.catastros.generar.lista_fondos import lista_fondos_vigentes, lista_de_fondos_nuevos
from acafi.view.catastros.generar.activos import activos_fondos_nuevos, activos_total_activos, activos_por_fondo, activo_por_afi
from acafi.view.catastros.generar.eeff import eeff

from operator import itemgetter



#"4- Activo por AFI" : [x for x in eval( hc.activo_por_afi )
#def traducion_divisa(nombre_hoja_original, moneda_original, moneda_destino):

def periodoSiguiente(periodo_actual):
    aux = periodo_actual.split('-')
    if aux[0] == '12':
        periodoSig = '03-'+str(int(aux[1])+1)
    elif aux[0] == '3':
        periodoSig = '06-'+str(aux[1])
    elif aux[0] == '6':
        periodoSig = '09-'+str(aux[1])
    elif aux[0] == '9':
        periodoSig = '12-'+str(aux[1])
    
    return periodoSig

def has_key(fondo, periodo_final, tipo):
    aux = periodo_final.split('-')
    if tipo == 'resumen':
        if int(aux[1]) > 2018:
            periodoAnt = '12-2018'
            while (periodoAnt != periodo_final ):
            
                try:
                    hc = hoja_catastro.objects.get(pk=periodoAnt)
                except hoja_catastro.DoesNotExist:
                    hc = hoja_catastro(periodo=periodoAnt)
                    hc.save()
                
                resumen = json.loads(hc.resumen)
                for res in resumen:
                    if fondo == res.get('run'):
                        return True

                periodoAnt = periodoSiguiente(periodoAnt)
    
        try:
            hc = hoja_catastro.objects.get(pk=periodo_final)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=periodo_final)
            hc.save()

        resumen = json.loads(hc.resumen)
        for res in resumen:
            if fondo == res.get('run'):
                return True

    if tipo == 'tabla_catastro':
        if int(aux[1]) > 2018:
            periodoAnt = '12-2018'
            while (periodoAnt != periodo_final ):
            
                try:
                    hc = hoja_catastro.objects.get(pk=periodoAnt)
                except hoja_catastro.DoesNotExist:
                    hc = hoja_catastro(periodo=periodoAnt)
                    hc.save()
                
                tabla = json.loads(hc.tabla_catastro)
                for tab in tabla:
                    if fondo == tab.get('run'):
                        return True

                periodoAnt = periodoSiguiente(periodoAnt)
    
        try:
            hc = hoja_catastro.objects.get(pk=periodo_final)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=periodo_final)
            hc.save()

        tabla = json.loads(hc.tabla_catastro)
        for tab in tabla:
            if fondo == tab.get('run'):
                return True

    if tipo == 'activos_fondo':
        if int(aux[1]) > 2018:
            periodoAnt = '12-2018'
            while (periodoAnt != periodo_final ):
            
                try:
                    hc = hoja_catastro.objects.get(pk=periodoAnt)
                except hoja_catastro.DoesNotExist:
                    hc = hoja_catastro(periodo=periodoAnt)
                    hc.save()
                
                activos = json.loads(hc.activos_fondo)
                for act in activos:
                    if fondo == act.get('run'):
                        return True

                periodoAnt = periodoSiguiente(periodoAnt)
    
        try:
            hc = hoja_catastro.objects.get(pk=periodo_final)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=periodo_final)
            hc.save()

        activos = json.loads(hc.activos_fondo)
        for act in activos:
            if fondo == act.get('run'):
                return True

    if tipo == 'activos_fondos_afi':
        if int(aux[1]) > 2018:
            periodoAnt = '12-2018'
            while (periodoAnt != periodo_final ):
            
                try:
                    hc = hoja_catastro.objects.get(pk=periodoAnt)
                except hoja_catastro.DoesNotExist:
                    hc = hoja_catastro(periodo=periodoAnt)
                    hc.save()
                
                activos = json.loads(hc.activos_fondos_afi)
                for act in activos:
                    if fondo == act.get('run'):
                        return True

                periodoAnt = periodoSiguiente(periodoAnt)
    
        try:
            hc = hoja_catastro.objects.get(pk=periodo_final)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=periodo_final)
            hc.save()

        activos = json.loads(hc.activos_fondos_afi)
        for act in activos:
            if fondo == act.get('run'):
                return True
    
    if tipo == 'patrimonio_fondo':
        if int(aux[1]) > 2018:
            periodoAnt = '12-2018'
            while (periodoAnt != periodo_final ):
            
                try:
                    hc = hoja_catastro.objects.get(pk=periodoAnt)
                except hoja_catastro.DoesNotExist:
                    hc = hoja_catastro(periodo=periodoAnt)
                    hc.save()
                
                patrimonio = json.loads(hc.patrimonio_fondo)
                for pat in patrimonio:
                    if fondo == pat.get('run'):
                        return True

                periodoAnt = periodoSiguiente(periodoAnt)
    
        try:
            hc = hoja_catastro.objects.get(pk=periodo_final)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=periodo_final)
            hc.save()

        patrimonio = json.loads(hc.patrimonio_fondo)
        for pat in patrimonio:
            if fondo == pat.get('run'):
                return True
        
    if tipo == 'patrimonio_afi':
        if int(aux[1]) > 2018:
            periodoAnt = '12-2018'
            while (periodoAnt != periodo_final ):
            
                try:
                    hc = hoja_catastro.objects.get(pk=periodoAnt)
                except hoja_catastro.DoesNotExist:
                    hc = hoja_catastro(periodo=periodoAnt)
                    hc.save()
                
                patrimonio = json.loads(hc.patrimonio_afi)
                for pat in patrimonio:
                    if fondo == pat.get('run_adm'):
                        return True

                periodoAnt = periodoSiguiente(periodoAnt)
    
        try:
            hc = hoja_catastro.objects.get(pk=periodo_final)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=periodo_final)
            hc.save()

        patrimonio = json.loads(hc.patrimonio_afi)
        for pat in patrimonio:
            if fondo == pat.get('run_adm'):
                return True

    if tipo == 'valor_cuotas':
        if int(aux[1]) > 2018:
            periodoAnt = '12-2018'
            while (periodoAnt != periodo_final ):
            
                try:
                    hc = hoja_catastro.objects.get(pk=periodoAnt)
                except hoja_catastro.DoesNotExist:
                    hc = hoja_catastro(periodo=periodoAnt)
                    hc.save()
                
                cuotas = json.loads(hc.valor_cuotas)
                for cuota in cuotas:
                    if fondo == cuota.get('run_fon'):
                        return True

                periodoAnt = periodoSiguiente(periodoAnt)
    
        try:
            hc = hoja_catastro.objects.get(pk=periodo_final)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=periodo_final)
            hc.save()

        cuotas = json.loads(hc.valor_cuotas)
        for cuota in cuotas:
            if fondo == cuota.get('run_fon'):
                return True

    if tipo == 'cuotas_pagadas':
        if int(aux[1]) > 2018:
            periodoAnt = '12-2018'
            while (periodoAnt != periodo_final ):
            
                try:
                    hc = hoja_catastro.objects.get(pk=periodoAnt)
                except hoja_catastro.DoesNotExist:
                    hc = hoja_catastro(periodo=periodoAnt)
                    hc.save()
                
                cuotas = json.loads(hc.cuotas_pagadas)
                for cuota in cuotas:
                    if fondo == cuota.get('run_fon'):
                        return True

                periodoAnt = periodoSiguiente(periodoAnt)
    
        try:
            hc = hoja_catastro.objects.get(pk=periodo_final)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=periodo_final)
            hc.save()

        cuotas = json.loads(hc.cuotas_pagadas)
        for cuota in cuotas:
            if fondo == cuota.get('run_fon'):
                return True
    return False


def celda_estilo(wb, rango, font_color= 'FF000000', font_name='Arial', font_size=10, bold=False, bgcolor =None):
    
    rows = wb.iter_rows(rango)
    for row in rows:
        for celda in row:
            celda.font = Font(color = font_color, name = font_name, size=font_size, bold=bold)

             #background

            if bgcolor != None:
                celda.fill = PatternFill("solid", fgColor=bgcolor)

def forfecha(period):
    return datetime.datetime.strptime("01-"+str(period), "%d-%m-%Y").strftime("%b %Y")

def valor_divisa(period, divisa):
    aux = period.split('-')
    
    i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
    if divisa == 'dolar':
        nueva_fecha = i.fecha + datetime.timedelta(days=1)
        i = indicador.objects.get(pk= nueva_fecha)
    
    if divisa != 'pesos':
        while i._meta.get_field( divisa.lower()).value_from_object(i) == None:
            nueva_fecha = i.fecha + datetime.timedelta(days=1)
            i = indicador.objects.get(pk= nueva_fecha)
        return i._meta.get_field( divisa.lower()).value_from_object(i)

def lista_vigentes(lista, period):
    aux = period.split('-')

    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))

    listaaux = []
    for l in lista:
        if not movimiento.objects.filter(fondo = l, periodo = period).exists():
            pass
       
    return lista

def lista_periodos(period, quitar_meses=False):
    anio_final = period.split('-')[1]
    mes = period.split('-')[0]

    resul = []

    for i in range(2005, int(anio_final)):
        resul.append("12-"+str(i))

    if quitar_meses == False:
        for j in range(3, 13, 3):
            if int(mes) >= j:
                if j < 10:
                    resul.append("0"+str(j)+"-"+str(anio_final))
                else:
                    resul.append(str(j)+"-"+str(anio_final))
    else:
        resul.append(period)

    return resul

def lista_meses_totales(period):
    anio_final = period.split('-')[1]
    mes = period.split('-')[0]

    resul = []
    resul.append("12-2004") 

    for i in range(2005, int(anio_final)+1):
        for j in range(1,13):
            resul.append(str(j)+"-"+str(i))

    return resul

def lista_periodos_totales(period):
    anio_final = period.split('-')[1]
    mes = period.split('-')[0]

    resul = []
    resul.append("12-2018") 

    '''
    for i in range(2005, int(anio_final)+1):
        for j in range(3, 13, 3):
            if i < int(anio_final):
                if j < 10:
                    resul.append("0"+str(j)+"-"+str(i))
                else:
                    resul.append(str(j)+"-"+str(i))

            if int(mes) >= j and i == int(anio_final):
                if j < 10:
                    resul.append("0"+str(j)+"-"+str(i))
                else:
                    resul.append(str(j)+"-"+str(i))                
    print(resul)
    '''
    return resul



#retorna la funcion de sumar los periodos de una celda
'''def suma_periodos(matriz):
    i = 0
    for fila in matriz:
        while fila[5+i*3] != 0 and fila[5+i*3+1] != 0 and fila[5+i*3+2] != 0:
        filan[5] = fila[5+i*3]+fila[5+i*3+1]+fila[5+i*3+2]
        filan[6] = fila[8]+fila[9]+fila[10]
        filan[7] = fila[11]+fila[12]+fila[13]
        filan[5] = fila[5]+fila[6]+fila[7]
        filan[5] = fila[5]+fila[6]+fila[7]
        filan[5] = fila[5]+fila[6]+fila[7]
        filan[5] = fila[5]+fila[6]+fila[7]
        pass
'''

#edita datos blandos factsheet fondo
def index_fondo(request, runsvs=None):
    user = request.user
    flag = 0
    try:
        acceso = perfil.objects.get(pk=user).nivel_acceso
        administradora = perfil.objects.get(pk=user).administrador
        if fondo.objects.get(pk=runsvs).admin == administradora:
            flag == 1
    except perfil.DoesNotExist:
        acceso = 0
    if user.is_staff or user.is_superuser or acceso >= 75:
        flag = 2
    if fondo.objects.get(pk=runsvs).user == user or fondo.objects.get(pk=runsvs).user == user:
        flag = 1

        # perfil = profile.objects.get(user_id = request.user.id)
    if flag >= 0:
        if request.method == "POST":
            form = Fondo_ext_form(request.POST)

            riesgo = {}
            riesgo_felier_rate = request.POST.get('riesgo_felier_rate')
            riesgo_fitch_ratings = request.POST.get('riesgo_fitch_ratings')
            riesgo_comision = request.POST.get('riesgo_comision')
            riesgo_icr = request.POST.get('riesgo_icr')
            riesgo_humphreys = request.POST.get('riesgo_humphreys')

            riesgo['felier_rate'] = riesgo_felier_rate
            riesgo['fitch_ratings'] = riesgo_fitch_ratings
            riesgo['comision'] = riesgo_comision
            riesgo['icr'] = riesgo_icr
            riesgo['humphreys'] = riesgo_humphreys

            try:
                fe = fondo_ext.objects.get(pk=runsvs)

            except fondo_ext.DoesNotExist:
                runsvs = request.POST.get('runsvs')
                fe = fondo_ext(pk=runsvs, bolsa_offshore='', portafolio_manager='')
                fe.save()

            fe.validado = 1
            #fe.codigo_bolsa = request.POST.get('codigo_bolsa')
            fe.bloomberg = request.POST.get('bloomberg')
            fe.isin = request.POST.get('isin')
            fe.bolsa_offshore = request.POST.get('bolsa_offshore')
            fe.portafolio_manager = request.POST.get('portafolio_manager')
            fe.estrategia = request.POST.get('estrategia')
            fe.comision = request.POST.get('comision')
            fe.comentario = request.POST.get('comentario')
            fe.riesgo = json.dumps(riesgo)
            fe.divisa =  divisa.objects.get(pk =request.POST.get('moneda'))
            fe.save()

            try:
                f = fondo.objects.get(pk=runsvs)
            except fondo.DoesNotExist:
                runsvs = request.POST.get('runsvs')
                f = fondo(vigente=1)
            if flag == 2:
                f.categoria = categoria.objects.get(pk=request.POST.get('tipo_operaciones'))
            f.save()

            try:
                riesg = json.loads(fe.riesgo)
            except json.JSONDecodeError:
                riesg = {'felier_rate': '', 'fitch_ratings': '', 'comision': '', 'icr': '', 'humphreys': ''}

            pal = "Usuario id:"+str(request.user.id)+" nombre:"+str(request.user.username)+" guardo datos "
                #pal = ''
            if  fondo_ext.objects.get(pk=runsvs) != None:
                fe = fondo_ext.objects.get(pk=runsvs)
                pal += ' Datos: Categoria '+ str(f.categoria.indice)+\
                ', bloomberg '+ str(fe.bloomberg)+', runsvs '+ str(fe.runsvs)+', isin '+str(fe.isin)+\
                ', bolsa_offshore '+str(fe.bolsa_offshore)+' , portafolio_manager '+ str(fe.portafolio_manager)+\
                ', estrategia '+str(fe.estrategia)+', comision '+ str(fe.comision)+', comentario '+ str(fe.comentario)+\
                ', riesgo_felier_rate '+str(riesg['felier_rate'])+', riesgo_fitch_ratings '+ str(riesg['fitch_ratings'])+\
                ', riesgo_comision'+ str(riesg['comision'])+', riesgo_icr'+ str(riesg['icr'])+', riesgo_humphreys '+ str(riesg['humphreys'])+\
                ', moneda '+str(fe.divisa.divisa_id)

            escribir_log( pal , "secciones/Editar formulario fondo")


            response = 2
            return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))


        else:

            try:
                f = fondo.objects.get(pk=runsvs)
                nombrefondo = f.nombre
            except ObjectDoesNotExist:
                nombrefondo = 'No definido'

            try:
                fe = fondo_ext.objects.get(pk=runsvs)
                if fe.divisa is None:
                    fe.divisa = divisa.objects.get(pk='---')
                try:
                    riesg = json.loads(fe.riesgo)
                except json.JSONDecodeError:
                    riesg = {'felier_rate': '', 'fitch_ratings': '', 'comision': '', 'icr': '', 'humphreys': ''}

                pal = "Usuario id:"+str(request.user.id)+" nombre:"+str(request.user.username)+" ingreso"
                #pal = ''
                if  fondo_ext.objects.get(pk=runsvs) != None:
                    fe = fondo_ext.objects.get(pk=runsvs)
                    if f.categoria != None:
                        pal += ' Datos: Categoria '+ str(f.categoria.indice)+ ', '
                    if fe.divisa != None:
                        pal += 'moneda '+str(fe.divisa.divisa_id)
                    pal += ', bloomberg '+ str(fe.bloomberg)+', runsvs '+ str(fe.runsvs)+', isin '+str(fe.isin)+\
                    ', bolsa_offshore '+str(fe.bolsa_offshore)+' , portafolio_manager '+ str(fe.portafolio_manager)+\
                    ', estrategia '+str(fe.estrategia)+', comision '+ str(fe.comision)+', comentario '+ str(fe.comentario)+\
                    ', riesgo_felier_rate '+str(riesg['felier_rate'])+', riesgo_fitch_ratings '+ str(riesg['fitch_ratings'])+\
                    ', riesgo_comision'+ str(riesg['comision'])+', riesgo_icr'+ str(riesg['icr'])+', riesgo_humphreys '+ str(riesg['humphreys'])

                escribir_log( pal , "secciones/Editar formulario fondo")


                if fe.divisa == None:
                    div = ''
                else:
                    div = fe.divisa.divisa_id

                if f.categoria == None:

                    form = Fondo_ext_form(
                        initial={'bloomberg': fe.bloomberg, 'runsvs': fe.runsvs,
                                 'isin': fe.isin, 'bolsa_offshore': fe.bolsa_offshore, 'portafolio_manager': fe.portafolio_manager,
                                 'estrategia': fe.estrategia, 'comision': fe.comision, 'comentario': fe.comentario,
                                 'riesgo_felier_rate': riesg['felier_rate'],
                                 'riesgo_fitch_ratings': riesg['fitch_ratings'], 'riesgo_comision': riesg['comision'],
                                 'riesgo_icr': riesg['icr'], 'riesgo_humphreys': riesg['humphreys'], 'moneda' : div})
                else:
                    form = Fondo_ext_form(
                        initial={'tipo_operaciones': f.categoria.indice,
                                 'bloomberg': fe.bloomberg, 'runsvs': fe.runsvs, 'isin': fe.isin,
                                 'bolsa_offshore': fe.bolsa_offshore, 'portafolio_manager': fe.portafolio_manager,
                                 'estrategia': fe.estrategia, 'comision': fe.comision, 'comentario': fe.comentario,
                                 'riesgo_felier_rate': riesg['felier_rate'],
                                 'riesgo_fitch_ratings': riesg['fitch_ratings'], 'riesgo_comision': riesg['comision'],
                                 'riesgo_icr': riesg['icr'], 'riesgo_humphreys': riesg['humphreys'], 'moneda' : div })
            except ObjectDoesNotExist:
                fe = None
                form = Fondo_ext_form(initial={'runsvs': runsvs})

            form2 = Bug_form(initial={'fondo': runsvs})

            return render(request, 'fondo/index.html',
                          {'form': form, 'form2': form2, 'fondo': f, 'fondo_ext': fe, 'runsvs': runsvs,
                           'nombrefondo': nombrefondo, 'flag': flag})

    else:
        response = 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

#lista los fondos
def listar_fondos(request, periodo = None, opcion=None):
    if request.user.is_staff or request.user.is_superuser or perfil.objects.get(pk=user).nivel_acceso >= 75:
        lista = fondo.objects.all()
        lista = lista.filter(inicio_operaciones__isnull=False)

        try:
            aux = periodo.split('-')
            mes = (int(aux[0])+1)%12
            lista = lista.filter(inicio_operaciones__lt=aux[1]+'-'+str(mes)+'-01')
        except AttributeError:
            aux = []
        
        if periodo != None:
            #periodo = MM-YYYY
            aux = periodo.split('-')
            #lista2 = lista.filter(inicio_operaciones__year=aux[1], termino_operaciones__month__lte=aux[0])
            
            #403 fondos al 09-2018
            lista2 = lista.filter(Q(termino_operaciones__isnull=True) | \
                ( \
                    #Q(termino_operaciones__lt=aux[1]+'-'+str(mes)+'-01') &  \
                    Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')  \
                    #Q(termino_operaciones__year=aux[1], termino_operaciones__month__gte=int(aux[0])-2)    \
                ))

            #lista2 = lista.filter(Q(termino_operaciones__isnull=True) | \
            #    ( \
            #        Q(termino_operaciones__year=aux[1], termino_operaciones__month__lte=aux[0])) &  \
            #        Q(termino_operaciones__year=aux[1], termino_operaciones__month__gte=int(aux[0])-2)    \
            #    )
            
            #termino_operaciones__range=(start_date, end_date))

            lista = lista2

        if opcion == 'nuevos':
            aux = periodo.split('-')

            lista3 = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
            lista3 = lista3.filter(inicio_operaciones__month__gte=int(aux[0])-2)
            lista = lista3

        page = request.GET.get('page')
        if page is None:
            page = 1

              
        count = lista.count()
        paginator = Paginator(lista, 50)
        lista = lista[(50 * (int(page) - 1)): 50 * (int(page) - 1) + 50]
        categorias = categoria.objects.filter(seleccionable=1)


        try:
            lista = paginator.page(page)
        except PageNotAnInteger:
            lista = paginator.page(1)
        except EmptyPage:
            lista = paginator.page(paginator.num_pages)

        for l in lista:

            aux  = fondo_ext.objects.get(pk = l.runsvs)
            
            try:
                series = serie.objects.filter(fondo=id).values_list('nemotecnico__nemotecnico', flat=True)
            except serie.DoesNotExist:
                series = None

            l.vigente = series


        filtros = {'page': page }
        context = {"lista_fondos": lista, "count": count, "lista": lista, 'lista_categorias': categorias, 'administradores' : administrador.objects.all().order_by('razon_social'),
                   'user': request.user}

        merged_dict = dict(list(filtros.items()) + list(context.items()))
        return render(request, 'fondo/lista_fondos.html', merged_dict)

    else:
        response = 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

#asigna a un usuario para que pueda editar el fondo
def asignar_fondo(request, runsvs):
    if request.user.is_staff or request.user.is_superuser:
        user = request.user
        # perfil = profile.objects.get(user_id = request.user.id)
        fe = fondo.objects.get(pk=runsvs)
        usuario = fe.user

        if request.method == "POST":
            form = Fondo_asignar(request.POST)
            fe = fondo.objects.get(pk=runsvs)
            fe.user = User.objects.get(pk=int(request.POST.get('usuario')))
            fe.save()
            # return HttpResponseRedirect('/perfil/')
            response = 1
            return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

        else:
            if usuario is not None:
                form = Fondo_asignar(initial={'usuario': usuario.id})
            else:
                form = Fondo_asignar()

            return render(request, 'fondo/asignar.html', {'form': form, 'fondo': fe, 'usuario': usuario})

    else:
        response = 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

#despliega factsheet web
def factsheet(request, id, anio=None, mes=12):
    flag = 1
    if flag == 1:
        f = fondo.objects.get(pk = id)
        if anio == None:
            fecha = datetime.datetime.today()
            anio = str(fecha.year)
        
        meses_esp = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

        hoy = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d")
        hace_un_anio = datetime.datetime.now() - datetime.timedelta(days=365)

        fechas = []
        valores_libro = []
        datos = []
        promedios =[] #rent_anios para diaria
        rentabilidades = []
        
        flag_inconsistente = False
        pers= getperiodosanteriores(f, '2012-12', str(anio)+'-'+str(mes)).order_by(Substr('periodo', 3), 'periodo')
            
        df = detalle_fondo.objects.get(fondo = f)

        if df.rentabilidad == 'D':
            rentablidades = []
            rent_anios = []
            ahora = datetime.datetime.now()
            anio_actual= int(ahora.year)
            l = df.serie_antigua
            primera_cuota = cuota.objects.filter(serie= l).order_by('fecha').first()
            anio_pivote = int(primera_cuota.fecha.year)
            #print(anio_pivote)


            try:
                c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                if anio_pivote-1 == anio_actual-2:
                    c2 = primera_cuota
                else:
                    c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
                if c1.indice== 0 or c2.indice== 0 or c1.indice == None or c2.indice == None:
                    promedios.insert(0, [str(int(anio)), 0])
                else:
                    diario_rentab_anio =(float(c1.indice)/float(c2.indice)-1)*100
                    promedios.insert(0, [str(int(anio)), diario_rentab_anio])
            except cuota.DoesNotExist:
                promedios.insert(0, [str(int(anio)), 0])

            try:
                if anio_pivote-1 == anio_actual-3:
                    c3 = primera_cuota
                else: 
                    c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
                c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
                        
                if c3.indice== 0 or c2.indice== 0 or c3.indice == None or c2.indice == None:
                    promedios.insert(0, [str(int(anio)-1), 0])
                    
                #rentablidades.append( str(float(c1.indice))+'/'+str(float(c2.indice)) )
                else:
                    diario_rentab_anio_pasado = (float(c2.indice)/float(c3.indice)-1)*100
                    promedios.insert(0, [str(int(anio)-1), diario_rentab_anio_pasado])   
            except cuota.DoesNotExist:
                promedios.insert(0, [str(int(anio)-1), 0])
                    
            try:
                c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
                if anio_pivote-1 == anio_actual-4: 
                    c4 = primera_cuota
                else:
                    c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4)
                if c3.indice== 0 or c4.indice== 0 or c3.indice== None or c4.indice==None:
                    promedios.insert(0, [str(int(anio)-2), 0])
                else:
                    diario_rentab_anio_antepasado = (float(c3.indice)/float(c4.indice)-1)*100
                    promedios.insert(0, [str(int(anio)-2), diario_rentab_anio_antepasado])
            except cuota.DoesNotExist:
                promedios.insert(0, [str(int(anio)-2), 0])
                        
                #hace 3 años
            try:
                c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4)
                            
                if c1.indice== 0 or c4.indice== 0 or c1.indice==None or c4.indice== None:
                    rentabilidades.append(0)
                else:
                    exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), '31/12/'+str(anio_actual-4) ) ))
                    diario_rentab_3er_anio = (((c1.indice/c4.indice)**(exp))-1 )*100
                    rentabilidades.append(diario_rentab_3er_anio)
            except cuota.DoesNotExist:
                rentabilidades.append(0)

            #hace 5 años
            try:
                c5 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-6) #hace 5 años
                c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                            
                if c1.indice== 0 or c5.indice== 0 or c1.indice==None or c5.indice== None or c1==None or c5== None:
                    rentabilidades.append(0)
                else:
                    exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), '31/12/'+str(anio_actual-6) ) ))
                    diario_rentab_5to_anio = (((c1.indice/c5.indice)**(exp))-1 )*100
                    rentabilidades.append(diario_rentab_5to_anio)
            except cuota.DoesNotExist:
                rentabilidades.append(0)

            #inicio
            try:
                ci = cuota.objects.filter(serie= l).order_by('fecha')[:1] #inicio
                c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                        
                if c1.indice==None:
                    rentabilidades.append(0)
                else:
                    exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), ci[0].fecha.strftime("%d/%m/%Y") ) ))
                    diario_rentab_ini_anio = (((c1.indice/100)**(exp))-1 )*100
                    rentabilidades.append(diario_rentab_ini_anio)
            except cuota.DoesNotExist:
                rentabilidades.append(0)
            
            #context = {"serie": l, "rentablidades" : [i * 100 for i in rentablidades], "rent_anios": [i * 100 for i in rent_anios]}        
            #row = writer.writerow([obj.fondo.runsvs, [i * 100 for i in rent_anios], [i * 100 for i in rentablidades]])
            periodos = []
            label_periodo = []

            for p in pers:
                periodos.append([str(p.periodo), float (round(p.rentabilidad(), 2) )])
                label_periodo.append(str(p.periodo))

            if len(periodos) <= 1:
                periodos = []

        else:
            if f.desactivar_rentabilidades == True:
                try:
                    r = rent.objects.get(fondo=f, periodo="12-"+str(anio))
                    rentabilidades = [ r.rentab_3er_anio, r.rentab_5to_anio, r.rentab_ini_anio ]

                    promedios.insert(0, [str(int(anio)), r.rentab_anio])
                    promedios.insert(0, [str(int(anio)-1), r.rentab_anio_pasado])
                    promedios.insert(0, [str(int(anio)-2), r.rentab_anio_antepasado])

                
                except rent.DoesNotExist:
                    rentabilidades = [0, 0, 0]
                    promedios.insert(0, [str(int(anio)), '0'])
                    promedios.insert(0, [str(int(anio)-1), '0'])
                    promedios.insert(0, [str(int(anio)-2), '0'])               
            else:
                #DATOS RENTABILIDAD ANUAL

                promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:3][::-1]
                for pa in promedios_aux:
                    paperiodo= pa.periodo.split('-')
                    promedios.append([paperiodo[1], round(pa.indice_rentabilidad(), 1)])

                #acotamos a 3 anios
                if len(promedios) == 2:
                    promedios.insert(0, [str(int(anio)-2), '0'])

                if len(promedios) == 1:
                    promedios.insert(0, [str(int(anio)-1), '0'])
                    promedios.insert(0, [str(int(anio)-2), '0'])

                if len(promedios) == 0:
                    promedios.insert(0, [str(int(anio)), '0'])
                    promedios.insert(0, [str(int(anio)-1), '0'])
                    promedios.insert(0, [str(int(anio)-2), '0'])



                #DATOS RENTABILIDAD PROMEDIO
                aux= 1
                index = 0
                promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:3][::-1]
                for pa in promedios_aux:
                    if pa.indice_rentabilidad() != 0:
                        index += 1
                        #aux += pa.indice_rentabilidad()
                        #agregado el 1+ rent
                        #print(pa.indice_rentabilidad()/100+1)
                        aux *= (pa.indice_rentabilidad()/100+1)

                        
                if index < 3:
                    rentabilidades.append(0)
                else:
                    rentabilidades.append(((aux**(1/index))-1)*100)
                    #rentabilidades.append(aux/index)

                aux= 1
                index = 0
                promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:5][::-1]
                for pa in promedios_aux:
                    if pa.indice_rentabilidad() != 0:
                        index += 1
                        #aux += pa.indice_rentabilidad()
                        #agregado el 1+ rent
                        aux *= (pa.indice_rentabilidad()/100+1)


                if index < 5:
                    rentabilidades.append(0)
                else:
                    #rentabilidades.append(aux/index)
                    rentabilidades.append( ((aux**(1/index))-1)*100 )

                aux = 1
                index = 0
                anios_totales = int(f.anios_totales())
                promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:anios_totales][::-1]
                for pa in promedios_aux:
                    if pa.indice_rentabilidad() != 0:
                        index += 1
                        #aux += pa.indice_rentabilidad()
                        #agregado el 1+ rent
                        aux *= (pa.indice_rentabilidad()/100+1)
                        #print(pa.indice_rentabilidad()+100)
                    else:
                        anios_totales -= 1
                if index == 0:
                    rentabilidades.append(0)
                else:
                    rentabilidades.append( ((aux**(1/anios_totales))-1)*100 )
                    
                    #rentabilidades.append(aux/anios_totales)
                    #rentabilidades.append((aux**(1/anios_totales)))

            #aplicamos  n
            #         ( Σ n√(1+ rent)) -1
            #          i=1
            
            #print(rentabilidades)
                
            flag_inconsistente = False
            for finc in promedios:
                if int(finc[1]) >= 1000:
                    flag_inconsistente = True
            for finc in rentabilidades:
                if int(finc) >= 1000:
                    flag_inconsistente = True

            periodos = []
            label_periodo = []

            #DATOS DE GRAFICO RENTABILIDAD
            
            #pers = periodo.objects.filter(periodo__endswith="-"+str(anio), fondo=f).order_by('periodo')
            #pers = periodo.objects.filter(fondo=f).order_by(Substr('periodo', 3), 'periodo')
            for p in pers:
                periodos.append([str(p.periodo), float (round(p.rentabilidad(), 2) )])
                label_periodo.append(str(p.periodo))
            
            if len(periodos) <= 1:
                periodos = []
        
        #promedios = json.dumps(promedios)

        invs = inversion.objects.filter(periodo=str(mes)+"-"+anio, fondo=f).values('empresa','nemotecnico','codigo').annotate(porcentaje=Sum('porcentaje')).order_by("-porcentaje")
        

        cont = 10 - invs.count()
        invs = invs[:10]



        if f.categoria != None:
            f.categoria.nombre = f.categoria.nombre.replace("Fondos de Inversion de", "")

        try:
            fe = fondo_ext.objects.get(pk = id)
            
            try:
                series = serie.objects.filter(fondo=id).values_list('nemotecnico__nemotecnico', flat=True)
            except serie.DoesNotExist:
                series = None
            
            #if fe.codigo_bolsa != None:
            #    fe.codigo_bolsa = fe.codigo_bolsa.replace(",",", ")

            try:
                fe.riesgo = json.loads(fe.riesgo)
            except json.JSONDecodeError:
                fe.riesgo = "{}"

        except fondo_ext.DoesNotExist:
            fe = None

        fecha = datetime.datetime.now()
        pa = pais.objects.get(pk = f.admin.pais).nombre

        #patrimonio_total = 0
        #patrimonios  = movimientos = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio, etiqueta_svs__startswith="PN").exclude(monto=0).exclude(
        #    etiqueta_svs__icontains='total')
        #for pat in patrimonios:
        #    patrimonio_total += pat.monto
        


        context = {"fondo" : f, "fondo_ext": fe, "yyyy" : anio, "mm" : meses_esp[int(mes)-1], "pais" :pa, "id":id, "invs" : invs, "cont" : range(cont), "fechas" : mark_safe(fechas), "valores_libro" : mark_safe(valores_libro), "promedios" : promedios, 'p' : mark_safe(periodos), 'label_periodo':label_periodo, 'rentabilidades': rentabilidades, 'flag_inconsistente':flag_inconsistente,  "nemos" : ','.join(series)}

        try:
            periodos = periodo.objects.filter(periodo=str(mes) + "-" + anio, fondo=f)[:1]
            composiciones = composicion.objects.filter(periodo=periodos).order_by('-porc_propiedad')

            cont2 = 12 - composiciones.count()
            composiciones = composiciones[:12]

        except periodo.DoesNotExist:
            periodos = None
            composiciones = None;

        invs2 = inversion.objects.filter(periodo=str(mes) + "-" + anio, fondo=f).order_by("-porcentaje")

        divisas = inversion.objects.filter(periodo=str(mes) + "-" + anio, fondo=f).values("moneda").annotate(Sum("porcentaje"))
        paises = inversion.objects.filter(periodo=str(mes) + "-" + anio, fondo=f).values("pais").annotate(Sum("porcentaje"))
        porc_total = divisas.aggregate(Sum('porcentaje__sum'))['porcentaje__sum__sum']
        divs2 = []
        pais_es = []
        for d in divisas:
            try:
                d['moneda'] = divisa.objects.get(alias_svs=d['moneda']).nombre
            except divisa.DoesNotExist:
                d['moneda'] = "No especificada"

            if int(porc_total) == 0:
                porc_total = 100
            divs2.append([d['moneda'], float(d['porcentaje__sum'] * 100 / porc_total)])
        for p in paises:
            try:
                p['pais'] = pais.objects.get(pk=p['pais']).nombre
            except pais.DoesNotExist:
                pass
                #p['pais'] = 'No especificado'
            if float(float(p['porcentaje__sum']) * 100 / float(porc_total)) > 0.05:
                pais_es.append([p['pais'], float(p['porcentaje__sum'] * 100 / porc_total)])

        pais_es = sorted(pais_es,key=lambda x: x[1], reverse=True)
        suma_principales = 0
        for index in range(5):
            try:
                suma_principales += pais_es[index][1]
            except IndexError:
                continue
            #print(str(pais_es[index][0].encode('ascii', 'ignore').decode('ascii'))+":"+str(pais_es[index][1])+" + ")
        pais_es = sorted(pais_es,key=lambda x: x[1], reverse=True)[:5]
        if suma_principales != 100 and suma_principales != 0:
            pais_es.append(['Otros', float(100 -suma_principales)])



            # d['porcentaje__sum'] = str(float(d['porcentaje__sum']))

        try:
            movimientos = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio).exclude(
            etiqueta_svs__icontains='total').order_by('-ap')
        except movimiento.DoesNotExist:
            movimientos = []
            movimientos2 = []

        activos = movimientos.filter(ap=1)
        pasivos = movimientos.filter(ap=0)


        aa_valores = list(movimiento.objects.filter(fondo=f, periodo__in = pers.values_list('periodo', flat=True), periodo__startswith="12-", etiqueta_svs='TOTAL ACTIVO').order_by('periodo').values_list('monto', flat=True))
        
        for idx in range(len(aa_valores)):
            aa_valores[idx] /= 1000

        aa_periodos = movimiento.objects.filter(fondo=f,  periodo__in = pers.values_list('periodo', flat=True), periodo__startswith="12-", etiqueta_svs='TOTAL ACTIVO').order_by('periodo').values_list('periodo', flat=True)
        # divisas = json.dumps(divisas)


        per = periodo.objects.filter(periodo=str(mes) + "-" + anio, fondo=f)[:1]
        listResumen = resumen.objects.filter(periodo_id=per).exclude(nacional=0, extranjero=0)

        auxnac = auxint = auxporc =0
        for aux in listResumen:
            auxnac += aux.nacional
            auxint += aux.extranjero
            auxporc += aux.porcentual


        totalesresumen = [auxnac, auxint, auxporc] 

        movimientos = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio).order_by('-ap')
        patrimonio_raw = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio, etiqueta_svs__icontains='PN-')
        patrimonio = {pati.etiqueta_svs[3:].title(): pati.monto for pati in patrimonio_raw}
        totales_raw = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio, etiqueta_svs__icontains='total')
        totales = {total.etiqueta_svs.title(): total.monto for total in totales_raw}
        cartera = listResumen.exclude(descripcion__icontains='total').aggregate(suma=Sum('nacional') + Sum('extranjero'))['suma']
        if cartera == None:
            cartera = 0
        try:
            dic_mov = {
            'Disponible': movimientos.get(etiqueta_svs__icontains='efectivo').monto,
            'Cartera de Inversiones': cartera,
            }
        except movimiento.DoesNotExist:
            dic_mov = {
            'Disponible': 0,
            'Cartera de Inversiones': 0,
            }

        for llave in patrimonio:
            if llave[-3:] == '  O':
                llave2 = llave[:-3]
                patrimonio[llave2] = patrimonio.pop(llave)
        anio2 = str(int(anio) - 1)
        try:
            per2 = periodo.objects.get(periodo=str(mes) + "-" + anio2, fondo=f)
            listResumen2 = resumen.objects.filter(periodo=per2).exclude(nacional=0, extranjero=0)
            cartera2 = listResumen2.exclude(descripcion__icontains='total').aggregate(suma=Sum('nacional') + Sum('extranjero'))['suma']
        except periodo.DoesNotExist:
            per2 = []
            listResumen2 = []
            cartera2 = []

        if cartera2 == None or cartera2 == []:
            cartera2 = 0
        movimientos2 = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio2).order_by('-ap')
        patrimonio_raw2 = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio2, etiqueta_svs__icontains='PN-')
        patrimonio2 = {pati.etiqueta_svs[3:].title(): pati.monto for pati in patrimonio_raw2}
        totales_raw2 = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio2, etiqueta_svs__icontains='total')
        totales2 = {total.etiqueta_svs.title(): total.monto for total in totales_raw2}
        try:
            dic_mov2 = {
                'Disponible': movimientos2.get(etiqueta_svs__icontains='efectivo').monto,
                'Cartera de Inversiones': cartera2,
            }

        except movimiento.DoesNotExist:
            dic_mov2 = {
                'Disponible': 0,
                'Cartera de Inversiones': 0,
            }


        for llave in patrimonio2:
            if llave[-3:] == '  O':
                llave2 = llave[:-3]
                patrimonio2[llave2] = patrimonio2.pop(llave)

        try:
            patrimonio['Resultado Del Ejercicio']
        except KeyError:
            patrimonio['Resultado Del Ejercicio'] = 0

        try:
            totales['Total Activo']
        except KeyError:
            totales['Total Activo'] = 0

        try:
            totales2['Total Activo']
        except KeyError:
            totales2['Total Activo'] = 0


        try:
            totales['Total Pasivo']
        except KeyError:
            totales['Total Pasivo'] = 0

        try:
            totales2['Total Pasivo']
        except KeyError:
            totales2['Total Pasivo'] = 0

        try:
            totales['Total Pasivo Corriente']
        except KeyError:
            totales['Total Pasivo Corriente'] = 0

        try:
            totales2['Total Pasivo Corriente']
        except KeyError:
            totales2['Total Pasivo Corriente'] = 0

        try:
            totales['Total Pasivo No Corriente']
        except KeyError:
            totales['Total Pasivo No Corriente'] = 0

        try:
            totales2['Total Pasivo No Corriente']
        except KeyError:
            totales2['Total Pasivo No Corriente'] = 0

        try:
            totales['Total Patrimonio Neto  O']
        except KeyError:
            totales['Total Patrimonio Neto  O'] = 0

        try:
            totales2['Total Patrimonio Neto  O']
        except KeyError:
            totales2['Total Patrimonio Neto  O'] = 0

        try:
            patrimonio['Aportes']
        except KeyError:
            patrimonio['Aportes'] = 0

        try:
            patrimonio2['Aportes']
        except KeyError:
            patrimonio2['Aportes'] = 0

        try:
            patrimonio['Otras Reservas']
        except KeyError:
            patrimonio['Otras Reservas'] = 0

        try:
            patrimonio2['Otras Reservas']
        except KeyError:
            patrimonio2['Otras Reservas'] = 0

        try:
            patrimonio['Resultados Acumulados']
        except KeyError:
            patrimonio['Resultados Acumulados'] = 0

        try:
            patrimonio2['Resultados Acumulados']
        except KeyError:
            patrimonio2['Resultados Acumulados'] = 0
        try:
            patrimonio['Resultado Del Ejercicio']
        except KeyError:
            patrimonio['Resultado Del Ejercicio'] = 0
        try:
            patrimonio2['Resultado Del Ejercicio']
        except KeyError:
            patrimonio2['Resultado Del Ejercicio'] = 0
        try:
            patrimonio['Dividendos Provisorios']
        except KeyError:
            patrimonio['Dividendos Provisorios'] = 0
        try:
            patrimonio2['Dividendos Provisorios']
        except KeyError:
            patrimonio2['Dividendos Provisorios'] = 0



        lista = [
            ('Total Activos', totales['Total Activo'], totales2['Total Activo']),
            ('Disponible', dic_mov['Disponible'], dic_mov2['Disponible']),
            ('Cartera de Inversiones', dic_mov['Cartera de Inversiones'], dic_mov2['Cartera de Inversiones']),
            ('Otros Activos', totales['Total Activo'] - dic_mov['Disponible'] - dic_mov['Cartera de Inversiones'],
            totales2['Total Activo'] - dic_mov2['Disponible'] - dic_mov2['Cartera de Inversiones']),
            ('Total Pasivos y Patrimonio', totales['Total Pasivo'], totales2['Total Pasivo']),
            ('Pasivos de Corto Plazo', totales['Total Pasivo Corriente'], totales2['Total Pasivo Corriente']),
            ('Pasivos de Largo Plazo', totales['Total Pasivo No Corriente'], totales2['Total Pasivo No Corriente']),
            ('Patrimonio Neto', totales['Total Patrimonio Neto  O'], totales2['Total Patrimonio Neto  O']),
            ('Aportes (+)', patrimonio['Aportes'], patrimonio2['Aportes']),
            ('Otras Reservas (+)', patrimonio['Otras Reservas'], patrimonio2['Otras Reservas']),
            ('Resultados Acumulados (+ o -)', patrimonio['Resultados Acumulados'], patrimonio2['Resultados Acumulados']),
            ('Resultado del Ejercicio (+ o -)', patrimonio['Resultado Del Ejercicio'], patrimonio2['Resultado Del Ejercicio']),
            ('Dividendos Provisorios (-)', patrimonio['Dividendos Provisorios'], patrimonio2['Dividendos Provisorios'])
        ]

        try:
            mov_princ = movimiento.objects.get(periodo=str(mes) + "-" + anio, fondo=f, etiqueta_svs = 'TOTAL ACTIVO')

            total_categoria = 0
            total_categoria1 = movimiento.objects.filter(fondo__categoria = f.categoria, periodo=str(mes) + "-" + anio, etiqueta_svs__iexact = 'total activo')
            for t1 in total_categoria1:
                total_categoria += t1.monto_real('pesos')

            #   print(str(mov_princ.monto_real())+"*100/"+str(total_categoria))

            total_categoria_madre = 0
            total_categoria_madre1 = movimiento.objects.filter(periodo=str(mes) + "-" + anio, etiqueta_svs__iexact = 'total activo')
            for t1 in total_categoria_madre1:
                total_categoria_madre += t1.monto_real('pesos')
            
            #print(str(mov_princ.monto_real())+"*100/"+str(total_categoria_madre))
            
            porcentaje_madre = fe.get_porcentaje_madre()
            porcentaje= float(mov_princ.monto_real('pesos'))*100/float(total_categoria)
            #porcentaje_madre= float(mov_princ.monto_real())*100/float(total_categoria_madre)


            magnitud = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio).first().moneda
            magnitud = magnitud.replace("Dolar", "Dólar")
            
        except movimiento.DoesNotExist:
            mov_princ = []
            total_categoria = 0
            total_categoria_madre = 0
        
            porcentaje= 0
            porcentaje_madre= 0
            magnitud = 'NA'            

        context2 = { "composiciones": composiciones, 'pt' : str(float(totales['Total Patrimonio Neto  O']/1000)),  
                   'invs2': invs2, 'divisas2': divs2, 'paises': pais_es, "activos": activos, "pasivos": pasivos,
                   'aa_valores': aa_valores, 'aa_periodos': list(aa_periodos), 'cont2': range(cont2),
                   'listResumen': listResumen, 'per': per, "composicion": lista,'magnitud': magnitud, 
                   'porcentaje' : porcentaje, 'porcentaje_madre' : porcentaje_madre, "totalesresumen" :totalesresumen}


        merged_dict = dict(list(context.items()) + list(context2.items()))

        return render(request, "factsheets/hoja.html", merged_dict)
        #return render(request, 'factsheets/hoja3.html', merged_dict, content_type='application/pdf')
        #t = loader.get_template('actsheets/hoja3.html')
        #return HttpResponse(t.render(merged_dict, request), mimetype='application/pdf')

#despliega factsheet pdf
def factsheetpdf(request, id, anio=None, paginador=1):

    mes = 12
    flag = 1
    if flag == 1:
        f = fondo.objects.get(pk = id)
        pagindor = get_paginador(f)
        if anio == None:
            fecha = datetime.datetime.today()
            anio = str(fecha.year)
        
        meses_esp = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

        hoy = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d")
        hace_un_anio = datetime.datetime.now() - datetime.timedelta(days=365)

        
        fechas = []
        valores_libro = []
        datos = []
        promedios =[]
        rentabilidades = []
        flag_inconsistente = False


        pers= getperiodosanteriores(f, '2012-12', str(anio)+'-'+str(mes)).order_by(Substr('periodo', 3), 'periodo')    
        df = detalle_fondo.objects.get(fondo = f)

        if df.rentabilidad == "D":

            if f.desactivar_rentabilidades == True:
                anio = int(anio)-1
                try:
                    r = rent.objects.get(fondo=f, periodo="12-"+str(anio))
                    rentabilidades = [ r.diario_rentab_3er_anio, r.diario_rentab_5to_anio, r.diario_rentab_ini_anio ]

                    promedios.insert(0, [str(int(anio)), r.diario_rentab_anio])
                    promedios.insert(0, [str(int(anio)-1), r.diario_rentab_anio_pasado])
                    promedios.insert(0, [str(int(anio)-2), r.diario_rentab_anio_antepasado])

                
                except rent.DoesNotExist:
                    rentabilidades = [0, 0, 0]
                    promedios.insert(0, [str(int(anio)), '0'])
                    promedios.insert(0, [str(int(anio)-1), '0'])
                    promedios.insert(0, [str(int(anio)-2), '0'])  


            else:
                l = df.serie_antigua
                flag_inconsistente = False

                anio_actual= int(anio)
                primera_cuota = cuota.objects.filter(serie= l).order_by('fecha').first()
                anio_pivote = int(primera_cuota.fecha.year)
                print(anio_pivote)
                try:

                    c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual)
                    if anio_pivote-1 == anio_actual-1:
                        c2 = primera_cuota
                    else:
                        c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                    if c1.indice== 0 or c2.indice== 0 or c1.indice == None or c2.indice == None:
                        promedios.append([anio, 0])
                    else:

                        promedios.append([anio, (float(c1.indice)/float(c2.indice)-1) * 100])
                except cuota.DoesNotExist:
                    promedios.append([anio, '--'])
                try:
                    if anio_pivote-1 == anio_actual-2:

                        c3 = primera_cuota
                    else: 
                        c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
                    c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                    
                    if c3.indice== 0 or c2.indice== 0 or c3.indice == None or c2.indice == None:
                        promedios.append([str(int(anio)-1), 0])
                    #rentabilidades.append( str(float(c1.indice))+'/'+str(float(c2.indice)) )
                    else:
                        promedios.append([str(int(anio)-1), (float(c2.indice)/float(c3.indice)-1) *100])
                except cuota.DoesNotExist:
                    promedios.append([str(int(anio)-1), '--'])
                try:
                    c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
                    if anio_pivote-1 == anio_actual-3: 
                        c4 = primera_cuota
                    else:
                        c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
                    if c3.indice== 0 or c4.indice== 0 or c3.indice== None or c4.indice==None:
                        promedios.append([str(int(anio)-2),0])
                    else:
                        promedios.append([str(int(anio)-2) , (float(c3.indice)/float(c4.indice)-1) * 100])
                except cuota.DoesNotExist:
                    promedios.append([str(int(anio)-2), '--'])
                    

                    #hace 3 años
                try:
                    c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual)
                    c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
                        
                    if c1.indice== 0 or c4.indice== 0 or c1.indice==None or c4.indice== None:
                        rentabilidades.append(0)
                    else:
                        exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual), '31/12/'+str(anio_actual-3) ) ))
                        rentabilidades.append( (((c1.indice/c4.indice)**(exp))-1)*100 )
                except cuota.DoesNotExist:
                    rentabilidades.append(0)        
                #hace 5 años
                try:
                    c5 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-5) #hace 5 años
                    c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual)
                        
                    if c1.indice== 0 or c5.indice== 0 or c1.indice==None or c5.indice== None or c1==None or c5== None:
                        rentabilidades.append(0)
                    else:
                        exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual), '31/12/'+str(anio_actual-5) ) ))
                        rentabilidades.append( (((c1.indice/c5.indice)**(exp))-1)*100 )
                except cuota.DoesNotExist:
                    rentabilidades.append(0)

                #inicio
                try:
                    ci = cuota.objects.filter(serie= l).order_by('fecha')[:1] #inicio
                    c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual)
                    
                    if c1.indice==None:
                        rent_anios.append(0)
                    else:
                        exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual), ci[0].fecha.strftime("%d/%m/%Y") ) ))
                        rentabilidades.append( (((c1.indice/100)**(exp))-1)*100 )
                except cuota.DoesNotExist:
                    rentabilidades.append(0)

                i = 0

        else:

            if f.desactivar_rentabilidades == True:
                try:
                    r = rent.objects.get(fondo=f, periodo="12-"+str(anio))
                    rentabilidades = [ r.rentab_3er_anio, r.rentab_5to_anio, r.rentab_ini_anio ]

                    promedios.insert(0, [str(int(anio)), r.rentab_anio])
                    promedios.insert(0, [str(int(anio)-1), r.rentab_anio_pasado])
                    promedios.insert(0, [str(int(anio)-2), r.rentab_anio_antepasado])

                
                except rent.DoesNotExist:
                    rentabilidades = [0, 0, 0]
                    promedios.insert(0, [str(int(anio)), '0'])
                    promedios.insert(0, [str(int(anio)-1), '0'])
                    promedios.insert(0, [str(int(anio)-2), '0'])   
            else:
                #DATOS RENTABILIDAD ANUAL

                promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:3][::-1]
                for pa in promedios_aux:
                    paperiodo= pa.periodo.split('-')
                    promedios.append([paperiodo[1], round(pa.indice_rentabilidad(), 1)])


                #acotamos a 3 anios
                if len(promedios) == 2:
                    promedios.insert(0, [str(int(anio)-2), '0'])

                if len(promedios) == 1:
                    promedios.insert(0, [str(int(anio)-1), '0'])
                    promedios.insert(0, [str(int(anio)-2), '0'])

                if len(promedios) == 0:
                    promedios.insert(0, [str(int(anio)), '0'])
                    promedios.insert(0, [str(int(anio)-1), '0'])
                    promedios.insert(0, [str(int(anio)-2), '0'])


                #DATOS RENTABILIDAD PROMEDIO
                rentabilidades = []
                
                aux= 1
                index = 0
                promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:3][::-1]
                for pa in promedios_aux:
                    if pa.indice_rentabilidad() != 0:
                        index += 1
                        #aux += pa.indice_rentabilidad()
                        #agregado el 1+ rent
                        #print(pa.indice_rentabilidad()/100+1)
                        aux *= (pa.indice_rentabilidad()/100+1)
                        
                if index < 3:
                    rentabilidades.append(0)
                else:
                    rentabilidades.append(((aux**(1/index))-1)*100)
                    #rentabilidades.append(aux/index)

                aux= 1
                index = 0
                promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:5][::-1]
                for pa in promedios_aux:
                    if pa.indice_rentabilidad() != 0:
                        index += 1
                        #aux += pa.indice_rentabilidad()
                        #agregado el 1+ rent
                        aux *= (pa.indice_rentabilidad()/100+1)

                if index < 5:
                    rentabilidades.append(0)
                else:
                    #rentabilidades.append(aux/index)
                    rentabilidades.append( ((aux**(1/index))-1)*100 )

                aux = 1
                index = 0
                anios_totales = int(f.anios_totales())
                promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:anios_totales][::-1]
                for pa in promedios_aux:
                    if pa.indice_rentabilidad() != 0:
                        index += 1
                        #aux += pa.indice_rentabilidad()
                        #agregado el 1+ rent
                        aux *= (pa.indice_rentabilidad()/100+1)
                        #print(pa.indice_rentabilidad()+100)
                    else:
                        anios_totales -= 1
                if index == 0:
                    rentabilidades.append(0)
                else:
                    rentabilidades.append( ((aux**(1/anios_totales))-1)*100 )
                #print(rentabilidades)

            flag_inconsistente = False
            for finc in promedios:
                if int(finc[1]) >= 1000:
                    flag_inconsistente = True
            for finc in rentabilidades:
                if int(finc) >= 1000:
                    flag_inconsistente = True

        

        print(rentabilidades)
        periodos = []
        label_periodo = []

        for p in pers:
            periodos.append([str(p.periodo), float (round(p.rentabilidad(), 2) )])
            label_periodo.append(str(p.periodo))

        if len(periodos) <= 1:
            periodos = []

        invs = inversion.objects.filter(periodo=str(mes)+"-"+str(anio), fondo=f).values('empresa','nemotecnico','codigo').annotate(porcentaje=Sum('porcentaje')).order_by("-porcentaje")


        cont = 10 - invs.count()
        invs = invs[:10]



        if f.categoria != None:
            f.categoria.nombre = f.categoria.nombre.replace("Fondos de Inversion de", "")

        try:
            

            fe = fondo_ext.objects.get(pk = id)
            #if fe.codigo_bolsa != None:
            #    fe.codigo_bolsa = fe.codigo_bolsa.replace(",",", ")
            try:
                series = serie.objects.filter(fondo=id).values_list('nemotecnico__nemotecnico', flat=True)
            except serie.DoesNotExist:
                series = None

            try:
                fe.riesgo = json.loads(fe.riesgo)
            except json.JSONDecodeError:
                fe.riesgo = "{}"

        except fondo_ext.DoesNotExist:
            fe = None

        fecha = datetime.datetime.now()
        pa = pais.objects.get(pk = f.admin.pais).nombre


        context = {"fondo" : f, "fondo_ext": fe, "yyyy" : anio, "mm" : meses_esp[int(mes)-1], "pais" :pa, "id":id, "invs" : invs, "cont" : range(cont), "fechas" : mark_safe(fechas), "valores_libro" : mark_safe(valores_libro), "promedios" : promedios, 'p' : mark_safe(periodos), 'label_periodo':label_periodo, 'rentabilidades': rentabilidades, "flag_inconsistente": flag_inconsistente, "nemos" : ','.join(series)}

        try:
            periodos = periodo.objects.filter(periodo=str(mes) + "-" + str(anio), fondo=f)[:1]
            composiciones = composicion.objects.filter(periodo=periodos).order_by('-porc_propiedad')


            cont2 = 12 - composiciones.count()
            composiciones = composiciones[:12]

        except periodo.DoesNotExist:
            periodos = None
            composiciones = None;

        invs2 = inversion.objects.filter(periodo=str(mes) + "-" + str(anio), fondo=f).order_by("-porcentaje")

        #divisas = inversion.objects.filter(periodo=str(mes) + "-" + anio, fondo=f).values("moneda").annotate(Sum("porcentaje"))
        #paises = inversion.objects.filter(periodo=str(mes) + "-" + anio, fondo=f).values("pais").annotate(Sum("porcentaje"))


        try:
            movimientos = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + str(anio)).exclude(
            etiqueta_svs__icontains='total').order_by('-ap')
        except movimiento.DoesNotExist:
            movimientos = []
            movimientos2 = []

        activos = movimientos.filter(ap=1)
        pasivos = movimientos.filter(ap=0)


        per = periodo.objects.filter(periodo=str(mes) + "-" + str(anio), fondo=f)[:1]
        listResumen = resumen.objects.filter(periodo_id=per).exclude(nacional=0, extranjero=0)

        auxnac = auxint = auxporc =0
        for aux in listResumen:
            auxnac += aux.nacional
            auxint += aux.extranjero
            auxporc += aux.porcentual


        totalesresumen = [auxnac, auxint, auxporc] 

        movimientos = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + str(anio)).order_by('-ap')
        patrimonio_raw = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + str(anio), etiqueta_svs__icontains='PN-')
        patrimonio = {pati.etiqueta_svs[3:].title(): pati.monto for pati in patrimonio_raw}
        totales_raw = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + str(anio), etiqueta_svs__icontains='total')
        totales = {total.etiqueta_svs.title(): total.monto for total in totales_raw}
        cartera = listResumen.exclude(descripcion__icontains='total').aggregate(suma=Sum('nacional') + Sum('extranjero'))['suma']
        if cartera == None:
            cartera = 0
        try:
            dic_mov = {
            'Disponible': movimientos.get(etiqueta_svs__icontains='efectivo').monto,
            'Cartera de Inversiones': cartera,
            }
        except movimiento.DoesNotExist:
            dic_mov = {
            'Disponible': 0,
            'Cartera de Inversiones': 0,
            }

        for llave in patrimonio:
            if llave[-3:] == '  O':
                llave2 = llave[:-3]
                patrimonio[llave2] = patrimonio.pop(llave)
        anio2 = str(int(anio) - 1)
        try:
            per2 = periodo.objects.get(periodo=str(mes) + "-" + anio2, fondo=f)
            listResumen2 = resumen.objects.filter(periodo=per2).exclude(nacional=0, extranjero=0)
            cartera2 = listResumen2.exclude(descripcion__icontains='total').aggregate(suma=Sum('nacional') + Sum('extranjero'))['suma']
        except periodo.DoesNotExist:
            per2 = []
            listResumen2 = []
            cartera2 = []

        if cartera2 == None or cartera2 == []:
            cartera2 = 0
        movimientos2 = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio2).order_by('-ap')
        patrimonio_raw2 = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio2, etiqueta_svs__icontains='PN-')
        patrimonio2 = {pati.etiqueta_svs[3:].title(): pati.monto for pati in patrimonio_raw2}
        totales_raw2 = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + anio2, etiqueta_svs__icontains='total')
        totales2 = {total.etiqueta_svs.title(): total.monto for total in totales_raw2}
        try:
            dic_mov2 = {
                'Disponible': movimientos2.get(etiqueta_svs__icontains='efectivo').monto,
                'Cartera de Inversiones': cartera2,
            }

        except movimiento.DoesNotExist:
            dic_mov2 = {
                'Disponible': 0,
                'Cartera de Inversiones': 0,
            }


        for llave in patrimonio2:
            if llave[-3:] == '  O':
                llave2 = llave[:-3]
                patrimonio2[llave2] = patrimonio2.pop(llave)

        try:
            patrimonio['Resultado Del Ejercicio']
        except KeyError:
            patrimonio['Resultado Del Ejercicio'] = 0

        try:
            totales['Total Activo']
        except KeyError:
            totales['Total Activo'] = 0

        try:
            totales2['Total Activo']
        except KeyError:
            totales2['Total Activo'] = 0


        try:
            totales['Total Pasivo']
        except KeyError:
            totales['Total Pasivo'] = 0

        try:
            totales2['Total Pasivo']
        except KeyError:
            totales2['Total Pasivo'] = 0

        try:
            totales['Total Pasivo Corriente']
        except KeyError:
            totales['Total Pasivo Corriente'] = 0

        try:
            totales2['Total Pasivo Corriente']
        except KeyError:
            totales2['Total Pasivo Corriente'] = 0

        try:
            totales['Total Pasivo No Corriente']
        except KeyError:
            totales['Total Pasivo No Corriente'] = 0

        try:
            totales2['Total Pasivo No Corriente']
        except KeyError:
            totales2['Total Pasivo No Corriente'] = 0

        try:
            totales['Total Patrimonio Neto  O']
        except KeyError:
            totales['Total Patrimonio Neto  O'] = 0

        try:
            totales2['Total Patrimonio Neto  O']
        except KeyError:
            totales2['Total Patrimonio Neto  O'] = 0

        try:
            patrimonio['Aportes']
        except KeyError:
            patrimonio['Aportes'] = 0

        try:
            patrimonio2['Aportes']
        except KeyError:
            patrimonio2['Aportes'] = 0

        try:
            patrimonio['Otras Reservas']
        except KeyError:
            patrimonio['Otras Reservas'] = 0

        try:
            patrimonio2['Otras Reservas']
        except KeyError:
            patrimonio2['Otras Reservas'] = 0

        try:
            patrimonio['Resultados Acumulados']
        except KeyError:
            patrimonio['Resultados Acumulados'] = 0

        try:
            patrimonio2['Resultados Acumulados']
        except KeyError:
            patrimonio2['Resultados Acumulados'] = 0
        try:
            patrimonio['Resultado Del Ejercicio']
        except KeyError:
            patrimonio['Resultado Del Ejercicio'] = 0
        try:
            patrimonio2['Resultado Del Ejercicio']
        except KeyError:
            patrimonio2['Resultado Del Ejercicio'] = 0
        try:
            patrimonio['Dividendos Provisorios']
        except KeyError:
            patrimonio['Dividendos Provisorios'] = 0
        try:
            patrimonio2['Dividendos Provisorios']
        except KeyError:
            patrimonio2['Dividendos Provisorios'] = 0

        lista = [
            ('Total Activos', totales['Total Activo'], totales2['Total Activo']),
            ('Disponible', dic_mov['Disponible'], dic_mov2['Disponible']),
            ('Cartera de Inversiones', dic_mov['Cartera de Inversiones'], dic_mov2['Cartera de Inversiones']),
            ('Otros Activos', totales['Total Activo'] - dic_mov['Disponible'] - dic_mov['Cartera de Inversiones'],
                totales2['Total Activo'] - dic_mov2['Disponible'] - dic_mov2['Cartera de Inversiones']),
            ('Total Pasivos y Patrimonio', totales['Total Pasivo'], totales2['Total Pasivo']),
            ('Pasivos de Corto Plazo', totales['Total Pasivo Corriente'], totales2['Total Pasivo Corriente']),
            ('Pasivos de Largo Plazo', totales['Total Pasivo No Corriente'], totales2['Total Pasivo No Corriente']),
            ('Patrimonio Neto', totales['Total Patrimonio Neto  O'], totales2['Total Patrimonio Neto  O']),
            ('Aportes (+)', patrimonio['Aportes'], patrimonio2['Aportes']),
            ('Otras Reservas (+)', patrimonio['Otras Reservas'], patrimonio2['Otras Reservas']),
            ('Resultados Acumulados (+ o -)', patrimonio['Resultados Acumulados'], patrimonio2['Resultados Acumulados']),
            ('Resultado del Ejercicio (+ o -)', patrimonio['Resultado Del Ejercicio'], patrimonio2['Resultado Del Ejercicio']),
            ('Dividendos Provisorios (-)', patrimonio['Dividendos Provisorios'], patrimonio2['Dividendos Provisorios'])
        ]

        try:
            mov_princ = movimiento.objects.get(periodo=str(mes) + "-" + str(anio), fondo=f, etiqueta_svs = 'TOTAL ACTIVO')

            total_categoria = 0
            total_categoria1 = movimiento.objects.filter(fondo__categoria = f.categoria, periodo=str(mes) + "-" + str(anio), etiqueta_svs__iexact = 'total activo')
            for t1 in total_categoria1:
                total_categoria += t1.monto_real('pesos')

            #   print(str(mov_princ.monto_real())+"*100/"+str(total_categoria))

            total_categoria_madre = 0
            total_categoria_madre1 = movimiento.objects.filter(periodo=str(mes) + "-" + str(anio), etiqueta_svs__iexact = 'total activo')
            for t1 in total_categoria_madre1:
                total_categoria_madre += t1.monto_real('pesos')
            
            #print(str(mov_princ.monto_real())+"*100/"+str(total_categoria_madre))
            
            porcentaje_madre = fe.get_porcentaje_madre()
            porcentaje= float(mov_princ.monto_real('pesos'))*100/float(total_categoria)
            #porcentaje_madre= float(mov_princ.monto_real())*100/float(total_categoria_madre)


            magnitud = movimiento.objects.filter(fondo=f, periodo=str(mes) + "-" + str(anio)).first().moneda
            magnitud = magnitud.replace("Dolar", "Dólar")
        except movimiento.DoesNotExist:
            mov_princ = []
            total_categoria = 0
            total_categoria_madre = 0
        
            porcentaje= 0
            porcentaje_madre= 0
            magnitud = 'NA'            

        context2 = {"paginador":paginador, "composiciones": composiciones, 'pt' : str(float(totales['Total Patrimonio Neto  O']/1000)),  
                   'invs2': invs2, "activos": activos, "pasivos": pasivos,
                   'cont2': range(cont2),
                   'listResumen': listResumen, 'per': per, "composicion": lista,'magnitud': magnitud, 
                   'porcentaje' : porcentaje, 'porcentaje_madre' : porcentaje_madre, "totalesresumen" :totalesresumen}


        merged_dict = dict(list(context.items()) + list(context2.items()))
        
        return render(request, "factsheets/hojapdf.html", merged_dict)
        #html_string = render('factsheets/hoja3.html', merged_dict)
        #html = HTML(string=html_string, base_url=request.build_absolute_uri())
        #html.write_pdf(target='/tmp/mypdf.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')]) 


        #return render(request, 'factsheets/hoja3.html', merged_dict, content_type='application/pdf')
        #t = loader.get_template('actsheets/hoja3.html')
        #return HttpResponse(t.render(merged_dict, request), mimetype='application/pdf')

#exporta factsheets en un zip
def exportar_fondo_adm(request, id=''):
    if request.user.is_staff or request.user.is_superuser:
        user = request.user
        # perfil = profile.objects.get(user_id = request.user.id)
        
        if id != '' and request.method == "GET":
            form = Fondo_exportar_adm(request.POST)
            adm = id

            fondos = list(fondo.objects.filter(admin_id=adm))

            a = administrador.objects.get(rut=adm)

            filenames = []
            for f in fondos:
                filenames.append(settings.STATIC_ROOT+"/static/tmp/"+str(f.runsvs)+".pdf")

            if len(filenames) == 0:
                return render(request, 'fondo/exportar.html', {'form': form, 'usuario': user, 'response': '1'})

            zip_subdir = a.razon_social
            zip_filename = "%s.zip" % zip_subdir

            s = BytesIO()
            zf = zipfile.ZipFile(s, "w")

            for fpath in filenames:
                fdir, fname = os.path.split(fpath)
                zip_path = os.path.join(zip_subdir, fname)
                try:
                    zf.write(fpath, zip_path)
                except FileNotFoundError:
                    pass

            zf.close()


            resp = HttpResponse(s.getvalue(), content_type = "application/x-zip-compressed")
            resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

            return resp

        if request.method == "POST":
            form = Fondo_exportar_adm(request.POST)
            adm = request.POST.get('adm')

            fondos = list(fondo.objects.filter(admin_id=adm))

            a = administrador.objects.get(rut=adm)

            filenames = []
            for f in fondos:
                filenames.append(settings.STATIC_ROOT+"/static/tmp/"+str(f.runsvs)+".pdf")

            if len(filenames) == 0:
                return render(request, 'fondo/exportar.html', {'form': form, 'usuario': user, 'response': '1'})

            zip_subdir = a.razon_social
            zip_filename = "%s.zip" % zip_subdir

            s = BytesIO()
            zf = zipfile.ZipFile(s, "w")

            for fpath in filenames:
                fdir, fname = os.path.split(fpath)
                zip_path = os.path.join(zip_subdir, fname)
                try:
                    zf.write(fpath, zip_path)
                except FileNotFoundError:
                    pass

            zf.close()


            resp = HttpResponse(s.getvalue(), content_type = "application/x-zip-compressed")
            resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

            return resp



            for f in fondos:
                print(f.runsvs)
            # return HttpResponseRedirect('/perfil/')
            #response = 1
            #return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

        else:
            fondos = None
            form = Fondo_exportar_adm()
            administradores = administrador.objects.all().order_by('razon_social')

        return render(request, 'fondo/exportar.html', {'form': form, 'usuario': user, 'fondos': fondos, 'administradores': administradores})

    else:
        response = 1
        return HttpResponseRedirect(reverse('perfil', kwargs={"response": response}))

def regenerar_pdf(request, runsvs, anio=2017):
    #anio = 2017
    
    salida =""
    fondos = fondo.objects.filter(fecha_resolucion__lt = datetime.datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    fondos = fondos.exclude(inicio_operaciones__year__gt=anio)

    fondos = fondo.objects.get(runsvs=runsvs)
    paginador = get_paginador(fondos)
    #print(paginador)
    filename =settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(runsvs)+'.pdf'
    file = Path(filename)
    generar_json(runsvs, anio)
    if os.path.exists(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(anio)+'/') == False:
        os.makedirs(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(anio)+'/', exist_ok = True)
    html= HTML(url=settings.URL_BASE+'pdf/'+str(runsvs)+'/'+str(anio)+"/"+str(paginador))
    html.write_pdf(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(anio)+'/'+str(runsvs)+'.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')])
    uid = pwd.getpwnam('ubuntu').pw_uid
    gid = grp.getgrnam('ubuntu').gr_gid
    os.chown(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(anio)+'/'+str(runsvs)+'.pdf', uid, gid)
    os.chmod(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(anio)+'/'+str(runsvs)+'.pdf', 0o644) 
    os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(runsvs)+"_1.png")
    os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(runsvs)+"_2.png")
    os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(runsvs)+"_3.png")
    os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(runsvs)+"_4.png")
    #print("remover imgs")        
    
    #escribir_log("Generado el pdf de "+runsvs+"<br>","Generar_factsheet")
    return redirect('/perfil/5')

def catastro(request, seccion=None, period=None, libro=None):
    if request.method == 'GET':
        if period != None:
            if len(period.split('-')[0]) > 2:
                periodo_anio =period.split('-')[0]
                periodo_mes =period.split('-')[1]
            else:
                periodo_mes =period.split('-')[0]
                periodo_anio =period.split('-')[1]
            
        else:
            periodo_mes =request.GET.get('periodo_mes')
            periodo_anio =request.GET.get('periodo_anio')
            
        if periodo_mes != None and periodo_anio != None:
            period = str(periodo_mes)+'-'+str(periodo_anio)
            
        else:
            period = None
        #Lista de fondos
        if libro == 'lista_fondos':

            try:
                hc = hoja_catastro.objects.values('lista_fondos_nuevos','lista_fondos').get(pk=str(periodo_mes)+'-'+str(periodo_anio))
            except hoja_catastro.DoesNotExist:
                hc = None

                
            if seccion ==  None and period == None:
                context = {'usuario': request.user, 'libro': libro, 'hoja_catastro' : hc}
                return render(request, 'fondo/catastro.html', context)

            else:
                #filtros = {'listado_periodos': listado_periodos }
                context = {'usuario': request.user, 'periodo': period, 'periodo_mes' : periodo_mes, 'periodo_anio':periodo_anio, 'hoja_catastro' : hc, 'libro' : libro}
                #context = dict(list(filtros.items()) + list(context.items())) 
                return render(request, 'fondo/catastro.html', context)

        #Lista de activos
        elif libro == 'activos':

            try:
                hc = hoja_catastro.objects.values('fondos_nuevos','total_activos','activos_fondo','activo_por_afi','inversiones','activos_fondos_afi', 'activos_por_clase').get(pk=str(periodo_mes)+'-'+str(periodo_anio))
            except hoja_catastro.DoesNotExist:
                hc = None

            if seccion ==  None and period == None:
                context = {'usuario': request.user, 'libro': libro, 'hoja_catastro' : hc}
                return render(request, 'fondo/catastro.html', context)

            else:
                #filtros = {'listado_periodos': listado_periodos }
                context = {'usuario': request.user, 'periodo': period, 'periodo_mes' : periodo_mes, 'periodo_anio':periodo_anio, 'hoja_catastro' : hc, 'libro' : libro}
                #context = dict(list(filtros.items()) + list(context.items())) 
                return render(request, 'fondo/catastro.html', context)
            
        #Estado Financiero
        elif libro == 'eeff':

            try:
                hc = hoja_catastro.objects.values('eeff','resumen','tabla_catastro','aportes_netos').get(pk=str(periodo_mes)+'-'+str(periodo_anio))
            except hoja_catastro.DoesNotExist:
                hc = None

                
            if seccion ==  None and period == None:
                context = {'usuario': request.user, 'libro': libro, 'hoja_catastro' : hc}
                return render(request, 'fondo/catastro.html', context)

            else:
                #filtros = {'listado_periodos': listado_periodos }
                context = {'usuario': request.user, 'periodo': period, 'periodo_mes' : periodo_mes, 'periodo_anio':periodo_anio, 'hoja_catastro' : hc, 'libro' : libro}
                #context = dict(list(filtros.items()) + list(context.items())) 
                return render(request, 'fondo/catastro.html', context)
        
        #Patrimonio
        elif libro == 'patrimonio':

            try:
                hc = hoja_catastro.objects.values('patrimonio_fondo','patrimonio_afi','patrimonio_clase','valor_cuotas','cuotas_pagadas').get(pk=str(periodo_mes)+'-'+str(periodo_anio))
            except hoja_catastro.DoesNotExist:
                hc = None

                
            if seccion ==  None and period == None:
                context = {'usuario': request.user, 'libro': libro, 'hoja_catastro' : hc}
                return render(request, 'fondo/catastro.html', context)

            else:
                #filtros = {'listado_periodos': listado_periodos }
                context = {'usuario': request.user, 'periodo': period, 'periodo_mes' : periodo_mes, 'periodo_anio':periodo_anio, 'hoja_catastro' : hc, 'libro' : libro}
                #context = dict(list(filtros.items()) + list(context.items())) 
                return render(request, 'fondo/catastro.html', context)
        
        #Precatastro
        elif libro == 'precatastro':

            hc = None

                
            if seccion ==  None and period == None:
                context = {'usuario': request.user, 'libro': libro, 'hoja_catastro' : hc}
                return render(request, 'fondo/catastro.html', context)

            else:
                #filtros = {'listado_periodos': listado_periodos }
                context = {'usuario': request.user, 'periodo': period, 'periodo_mes' : periodo_mes, 'periodo_anio':periodo_anio, 'hoja_catastro' : hc, 'libro' : libro}
                #context = dict(list(filtros.items()) + list(context.items())) 
                return render(request, 'fondo/catastro.html', context)
        
        #Volumen cuotas
        elif libro == 'volumen_cuotas':

            try:
                hc = hoja_catastro.objects.values('volumen_cuotas_mes','volumen_monto').get(pk=str(periodo_mes)+'-'+str(periodo_anio))
            except hoja_catastro.DoesNotExist:
                hc = None

                
            if seccion ==  None and period == None:
                context = {'usuario': request.user, 'libro': libro, 'hoja_catastro' : hc}
                return render(request, 'fondo/catastro.html', context)

            else:
                #filtros = {'listado_periodos': listado_periodos }
                context = {'usuario': request.user, 'periodo': period, 'periodo_mes' : periodo_mes, 'periodo_anio':periodo_anio, 'hoja_catastro' : hc, 'libro' : libro}
                #context = dict(list(filtros.items()) + list(context.items())) 
                return render(request, 'fondo/catastro.html', context)

        #Caso alternativo
        else:
            try:
                hc = hoja_catastro.objects.get(pk=str(periodo_mes)+'-'+str(periodo_anio))
            except hoja_catastro.DoesNotExist:
                hc = None

                
            if seccion ==  None and period == None:
                context = {'usuario': request.user, 'libro': libro, 'hoja_catastro' : hc}
                return render(request, 'fondo/catastro.html', context)

            lista = fondo.objects.filter(inicio_operaciones__isnull=False)
            lista_total = lista.exclude(categoria =0).count()

            mes = (int(periodo_mes)+1)%12
            if periodo_mes == '12':
                anio = int(periodo_anio)+1
            else:
                anio = periodo_anio
            #12-2018
            #01-10-2018 --> 01-01-2019


            lista = lista.filter(inicio_operaciones__lt=str(periodo_anio)+'-'+str(periodo_mes)+'-01')


            lista_fondos_nuevos = lista.filter(inicio_operaciones__year=periodo_anio, inicio_operaciones__month__lte=periodo_mes)
            lista_fondos_nuevos = lista_fondos_nuevos.filter(inicio_operaciones__month__gte=int(periodo_mes)-2)
            cont2 = lista_fondos_nuevos.count()


            cont = lista_vigentes(lista, period).count()

            listado_periodos =  len(lista_periodos(period))
            
            adms = administrador.objects.all()
            # here is the trick
            q_ids = [o.rut for o in adms if o.tiene_fondos() == True]
            adms = adms.filter(rut__in=q_ids)

            filtros = {'listado_periodos': listado_periodos }
            context = {'usuario': request.user, 'periodo': period, 'periodo_mes' : periodo_mes, 'periodo_anio':periodo_anio, 'hoja_catastro' : hc,\
            'lista_vigentes' : cont, 'fondos_nuevos' : cont2, 'lista_total' : lista_total, 'lista_admin' : adms.count(), 'lista_clases' : categoria.objects.exclude(indice = 0).count(), \
            'libro' : libro}
            context = dict(list(filtros.items()) + list(context.items())) 
            return render(request, 'fondo/catastro.html', context)
       
    else:

        aux = period.split('-')
        lista = fondo.objects.filter(inicio_operaciones__isnull=False)
        lista_total = lista.exclude(categoria =0).count()


        mes = (int(aux[0])+1)%12
        if aux[0] == '12':
            anio = int(aux[1])+1
        else:
            anio = aux[1]


        lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')

        lista_fondos_nuevos = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
        lista_fondos_nuevos = lista_fondos_nuevos.filter(inicio_operaciones__month__gte=int(aux[0])-2)
        

        try:
            hc = hoja_catastro.objects.get(pk=period)
        except hoja_catastro.DoesNotExist:
            hc = hoja_catastro(periodo=period)
            hc.save()

        flag = True

        res = []


        if seccion == 'lista_vigentes':
            if period != None:
                res = requests.get('http://52.14.249.252/api/lista_vigentes/'+str(period))
                res = res.json()
                hc.lista_fondos =json.dumps(res["lista_vigentes"])
                hc.save()

        if seccion == 'lista_nuevos':
            if period != None:
                res = requests.get('http://52.14.249.252/api/lista_nuevos/'+str(period))
                res = res.json()
                hc.lista_fondos_nuevos =json.dumps(res["lista_nuevos"])
                hc.save()

        if seccion == 'fondos_nuevos':
            if period != None:
                res = requests.get('http://52.14.249.252/api/activos_fondos_nuevos/'+str(period))
                res = res.json()
                hc.fondos_nuevos =json.dumps(res["activos_fondos_nuevos"])
                hc.save()

        if seccion == 'total_activos':
            if period != None:
                res = requests.get('http://52.14.249.252/api/total_activos/'+str(period))
                res = res.json()
                hc.total_activos =json.dumps(res["total_activos"])
                hc.save()

        if seccion == ('activos_fondo'):
            if period != None:
                res = requests.get('http://52.14.249.252/api/activos_fondos/'+str(period))
                res = res.json()
                hc.activos_fondo =json.dumps(res["activos_fondos"])
                hc.save()

        if seccion == 'activo_por_afi':
            if period != None:
                res = requests.get('http://52.14.249.252/api/activos_por_afi/'+str(period))
                res = res.json()
                hc.activo_por_afi =json.dumps(res["activos_por_afi"])
                hc.save()

        if seccion == ('activos_fondos_afi'):
            if period != None:
                res = requests.get('http://52.14.249.252/api/activos_fondos_afi/'+str(period))
                res = res.json()
                hc.activos_fondos_afi =json.dumps(res["activos_fondos_afi"])
                hc.save()

        if seccion == 'activos_por_clase':
            if period != None:
                res = requests.get('http://52.14.249.252/api/activos_por_clase/'+str(period))
                res = res.json()
                hc.activos_por_clase =json.dumps(res["activos_por_clase"])
                hc.save()

        if seccion == "inversiones":
            if period != None:
                res = requests.get('http://52.14.249.252/api/inversiones_nac/'+str(period))
                res = res.json()
                hc.inversiones =json.dumps(res["inversiones_nac"])
                hc.save()


        if seccion == 'eeff':
            #eeff(period)
            if period != None:
                res = requests.get('http://52.14.249.252/api/eeff/cat/'+str(period))
                res = res.json()
                hc.eeff =json.dumps(res["eeff"])
                hc.save()

        if seccion == 'resumen':
            #Cargar resumen mediante api y guardar en formato json:
            if period != None:
                res = requests.get('http://52.14.249.252/api/resumen_catastro/'+str(period))
                res = res.json()
                hc.resumen =json.dumps(res["resumen_catastro"])
                hc.save()

        if seccion == ('tabla_catastro'):

            if period != None:
                res = requests.get('http://52.14.249.252/api/tabla_catastro/'+str(period))
                res = res.json()
                hc.tabla_catastro =json.dumps(res["tabla_catastro"])
                hc.save()

        if seccion == 'aportes_netos':

            if period != None:
                res = requests.get('http://52.14.249.252/api/aportes_netos/'+str(period))
                res = res.json()
                hc.aportes_netos =json.dumps(res["aportes_netos"])
                hc.save()

        if seccion == ('patrimonio_fondo'):

            if period != None:
                res = requests.get('http://52.14.249.252/api/patrimonio_fondo/'+str(period))
                res = res.json()
                hc.patrimonio_fondo =json.dumps(res["patrimonio_fondo"])
                hc.save()

        if seccion == ('patrimonio_afi'):

            if period != None:
                res = requests.get('http://52.14.249.252/api/patrimonio_afi/'+str(period))
                res = res.json()
                hc.patrimonio_afi =json.dumps(res["patrimonio_afi"])
                hc.save()

        if seccion == ('patrimonio_clase'):

            if period != None:
                res = requests.get('http://52.14.249.252/api/patrimonio_clase/'+str(period))
                res = res.json()
                hc.patrimonio_clase =json.dumps(res["patrimonio_clase"])
                hc.save()

        if seccion == ('valor_cuotas'):

            if period != None:
                res = requests.get('http://52.14.249.252/api/valor_cuotas/'+str(period))
                res = res.json()
                hc.valor_cuotas =json.dumps(res["valor_cuotas"])
                hc.save()

        if seccion == ('cuotas_pagadas'):

            if period != None:
                res = requests.get('http://52.14.249.252/api/cuotas_pagadas/'+str(period))
                res = res.json()
                hc.cuotas_pagadas =json.dumps(res["cuotas_pagadas"])
                hc.save()

        if seccion ==  'volumen_cuotas_mes':
            lis = fondo.objects.filter(inicio_operaciones__isnull=False)        
            lis = lis.exclude(categoria =0).order_by('runsvs')
            cont = lis.count()
            try:
                contador = len(eval('['+hc.volumen_cuotas_mes+']')[0])
                res = eval(hc.volumen_cuotas_mes)
            except (IndexError, TypeError, NameError):
                contador = 0

            i = 0
            j = 0

            if contador == 0:
                #res.append(['','','','','','','',''])
                header = ['','', 'NÚMERO DE CUOTAS TRANSADOS MENSUALES POR FONDO']
                res.append(header)
                header = ['','', forfecha(period)]
                res.append(header)
                header = ['','', 'Número de Cuotas', '','','Clase', ]
                for pers in lista_meses_totales(period):
                    header += [forfecha(pers)]
                res.append(header)

            for l in lis:

                print(str(i)+': '+str(l.runsvs))
                print("i:"+str(i)+"  contador: "+str(contador))
                #if True:
                if i != contador:
                    i += 1
                    pass
                else:
                    if l.categoria == None:
                        lcategoriaindice = ''
                    else:
                        lcategoriaindice = l.categoria.indice

                    fila = ['', l.runsvs.split('-')[0], l.nombre, l.admin.rut, l.admin.razon_social, lcategoriaindice]

                    for pers in lista_meses_totales(period):
                        aux = pers.split('-')
                        # por cada fondo buscar todos sus nemos
                        nemos = serie.objects.filter(fondo = l).values_list('nemotecnico', flat=True)
                        # de esos nemos sacar la suma  de sus cuotas transadas
                        # en virtud de su mes y años
                        tc = transaccion_cuota.objects.filter(nemotecnico__in = nemos , fecha__month=str(aux[0]), fecha__year = str(aux[1])).aggregate(Sum('cuotas_transadas'))
                        print(pers+'--> sum cuotas: '+str(tc['cuotas_transadas__sum']))
                        if tc['cuotas_transadas__sum'] == None:
                            tc['cuotas_transadas__sum'] = 0
                        fila += [tc['cuotas_transadas__sum']]

                    
                    res.append(fila)
            hc.volumen_cuotas_mes = json.dumps(res)
            hc.save()

        if seccion ==  'volumen_monto':
            lis = fondo.objects.filter(inicio_operaciones__isnull=False)        
            lis = lis.exclude(categoria =0).order_by('runsvs')

            cont = lis.count()
            
            try:                
                contador = len(eval('['+hc.volumen_monto+']')[0])
                res = eval(hc.volumen_monto)
            except (IndexError, TypeError):
                contador = 0

            i = 0
            j = 0

            if contador == 0:
                #res.append(['','','','','','','',''])
                header = ['','', 'NÚMERO DE VOLUMENES TRANSADOS MENSUALES POR FONDO']
                res.append(header)
                header = ['','', forfecha(period)]
                res.append(header)
                header = ['','', 'Número de Volumen', '','','Clase', ]
                for pers in lista_meses_totales(period):
                    header += [forfecha(pers)]
                res.append(header)

            for l in lis:

                print(str(i)+': '+str(l.runsvs))
                #if True:
                if i != contador:
                    i += 1
                    pass
                else:
                    if l.categoria == None:
                        lcategoriaindice = ''
                    else:
                        lcategoriaindice = l.categoria.indice

                    fila = ['', l.runsvs.split('-')[0], l.nombre, l.admin.rut, l.admin.razon_social, lcategoriaindice]

                    for pers in lista_meses_totales(period):
                        aux = pers.split('-')
                        # por cada fondo buscar todos sus nemos
                        nemos = serie.objects.filter(fondo = l).values_list('nemotecnico', flat=True)
                        # de esos nemos sacar la suma  de sus cuotas transadas
                        # en virtud de su mes y años
                        tc = transaccion_cuota.objects.filter(nemotecnico__in = nemos , fecha__month=str(aux[0]), fecha__year = str(aux[1])).aggregate(Sum('monto_transado'))
                        print(pers+'--> sum montos: '+str(tc['monto_transado__sum']))
                        
                        fila += [tc['monto_transado__sum']]

                    res.append(fila)

                    hc.volumen_monto = json.dumps(res)
                    hc.save()

        return HttpResponseRedirect('/perfil/fondo/catastro/'+str(libro)+'/'+str(aux[1])+'-'+str(aux[0]))
                
def descargar_catastro(request, seccion, period):
    try:
        hc = hoja_catastro.objects.get(pk=period)
        ruta = settings.STATIC_ROOT+settings.STATIC_URL+"catastro/"

        if seccion == 'portada1':
            sheetx = {
            "Portada" : hc.portada1
            }

            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)
            
            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            
            celda_estilo(wb2, "B6:D6", 'FFFFFFFF', None, 8, True, 'FFFF6600')
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'lista_vigentes':

            res = []
            aux = period.split('-')
            i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while i.dolar == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                i = indicador.objects.get(pk= nueva_fecha)

            j = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while j.uf == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                j = indicador.objects.get(pk= nueva_fecha)

            res.append(['','','','','','','Fecha', forfecha(period)])
            res.append(['','','','','','','Valor Dólar', i.dolar])
            res.append(['','','','','','','Valor UF', j.uf])
            res.append([''])
            res.append(['','Fondos vigentes a '+forfecha(period)+' (Millones de pesos)'])
            res.append(['', 'Nombre Fondo', 'Rut Fondo', 'Rut administradora', 'Nombre administradora', 'Rescat/No Rescat', 'Clasificación Acafi', 'Nemotécnicos', 'Inicio operaciones', 'Término Operaciones', 'Link Sitio CMF', 'TOTAL ACTIVO', 'TOTAL ACTIVO CORRIENTE', 'TOTAL ACTIVO NO CORRIENTE',  'TOTAL PASIVO', 'TOTAL PASIVO CORRIENTE', 'TOTAL PASIVO NO CORRIENTE', 'TOTAL PATRIMONIO NETO'])

            titulo = 'Fon. vigentes '+forfecha(period)+' (mm CLP)'

            aux = hoja_catastro.objects.get(pk=period)
            hc = json.loads(aux.lista_fondos)
            for linea in hc:
                fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('run_adm'), linea.get('nom_adm'), linea.get('rescate'), linea.get('clase'), linea.get('nemo'), linea.get('inicio'), linea.get('termino'), linea.get('link'), linea.get('activo'), linea.get('act_corriente'), linea.get('act_no_corriente'), linea.get('pasivo'),linea.get('pas_corriente'), linea.get('pas_no_corriente'), linea.get('patrimonio')]
                res.append(fila)
            filas = len(res)
            filaTotal = ['','','','','','','','','','','TOTAL', '=SUM(L7:L'+str(filas)+')', '=SUM(M7:M'+str(filas)+')', '=SUM(N7:N'+str(filas)+')', '=SUM(O7:O'+str(filas)+')', '=SUM(P7:P'+str(filas)+')', '=SUM(Q7:Q'+str(filas)+')', '=SUM(R7:R'+str(filas)+')']
            res.append(filaTotal)
            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], str(filas+1)+':'+str(filas+1), 'FF000000', 'Arial', 9, True, '2eb3f4')
                
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

            titulo2 = 'Fon. vigentes '+forfecha(period)+' (m USD)'
            titulo3 = 'Fon. vigentes '+forfecha(period)+' (m UF)'
            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+1):
                celdaDolar = sheet.cell(row=2, column=8)
                for j in range(0, len(res[filas])+1):

                    if i == 5 and j == 2:
                        d = sheet.cell(row=i, column=j)
                        d.value = 'Fondos vigentes a '+forfecha(period)+' (Miles de Dolares)'

                    if j > 11 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar Valores a celdas a hoja con valor UF
            cont = 0
            for i in range(0, filas+1):
                celdaUF = sheet.cell(row=3, column=8)
                for j in range(0, len( res[filas] )+1):

                    if i == 5 and j == 2:
                        d = sheet.cell(row=i, column=j)
                        d.value = 'Fondos vigentes a '+forfecha(period)+' (Miles de UF)'

                    if j > 11 and i > 6:
                        c = (sheet2.cell(row=i, column=j))
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")


        if seccion == 'lista_nuevos':

            res = []
            aux = period.split('-')
            i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while i.dolar == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                i = indicador.objects.get(pk= nueva_fecha)

            j = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while j.uf == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                j = indicador.objects.get(pk= nueva_fecha)

            ant = str(int(aux[0])-2)+'-'+aux[1]
            res.append(['','','','','','','Fecha', forfecha(period)])
            res.append(['','','','','','','Valor Dólar', i.dolar])
            res.append(['','','','','','','Valor UF', j.uf])
            res.append([''])
            res.append(['','Nuevos Fondos entre '+forfecha(ant)+' y '+forfecha(period)+' (Millones de pesos)'])
            res.append(['', 'Nombre Fondo', 'Rut Fondo', 'Rut administradora', 'Nombre administradora', 'Rescat/No Rescat', 'Clasificación Acafi', 'Nemotécnicos', 'Inicio operaciones', 'Término Operaciones', 'Link Sitio CMF', 'TOTAL ACTIVO', 'TOTAL ACTIVO CORRIENTE', 'TOTAL ACTIVO NO CORRIENTE',  'TOTAL PASIVO', 'TOTAL PASIVO CORRIENTE', 'TOTAL PASIVO NO CORRIENTE', 'TOTAL PATRIMONIO NETO'])

            ant = forfecha(ant)

            titulo = 'Nuevos '+ant.strip(aux[1])+'- '+forfecha(period)+' (mm CLP)'
            titulo2 = 'Nuevos '+ant.strip(aux[1])+'- '+forfecha(period)+' (m USD)'
            titulo3 = 'Nuevos '+ant.strip(aux[1])+'- '+forfecha(period)+' (m UF)'

            aux = hoja_catastro.objects.get(pk=period)
            hc = json.loads(aux.lista_fondos_nuevos)
            for linea in hc:
                fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('run_adm'), linea.get('nom_adm'), linea.get('rescate'), linea.get('clase'), linea.get('nemo'), linea.get('inicio'), linea.get('termino'), linea.get('link'), linea.get('activo'), linea.get('act_corriente'), linea.get('act_no_corriente'), linea.get('pasivo'),linea.get('pas_corriente'), linea.get('pas_no_corriente'), linea.get('patrimonio')]
                res.append(fila)
            filas = len(res)
            filaTotal = ['','','','','','','','','','','TOTAL', '=SUM(L7:L'+str(filas)+')', '=SUM(M7:M'+str(filas)+')', '=SUM(N7:N'+str(filas)+')', '=SUM(O7:O'+str(filas)+')', '=SUM(P7:P'+str(filas)+')', '=SUM(Q7:Q'+str(filas)+')', '=SUM(R7:R'+str(filas)+')']
            res.append(filaTotal)
            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], str(filas+1)+':'+str(filas+1), 'FF000000', 'Arial', 9, True, '2eb3f4')
                
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+1):
                celdaDolar = sheet.cell(row=2, column=8)
                for j in range(0, len(res[filas])+1):

                    if i == 5 and j == 2:
                        d = sheet.cell(row=i, column=j)
                        d.value = 'Nuevos Fondos entre '+ant+' y '+forfecha(period)+' (Miles de Dolares)'

                    if j > 11 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar Valores a celdas a hoja con valor UF
            cont = 0
            for i in range(0, filas+1):
                celdaUF = sheet.cell(row=3, column=8)
                for j in range(0, len( res[filas] )+1):

                    if i == 5 and j == 2:
                        d = sheet.cell(row=i, column=j)
                        d.value = 'Nuevos Fondos entre '+ant+' y '+forfecha(period)+' (Miles de UF)'

                    if j > 11 and i > 6:
                        c = (sheet2.cell(row=i, column=j))
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")


        if seccion == 'activos_completo':

            try:
                wb = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except:

                aux = period.split('-')
                ant = str(int(aux[0])-2)+'-'+aux[1]
                ant = forfecha(ant)

                titulos = ['1-Fondos Nuevos '+ant.strip(aux[1])+'- '+forfecha(period),
                    '2-Total Activos '+forfecha(period),
                    '3-Activos por Tipo FI '+forfecha(period),
                    '4.1-Activos por Fondo (mm CLP)',
                    '4.2-Activos por Fondo (m USD)',
                    '4.3-Activos por Fondo (m UF)',
                    '5-Activo por AFI'+forfecha(period),
                    '6.1-Activos por Clase (mm CLP)',
                    '6.2-Activos por Clase (m USD)',
                    '6.3-Activos por Clase (m UF)',
                    '7.1-Act. por Fondo-AFI (mm CLP)',
                    '7.2-Act. por Fondo-AFI (m USD)',
                    '7.3-Act. por Fondo-AFI (m UF)',
                    '8-Cant. Fondos '+forfecha(period),
                    '9-Inv. nacionales (mm CLP)']

                wb = Workbook()
                s = wb['Sheet']
                wb.remove(s)
                for t in titulos:

                    if t[:1] == '1':
                        print('fondos nuevos')
                        wbcop = load_workbook(ruta+'fondos_nuevos '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value
                        
                        filas += 4
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
                        celda_estilo(wb[t], str(filas+1)+':'+str(filas+1), 'FF000000', 'Arial', 9, True, '2eb3f4')

                        sheet= wb[t]
                        max_col = sheet.max_column
                        for i in range(0, filas+1):
                            for j in range(2, max_col+1):
            
                                if i > 6 and j > 7:
                                    c = sheet.cell(row=i, column=j-1)
                                    d = sheet.cell(row=i, column=j)
                                    d.number_format = '#,##0.00'
                                    c.number_format = '#,##0.00'
                                
                                if i == filas and j > 7:
                                    d = sheet.cell(row=i+1, column=j)
                                    d.number_format = '#,##0.00'

                    if t[:1] == '2':
                        print('total activos')
                        wbcop = load_workbook(ruta+'total_activos '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        filas += 2
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        sheet= wb[t]
                        max_col = sheet.max_column

                        for i in range(1, filas+1):
                            for j in range(1, (max_col+1)):
                                if j==1 and i>6:
                                    c = sheet.cell(row=i, column=j)
                                    c.fill = PatternFill("solid", fgColor='2eb3f4')
                                    c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                                    c.alignment = Alignment(horizontal='center')

                                if i > 6 and j > 1 and j%2 != 0:
                                    b = sheet.cell(row=i-1, column=j)
                                    c = sheet.cell(row=i, column=j)
                                    c = sheet.cell(row=i, column=j-1)
                                    d = sheet.cell(row=i, column=j)
                                    d.number_format = '0.00%'
                                    c.number_format = '#,##0.00'

                                if i == 6 and j == max_col:
                                    c = sheet.cell(row=i+1, column=j)
                                    c.alignment = Alignment(horizontal='center')

                                if i > 7 and j == max_col:
                                    d = sheet.cell(row=i, column=j+1)
                                    d.number_format = '0.00%'
                                    c.number_format = '#,##0.00'
                                    b.number_format = '#,##0.00'

                        celda_estilo(wb[t], "1:1", 'FF000000', 'Arial', 9, True, '00CCFFFF')
                        celda_estilo(wb[t], "3:4", 'FF000000', 'Arial', 9, True, '00CCFFFF')
                        celda_estilo(wb[t], "6:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')

                    if t[:1] == '3':
                        print('activos por tipo')
                        wbcop = load_workbook(ruta+'activos_por_tipo '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        filas += 1
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
                        celda_estilo(wb[t], str(filas)+':'+str(filas), 'FF000000', 'Arial', 9, True, '2eb3f4')

                        sheet= wb[t]
                        max_col = sheet.max_column
                        for i in range(7, filas+1):
                            for j in range(3, max_col+1):
                                d = sheet.cell(row=i, column=j)
                                d.number_format = '#,##0.00'

                    if t[:1] == '4':
                        print('activos fondo')
                        wbcop = load_workbook(ruta+'activos_fondo '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        filas += 6
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")

                        for i in range(7, filas+1):
                            try:
                                if wb[t]['B'+str(i)].value.lower() == 'clase':
                                    celda_estilo(wb[t], str(i)+":"+str(i), 'FFFFFFFF', 'Arial', 9, False, "00538ED5")
                                if wb[t]['B'+str(i)].value.lower() == 'subclase':
                                    celda_estilo(wb[t], str(i)+":"+str(i), 'FF000000', 'Arial', 9, False, "00A5A5A5")
                                if wb[t]['B'+str(i)].value.lower() == 'subsubclase':
                                    celda_estilo(wb[t], str(i)+":"+str(i), 'FF000000', 'Arial', 9, False, "00D8D8D8")
                            except AttributeError:
                                pass
                    
                        sheet= wb[t]
                        max_col = sheet.max_column
                        for i in range(0, filas+1):
                            for j in range(0, max_col+1):
                                if j > 5 and i > 6:
                                    aux = sheet.cell(row=i, column=2)
                                    if aux.value == 'Fondo':
                                        d =sheet.cell(row=i, column=j)
                                        d.number_format = '#,##0.00'
                                    else:
                                        pass

                    if t[:1] == '5':
                        print('activos afi')
                        wbcop = load_workbook(ruta+'activo_por_afi '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, False, '00CCFFFF')
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
                        celda_estilo(wb[t], str(filas+6)+':'+str(filas+6), 'FF000000', 'Arial', 9, True, '2eb3f4')
                        
                        sheet= wb[t]
                        max_col = sheet.max_column

                        c = sheet.cell(row=(filas+6), column=3)
                        c.alignment = Alignment(horizontal='right')

                        for i in range(7, filas+7):
                            for j in range(4, max_col):
                                d = sheet.cell(row=i, column=j)
                                d.number_format = '#,##0.00'

                        for i in range (0,filas+6):
                            if i > 6:
                                c = sheet.cell(row=i, column=(max_col))
                                c.fill = PatternFill("solid", fgColor='00CCFFFF')
                                c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                                c.alignment = Alignment(horizontal='center')
                                c.number_format = '0.00%'

                    if t[:1] == '6':
                        print('activos por clase')
                        wbcop = load_workbook(ruta+'activos_por_clase '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value
                        
                        filas += 4
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
                        celda_estilo(wb[t], str(filas)+':'+str(filas), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")

                        sheet= wb[t]
                        max_col = sheet.max_column
                        for i in range(0, filas+1):
                            for j in range(0, max_col+1):

                                if j > 3 and i > 6:
                                    d =sheet.cell(row=i, column=j)
                                    d.number_format = '#,##0.00'

                    if t[:1] == '7':
                        print('activos fondos afi')
                        wbcop = load_workbook(ruta+'activos_fondos_afi '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        filas += 5
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
                        celda_estilo(wb[t], str(filas)+':'+str(filas), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")

                        sheet= wb[t]
                        max_col = sheet.max_column
                        for i in range(0, filas+1):
                            for j in range(0, max_col+1):
                                if j > 7 and i > 6:
                                    d =sheet.cell(row=i, column=j)
                                    d.number_format = '#,##0.00'

                    if t[:1] == '8':
                        print('numero de fondos')
                        wbcop = load_workbook(ruta+'nro_fondos '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        filas += 2
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        sheet= wb[t]
                        max_col = sheet.max_column

                        for i in range(1, filas+1):
                            for j in range(1, (max_col+1)):
                                if j==1 and i>6:
                                    c = sheet.cell(row=i, column=j)
                                    c.fill = PatternFill("solid", fgColor='2eb3f4')
                                    c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                                    c.alignment = Alignment(horizontal='center')

                                if i > 6 and j > 1 and j%2 != 0:
                                    d = sheet.cell(row=i, column=j)
                                    d.number_format = '0.00%'
            
                                if i == 6 and j == max_col:
                                    c = sheet.cell(row=i+1, column=j)
                                    c.alignment = Alignment(horizontal='center')

                                if i > 7 and j == max_col:
                                    d = sheet.cell(row=i, column=j+1)
                                    d.number_format = '0.00%'

                        celda_estilo(wb[t], "1:1", 'FF000000', 'Arial', 9, True, '00CCFFFF')
                        celda_estilo(wb[t], "3:4", 'FF000000', 'Arial', 9, True, '00CCFFFF')
                        celda_estilo(wb[t], "6:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')

                    if t[:1] == '9':
                        wbcop = load_workbook(ruta+'inversiones '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        sheet= wb[t]
                        max_col = sheet.max_column

                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
                        celda_estilo(wb[t], str(filas+4)+':'+str(filas+4), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")

                        for i in range(0, filas+5):
                            for j in range(0, max_col+1):

                                if j > 3 and i > 6 and j%2 == 0:
                                    c = sheet.cell(row=i, column=j)
                                    d = sheet.cell(row=i, column=j-1)
                                    c.number_format = '0.00%'
                                    c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                                    d.number_format = '#,##0.00'

                wb.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'fondos_nuevos':
            
            res = []
            aux = period.split('-')
            i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while i.dolar == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                i = indicador.objects.get(pk= nueva_fecha)

            j = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while j.uf == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                j = indicador.objects.get(pk= nueva_fecha)

            ant = str(int(aux[0])-2)+'-'+aux[1]
            res.append(['','','','','','','Fecha','', forfecha(period)])
            res.append(['','','','','','','Valor Dólar','', i.dolar])
            res.append(['','','','','','','Valor UF','', j.uf])
            res.append(['','','','','','','','',''])
            res.append(['','Nuevos Fondos que presentaron Estados Financieros entre '+forfecha(ant)+' y '+forfecha(period),'','','','','','',''])
            res.append(['','Fondo', 'Administradora', 'Rut Fondo', 'Clase', 'Moneda', 'Rescate', 'Mill. de $', 'Miles de US$'])

            ant = forfecha(ant)

            titulo = '1-Fondos Nuevos '+ant.strip(aux[1])+'- '+forfecha(period)

            aux = hoja_catastro.objects.get(pk=period)
            hc = json.loads(aux.fondos_nuevos)
            for linea in hc:
                fila = ['',linea.get('nombre_fondo'), linea.get('administradora'), linea.get('run_fondo'), linea.get('clase'), linea.get('moneda'), linea.get('tipo'), linea.get('activos'), '']
                res.append(fila)
            filas = len(res)
            filaTotal = ['','','','','','','TOTAL', '=SUM(H7:H'+str(filas)+')', '=SUM(I7:I'+str(filas)+')']
            res.append(filaTotal)
            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], str(filas+1)+':'+str(filas+1), 'FF000000', 'Arial', 9, True, '2eb3f4')
            
            sheet= wb2[titulo]
            for i in range(0, filas+1):
                celdaDolar = sheet.cell(row=2, column=9)
                for j in range(2, len(res[0])+1):

                    if i > 6 and j > 8:
                        c = sheet.cell(row=i, column=j-1)
                        d = sheet.cell(row=i, column=j)
                        d.value = "="+c.coordinate+'/('+celdaDolar.coordinate+'*1000)'
                        d.number_format = '#,##0.00'
                        c.number_format = '#,##0.00'
                    
                    if i == filas and j > 7:
                        d = sheet.cell(row=i+1, column=j)
                        d.number_format = '#,##0.00'

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'total_activos':

            res = []
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.total_activos))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.total_activos))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.total_activos))
                periodos.append(period)

            res.append(['','TOTAL ACTIVOS',''])
            res.append(['','',''])
            res.append(['','Total Activos Administrados',''])
            res.append(['','(Millones de pesos de cada periodo)',''])
            res.append(['','',''])

            i=0
            fila=[]
            fila2=[]
            for hoja in hc:
                j=0
                fila=[]
                fila2=[]
                for linea in hoja:
                    if i == 0:
                        if j == 0:
                            fila.extend(['Trimestre',linea.get('categoria')])
                            fila2.extend([forfecha(periodos[i]),float(linea.get('monto'))])
                            j+=1
                        else:
                            fila.extend(['%',linea.get('categoria')])
                            fila2.extend(['',float(linea.get('monto'))])
                    else:
                        if j == 0:
                            fila.extend([forfecha(periodos[i]),float(linea.get('monto'))])
                            j+=1
                        else:
                            fila.extend(['',float(linea.get('monto'))])
                        
                i += 1
                if len(fila2) == 0:
                    res.append(fila)
                else:
                    res.append(fila)
                    res.append(fila2)


            titulo = '2-Total Activos '+forfecha(period)

            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)
            
            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            sheet= wb2[titulo]
            filas = len(res)
            max_col = len(res[filas-1])
            
            for i in range(1, filas+1):
                for j in range(1, (max_col+1)):
                    if j==1 and i>6:
                        c = sheet.cell(row=i, column=j)
                        c.fill = PatternFill("solid", fgColor='2eb3f4')
                        c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                        c.alignment = Alignment(horizontal='center')

                    if i > 6:
                        celdaTotal = sheet.cell(row=i, column=max_col)
                       
                    if i > 6 and j > 1 and j%2 != 0:
                        c = sheet.cell(row=i, column=j-1)
                        d = sheet.cell(row=i, column=j)
                        d.value = "="+c.coordinate+'/'+celdaTotal.coordinate
                        d.number_format = '0.00%'
                        c.number_format = '#,##0.00'

                    if i == 6 and j == max_col:
                        c = sheet.cell(row=i+1, column=j+1)
                        c.value = '--'
                        c.alignment = Alignment(horizontal='center')
                        d = sheet.cell(row=i, column=j+1)
                        d.value = 'Crecimiento Trimestral'
                    
                    if i > 7 and j == max_col:
                        b = sheet.cell(row=i-1, column=j)
                        c = sheet.cell(row=i, column=j)
                        d = sheet.cell(row=i, column=j+1)
                        d.value = '=(('+c.coordinate+'-'+b.coordinate+')/'+b.coordinate+')'
                        d.number_format = '0.00%'
                        c.number_format = '#,##0.00'
                        b.number_format = '#,##0.00'

            celda_estilo(wb2[titulo], "1:1", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "3:4", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "6:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'activos_por_tipo':
            
            res = []
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.total_activos))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.total_activos))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.total_activos))
                periodos.append(period)

            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            res = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','Fecha:',forfecha(p)])
                    fila2.extend(['','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','Valor UF',iUF[con]])
                    fila4.extend(['','',''])
                    fila5.extend(['','Activos por tipo de Fondo de Inversión',''])
                    fila6.extend(['','(Millones de pesos de cada periodo)','Activos'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([''])
                    fila6.extend(['Activos'])
                con +=1
            
            res.append(fila1)
            res.append(fila2)
            res.append(fila3)
            res.append(fila4)
            res.append(fila5)
            res.append(fila6)

            categs=[]
            for linea in hc[0]:
                categs.append(linea.get('categoria'))

            for c in categs:
                fila = []
                j = 0
                for hoja in hc:
                    for linea in hoja:
                        if linea.get('categoria') == c:
                            if j == 0:
                                fila.extend(['',linea.get('categoria'), float(linea.get('monto'))])
                                j+=1
                            else:
                                fila.extend([float(linea.get('monto'))])
                        else:
                            pass
                res.append(fila)



            titulo = '3-Activos por Tipo FI '+forfecha(period)

            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)
            
            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")

            filas = len(res)
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], str(filas)+':'+str(filas), 'FF000000', 'Arial', 9, True, '2eb3f4')

            sheet = wb2[titulo]
            for i in range(7, filas+1):
                for j in range(3, len(res[0])+1):
                    d = sheet.cell(row=i, column=j)
                    d.number_format = '#,##0.00'

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'activos_fondo':

            total_fondos = []
            aux = fondo.objects.all().order_by('categoria')
            aux = aux.values('runsvs')
            for a in aux:
                a=a.get('runsvs')
                total_fondos.append(a.split('-')[0])
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.activos_fondo))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.activos_fondo))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.activos_fondo))
                periodos.append(period)

            
            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','','','Fecha:',forfecha(p)])
                    fila2.extend(['','','','','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','','','','Valor UF',iUF[con]])
                    fila4.extend(['','','','','',''])
                    fila5.extend(['','','Activos Administrados por Fondo de Inversión','','',forfecha(p)])
                    fila6.extend(['','','(Millones de pesos de cada periodo)','RUT Fondo','Clase','Activos'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([forfecha(p)])
                    fila6.extend(['Activos'])
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)

                       
            i=0
            fondos = []
            for fon in total_fondos:
                if has_key(fon , period, 'activos_fondo') == True:
                    fila = []
                    j=0
                    cont = 0
                    flag = 0
                    num_hoja = 0
                    
                    for hoja in hc:
                        largo = len(hoja)
                        num_hoja +=1

                        for linea in hoja:
                            cont +=1
                            if linea.get('tipo') == 'Fondo':
                                if fon == linea.get('run'):
                                    flag = 1
                                    if j == 0:
                                        fila = []
                                        fila.extend(['',linea.get('tipo'),linea.get('nombre'),linea.get('run'),linea.get('indice'),float(linea.get('activos'))]) 
                                        j+=1
                                    else:
                                        if fila[1] == '':
                                            fila[1]=linea.get('tipo')
                                            fila[2]=linea.get('nombre')
                                            fila[4]=linea.get('indice')
                                        fila.extend([float(linea.get('activos'))])
                                else:
                                    if cont == largo and flag == 0:
                                        flag = 1
                                        if j == 0:
                                            fila = []
                                            fila.extend(['','','',fon,'','']) 
                                            j+=1
                                        else:
                                            fila.extend([''])
                                    else:
                                        continue
                            else:
                                pass
  
                    fondos.append(fila)

            clases = []
            for linea in hc[0]:
                fila = []               
                if linea.get('tipo') != 'Fondo':
                    for i in range (0, len(hc)):
                        if i == 0:
                            fila.extend(['',linea.get('tipo'),linea.get('nombre'),linea.get('run'),linea.get('indice'),linea.get('activos')])
                        else:
                            fila.extend([linea.get('activos')])
                    clases.append(fila)
            
            for f in fondos:
                clases.append(f)

            clases= sorted(clases, key=itemgetter(4))
            
            for c in clases:
                cabecera.append(c)

            titulo = '4.1-Activos por Fondo (mm CLP)'
        
            sheetx = {
            titulo : cabecera
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")
            
            #F7 en adelante
            i=7
            row = wb2[titulo].max_row
            for i in range(7, row+1):
                try:
                    if wb2[titulo]['B'+str(i)].value.lower() == 'clase':
                        celda_estilo(wb2[titulo], str(i)+":"+str(i), 'FFFFFFFF', 'Arial', 9, False, "00538ED5")
                    if wb2[titulo]['B'+str(i)].value.lower() == 'subclase':
                        celda_estilo(wb2[titulo], str(i)+":"+str(i), 'FF000000', 'Arial', 9, False, "00A5A5A5")
                    if wb2[titulo]['B'+str(i)].value.lower() == 'subsubclase':
                        celda_estilo(wb2[titulo], str(i)+":"+str(i), 'FF000000', 'Arial', 9, False, "00D8D8D8")
                except AttributeError:
                    pass

            titulo2 = '4.2-Activos por Fondo (m USD)'
            titulo3 = '4.3-Activos por Fondo (m UF)'
            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2[titulo]
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, len(cabecera)+1):
                aux1=6
                for j in range(0, len(cabecera[0])+1):

                    if j > 5 and j == aux1:
                        celdaDolar = sheet.cell(row=2, column=aux1)
                        aux1+=1

                    if j > 5 and i > 6:
                        aux = sheet.cell(row=i, column=2)
                        if aux.value == 'Fondo':
                            c = sheet2.cell(row=i, column=j)
                            d =sheet.cell(row=i, column=j)
                            d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"
                            d.number_format = '#,##0.00'
                        else:
                            pass

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2[titulo]
            
            #Dar Valores a celdas a hoja con valor UF
            cont = 0
            for i in range(0, len(cabecera)+1):
                aux1=6
                for j in range(0, len( cabecera[0] )+1):

                    if j > 5 and j == aux1:
                        celdaUF = sheet.cell(row=3, column=aux1)
                        aux1+=1

                    if j > 5 and i > 6:
                        aux = sheet.cell(row=i, column=2)
                        if aux.value == 'Fondo':
                            c = (sheet2.cell(row=i, column=j))
                            d =sheet.cell(row=i, column=j)
                            d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"
                            d.number_format = '#,##0.00'
                            c.number_format = '#,##0.00'
                        else:
                            pass
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'activo_por_afi':

            res = []
            aux = period.split('-')
            i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while i.dolar == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                i = indicador.objects.get(pk= nueva_fecha)

            j = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while j.uf == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                j = indicador.objects.get(pk= nueva_fecha)

            res.append(['','','','','','','','Fecha',forfecha(period)])
            res.append(['','','','','','','','Valor Dólar',i.dolar])
            res.append(['','','','','','','','Valor UF',j.uf])
            res.append(['','','','','','','','',''])
            res.append(['','','(Millones de Pesos)','','','','','',''])
            
            titulo = '5-Activo por AFI'+forfecha(period)

            aux = hoja_catastro.objects.get(pk=period)
            hc = json.loads(aux.activo_por_afi)

            categs = []
            fila = []
            categs = hc[0].keys()
            categs = list(categs)
            categs.remove('total')
            categs.remove('run_adm')
            categs.remove('nombre')
            fila.extend(['','RUT','Administradora'])

            for i in range(0, len(categs)):
                fila.extend([categs[i]])
            fila.extend(['TOTAL','% Participacion'])
            res.append(fila)

            for linea in hc:
                fila = []
                fila.extend(['',linea.get('run_adm'), linea.get('nombre')])
                for i in range(0, len(categs)):
                    fila.extend([linea.get(categs[i])])
                fila.extend([linea.get('total')])
                res.append(fila)

            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            filas = len(res)
            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, False, '00CCFFFF')
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], str(filas+1)+':'+str(filas+1), 'FF000000', 'Arial', 9, True, '2eb3f4')
            
            sheet= wb2[titulo]
            max_col = len(res[filas-1])

            for j in range (0, max_col+1):
                if j == 3:
                    c = sheet.cell(row=(filas+1), column=j)
                    c.value='TOTAL'
                    c.alignment = Alignment(horizontal='right')
                
                if j > 3:
                    c = sheet.cell(row=(filas+1), column=j)
                    col = get_column_letter(j)
                    c.value ='=SUM('+col+'7:'+col+str(filas)+')'
            
            for i in range(7, filas+2):
                for j in range(4, max_col+1):
                    d = sheet.cell(row=i, column=j)
                    d.number_format = '#,##0.00'
            
            for i in range (0,filas+1):
                celdaTotal = sheet.cell(row=filas+1, column=(max_col))
                if i > 6:
                    d = sheet.cell(row=i, column=max_col)
                    c = sheet.cell(row=i, column=(max_col+1))
                    c.value = '='+d.coordinate+'/'+celdaTotal.coordinate
                    c.fill = PatternFill("solid", fgColor='00CCFFFF')
                    c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                    c.alignment = Alignment(horizontal='center')
                    c.number_format = '0.00%'
              
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'activos_por_clase':
            
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.activos_por_clase))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.activos_por_clase))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.activos_por_clase))
                periodos.append(period)

            
            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','Fecha:',forfecha(p)])
                    fila2.extend(['','','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','','Valor UF',iUF[con]])
                    fila4.extend(['','','',''])
                    fila5.extend(['','Activos Administrados por Tipo de Fondo','',''])
                    fila6.extend(['','(Millones de pesos de cada periodo)','Clase','Activos'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([''])
                    fila6.extend(['Activos'])
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)

            clases = []
            categs = categoria.objects.exclude(pk=0).order_by('indice')
            for c in categs:
                fila = []
                j = 0
                for hoja in hc:
                    for linea in hoja:
                        if linea.get('indice') == c.indice and j == 0:
                            fila.extend(['',linea.get('nombre_cat'),linea.get('indice'),float(linea.get('monto'))]) 
                            j+=1
                        elif linea.get('indice') == c.indice and j != 0:
                            fila.extend([float(linea.get('monto'))])
                        else:
                            pass
                clases.append(fila)
            fila = []
            j = 0
            for hoja in hc:
                for linea in hoja:
                    if linea.get('indice') == '' and j == 0:
                        fila.extend(['',linea.get('nombre_cat'),linea.get('indice'),float(linea.get('monto'))]) 
                        j+=1
                    elif linea.get('indice') == '' and j != 0:
                        fila.extend([float(linea.get('monto'))])
            
            clases= sorted(clases, key=itemgetter(2))
            clases.append(fila)
            for c in clases:
                cabecera.append(c)

            titulo = '6.1-Activos por Clase (mm CLP)'
        
            sheetx = {
            titulo : cabecera
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            filas = len(cabecera)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            celda_estilo(wb2[titulo], str(filas)+':'+str(filas), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")
            
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

            titulo2 = '6.2-Activos por Clase (m USD)'
            titulo3 = '6.3-Activos por Clase (m UF)'
            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2[titulo]
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+1):
                aux1=4
                for j in range(0, len(cabecera[0])+1):

                    if j > 3 and j == aux1:
                        celdaDolar = sheet.cell(row=2, column=aux1)
                        aux1+=1

                    if j > 3 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"
                        d.number_format = '#,##0.00'
                        c.number_format = '#,##0.00'
                    
                    if i == 6 and j == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(Miles de dolares de cada periodo)'
                    

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2[titulo]
            
            #Dar Valores a celdas a hoja con valor UF
            for i in range(0, filas+1):
                aux1=4
                for j in range(0, len(cabecera[0])+1):

                    if j > 3 and j == aux1:
                        celdaUF = sheet.cell(row=3, column=aux1)
                        aux1+=1

                    if j > 3 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"
                        d.number_format = '#,##0.00'
                        
                    
                    if i == 6 and j == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(Miles de UF de cada periodo)'

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'activos_fondos_afi':

            total_fondos = []
            aux = fondo.objects.all().order_by('categoria')
            aux = aux.values('runsvs')
            for a in aux:
                a=a.get('runsvs')
                total_fondos.append(a.split('-')[0])

            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.activos_fondos_afi))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.activos_fondos_afi))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.activos_fondos_afi))
                periodos.append(period)

            
            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','','','','Fecha:','',forfecha(p)])
                    fila2.extend(['','','','','','Valor Dolar:','',iDolar[con]])
                    fila3.extend(['','','','','','Valor UF','',iUF[con]])
                    fila4.extend(['','','','','','','',''])
                    fila5.extend(['','Activos Administrados por Tipo de Fondo (Millones de pesos de cada periodo)','','','','','',''])
                    fila6.extend(['','Fondo','RUT Admin','Administradora','Categoria','Clase','RUT Fondo','Activos'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([''])
                    fila6.extend(['Activos'])
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)


            i=0
            fondos = []
            for fon in total_fondos:
                if has_key(fon , period, 'activos_fondos_afi') == True:
                    fila = []
                    j=0
                    cont = 0
                    flag = 0
                    num_hoja = 0
                    
                    for hoja in hc:
                        largo = len(hoja)
                        num_hoja +=1

                        for linea in hoja:
                            cont +=1
                            if fon == linea.get('run'):
                                flag = 1
                                if j == 0:
                                    fila = []
                                    fila.extend(['',linea.get('nombre_fon'),linea.get('run_adm'),linea.get('nombre_adm'),linea.get('nom_categ'),linea.get('indice'),linea.get('run'),float(linea.get('activos'))]) 
                                    j+=1
                                else:
                                    if fila[1] == '':
                                        fila[1]=linea.get('nombre_fon')
                                        fila[2]=linea.get('run_adm')
                                        fila[3]=linea.get('nombre_adm')
                                        fila[4]=linea.get('nom_categ')
                                        fila[5]=linea.get('indice')
                                    fila.extend([float(linea.get('activos'))])
                            else:
                                if cont == largo and flag == 0:
                                    flag = 1
                                    if j == 0:
                                        fila = []
                                        fila.extend(['','','','','','',fon,'']) 
                                        j+=1
                                    else:
                                        fila.extend([''])
                                else:
                                    continue
                    fondos.append(fila)

            
            fondos= sorted(fondos, key=itemgetter(5))
            for f in fondos:
                cabecera.append(f)
            
            filas = len(cabecera)
            fila = []
            for i in range(0, len(hc)):
                col = get_column_letter((8+i))
                if i == 0:
                    fila.extend(['','','','','','','TOTAL','=SUM('+col+'7:'+col+str(filas)+')'])
                else:
                    fila.extend(['=SUM('+col+'7:'+col+str(filas)+')'])
            cabecera.append(fila)

            titulo = '7.1-Act. por Fondo-AFI (mm CLP)'
        
            sheetx = {
            titulo : cabecera
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            celda_estilo(wb2[titulo], str(filas+1)+':'+str(filas+1), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")
            
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

            titulo2 = '7.2-Act. por Fondo-AFI (m USD)'
            titulo3 = '7.3-Act. por Fondo-AFI (m UF)'
            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2[titulo]
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+2):
                aux1=8
                for j in range(0, len(cabecera[0])+1):

                    if j == aux1:
                        celdaDolar = sheet.cell(row=2, column=aux1)
                        aux1+=1

                    if j > 7 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"
                        d.number_format = '#,##0.00'
                    
                    if i == 6 and j == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(Miles de dolares de cada periodo)'
                    

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2[titulo]
            
            #Dar Valores a celdas a hoja con valor UF
            for i in range(0, filas+2):
                aux1=8
                for j in range(0, len(cabecera[0])+1):

                    if j == aux1:
                        celdaUF = sheet.cell(row=3, column=aux1)
                        aux1+=1

                    if j > 7 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"
                        d.number_format = '#,##0.00'
                        c.number_format = '#,##0.00'
                    
                    if i == 6 and j == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(Miles de UF de cada periodo)'

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'nro_fondos':
            
            res = []
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.total_activos))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.total_activos))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.total_activos))
                periodos.append(period)

            res.append(['','CANTIDAD DE FONDOS',''])
            res.append(['','',''])
            res.append(['','Total Activos Administrados',''])
            res.append(['','(Millones de pesos de cada periodo)',''])
            res.append(['','',''])

            i=0
            fila=[]
            fila2=[]
            for hoja in hc:
                j=0
                fila=[]
                fila2=[]
                for linea in hoja:
                    if i == 0:
                        if j == 0:
                            fila.extend(['Trimestre',linea.get('categoria')])
                            fila2.extend([forfecha(periodos[i]),linea.get('cant_fondos')])
                            j+=1
                        else:
                            fila.extend(['%',linea.get('categoria')])
                            fila2.extend(['',linea.get('cant_fondos')])
                    else:
                        if j == 0:
                            fila.extend([forfecha(periodos[i]),linea.get('cant_fondos')])
                            j+=1
                        else:
                            fila.extend(['',linea.get('cant_fondos')])
                        
                i += 1
                if len(fila2) == 0:
                    res.append(fila)
                else:
                    res.append(fila)
                    res.append(fila2)


            titulo = '8-Cant. Fondos '+forfecha(period)

            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)
            
            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            sheet= wb2[titulo]
            filas = len(res)
            max_col = len(res[filas-1])

            for i in range(1, filas+1):
                for j in range(1, (max_col+1)):
                    if j==1 and i>6:
                        c = sheet.cell(row=i, column=j)
                        c.fill = PatternFill("solid", fgColor='2eb3f4')
                        c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                        c.alignment = Alignment(horizontal='center')

                    if i > 6:
                        celdaTotal = sheet.cell(row=i, column=max_col)
                       
                    if i > 6 and j > 1 and j%2 != 0:
                        c = sheet.cell(row=i, column=j-1)
                        d = sheet.cell(row=i, column=j)
                        d.value = "="+c.coordinate+'/'+celdaTotal.coordinate
                        d.number_format = '0.00%'

                    if i == 6 and j == max_col:
                        c = sheet.cell(row=i+1, column=j+1)
                        c.value = '--'
                        c.alignment = Alignment(horizontal='center')
                        d = sheet.cell(row=i, column=j+1)
                        d.value = 'Crecimiento Trimestral'
                    
                    if i > 7 and j == max_col:
                        b = sheet.cell(row=i-1, column=j)
                        c = sheet.cell(row=i, column=j)
                        d = sheet.cell(row=i, column=j+1)
                        d.value = '=(('+c.coordinate+'-'+b.coordinate+')/'+b.coordinate+')'
                        d.number_format = '0.00%'

            celda_estilo(wb2[titulo], "1:1", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "3:4", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "6:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'inversiones':

            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.inversiones))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.inversiones))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.inversiones))
                periodos.append(period)

            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','Fecha:',forfecha(p)])
                    fila2.extend(['','','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','','Valor UF',iUF[con]])
                    fila4.extend(['','','',''])
                    fila5.extend(['','Activos Administrados por Sector','',''])
                    fila6.extend(['','Nombre Sector','Monto (mm CLP)','% Participacion'])
                else:
                    fila1.extend(['',forfecha(p)])
                    fila2.extend(['',iDolar[con]])
                    fila3.extend(['',iUF[con]])
                    fila4.extend(['',''])
                    fila5.extend(['',''])
                    fila6.extend(['Monto (mm CLP)','% Participacion'])
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)

            categs=[]
            for linea in hc[0]:
                categs.append(linea.get('nombre'))
            
            for c in categs:
                fila = []
                j = 0
                for hoja in hc:
                    for linea in hoja:
                        if linea.get('nombre') == c:
                            if j == 0:
                                fila.extend(['', linea.get('nombre'),linea.get('monto'),''])
                                j=1
                            else:
                                fila.extend([linea.get('monto'),''])
                        else:
                            pass
                cabecera.append(fila)

            filas = len(cabecera)

            titulo = '9-Inv. nacionales (mm CLP)'
        
            sheetx = {
            titulo : cabecera
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            celda_estilo(wb2[titulo], str(filas)+':'+str(filas), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")

            sheet = wb2[titulo]
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+1):
                for j in range(0, len(cabecera[0])+1):

                    if j > 3 and j%2 == 0:
                        celdaTotal = sheet.cell(row=filas, column=j-1)

                    if j > 3 and i > 6 and j%2 == 0:
                        c = sheet.cell(row=i, column=j)
                        d = sheet.cell(row=i, column=j-1)
                        c.value = "="+d.coordinate+"/"+celdaTotal.coordinate
                        c.number_format = '0.00%'
                        c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                        d.number_format = '#,##0.00'

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'inversiones_ext':

            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        res = requests.get('http://52.14.249.252/api/inversiones_ext/'+str(periodoAnt))
                        res = res.json()
                        aux =json.dumps(res["inversiones_ext"])
                        hc.append(json.loads(aux))

                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    res = requests.get('http://52.14.249.252/api/inversiones_ext/'+str(periodoAnt))
                    res = res.json()
                    aux =json.dumps(res["inversiones_ext"])
                    hc.append(json.loads(aux))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                res = requests.get('http://52.14.249.252/api/inversiones_ext/'+str(period))
                res = res.json()
                aux =json.dumps(res["inversiones_ext"])
                hc.append(json.loads(aux))

                periodos.append(period)

            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','Fecha:',forfecha(p)])
                    fila2.extend(['','','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','','Valor UF',iUF[con]])
                    fila4.extend(['','','',''])
                    fila5.extend(['','Activos Administrados por Sector','',''])
                    fila6.extend(['','Nombre Sector','Monto Nacional','% Participacion','Monto Extranjero','% Participacion'])
                else:
                    fila1.extend(['','','',forfecha(p)])
                    fila2.extend(['','','',iDolar[con]])
                    fila3.extend(['','','',iUF[con]])
                    fila4.extend(['',''])
                    fila5.extend(['',''])
                    fila6.extend(['Monto Nacional','% Participacion','Monto Extranjero','% Participacion'])
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)

            categs=[]
            for linea in hc[0]:
                categs.append(linea.get('categoria'))

            for c in categs:
                fila = []
                j = 0
                for hoja in hc:
                    for linea in hoja:
                        if linea.get('categoria') == c:
                            if j == 0:
                                fila.extend(['', linea.get('categoria'),linea.get('monto_nac'),'',linea.get('monto_ext'),''])
                                j=1
                            else:
                                fila.extend([linea.get('monto_nac'),'',linea.get('monto_ext'),''])
                        else:
                            pass
                cabecera.append(fila)

            filas = len(cabecera)

            titulo = '9.B-Inv. Extanjeras'
        
            sheetx = {
            titulo : cabecera
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            celda_estilo(wb2[titulo], str(filas)+':'+str(filas), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")

            sheet = wb2[titulo]
            for i in range(0, filas+1):
                for j in range(0, len(cabecera[filas-1])+1):

                    if j > 3 and j%2 == 0:
                        celdaTotal = sheet.cell(row=filas, column=j-1)

                    if j > 3 and i > 6 and j%2 == 0:
                        c = sheet.cell(row=i, column=j)
                        d = sheet.cell(row=i, column=j-1)
                        c.value = "="+d.coordinate+"/"+celdaTotal.coordinate
                        c.number_format = '0.00%'
                        c.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                        d.number_format = '#,##0.00'

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")


        if seccion == 'eeff_completo':

            try:
                wb = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except:

                titulos = ['1- EEFF '+forfecha(period),
                    '2.1 - Resumen '+forfecha(period)+' (mm CLP)',
                    '2.2 - Resumen '+forfecha(period)+' (m USD)',
                    '2.3 - Resumen '+forfecha(period)+' (m UF)',
                    '3.1-T.Catastro '+forfecha(period)+'(mm CLP)',
                    '3.2-T.Catastro '+forfecha(period)+'(m USD)',
                    '3.3-T.Catastro '+forfecha(period)+'(m UF)',
                    '4- Ap. Netos (mm CLP; mm USD)']
            
                wb = Workbook()
                s = wb['Sheet']
                wb.remove(s)
                for t in titulos:
                    if t[:1] == '1':
                        wbcop = load_workbook(ruta+'eeff '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]
        
                        for fila in hojacop:
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value


                        #Estilos hoja eeff

                        filas = wb[t].max_row
                        print("eeff")
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        wb[t].column_dimensions['A'].width = 5
                        wb[t].column_dimensions['B'].width = 60
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
                        celda_estilo(wb[t], "7:10", 'FF000000', 'Arial', 9, False, '00F2F2F2')
                        celda_estilo(wb[t], "A7:A10", 'FF000000', 'Arial', 9, True, '00BFBFBF')
                        celda_estilo(wb[t], "B7:B10", 'FF000000', 'Arial', 9, True, '00BFBFBF')
                        celda_estilo(wb[t], "12:12", 'FFFFFFFF', 'Arial', 9, True, '001F497D')

                        max_col = (wb[t].max_column)
            
                        peso = wb[t].cell(row=12, column=max_col-2)
                        dolar = wb[t].cell(row=12, column=max_col-1)
                        uf = wb[t].cell(row=12, column=max_col)
                        peso.alignment = Alignment(horizontal='center')
                        dolar.alignment = Alignment(horizontal='center')
                        uf.alignment = Alignment(horizontal='center')
            
                        for i in range(10, filas+1):
                            c = wb[t].cell(row=i, column=2)
                            aux = str(c.value)
                            if aux.startswith('Total') or aux.startswith('Utilidad') or aux.startswith('Resultado'):
                                celda_estilo(wb[t], str(i)+':'+str(i), 'FF000000', 'Arial', 9, False, '00DAEEF3')
                            if aux.isupper():
                                celda_estilo(wb[t], str(i)+':'+str(i), 'FFFFFFFF', 'Arial', 9, False, '000070C0')
                            if aux.startswith('Activo') or aux.startswith('Pasivo'):
                                celda_estilo(wb[t], str(i)+':'+str(i), 'FFFFFFFF', 'Arial', 9, False, '004BACC6')
                            if aux.startswith('ESTADO'):
                                celda_estilo(wb[t], str(i)+':'+str(i), 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            
            
                        for i in range (1, filas+1):
                            c = wb[t].cell(row=i, column=2)
                            aux = str(c.value)
                            if aux.isupper() or aux.startswith('Activo') or aux.startswith('Pasivo') or aux.startswith('ESTADO') or c.value == None:
                                pass
                            else:
                                if i > 11:
                                    peso = wb[t].cell(row=i, column=max_col-2)
                                    dolar = wb[t].cell(row=i, column=max_col-1)
                                    uf = wb[t].cell(row=i, column=max_col)
                                    peso.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                                    dolar.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                                    uf.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)

                    if t[:1] == '2':
                        wbcop = load_workbook(ruta+'resumen '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]
        
                        for fila in hojacop:
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value
                        print("resumen")
                        #estilos hoja resumen
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "2:3", 'FFFFFFFF', 'Arial', 9, False, '001F497D')
                        
                    if t[:1] == '3':
                        wbcop = load_workbook(ruta+'tabla_catastro '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]
        
                        for fila in hojacop:
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value
                        print("t catastro")
                        #Estilos hojas tabla catastro
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, False, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
                        #F7 en adelante
                        i=7
                        row = wb[t].max_row
                        for i in range(7, row):
                            try:
                                if wb[t]['B'+str(i)].value.lower() == 'clase':
                                    celda_estilo(wb[t], str(i)+":"+str(i), 'FFFFFFFF', 'Arial', 9, False, "00538ED5")
                                if wb[t]['B'+str(i)].value.lower() == 'subclase':
                                    celda_estilo(wb[t], str(i)+":"+str(i), 'FF000000', 'Arial', 9, False, "00A5A5A5")
                                if wb[t]['B'+str(i)].value.lower() == 'subsubclase':
                                    celda_estilo(wb[t], str(i)+":"+str(i), 'FF000000', 'Arial', 9, False, "00D8D8D8")
                            except AttributeError:
                                pass

                    if t[:1] == '4':
                        wbcop = load_workbook(ruta+'aportes_netos '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]
                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value
                        
                        sheet = wb[t]
                        filas = int((filas - 3)/2)+4
                        print("aportes")

                        #Estilos hoja aportes
                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, False, '00CCFFFF')
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
                        celda_estilo(wb[t], str(filas+5)+":"+str(filas+6), 'FFFFFFFF', 'Arial', 9, True, '001F497D')
                        celda_estilo(wb[t], str(filas)+":"+str(filas), 'FFFFFFFF', 'Arial', 9, True, '00538ED5')
                        celda_estilo(wb[t], str(filas*2)+":"+str(filas*2), 'FFFFFFFF', 'Arial', 9, True, '00538ED5')
    
                        #Grafico Circular en pesos
                        max_col = int(sheet.max_column)
            
                        data = Reference(wb[t], min_col=max_col, min_row=6, max_col=(max_col), max_row=(filas-1))
                        titles = Reference(wb[t], min_col=2, min_row=7, max_row=(filas-1))
                        chart = PieChart3D()
                        chart.title = "Aportes Netos por Categoria (mm CLP) "+forfecha(period)
                        chart.add_data(data=data, titles_from_data=True)
                        chart.set_categories(titles)
                        
                        sheet = wb.get_sheet_by_name(t)
            
                        pos = sheet.cell(row= 5, column=(max_col+2))
                        wb[t].add_chart(chart, pos.coordinate)

                        #Grafico de Barras en USD
                        data = Reference(wb[t], min_col=3, min_row=(6+filas), max_col=max_col, max_row=((filas*2)-1))
                        titles = Reference(wb[t], min_col=2, min_row=(7+filas), max_row=((filas*2)-1))
                        chart = BarChart3D()
                        chart.title = "Aportes Netos por Categoria (mm USD)"
                        chart.add_data(data=data, titles_from_data=True)
                        chart.set_categories(titles)
                        pos = sheet.cell(row= (5+filas), column=(max_col+2))
                        wb[t].add_chart(chart, pos.coordinate)

                wb.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'eeff':

            res = []
            aux = period.split('-')
            i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while i.dolar == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                i = indicador.objects.get(pk= nueva_fecha)

            j = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            while j.uf == None:
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                j = indicador.objects.get(pk= nueva_fecha)

            res.append(['','','Fecha',forfecha(period)])
            res.append(['','','Valor Dólar',i.dolar])
            res.append(['','','Valor UF',j.uf])
            res.append(['','',''])
            res.append(['','DATOS FONDO',''])
            #res.append(['','ESTADO DE SITUACIÓN FINANCIERA al '+forfecha(period)+' (Millones de pesos)',''])
            
            titulo = '1- EEFF '+forfecha(period)

            aux = hoja_catastro.objects.get(pk=period)
            hc = json.loads(aux.eeff)

            datos= [{'obj':'run_fon','dato':'RUN Fondo =>'},
                {'obj':'nom_fon','dato':'Nombre Fondo'},
                {'obj':'nom_adm','dato':'Administradora'},
                {'obj':'run_adm','dato':'R.U.T. Administradora'},
                {'obj':'clase','dato':'Clase Fondo'}]

            for d in datos:
                fila = []
                j = 0
                ob = d.get('obj')
                st = d.get('dato')
                for linea in hc:
                    if j == 0:
                        fila.extend(['',st,linea.get(ob)])
                        j += 1
                    else:
                        fila.extend([linea.get(ob)])
                res.append(fila)
            res.append([''])
            res.append(['','ESTADO DE SITUACIÓN FINANCIERA al '+forfecha(period)+' (Millones de pesos)',''])

            categs = []
            fila = []
            categs = hc[0].keys()
            categs = list(categs)
            categs.remove('run_fon')
            categs.remove('nom_fon')
            categs.remove('run_adm')
            categs.remove('nom_adm')
            categs.remove('clase')

            categs.sort()

            for cat in categs:
                fila = []
                j = 0
                aux = cat[3:]
                for linea in hc:
                    if j == 0:
                        fila.extend(['',aux,linea.get(cat)])
                        j+=1
                    else:
                        fila.extend([linea.get(cat)])
                res.append(fila)
                if aux.startswith('Total') or aux.startswith('Utilidad') or aux.startswith('Resultado') or aux.startswith('G-Costos f') or aux.startswith('G-Impuesto a'):
                    res.append([''])
                if aux == 'Total Pasivo':
                    res.append(['','ESTADO DE RESULTADOS INTEGRALES (Miles de pesos)'])
                    res.append(['','INGRESOS/ PERDIDAS DE LA OPERACION'])
            
            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            filas = len(res)
            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            wb2[titulo].column_dimensions['A'].width = 5
            wb2[titulo].column_dimensions['B'].width = 60
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], "7:10", 'FF000000', 'Arial', 9, False, '00F2F2F2')
            celda_estilo(wb2[titulo], "A7:A10", 'FF000000', 'Arial', 9, True, '00BFBFBF')
            celda_estilo(wb2[titulo], "B7:B10", 'FF000000', 'Arial', 9, True, '00BFBFBF')
            celda_estilo(wb2[titulo], "12:12", 'FFFFFFFF', 'Arial', 9, True, '001F497D')

            sheet= wb2.get_sheet_by_name(titulo)
            max_col = len(hc)+2

            peso = sheet.cell(row=12, column=max_col+1)
            dolar = sheet.cell(row=12, column=max_col+2)
            uf = sheet.cell(row=12, column=max_col+3)
            peso.value = 'Total (mm CLP)'
            dolar.value = 'Total (m USD)'
            uf.value = 'Total (m UF)'
            peso.alignment = Alignment(horizontal='center')
            dolar.alignment = Alignment(horizontal='center')
            uf.alignment = Alignment(horizontal='center')

            for i in range(10, filas+1):
                c = sheet.cell(row=i, column=2)
                aux = str(c.value)
                if aux.startswith('Total') or aux.startswith('Utilidad') or aux.startswith('Resultado'):
                    celda_estilo(wb2[titulo], str(i)+':'+str(i), 'FF000000', 'Arial', 9, False, '00DAEEF3')
                if aux.isupper():
                    celda_estilo(wb2[titulo], str(i)+':'+str(i), 'FFFFFFFF', 'Arial', 9, False, '000070C0')
                if aux.startswith('Activo') or aux.startswith('Pasivo'):
                    celda_estilo(wb2[titulo], str(i)+':'+str(i), 'FFFFFFFF', 'Arial', 9, False, '004BACC6')
                if aux.startswith('ESTADO'):
                    celda_estilo(wb2[titulo], str(i)+':'+str(i), 'FFFFFFFF', 'Arial', 9, True, '001F497D')


            for i in range (1, filas+1):
                c = sheet.cell(row=i, column=2)
                aux = str(c.value)
                if aux.isupper() or aux.startswith('Activo') or aux.startswith('Pasivo') or aux.startswith('ESTADO') or c.value == None:
                    pass
                else:
                    if i > 11:
                        peso = sheet.cell(row=i, column=max_col+1)
                        dolar = sheet.cell(row=i, column=max_col+2)
                        uf = sheet.cell(row=i, column=max_col+3)
                        peso.value = '=SUM(B'+str(i)+':'+get_column_letter(max_col)+str(i)+')'
                        dolar.value = '='+peso.coordinate+'/(D2*1000)'
                        uf.value = '='+peso.coordinate+'/(D3*1000)'
                        peso.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                        dolar.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)
                        uf.font = Font(color = 'FF000000', name = 'Arial', size=9, bold=True)

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'resumen':

            total_fondos = []
            aux = fondo.objects.all().values('runsvs')
            for a in aux:
                a=a.get('runsvs')
                total_fondos.append(a.split('-')[0])

            total_fondos.sort()
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.resumen))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.resumen))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.resumen))
                periodos.append(period)

            resumen = []
            iDolar = []
            iUF = []
            con = 0
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)

            fila1= []
            fila2= []
            fila3= []
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','','','','','',''])
                    fila2.extend(['','','','',periodos[con],'',iDolar[con],'(en millones de pesos)'])
                    fila3.extend(['','RUN', 'Fondo','Clase','Aportes (+)','Reparto Patrimonio (+)','Dividendos (-)',' Aporte Neto'])
                else:
                    fila1.extend(['','','',''])
                    fila2.extend([periodos[con],'',iDolar[con],'(en millones de pesos)'])
                    fila3.extend(['Aportes (+)','Reparto Patrimonio (+)','Dividendos (-)',' Aporte Neto'])    
                con +=1
            
            resumen.append(fila1)
            resumen.append(fila2)
            resumen.append(fila3)

            i=0
            for fon in total_fondos:
                if has_key(fon , period, 'resumen') == True:
                    
                    fila = []
                    j=0
                    cont = 0
                    flag = 0
                    num_hoja = 0
                    
                    for hoja in hc:
                        largo = len(hoja)
                        num_hoja +=1

                        for linea in hoja:
                            cont +=1
                            if fon == linea.get('run'):
                                flag = 1
                                if j == 0:
                                    fila.extend([str(i+1),linea.get('run'),linea.get('fondo'),linea.get('clase'),float(linea.get('aportes')),float(linea.get('reparto')),float(linea.get('dividendos')),'']) 
                                    j+=1
                                    i+=1                                            
                                else:
                                    if fila[2] == '':
                                        fila[2]=linea.get('fondo')
                                        fila[3]=linea.get('clase')
                                    fila.extend([float(linea.get('aportes')),float(linea.get('reparto')),float(linea.get('dividendos')),''])                    
                            else:
                                if cont == largo and flag == 0:
                                    flag = 1
                                    if j == 0:
                                        fila.extend([str(i+1),fon ,'','','','','','']) 
                                        j+=1
                                        i+=1                                                
                                    else:
                                        fila.extend(['','','',''])                                                
                                else:
                                    continue

                    resumen.append(fila)

            titulo = '2.1 - Resumen '+forfecha(period)+' (mm CLP)'
            sheetx = {
            titulo : resumen
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "2:3", 'FFFFFFFF', 'Arial', 9, False, '001F497D')
            
            celda_estilo(wb2[titulo], str(len(resumen)+2)+':'+str(len(resumen)+3), 'FF000000', 'Arial', 9, False, 'DAEEF3')
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

            titulo2 = '2.2 - Resumen '+forfecha(period)+' (m USD)'
            titulo3 = '2.3 - Resumen '+forfecha(period)+' (m UF)'
            active = wb2.active

            sheet = wb2.get_sheet_by_name(titulo)
            for i in range(0, len(resumen)+1):
                for j in range(0, len( resumen[0] )+1):
                    if j >7 and i >3:
                        if j%4 == 0:
                            d =sheet.cell(row=i, column=j)
                            a =sheet.cell(row=i, column=j-3)
                            b =sheet.cell(row=i, column=j-1)
                            d.value = '=SUM('+a.coordinate+':'+b.coordinate+')'
            
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, len(resumen)+1):
                aux1=7
                for j in range(0, len( resumen[0] )+1):

                    if (j-2) < aux1:
                            celdaDolar = sheet.cell(row=2, column=aux1)
                    else:
                        aux1 +=4
                        celdaDolar = sheet.cell(row=2, column=aux1)

                    if j > 4 and (j-2)%4 == 0 and i == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = 'Valor Dolar'
                        d.alignment = Alignment(horizontal='right')  

                    if j > 7 and j%4 ==0 and i == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(en miles de dolares)'

                    if j > 4 and i > 3:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar Valores a celdas a hoja con valor UF
            cont = 0
            for i in range(0, len(resumen)+1):
                aux1=7
                for j in range(0, len( resumen[0] )+1):

                    if (j-2) < aux1:
                            celdaUF = sheet.cell(row=2, column=aux1)
                    else:
                        aux1 +=4
                        celdaUF = sheet.cell(row=2, column=aux1)

                    if j > 4 and (j-3)%4 == 0 and i == 2:

                        d =sheet.cell(row=i, column=j)
                        d.value = iUF[cont]
                        cont +=1

                    if j > 4 and (j-2)%4 == 0 and i == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = 'Valor UF'
                        d.alignment = Alignment(horizontal='right')  

                    if j > 7 and j%4 ==0 and i == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(en miles de UF)'

                    if j > 4 and i > 3:
                        c = (sheet2.cell(row=i, column=j))
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")
        
        if seccion == ('tabla_catastro'):

            total_fondos = []
            aux = fondo.objects.all().order_by('categoria')
            aux = aux.values('runsvs')
            for a in aux:
                a=a.get('runsvs')
                total_fondos.append(a.split('-')[0])
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.tabla_catastro))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.tabla_catastro))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.tabla_catastro))
                periodos.append(period)

            
            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','','Fecha:','','',forfecha(p),''])
                    fila2.extend(['','','','Valor Dolar:','','',iDolar[con],''])
                    fila3.extend(['','','','Valor UF','','',iUF[con],''])
                    fila4.extend(['','','','','','','',''])
                    fila5.extend(['','','Aportes, Reparto de Patrimonio y Dividendos por Fondo','','','',forfecha(p),''])
                    fila6.extend(['','','(Miles de dólares de cada periodo)','RUT Fondo','Clase','Aportes','Reparto Patrimonio','Dividendos'])
                else:
                    fila1.extend(['',forfecha(p),''])
                    fila2.extend(['',iDolar[con],''])
                    fila3.extend(['',iUF[con],''])
                    fila4.extend(['','',''])
                    fila5.extend(['',forfecha(p),''])
                    fila6.extend(['Aportes','Reparto Patrimonio','Dividendos'])
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)

            i=0
            fondos = []
            for fon in total_fondos:
                if has_key(fon , period, 'tabla_catastro') == True:
                    fila = []
                    j=0
                    cont = 0
                    flag = 0
                    num_hoja = 0
                    
                    for hoja in hc:
                        largo = len(hoja)
                        num_hoja +=1

                        for linea in hoja:
                            cont +=1
                            if linea.get('tipo') == 'Fondo':
                                if fon == linea.get('run'):
                                    flag = 1
                                    if j == 0:
                                        fila = []
                                        fila.extend(['',linea.get('tipo'),linea.get('nombre'),linea.get('run'),linea.get('indice'),float(linea.get('aportes')),float(linea.get('reparto')),float(linea.get('dividendos'))]) 
                                        j+=1
                                        i+=1                                            
                                    else:
                                        if fila[1] == '':
                                            fila[1]=linea.get('tipo')
                                            fila[2]=linea.get('nombre')
                                            fila[4]=linea.get('indice')
                                        fila.extend([float(linea.get('aportes')),float(linea.get('reparto')),float(linea.get('dividendos'))])
                                else:
                                    if cont == largo and flag == 0:
                                        flag = 1
                                        if j == 0:
                                            fila = []
                                            fila.extend(['','','',fon,'','','','']) 
                                            j+=1
                                            i+=1                                                
                                        else:
                                            fila.extend(['','',''])                                                
                                    else:
                                        continue
                            else:
                                pass
  
                    fondos.append(fila)

            clases = []
            for linea in hc[0]:
                fila = []               
                if linea.get('tipo') != 'Fondo':
                    for i in range (0, len(hc)):
                        if i == 0:
                            fila.extend(['',linea.get('tipo'),linea.get('nombre'),linea.get('run'),linea.get('indice'),linea.get('aportes'),linea.get('reparto'),linea.get('dividendos')])
                        else:
                            fila.extend([linea.get('aportes'),linea.get('reparto'),linea.get('dividendos')])
                    clases.append(fila)
            for f in fondos:
                clases.append(f)

            clases= sorted(clases, key=itemgetter(4))
            tabla_catastro = []
            for c in cabecera:
                tabla_catastro.append(c)
            for c in clases:
                tabla_catastro.append(c)

            titulo = '3.1-T.Catastro '+forfecha(period)+'(mm CLP)'
        
            sheetx = {
            titulo : tabla_catastro
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, False, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")
            
            #F7 en adelante
            i=7
            row = wb2[titulo].max_row
            for i in range(7, row):
                try:
                    if wb2[titulo]['B'+str(i)].value.lower() == 'clase':
                        celda_estilo(wb2[titulo], str(i)+":"+str(i), 'FFFFFFFF', 'Arial', 9, False, "00538ED5")
                    if wb2[titulo]['B'+str(i)].value.lower() == 'subclase':
                        celda_estilo(wb2[titulo], str(i)+":"+str(i), 'FF000000', 'Arial', 9, False, "00A5A5A5")
                    if wb2[titulo]['B'+str(i)].value.lower() == 'subsubclase':
                        celda_estilo(wb2[titulo], str(i)+":"+str(i), 'FF000000', 'Arial', 9, False, "00D8D8D8")
                except AttributeError:
                    pass

            titulo2 = '3.2-T.Catastro '+forfecha(period)+'(m USD)'
            titulo3 = '3.3-T.Catastro '+forfecha(period)+'(m UF)'
            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, len(tabla_catastro)+1):
                aux1=7
                for j in range(0, len(tabla_catastro[0])+1):

                    if (j-2) < aux1:
                            celdaDolar = sheet.cell(row=2, column=aux1)
                    else:
                        aux1 +=3
                        celdaDolar = sheet.cell(row=2, column=aux1)

                    if j > 5 and i > 6:
                        aux = sheet.cell(row=i, column=2)
                        if aux.value == 'Fondo':
                            c = sheet2.cell(row=i, column=j)
                            d =sheet.cell(row=i, column=j)
                            d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"
                        else:
                            pass

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar Valores a celdas a hoja con valor UF
            cont = 0
            for i in range(0, len(tabla_catastro)+1):
                aux1=7
                for j in range(0, len( tabla_catastro[0] )+1):

                    if (j-2) < aux1:
                            celdaUF = sheet.cell(row=3, column=aux1)
                    else:
                        aux1 +=3
                        celdaUF = sheet.cell(row=3, column=aux1)

                    if j > 5 and i > 6:
                        aux = sheet.cell(row=i, column=2)
                        if aux.value == 'Fondo':
                            c = (sheet2.cell(row=i, column=j))
                            d =sheet.cell(row=i, column=j)
                            d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"
                        else:
                            pass
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'aportes_netos':

            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.aportes_netos))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.aportes_netos))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.aportes_netos))
                periodos.append(period)

            
            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','Fecha:',forfecha(p)])
                    fila2.extend(['','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','Valor UF',iUF[con]])
                    fila4.extend(['','',''])
                    fila5.extend(['','Aporte Neto por Tipo de Fondo','Aporte Neto'])
                    fila6.extend(['','(en millones de pesos)',forfecha(p)])
                    

                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend(['Aporte Neto'])
                    fila6.extend([forfecha(p)])
                    
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)

            aportes = []
            for i in range(0, len(hc[0])):
                fila = []
                flag = 0
                for hoja in hc:
                    if flag == 0:
                        fila.extend(['',hoja[i].get('categoria'),float(hoja[i].get('monto'))])
                        flag =1
                    else:
                        fila.extend([float(hoja[i].get('monto'))])
                aportes.append(fila)

            for ap in aportes:
                cabecera.append(ap)

            titulo = '4- Ap. Netos (mm CLP; mm USD)'
            sheetx = {
            titulo : cabecera
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)
            
            filas = len(cabecera)

            #estilos para las hojas
            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, False, '00CCFFFF')
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], str(filas+5)+":"+str(filas+6), 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], str(filas)+":"+str(filas), 'FFFFFFFF', 'Arial', 9, True, '00538ED5')
            celda_estilo(wb2[titulo], str(filas*2)+":"+str(filas*2), 'FFFFFFFF', 'Arial', 9, True, '00538ED5')

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")
            
            #Grafico Circular en pesos
            max_col = len(aportes[0])

            data = Reference(wb2[titulo], min_col=max_col, min_row=6, max_col=(max_col), max_row=(filas-1))
            titles = Reference(wb2[titulo], min_col=2, min_row=7, max_row=(filas-1))
            chart = PieChart3D()
            chart.title = "Aportes Netos por Categoria (mm CLP) "+forfecha(period)
            chart.add_data(data=data, titles_from_data=True)
            chart.set_categories(titles)
            
            sheet = wb2.get_sheet_by_name(titulo)

            pos = sheet.cell(row= 5, column=(max_col+2))
            wb2[titulo].add_chart(chart, pos.coordinate)

            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+1):
                for j in range(2, len(aportes[0])+1):
                    if j > 2:
                        celdaDolar = sheet.cell(row=2, column=j)

                    if i > 4:
                        c = sheet.cell(row=i, column=j)
                        d = sheet.cell(row=(i + filas), column=j)
                        d.value = "="+c.coordinate
                    
                    if i > 6 and j > 2 :
                        c = sheet.cell(row=i, column=j)
                        d = sheet.cell(row=(i + filas), column=j)
                        d.value = "="+c.coordinate+"/"+celdaDolar.coordinate

                    if i == 6 and j == 2:
                        d = sheet.cell(row=(i + filas), column=j)
                        d.value = "(en millones de dolares)"
            
            #Grafico de Barras en USD
            data = Reference(wb2[titulo], min_col=3, min_row=(6+filas), max_col=max_col, max_row=((filas*2)-1))
            titles = Reference(wb2[titulo], min_col=2, min_row=(7+filas), max_row=((filas*2)-1))
            chart = BarChart3D()
            chart.title = "Aportes Netos por Categoria (mm USD)"
            chart.add_data(data=data, titles_from_data=True)
            chart.set_categories(titles)
            pos = sheet.cell(row= (5+filas), column=(max_col+2))
            wb2[titulo].add_chart(chart, pos.coordinate)

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'patrimonio_completo':

            try:
                wb = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except:

                titulos = ['1.1-Pat. por Fondo-AFI (mm CLP)',
                    '1.2-Pat. por Fondo-AFI (m USD)',
                    '1.3-Pat. por Fondo-AFI (m UF)',
                    '2.1 Pat. por AFI (mm CLP)',
                    '2.2-Pat. por AFI (m USD)',
                    '2.3-Pat. por AFI (m UF)',
                    '3.1-Pat. por Clase (mm CLP)',
                    '3.2-Pat. por Clase (m USD)',
                    '3.3-Pat. por Clase (m UF)',
                    '4.1-Valor Cuota (mm CLP)',
                    '4.2-Valor Cuota (m USD)',
                    '4.3-Valor Cuota (m UF)',
                    '5-Numero de Cuotas Pagadas']
            
                wb = Workbook()
                s = wb['Sheet']
                wb.remove(s)
                for t in titulos:
                    if t[:1] == '1':
                        wbcop = load_workbook(ruta+'patrimonio_fondo '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
                        celda_estilo(wb[t], str(filas+5)+':'+str(filas+5), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")

                    if t[:1] == '2':
                        wbcop = load_workbook(ruta+'patrimonio_afi '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
                        celda_estilo(wb[t], str(filas+5)+':'+str(filas+5), 'FF000000', 'Arial', 9, True, '2eb3f4')

                    if t[:1] == '3':
                        wbcop = load_workbook(ruta+'patrimonio_clase '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        cont = 0
                        filas = 0
                        for fila in hojacop:
                            cont+=1
                            c = hojacop.cell(row=cont, column=2)
                            if c.value:
                                filas +=1
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
                        celda_estilo(wb[t], str(filas+4)+':'+str(filas+4), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")

                    if t[:1] == '4':
                        wbcop = load_workbook(ruta+'valor_cuotas '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        for fila in hojacop:
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")

                    if t[:1] == '5':
                        wbcop = load_workbook(ruta+'cuotas_pagadas '+str(period)+".xlsx")
                        hoja = wb.create_sheet(t)
                        hojacop = wbcop[t]

                        for fila in hojacop:
                            for celda in fila:
                                hoja[celda.coordinate].value = celda.value

                        celda_estilo(wb[t], "1:1000", 'FF000000', 'Arial', 9, False, None)
                        celda_estilo(wb[t], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
                        celda_estilo(wb[t], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")

                wb.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'patrimonio_fondo':

            total_fondos = []
            aux = fondo.objects.all().order_by('categoria')
            aux = aux.values('runsvs')
            for a in aux:
                a=a.get('runsvs')
                total_fondos.append(a.split('-')[0])

            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.patrimonio_fondo))
 
                    except:
                        hc.append({})

                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.patrimonio_fondo))
                except:
                    hc.append({})

                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.patrimonio_fondo))
                periodos.append(period)

            iDolar = []
            iUF = []

            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','','','Fecha:','',forfecha(p)])
                    fila2.extend(['','','','','Valor Dolar:','',iDolar[con]])
                    fila3.extend(['','','','','Valor UF','',iUF[con]])
                    fila4.extend(['','','','','','',''])
                    fila5.extend(['','Patrimonios Administrados por Tipo de Fondo','','','','',''])
                    fila6.extend(['','(Millones de pesos de cada periodo)','RUT Admin','Administradora','Clase','RUT Fondo','Patrimonio'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([''])
                    fila6.extend(['Patrimonio'])
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)


            i=0
            fondos = []
            for fon in total_fondos:
                if has_key(fon , period, 'patrimonio_fondo') == True:
                    fila = []
                    j=0
                    cont = 0
                    flag = 0
                    num_hoja = 0
                    
                    for hoja in hc:
                        largo = len(hoja)
                        num_hoja +=1

                        for linea in hoja:
                            cont +=1
                            if fon == linea.get('run'):
                                flag = 1
                                if j == 0:
                                    fila = []
                                    fila.extend(['',linea.get('nombre_fon'),linea.get('run_adm'),linea.get('nombre_adm'),linea.get('indice'),linea.get('run'),float(linea.get('patrimonio'))]) 
                                    j+=1
                                else:
                                    if fila[1] == '':
                                        fila[1]=linea.get('nombre_fon')
                                        fila[2]=linea.get('run_adm')
                                        fila[3]=linea.get('nombre_adm')
                                        fila[4]=linea.get('indice')
                                    fila.extend([float(linea.get('patrimonio'))])
                            else:
                                if cont == largo and flag == 0:
                                    flag = 1
                                    if j == 0:
                                        fila = []
                                        fila.extend(['','','','','',fon,'']) 
                                        j+=1
                                    else:
                                        fila.extend([''])
                                else:
                                    continue
                    fondos.append(fila)

            fondos= sorted(fondos, key=itemgetter(4))
            for f in fondos:
                cabecera.append(f)
            
            filas = len(cabecera)
            fila = []
            for i in range(0, len(hc)):
                col = get_column_letter((7+i))
                if i == 0:
                    fila.extend(['','','','','','TOTAL','=SUM('+col+'7:'+col+str(filas)+')'])
                else:
                    fila.extend(['=SUM('+col+'7:'+col+str(filas)+')'])
            cabecera.append(fila)

            titulo = '1.1-Pat. por Fondo-AFI (mm CLP)'
            sheetx = {
            titulo : cabecera
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            celda_estilo(wb2[titulo], str(filas+1)+':'+str(filas+1), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")
            
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

            titulo2 = '1.2-Pat. por Fondo-AFI (m USD)'
            titulo3 = '1.3-Pat. por Fondo-AFI (m UF)'
            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+2):
                aux1=7
                for j in range(0, len(cabecera[0])+1):

                    if j == aux1:
                        celdaDolar = sheet.cell(row=2, column=aux1)
                        aux1+=1

                    if j > 6 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"
                    
                    if i == 6 and j == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(Miles de dolares de cada periodo)'

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar Valores a celdas a hoja con valor UF
            for i in range(0, filas+2):
                aux1=7
                for j in range(0, len(cabecera[0])+1):

                    if j == aux1:
                        celdaUF = sheet.cell(row=3, column=aux1)
                        aux1+=1

                    if j > 6 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"
                    
                    if i == 6 and j == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(Miles de UF de cada periodo)'

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'patrimonio_afi':

            res = []
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.patrimonio_afi))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.patrimonio_afi))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.patrimonio_afi))
                periodos.append(period)

            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            res = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','Fecha:',forfecha(p)])
                    fila2.extend(['','','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','','Valor UF',iUF[con]])
                    fila4.extend(['','','',''])
                    fila5.extend(['','Patrimonio Administrado por AFI (Millones de pesos de cada periodo)','',''])
                    fila6.extend(['','Administradora','RUT Admin','Patrimonio'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([''])
                    fila6.extend(['patrimonio'])
                con +=1
            
            res.append(fila1)
            res.append(fila2)
            res.append(fila3)
            res.append(fila4)
            res.append(fila5)
            res.append(fila6)

            admins = []
            aux = administrador.objects.all().order_by('razon_social')
            aux = aux.values('rut')
            for a in aux:
                a=a.get('rut')
                admins.append(a)

            for ad in admins:
                if has_key(ad , period, 'patrimonio_afi') == True:
                    fila = []
                    j=0
                    cont = 0
                    flag = 0
                    num_hoja = 0
                    
                    for hoja in hc:
                        largo = len(hoja)
                        num_hoja +=1

                        for linea in hoja:
                            cont +=1
                            if ad == linea.get('run_adm'):
                                flag = 1
                                if j == 0:
                                    fila = []
                                    fila.extend(['',linea.get('nombre'),linea.get('run_adm'),float(linea.get('patrimonio'))])
                                    j+=1
                                else:
                                    if fila[1] == '':
                                        fila[1]=linea.get('nombre')
                                    fila.extend([float(linea.get('patrimonio'))])
                            else:
                                if cont == largo and flag == 0:
                                    flag = 1
                                    if j == 0:
                                        fila = []
                                        fila.extend(['','',ad,'']) 
                                        j+=1
                                    else:
                                        fila.extend([''])
                                else:
                                    continue
                    res.append(fila)

            filas = len(res)
            fila = []
            for i in range(0, len(hc)):
                col = get_column_letter((4+i))
                if i == 0:
                    fila.extend(['','','TOTAL','=SUM('+col+'7:'+col+str(filas)+')'])
                else:
                    fila.extend(['=SUM('+col+'7:'+col+str(filas)+')'])
            res.append(fila)
            titulo = '2.1 Pat. por AFI (mm CLP)'

            sheetx = {
            titulo : res
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)
            
            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")

            
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, '00CCFFFF')
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, '001F497D')
            celda_estilo(wb2[titulo], str(filas+1)+':'+str(filas+1), 'FF000000', 'Arial', 9, True, '2eb3f4')
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")
            
            titulo2 = '2.2-Pat. por AFI (m USD)'
            titulo3 = '2.3-Pat. por AFI (m UF)'
            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+1):
                aux1=4
                for j in range(0, len(res[0])+1):

                    if j == aux1:
                        celdaDolar = sheet.cell(row=2, column=aux1)
                        aux1+=1

                    if j > 3 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar Valores a celdas a hoja con valor UF
            cont = 0
            for i in range(0, len(res)+1):
                aux1=4
                for j in range(0, len( res[0] )+1):

                    if j == aux1:
                        celdaUF = sheet.cell(row=3, column=aux1)
                        aux1+=1

                    if j > 3 and i > 6:
                        c = (sheet2.cell(row=i, column=j))
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion.startswith('patrimonio_clase'):

            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.patrimonio_clase))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.patrimonio_clase))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.patrimonio_clase))
                periodos.append(period)

            
            iDolar = []
            iUF = []
            
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)
        
            cabecera = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','Fecha:',forfecha(p)])
                    fila2.extend(['','','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','','Valor UF',iUF[con]])
                    fila4.extend(['','','',''])
                    fila5.extend(['','Patrimonio Administrado por Tipo de Fondo','',''])
                    fila6.extend(['','(Millones de pesos de cada periodo)','Clase','Patrimonio'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([''])
                    fila6.extend(['Patrimonio'])
                con +=1
            
            cabecera.append(fila1)
            cabecera.append(fila2)
            cabecera.append(fila3)
            cabecera.append(fila4)
            cabecera.append(fila5)
            cabecera.append(fila6)

            clases = []
            categs = categoria.objects.exclude(pk=0).order_by('indice')
            for c in categs:
                fila = []
                j = 0
                for hoja in hc:
                    for linea in hoja:
                        if linea.get('indice') == c.indice and j == 0:
                            fila.extend(['',linea.get('nombre_cat'),linea.get('indice'),float(linea.get('monto'))]) 
                            j+=1
                        elif linea.get('indice') == c.indice and j != 0:
                            fila.extend([float(linea.get('monto'))])
                        else:
                            pass
                clases.append(fila)
            fila = []
            j = 0
            for hoja in hc:
                for linea in hoja:
                    if linea.get('indice') == '' and j == 0:
                        fila.extend(['',linea.get('nombre_cat'),linea.get('indice'),float(linea.get('monto'))]) 
                        j+=1
                    elif linea.get('indice') == '' and j != 0:
                        fila.extend([float(linea.get('monto'))])
            
            clases= sorted(clases, key=itemgetter(2))
            clases.append(fila)
            for c in clases:
                cabecera.append(c)

            titulo = '3.1-Pat. por Clase (mm CLP)'
        
            sheetx = {
            titulo : cabecera
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            filas = len(cabecera)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            celda_estilo(wb2[titulo], str(filas)+':'+str(filas), 'FFFFFFFF', 'Arial', 9, True, "2eb3f4")
            
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

            titulo2 = '3.2-Pat. por Clase (m USD)'
            titulo3 = '3.3-Pat. por Clase (m UF)'
            active = wb2.active
            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, filas+1):
                aux1=4
                for j in range(0, len(cabecera[0])+1):

                    if j > 3 and j == aux1:
                        celdaDolar = sheet.cell(row=2, column=aux1)
                        aux1+=1

                    if j > 3 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaDolar.coordinate+"*1000)"
                    
                    if i == 6 and j == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(Miles de dolares de cada periodo)'
                    

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar Valores a celdas a hoja con valor UF
            for i in range(0, filas+1):
                aux1=4
                for j in range(0, len(cabecera[0])+1):

                    if j > 3 and j == aux1:
                        celdaUF = sheet.cell(row=3, column=aux1)
                        aux1+=1

                    if j > 3 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/("+celdaUF.coordinate+"*1000)"
                    
                    if i == 6 and j == 2:
                        d =sheet.cell(row=i, column=j)
                        d.value = '(Miles de UF de cada periodo)'

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == ('valor_cuotas'):

            total_fondos = []
            aux = fondo.objects.all().values('runsvs')
            for a in aux:
                a=a.get('runsvs')
                total_fondos.append(a.split('-')[0])

            total_fondos.sort()
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.valor_cuotas))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.valor_cuotas))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.valor_cuotas))
                periodos.append(period)

            cuotas = []
            iDolar = []
            iUF = []
            con = 0
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)

            cuotas = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','','','','Fecha:',forfecha(p)])
                    fila2.extend(['','','','','','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','','','','','Valor UF',iUF[con]])
                    fila4.extend(['','','','','','',''])
                    fila5.extend(['','Patrimonio Valor Cuota (En pesos de cada periodo)','',''])
                    fila6.extend(['','Administradora','RUT Admin','Fondo','RUT Fondo','Clase','Valor Cuota'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([''])
                    fila6.extend(['Valor Cuota'])
                con +=1
            
            cuotas.append(fila1)
            cuotas.append(fila2)
            cuotas.append(fila3)
            cuotas.append(fila4)
            cuotas.append(fila5)
            cuotas.append(fila6)

            i=0
            for fon in total_fondos:
                if has_key(fon , period, 'valor_cuotas') == True:
                    
                    fila = []
                    j=0
                    cont = 0
                    flag = 0
                    num_hoja = 0
                    
                    for hoja in hc:
                        largo = len(hoja)
                        num_hoja +=1

                        for linea in hoja:
                            cont +=1
                            if fon == linea.get('run_fon'):
                                flag = 1
                                if j == 0:
                                    fila.extend(['',linea.get('nom_adm'),linea.get('run_adm'),linea.get('nom_fon'),linea.get('run_fon'),linea.get('clase'),float(linea.get('monto'))]) 
                                    j+=1
                                    i+=1
                                else:
                                    if fila[1] == '':
                                        fila[1]=linea.get('nom_adm')
                                        fila[2]=linea.get('run_adm')
                                        fila[3]=linea.get('nom_fon')
                                        fila[5]=linea.get('clase')
                                    fila.extend([float(linea.get('monto'))])
                            else:
                                if cont == largo and flag == 0:
                                    flag = 1
                                    if j == 0:
                                        fila.extend(['','','','',fon,'','']) 
                                        j+=1
                                        i+=1
                                    else:
                                        fila.extend([''])
                                else:
                                    continue

                    cuotas.append(fila)

            titulo = '4.1-Valor Cuota (mm CLP)'
            sheetx = {
            titulo : cuotas
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

            titulo2 = '4.2-Valor Cuota (m USD)'
            titulo3 = '4.3-Valor Cuota (m UF)'
            active = wb2.active

            #Generar hoja en USD
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo2 
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar valor a celdas a hoja con valor en USD
            for i in range(0, len(cuotas)+1):
                aux1=7
                for j in range(0, len( cuotas[0] )+1):

                    if j == aux1:
                        celdaDolar = sheet.cell(row=2, column=aux1)
                        aux1+=1

                    if j == 2 and i == 5:
                        d =sheet.cell(row=i, column=j)
                        d.value = 'Patrimonio Valor Cuota (En dolares de cada periodo)'

                    if j > 6 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/"+celdaDolar.coordinate

            #Generar Hoja en UF
            wb2.copy_worksheet(active)
            sheet = wb2.worksheets[-1]
            sheet.title = titulo3
            sheet2 = wb2.get_sheet_by_name(titulo)
            
            #Dar Valores a celdas a hoja con valor UF
            cont = 0
            for i in range(0, len(cuotas)+1):
                aux1=7
                for j in range(0, len( cuotas[0] )+1):
                    if j == aux1:
                        celdaUF = sheet.cell(row=3, column=aux1)
                        aux1+=1

                    if j == 2 and i == 5:
                        d =sheet.cell(row=i, column=j)
                        d.value = 'Patrimonio Valor Cuota (En UF de cada periodo)'

                    if j > 6 and i > 6:
                        c = sheet2.cell(row=i, column=j)
                        d =sheet.cell(row=i, column=j)
                        d.value = "='"+titulo+"'!"+c.coordinate+"/"+celdaUF.coordinate

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion.startswith('cuotas_pagadas'):

            total_fondos = []
            aux = fondo.objects.all().values('runsvs')
            for a in aux:
                a=a.get('runsvs')
                total_fondos.append(a.split('-')[0])

            total_fondos.sort()
            hc = []
            aux = period.split('-')
            periodos = []
            if int(aux[1]) > 2018:
                periodoAnt = '12-2018'

                while (periodoAnt != period ):

                    try:
                        aux = hoja_catastro.objects.get(pk=periodoAnt)
                        hc.append(json.loads(aux.cuotas_pagadas))
 
                    except:
                        hc.append({})
    
                    periodos.append(periodoAnt)
                    periodoAnt = periodoSiguiente(periodoAnt)

                try: 
                    aux = hoja_catastro.objects.get(pk=periodoAnt)     
                    hc.append(json.loads(aux.cuotas_pagadas))
                except:
                    hc.append({})
                
                periodos.append(periodoAnt)

            else:
                aux =  hoja_catastro.objects.get(pk=period)
                hc.append(json.loads(aux.cuotas_pagadas))
                periodos.append(period)

            cuotas = []
            iDolar = []
            iUF = []
            con = 0
            for p in periodos:

                aux = p.split('-')
                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.dolar == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iDolar.append(i.dolar)

                i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
                while i.uf == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                iUF.append(i.uf)

            cuotas = []
            fila1= []
            fila2= []
            fila3= []
            fila4= []
            fila5= []
            fila6= []
            con = 0
            for p in periodos:
                if con == 0:
                    fila1.extend(['','','','','','Fecha:',forfecha(p)])
                    fila2.extend(['','','','','','Valor Dolar:',iDolar[con]])
                    fila3.extend(['','','','','','Valor UF',iUF[con]])
                    fila4.extend(['','','','','','',''])
                    fila5.extend(['','Número de Cuotas Suscritas y Pagadas','',''])
                    fila6.extend(['','Administradora','RUT Admin','Fondo','RUT Fondo','Clase','Cuotas Pagadas'])
                else:
                    fila1.extend([forfecha(p)])
                    fila2.extend([iDolar[con]])
                    fila3.extend([iUF[con]])
                    fila4.extend([''])
                    fila5.extend([''])
                    fila6.extend(['Cuotas Pagadas'])
                con +=1
            
            cuotas.append(fila1)
            cuotas.append(fila2)
            cuotas.append(fila3)
            cuotas.append(fila4)
            cuotas.append(fila5)
            cuotas.append(fila6)

            i=0
            for fon in total_fondos:
                if has_key(fon , period, 'cuotas_pagadas') == True:
                    
                    fila = []
                    j=0
                    cont = 0
                    flag = 0
                    num_hoja = 0
                    
                    for hoja in hc:
                        largo = len(hoja)
                        num_hoja +=1

                        for linea in hoja:
                            cont +=1
                            if fon == linea.get('run_fon'):
                                flag = 1
                                if j == 0:
                                    fila.extend(['',linea.get('nom_adm'),linea.get('run_adm'),linea.get('nom_fon'),linea.get('run_fon'),linea.get('clase'),float(linea.get('cuotas'))]) 
                                    j+=1
                                    i+=1
                                else:
                                    if fila[1] == '':
                                        fila[1]=linea.get('nom_adm')
                                        fila[2]=linea.get('run_adm')
                                        fila[3]=linea.get('nom_fon')
                                        fila[5]=linea.get('clase')
                                    fila.extend([float(linea.get('cuotas'))])
                            else:
                                if cont == largo and flag == 0:
                                    flag = 1
                                    if j == 0:
                                        fila.extend(['','','','',fon,'','']) 
                                        j+=1
                                        i+=1
                                    else:
                                        fila.extend([''])
                                else:
                                    continue

                    cuotas.append(fila)

            titulo = '5-Numero de Cuotas Pagadas'
            sheetx = {
            titulo : cuotas
            }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "1:3", 'FF000000', 'Arial', 9, True, "00CCFFFF")
            celda_estilo(wb2[titulo], "5:6", 'FFFFFFFF', 'Arial', 9, True, "00003366")
            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'volumen_cuotas_mes':
            titulo = 'Volumen Cuotas Mes'    
            sheetx = {
                titulo : [x for x in eval( hc.volumen_cuotas_mes )]
                }
            save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

            wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
            celda_estilo(wb2[titulo], "3:3", 'FFFFFFFF', 'Arial', 9, True, "00003366")

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'volumen_cuotas_trimestre':
            titulo = 'Volumen Cuotas Trimestre'
            
            sheetx = {
                titulo : [x for x in eval(  )]
                }
            
            wb2 = load_workbook(ruta+'volumen_cuotas_mes '+str(period)+".xlsx")

            wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'pre_eeff':
            
            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/precatastro/"+str(period)+"/"
            #ruta +='c/tmp/precatastro/'+str(period)+'/'

            try:
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except FileNotFoundError:
                res = []
                res2 = []

                eeff = requests.get('http://52.14.249.252/api/eeff/pre/'+str(period))
                eeff = eeff.json()
                aux = json.dumps(eeff["eeff"])
                eeff = json.loads(aux)

                res.append(['',''])
                res.append(['','DATOS FONDO'])

                res2.append(['',''])
                res2.append(['','DATOS FONDO'])

                titulo = '1- EEFF '+forfecha(period)+' mon. Original'
                titulo2 = '1- EEFF '+forfecha(period)+' en Pesos'

                #aux = hoja_catastro.objects.get(pk=period)
                #hc = json.loads(aux.eeff)

                datos= [{'obj':'run_fon','dato':'RUN Fondo =>'},
                    {'obj':'nom_fon','dato':'Nombre Fondo'},
                    {'obj':'nom_adm','dato':'Administradora'},
                    {'obj':'run_adm','dato':'R.U.T. Administradora'},
                    {'obj':'clase','dato':'Clase Fondo'}]

                for d in datos:
                    fila = []
                    j = 0
                    ob = d.get('obj')
                    st = d.get('dato')
                    for linea in eeff:
                        if j == 0:
                            fila.extend(['',st,linea.get(ob)])
                            j += 1
                        else:
                            fila.extend([linea.get(ob)])
                    res.append(fila)
                res.append([''])
                res.append(['','ESTADO DE SITUACIÓN FINANCIERA al '+forfecha(period)+' (Moneda Original)',''])

                categs = []
                categs = eeff[0].keys()
                categs = list(categs)
                categs.remove('run_fon')
                categs.remove('nom_fon')
                categs.remove('run_adm')
                categs.remove('nom_adm')
                categs.remove('clase')
                
                categs2 = []
                for c in categs:
                    if c[:3] != 'pre':
                        categs2.append(c)

                categs2.sort()
                #hoja moneda original
                for cat in categs2:
                    fila = []
                    j = 0
                    aux = cat[3:]
                    cat = 'pre '+cat
                    for linea in eeff:
                        if j == 0:
                            fila.extend(['',aux,linea.get(cat)])
                            j+=1
                        else:
                            fila.extend([linea.get(cat)])
                    res.append(fila)
                    if aux.startswith('Total') or aux.startswith('Utilidad') or aux.startswith('Resultado') or aux.startswith('G-Costos f') or aux.startswith('G-Impuesto a'):
                        res.append([''])
                    if aux == 'Total Pasivo':
                        res.append(['','ESTADO DE RESULTADOS INTEGRALES (Miles de pesos)'])
                        res.append(['','INGRESOS/ PERDIDAS DE LA OPERACION'])
                #hoja en pesos
                for d in datos:
                    fila = []
                    j = 0
                    ob = d.get('obj')
                    st = d.get('dato')
                    for linea in eeff:
                        if j == 0:
                            fila.extend(['',st,linea.get(ob)])
                            j += 1
                        else:
                            fila.extend([linea.get(ob)])
                    res2.append(fila)
                res2.append([''])
                res2.append(['','ESTADO DE SITUACIÓN FINANCIERA al '+forfecha(period)+' (en Pesos)',''])

                for cat in categs2:
                    fila = []
                    j = 0
                    aux = cat[3:]
                    for linea in eeff:
                        if j == 0:
                            fila.extend(['',aux,linea.get(cat)])
                            j+=1
                        else:
                            fila.extend([linea.get(cat)])
                    res2.append(fila)
                    if aux.startswith('Total') or aux.startswith('Utilidad') or aux.startswith('Resultado') or aux.startswith('G-Costos f') or aux.startswith('G-Impuesto a'):
                        res2.append([''])
                    if aux == 'Total Pasivo':
                        res2.append(['','ESTADO DE RESULTADOS INTEGRALES (Miles de pesos)'])
                        res2.append(['','INGRESOS/ PERDIDAS DE LA OPERACION'])

                sheetx = {
                titulo : res,
                titulo2 : res2
                }
                save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

                filas = len(res)
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
                celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
                wb2[titulo].column_dimensions['A'].width = 5
                wb2[titulo].column_dimensions['B'].width = 60
                celda_estilo(wb2[titulo], "B1:B"+str(filas), 'FF000000', 'Arial', 9, True, '00BFBFBF')

                celda_estilo(wb2[titulo2], "1:1000", 'FF000000', 'Arial', 9, False, None)
                wb2[titulo2].column_dimensions['A'].width = 5
                wb2[titulo2].column_dimensions['B'].width = 60
                celda_estilo(wb2[titulo2], "B1:B"+str(filas), 'FF000000', 'Arial', 9, True, '00BFBFBF')


                wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'parametros_iniciales':
            
            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/precatastro/"+str(period)+"/"

            try:
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except FileNotFoundError:
                res = []

                param = requests.get('http://52.14.249.252/api/parametros_iniciales/'+str(period))
                param = param.json()
                aux = json.dumps(param["parametros_iniciales"])
                param = json.loads(aux)

                res.append([''])
                res.append(['','Activos Administrados por Administradora y Tipo de Fondo'])
                res.append([''])
                res.append(['','Fondo', 'RUN Fondo','Administradora','RUN Admin','Clase','Rescate','Moneda','Activos','Aportes','Reparto Patrimonio','Reparto Dividendo','Patrimonio Neto','Valores Cuota','Fecha Inicio','Fecha Termino'])

                titulo = '1- Param. Iniciales '+forfecha(period)

                for linea in param:
                    res.append(['', linea.get('nombre'), linea.get('runsvs'),linea.get('admin_nombre'),linea.get('admin_rut'),linea.get('categoria'),linea.get('tipo_inversion'),linea.get('moneda_activo'),linea.get('total_activo'),linea.get('aportes'),linea.get('reparto_patrimonio'),linea.get('reparto_dividendo'),linea.get('patrimonio_monto'),linea.get('patrimonio_obs'),linea.get('inicio'),linea.get('termino')])

                sheetx = {
                titulo : res
                }
                save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

                filas = len(res)
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
                celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
                wb2[titulo].column_dimensions['A'].width = 5
                wb2[titulo].column_dimensions['B'].width = 60
                celda_estilo(wb2[titulo], "B1:B"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "4:4", 'FF000000', 'Arial', 9, True, '00BFBFBF')

                wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'nuevos_fondos_pre':

            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/precatastro/"+str(period)+"/"

            try:
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except FileNotFoundError:
                res = []

                nuevos = requests.get('http://52.14.249.252/api/lista_nuevos/'+str(period))
                nuevos = nuevos.json()
                aux = json.dumps(nuevos["lista_nuevos"])
                nuevos = json.loads(aux)

                res.append([''])
                res.append(['','Nuevos Fondos entre del Trimestre (Millones de pesos)'])
                res.append([''])
                res.append(['','Nombre Fondo', 'RUT Fondo', 'Rescat/No Rescat', 'Inicio operaciones', 'Clasificación Acafi', 'Administradora'])


                titulo = 'Nuevos Fondos Trimestre'

                for linea in nuevos:
                    fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('rescate'), linea.get('inicio'), linea.get('clase'), linea.get('nom_adm')]
                    res.append(fila)

                sheetx = {
                titulo : res
                }
                save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

                filas = len(res)
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
                celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
                wb2[titulo].column_dimensions['A'].width = 5
                wb2[titulo].column_dimensions['B'].width = 60
                celda_estilo(wb2[titulo], "B1:B"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "4:4", 'FF000000', 'Arial', 9, True, '00BFBFBF')

                wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")


        if seccion == 'cartera_inversiones':

            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/precatastro/"+str(period)+"/"

            try:
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except FileNotFoundError:
                res = []

                arr = ["Acciones de sociedades anónimas abiertas",
                    "Derechos preferentes de suscripción de acciones de sociedades anónimas abiertas",
                    "Cuotas de fondos mutuos",
                    "Cuotas de fondos de inversión",
                    "Certificados de depósitos de valores (CDV)",
                    "Títulos que representen productos",
                    "Otros títulos de renta variable",
                    "Depósitos a plazo y otros títulos de bancos e instituciones financieras",
                    "Cartera de créditos o de cobranzas",
                    "Títulos emitidos o garantizados por Estados o Bancos Centrales",
                    "Otros títulos de deuda",
                    "Acciones no registradas",
                    "Cuotas de fondos de inversión privados",
                    "Títulos de deuda no registrados",
                    "Bienes raíces",
                    "Proyectos en desarrollo",
                    "Deuda de operaciones de leasing",
                    "Acciones de sociedades anónimas inmobiliarias y concesionarias",
                    "Otras inversiones"]

                cartera = requests.get('http://52.14.249.252/api/cartera_inversiones/'+str(period))
                cartera = cartera.json()
                aux = json.dumps(cartera["cartera_inversiones"])
                cartera = json.loads(aux)

                res.append([''])
                res.append(['','Cartera inversiones Nacional y Extranjera del periodo'])
                res.append([''])

                titulo = 'Cartera de Inversiones'
                fila2 = []
                j = 0
                for linea in cartera:
                    if j == 0:
                        fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('run_adm'),  linea.get('nom_adm'), linea.get('clase'), linea.get('rescate'),' ']
                        fila2 = ['','Nombre Fondo', 'RUT Fondo', 'Nombre Administradora', 'RUT Administradora', 'Clasificación Acafi','Rescate',' ']
                    else:
                        fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('run_adm'),  linea.get('nom_adm'), linea.get('clase'), linea.get('rescate'),' ']

                    for a in arr:
                        if j == 0:
                            fila.extend([linea.get('nacional').get(a)])
                            fila2.extend([a])
                        else:
                            fila.extend([linea.get('nacional').get(a)])
                    if j == 0:
                        fila.extend([' '])
                        fila2.extend([' '])
                    else:
                        fila.extend([' '])
                    for a in arr:
                        if j == 0:
                            fila.extend([linea.get('extranjero').get(a)])
                            fila2.extend([a])
                        else:
                            fila.extend([linea.get('extranjero').get(a)])

                    j += 1
                    if j == 1:
                        res.append(fila2)
                        res.append(fila)
                    else:
                        res.append(fila)

                sheetx = {
                titulo : res
                }
                save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

                filas = len(res)
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
                sheet = wb2[titulo]
                sheet['I3'] = 'Nacional'
                col = get_column_letter(10+len(arr))
                sheet[(col+'3')] = 'Extranjero'
                celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
                wb2[titulo].column_dimensions['A'].width = 5
                wb2[titulo].column_dimensions['B'].width = 60
                wb2[titulo].column_dimensions['H'].width = 5
                col = get_column_letter((9+len(arr)))
                wb2[titulo].column_dimensions[col].width = 5
                celda_estilo(wb2[titulo], "B1:B"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "4:4", 'FF000000', 'Arial', 9, True, '00BFBFBF')
                celda_estilo(wb2[titulo], "H5:H"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], col+"5:"+col+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')

                wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'acciones_nacionales':

            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/precatastro/"+str(period)+"/"

            try:
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except FileNotFoundError:
                res = []

                acciones = requests.get('http://52.14.249.252/api/acciones_nacionales/'+str(period))
                acciones = acciones.json()
                aux = json.dumps(acciones["acciones_nacionales"])
                acciones = json.loads(aux)

                res.append([''])
                res.append(['','Acciones Nacionales del Periodo'])
                res.append([''])
                res.append(['','Nombre Fondo', 'RUT Fondo', 'Nombre Administradora', 'RUT Administradora', 'Nombre Empresa', 'Codigo', 'Pais','Sector', 'Moneda','Valolizacion', 'Monto', 'Porcentaje', 'Unidades', 'Nemotecnico', 'Fecha vencimiento', 'Tipo Unidad', 'Cod. Pais Transaccion'])


                titulo = 'Acciones Nacionales'

                for linea in acciones:
                    fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('nom_adm'), linea.get('run_adm'), linea.get('empresa'), linea.get('codigo'),linea.get('pais'),linea.get('sector'),linea.get('moneda'),linea.get('valolizacion'),linea.get('monto'),linea.get('porcentaje'),linea.get('unidades'),linea.get('nemotecnico'),linea.get('fecha_vencimiento'),linea.get('tipo_unidades'),linea.get('cod_pais_transaccion')]
                    res.append(fila)

                sheetx = {
                titulo : res
                }
                save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

                filas = len(res)
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
                celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
                wb2[titulo].column_dimensions['A'].width = 5
                wb2[titulo].column_dimensions['B'].width = 60
                celda_estilo(wb2[titulo], "B1:B"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "4:4", 'FF000000', 'Arial', 9, True, '00BFBFBF')

                wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'cuotas_aportantes':

            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/precatastro/"+str(period)+"/"

            try:
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except FileNotFoundError:
                res = []

                cuotas = requests.get('http://52.14.249.252/api/cuotas_aportantes/'+str(period))
                cuotas = cuotas.json()
                aux = json.dumps(cuotas["cuotas_aportantes"])
                cuotas = json.loads(aux)

                res.append([''])
                res.append(['','Cuotas de aportantes del Periodo'])
                res.append([''])


                titulo = 'Cuotas aportantes'
                j = 0
                for linea in cuotas:
                    if j == 0:
                        fila2 = ['','Nombre Fondo', 'RUT Fondo', 'Nombre Administradora', 'RUT Administradora', 'Clasif. Acafi', 'Rescate', 'N° Aportantes','N° Cuotas Emitidas', 'N° Cuotas Pagadas',' ']
                        fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('nom_adm'), linea.get('run_adm'), linea.get('clase'), linea.get('rescate'),linea.get('total_aportantes'),linea.get('cuotas_emitidas'),linea.get('cuotas_pagadas'),' ']
                        datos = linea.get('composiciones')
                        filaAux1 = []
                        filaAux2 = []
                        filaAux3 = []
                        fila3 = []
                        fila4 = []
                        fila5 = []
                        for i in range(0,12):
                            try:
                                aux = datos[i]
                                fila2.extend([str(i+1)])
                                filaAux1.extend([str(i+1)])
                                filaAux2.extend([str(i+1)])
                                filaAux3.extend([str(i+1)])
                                fila.extend([aux.get('nom_ap')])
                                fila3.extend([aux.get('run_ap')])
                                fila4.extend([aux.get('pers_ap')])
                                fila5.extend([aux.get('porc_propiedad')])
                            except:
                                fila2.extend([str(i+1)])
                                fila.extend([' '])
                                fila3.extend([' '])
                                fila4.extend([' '])
                                fila5.extend([' '])
                        filaAux1.extend([' '])
                        filaAux2.extend([' '])
                        fila2.extend([' '])
                        fila2.extend(filaAux1)
                        fila2.extend(filaAux2)
                        fila2.extend(filaAux3)
                        fila.extend([' '])
                        fila.extend(fila3)
                        fila.extend([' '])
                        fila.extend(fila4)
                        fila.extend([' '])
                        fila.extend(fila5)

                    else:
                        fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('nom_adm'), linea.get('run_adm'), linea.get('clase'), linea.get('rescate'),linea.get('total_aportantes'),linea.get('cuotas_emitidas'),linea.get('cuotas_pagadas'),' ']
                        datos = linea.get('composiciones')
                        fila3 = []
                        fila4 = []
                        fila5 = []
                        for i in range(0,12):
                            try:
                                aux = datos[i]
                                fila.extend([aux.get('nom_ap')])
                                fila3.extend([aux.get('run_ap')])
                                fila4.extend([aux.get('pers_ap')])
                                fila5.extend([aux.get('porc_propiedad')])
                            except:
                                fila.extend([' '])
                                fila3.extend([' '])
                                fila4.extend([' '])
                                fila5.extend([' '])
                        fila.extend([' '])
                        fila.extend(fila3)
                        fila.extend([' '])
                        fila.extend(fila4)
                        fila.extend([' '])
                        fila.extend(fila5)
                    j += 1
                    if j == 1:
                        res.append(fila2)
                        res.append(fila)
                    else:
                        res.append(fila)

                sheetx = {
                titulo : res
                }
                save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

                filas = len(res)
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
                celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
                sheet = wb2[titulo]
                sheet['L3'] = 'Nombre Aportante'
                sheet['Y3'] = 'RUT Aportante'
                sheet['AL3'] = 'Tipo Persona'
                sheet['AY3'] = '% Propiedad'

                wb2[titulo].column_dimensions['A'].width = 5
                wb2[titulo].column_dimensions['B'].width = 60
                wb2[titulo].column_dimensions['K'].width = 5
                wb2[titulo].column_dimensions['X'].width = 5
                wb2[titulo].column_dimensions['AK'].width = 5
                wb2[titulo].column_dimensions['AX'].width = 5
                celda_estilo(wb2[titulo], "B1:B"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "4:4", 'FF000000', 'Arial', 9, True, '00BFBFBF')
                celda_estilo(wb2[titulo], "K4:K"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "X4:X"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "AK4:AK"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "AX4:AX"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')

                wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'clasif_riesgo':

            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/precatastro/"+str(period)+"/"

            try:
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except FileNotFoundError:
                res = []

                acciones = requests.get('http://52.14.249.252/api/clasif_riesgo/'+str(period))
                acciones = acciones.json()
                aux = json.dumps(acciones["clasif_riesgo"])
                acciones = json.loads(aux)

                res.append([''])
                res.append(['','Clasificacion de riesgo por Fondo'])
                res.append([''])
                res.append(['','Nombre Fondo', 'RUT Fondo', 'Clasif. Acafi','Fitch Chile Clasificadora de Riesgo Ltda.','Clasificadora de Riesgo Humphreys Ltda.','Feller Rate Clasificadora de Riesgo Ltda.','ICR Compañía Clasificadora de Riesgo Ltda.'])


                titulo = 'Clasif. Riesgos'

                for linea in acciones:
                    fila = ['',linea.get('nom_fon'), linea.get('run_fon'), linea.get('clase'), linea.get('fitch'), linea.get('humphreys'), linea.get('feller'),linea.get('icr')]
                    res.append(fila)

                sheetx = {
                titulo : res
                }
                save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)

                filas = len(res)
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
                celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
                wb2[titulo].column_dimensions['A'].width = 5
                wb2[titulo].column_dimensions['B'].width = 60
                celda_estilo(wb2[titulo], "B1:B"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "4:4", 'FF000000', 'Arial', 9, True, '00BFBFBF')

                wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if seccion == 'inv_pais':
            
            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/precatastro/"+str(period)+"/"

            try:
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
            except FileNotFoundError:
                res = []

                inv_pais = requests.get('http://52.14.249.252/api/inv_pais/'+str(period))
                inv_pais = inv_pais.json()
                aux = json.dumps(inv_pais["inv_pais"])
                inv_pais = json.loads(aux)

                res.append([''])
                res.append(['','% Participacion en Inversion por Pais para cada Fondo'])
                res.append([''])

                datos = inversion.objects.filter(periodo = period).values('pais','cod_pais_transaccion')
                paises = []
                paises_transac = []
                print('guardo json')
                for d in datos:
                    if d.get('pais') not in paises:
                        paises.append(d.get('pais'))
                        print('\t Paises')
                        print(d.get('pais'))
                    if d.get('cod_pais_transaccion') not in paises_transac:
                        paises_transac.append(d.get('cod_pais_transaccion'))
                        print('\t Paises trans')
                        print(d.get('cod_pais_transaccion'))
                
                print('guardo paises')
                
                titulo = 'Iversiones Pais'

                j = 0
                for linea in inv_pais:
                    fila1 = []
                    fila2 = []
                    if j == 0:
                        fila1.extend(['','Fondo', 'RUN Fondo','Administradora','RUN Admin','Clase', 'Rescate'])
                        fila2.extend(['', linea.get('nom_fon'), linea.get('run_fon'),linea.get('nom_adm'),linea.get('run_adm'),linea.get('categoria'),linea.get('rescate')])
                        
                        for p in paises:
                            flag = 0
                            if p == None:
                                fila1.extend(['Sin Nombre'])
                            else:
                                fila1.extend([p])
                            aux = linea.get('paises')
                            for a in aux:
                                if a.get('pais') == p:
                                    flag = 1
                                    fila2.extend([a.get('suma_porcentaje')])
                            if flag == 0:
                                fila2.extend([' '])

                        fila1.extend(['---'])
                        fila2.extend(['--'])

                        for p in paises_transac:
                            flag = 0
                            if p == None:
                                fila1.extend(['Sin Nombre'])
                            else:
                                fila1.extend([p])
                            aux = linea.get('cod_paises_transaccion')
                            for a in aux:
                                if a.get('cod_paises_transaccion') == p:
                                    flag = 1
                                    fila2.extend([a.get('suma_porcentaje')])
                            if flag == 0:
                                fila2.extend([' '])
                    else:
                        fila2.extend(['', linea.get('nom_fon'), linea.get('run_fon'),linea.get('nom_adm'),linea.get('run_adm'),linea.get('categoria'),linea.get('rescate')])

                        for p in paises:
                            flag = 0
                            aux = linea.get('paises')
                            for a in aux:
                                if a.get('pais') == p:
                                    flag = 1
                                    fila2.extend([a.get('suma_porcentaje')])
                            if flag == 0:
                                fila2.extend([' '])
                        fila2.extend(['--'])

                        for p in paises_transac:
                            flag = 0
                            aux = linea.get('cod_paises_transaccion')
                            for a in aux:
                                if p == None and a.get('cod_paises_transaccion') == None:
                                    flag = 1
                                    fila2.extend([a.get('suma_porcentaje')])
                                elif a.get('cod_paises_transaccion') == p and p != None:
                                    flag = 1
                                    fila2.extend([a.get('suma_porcentaje')])
                            if flag == 0:
                                fila2.extend([' '])
                    j += 1
                    if j == 1:
                        res.append(fila1)
                        res.append(fila2)
                    else:
                        res.append(fila2)
                print('se guardaron los datos')
                sheetx = {
                titulo : res
                }
                save_data(ruta+seccion+' '+str(period)+".xlsx", sheetx)
                print('se creo excel')
                filas = len(res)
                wb2 = load_workbook(ruta+seccion+' '+str(period)+".xlsx")
                celda_estilo(wb2[titulo], "1:1000", 'FF000000', 'Arial', 9, False, None)
                wb2[titulo].column_dimensions['A'].width = 5
                wb2[titulo].column_dimensions['B'].width = 60
                celda_estilo(wb2[titulo], "B1:B"+str(filas), 'FF000000', 'Arial', 9, False, '00BFBFBF')
                celda_estilo(wb2[titulo], "4:4", 'FF000000', 'Arial', 9, True, '00BFBFBF')

                wb2.save(filename = ruta+seccion+' '+str(period)+".xlsx")

        if os.path.exists(ruta+seccion+' '+str(period)+".xlsx"):
            with open(ruta+seccion+' '+str(period)+".xlsx", 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' +seccion+' '+str(period)+".xlsx"
                return response

    except hoja_catastro.DoesNotExist:
        pass

#importa cuotas transadas desde un .xls, .ods. ó csv
def importar_cuotastr(request):

    if request.method == "POST":
        form = Formimportarlista(request.POST, request.FILES)
        user = request.user
        filename = request.FILES['archivo'].name
        extension = filename.split(".")[-1]
        content = request.FILES['archivo'].read()
        sheet = pyexcel.get_array(file_type=extension, file_content=content)
        
        for i in range(7):
            del sheet[0]
            
        
        for fila in range(len(sheet)):
            for col in range(len(sheet[0])):
        #        if type(sheet[fila][col]) != types.UnicodeType:
                sheet[fila][col] = str(sheet[fila][col])
        
        respuesta=''
        for s in sheet:
            if s[0] != '' and s[0] != None and s[0] != 'PERIODO': #empresa
                i =1
                if len(s[0]) == 7:
                     s[0] = s[0]+'-01'
                try:
                    try:
                        ni = nemo_inversion.objects.get(pk=str(s[1]))
                            
                    except nemo_inversion.DoesNotExist:
                        ni = nemo_inversion(nemotecnico=str(s[1]))
                        ni.save()
                        respuesta += str('Se ha creado el nemotecnico '+str(s[1])+'<br>')
                        print('Se ha creado el nemotecnico '+str(s[1]))

                    try:

                        tr = transaccion_cuota.objects.get(nemotecnico=ni, fecha=str(s[0]))
                        respuesta += str('La cuota '+str(s[1])+' '+ str(s[0])+' ya existe<br>')
                    except transaccion_cuota.DoesNotExist:
                        tr = transaccion_cuota(fecha=str(s[0]), nemotecnico=ni, negocios=s[4], cuotas_transadas=s[5], monto_transado=s[6])
                        tr.save()
                        respuesta += str('Se ha creado la cuota '+str(s[1])+' --> '+ str(s[0])+'<br>')
                    print('Se ha creado la cuota '+str(s[1])+' --> '+ str(s[0]))

                    i += 1
                except IndexError:
                    pass

           #response = 1
        return render(request, 'fondo/importar_cuotastr.html', {'form': form, 'usuario': request.user, "sheet" : sheet, "i":i, "respuesta" : respuesta})

    else:
        form = Formimportarlista()
        return render(request, 'fondo/importar_cuotastr.html', {'form': form, 'usuario': request.user})
