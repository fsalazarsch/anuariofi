import json, os, datetime, unicodedata
from weasyprint import HTML, CSS
from PyPDF2 import PdfFileMerger, PdfFileReader
from io import FileIO as file
from pathlib import Path
import shutil

from django.conf import settings
from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.functions import Substr
from django.db.models import Avg, Sum
from acafi.models import fondo, fondo_corfo, movimiento, inversion,divisa, pais, categoria, serie, cuota, administrador, detalle_fondo
from acafi.deploy import escribir_log, getperiodosanteriores, get_paginador



def generar_png(fond, indice):
    comando =  "sudo phantomjs highcharts-convert.js -infile "
    comando += "ev/anuariofi/acafi/static/tmp/json/"+str(fond)+"_grafico"+str(indice)+".js "
    comando += "-outfile ev/anuariofi/acafi/static/tmp/imgs/"+str(fond)+"_"+str(indice)+".png"
    if indice == 1:
        comando += " -width 490 -height 200 -resources "
    else:
        comando += " -scale 2.5 -width 600 -resources "
    comando += "ev/anuariofi/acafi/static/bower_components/jquery/dist/jquery.js,"
    comando += "ev/anuariofi/acafi/static/bower_components/highcharts/highcharts.js"
    ruta = "ev/anuariofi/acafi/static/tmp/json/"
    print(ruta+str(fond)+"_grafico"+str(indice)+".sh")
    
    arch = open(ruta+str(fond)+"_grafico"+str(indice)+".sh", "w+", encoding="utf-8")
    arch.write(str(comando))
    arch.close()


    os.system("sudo sh "+ruta+str(fond)+"_grafico"+str(indice)+".sh")
    
    #borramos los js y sh implicados
    os.remove("ev/anuariofi/acafi/static/tmp/json/"+str(fond)+"_grafico"+str(indice)+".js")
    os.remove(ruta+str(fond)+"_grafico"+str(indice)+".sh")

#genera json para ser convertidos a imagenes
def generar_json( fond, anio):
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/json/"

    periodos = []
    label_periodo = []
    f = fondo.objects.get(pk=fond)

    #AQUI CAMBIAMOS SI ES POR TIRMESTRAL O DIARIO
    df = detalle_fondo.objects.get(fondo= f)
    if df.rentabilidad == 'D':
        ns = detalle_fondo.objects.get(pk=f).serie_antigua
        ns = ns.nombre
        #nombre serie

        s = serie.objects.get(fondo =f, nombre=ns)
        ns = ns.replace('/', '')
        cuotas = cuota.objects.filter(serie=s, fecha__lt="2018-01-01").order_by('fecha')

        for c in cuotas:
            c.fecha= c.fecha.strftime('%d/%m/%Y')
            if c.indice == None:
                c.indice = 0
            periodos.append( float (round(c.indice, 2)) )
            label_periodo.append(str(c.fecha))
    
    pers= getperiodosanteriores(f, '2012-12', str(anio)+'-12').order_by(Substr('periodo', 3), 'periodo')
    if df.rentabilidad == 'T':
        for p in pers:
            periodos.append([str(p.periodo), float (round(p.rentabilidad(), 2) )])
            label_periodo.append(str(p.periodo))

    #grafico rentablidad
    texto ="{\n\
                chart: {\n\
                    renderTo: 'graficoRentabilidad',\n\
                    type: 'line',\n\
                    marginRight: 0,\n\
                    marginBottom: 80,\n\
                    backgroundColor: '#FFFFFF',\n\
                    borderColor: '#FFFFFF',\n\
                    borderWidth: 0\n\
                },\n\
                xAxis: {\n\
                    categories: "+str(label_periodo)+",\n\
                    lineWidth : 0,\n\
                    gridLineWidth: 0,\n\
                    tickLength: 0,\n\
                    labels: {\n\
                        style: {\n\
                            font: 'normal 12px Helvetica, Arial, Geneva, sans-serif',\n\
                            lineHeight: '12px'\n\
                        }\n\
                    },\n\
                    endOnTick: false,\n\
                    showFirstLabel: true,\n\
                    startOnTick: false\n\
                },\n\
                \n\
                yAxis: {\n\
                    title: {\n\
                        text: ''\n\
                    },\n\
                    tickColor: '#C0D0E0',\n\
                    tickWidth: 1,\n\
                    tickLength: 4,\n\
                    lineWidth : 1,\n\
                    gridLineWidth: 0,\n\
                    startOnTick:false,\n\
                    plotLines: [{\n\
                    value: 0,\n\
                    width: 0,\n\
                    color: '#808080'\n\
                    }],\n\
                    tickPixelInterval: 30,\n\
                    alternateGridColor: 'rgba(112, 113, 115, 0.1)',\n\
                    labels: {\n\
                        formatter: function() {\n\
                            return Highcharts.numberFormat(this.value,0, ',', '.');\n\
                        },\n\
                        style: {\n\
                            font: 'normal 10pt Helvetica, Arial, Geneva, sans-serif'\n\
                        }\n\
                    }\n\
                },\n\
                legend: {\n\
                    layout: 'horizontal',\n\
                    align: 'right',\n\
                    verticalAlign: 'bottom',\n\
                    floating:false,\n\
                    x: 0,\n\
                    y: 15,\n\
                    borderWidth: 1,\n\
                    borderColor: 'rgba(112, 113, 115, 0.2)',\n\
                    symbolWidth: 12,\n\
                    symbolPadding: 2,\n\
                    itemMarginTop: 0,\n\
                    itemMarginBottom: 0,\n\
                    padding: 8,\n\
                    itemStyle: {\n\
                        font: 'normal 11px Helvetica, Arial, Geneva, sans-serif'\n\
                    }\n\
                },\n\
                plotOptions: {\n\
                    series: {\n\
                        lineWidth: 2,\n\
                        shadow: false,\n\
                        animation: false,\n\
                        marker: {\n\
                            enabled: false\n\
                        }\n\
                    }\n\
                },\n\
                credits: {\n\
                    enabled: false\n\
                },\n\
                title: {\n\
                    text: ''\n\
                },\n\
                exporting: {\n\
                    enabled: false\n\
                },\n\
                colors: ['rgba(61, 70, 76, 1)'],\n\
                series: [\n\
                \n\
                {\n\
                    name: 'Rentabilidad Fondo',\n\
                    data: "+str(periodos)+"    \n\
                }                \n\
                ],\n\
                pointInterval: 24 * 3600 * 1000 * 30\n\
                }"
    arch = open(ruta+str(fond)+"_grafico1.js", "w+", encoding="utf-8")
    arch.write(str(texto))
    arch.close()
    print("Generado el js del grfico1")
   
     #Se genera el png desde el json
    generar_png(fond, 1)
    
    print("Generado el png del grfico1")
   
    salida ="Generado el png del grafico1<br>"
    

    aa_periodos = list(movimiento.objects.filter(fondo=f, periodo__in = pers.values_list('periodo', flat=True), periodo__startswith="12-", etiqueta_svs='TOTAL ACTIVO').order_by('periodo').values_list('periodo', flat=True))
    aa_valores = list(movimiento.objects.filter(fondo=f, periodo__in = pers.values_list('periodo', flat=True), periodo__startswith="12-", etiqueta_svs='TOTAL ACTIVO').order_by('periodo').values_list('monto', flat=True))
        
    for idx in range(len(aa_valores)):
        aa_valores[idx] /= 1000

    #grafico estdisticas del fondo
    texto="{\n\
        chart: {\n\
            renderTo: 'graficoActivosAdministrados',\n\
            type: 'column',\n\
            backgroundColor: '#E8EAEC',\n\
            borderColor: '#E8EAEC',\n\
            borderWidth: 0,\n\
            plotBorderWidth: 1\n\
            },\n\
            title: {\n\
                text: ''\n\
            },\n\
            legend : {\n\
                enabled: false\n\
            },\n\
            credits: {\n\
                enabled: false\n\
            },\n\
            xAxis: {\n\
                categories: "+str(aa_periodos)+",\n\
                lineWidth : 0,\n\
                gridLineWidth: 0,\n\
                tickLength: 0,\n\
                labels: {\n\
                    style: {\n\
                        font: 'normal 12px Helvetica, Arial, Geneva, sans-serif',\n\
                        lineHeight: '11px'\n\
                    }\n\
                },\n\
                endOnTick: false,\n\
                showFirstLabel: true,\n\
                startOnTick: false\n\
                \n\
            },\n\
            yAxis: {\n\
                title: {\n\
                    text: ''\n\
                },\n\
                labels: {\n\
                    formatter: function() {\n\
                        return Highcharts.numberFormat(this.value,0, ',','.');\n\
                    },\n\
                    style: {\n\
                        font: 'normal 12px Helvetica, Arial, Geneva, sans-serif'\n\
                    }\n\
                },\n\
                tickColor: '#808080',\n\
                tickPixelInterval: 28,\n\
                tickWidth: 0,\n\
                tickLength: 0,\n\
                lineWidth : 0,\n\
                gridLineWidth: 1,\n\
                plotLines: [{\n\
                    value: 0,\n\
                    width: 0,\n\
                    color: '#808080'\n\
                }]\n\
            },\n\
            plotOptions: {\n\
                series: {\n\
                    animation: false,\n\
                    borderWidth: 0,\n\
                    shadow: false\n\
                }\n\
            },\n\
            tooltip:{ \n\
                formatter: function() {\n\
                        return '<b>Activos Administrados:</b>  '+Highcharts.numberFormat(this.y,0, ',','.');\n\
                    }\n\
                },\n\
            exporting: {\n\
                enabled: false\n\
            },\n\
            colors: ['rgba(61, 70, 76, 1)','rgba(61, 70, 76, 1)','rgba(61, 70, 76, 1)','rgba(61, 70, 76, 1)','rgba(61, 70, 76, 1)','rgba(103, 133, 132, 1)'],\n\
            series: [{\n\
                        name: 'Activos Administrados',\n\
                        data: "+str(aa_valores)+"\n\
                    }]        \n\
            }"
    arch = open(ruta+str(fond)+"_grafico2.js", "w+", encoding="utf-8")
    arch.write(str(texto))
    arch.close()
    #Se genera el png desde el json
    generar_png(fond, 2)
    salida +="Generado el png del grafico2<br>"
    
    divisas = inversion.objects.filter(periodo="12-" + str(anio), fondo=f).values("moneda").annotate(Sum("porcentaje"))
    paises = inversion.objects.filter(periodo="12-" + str(anio), fondo=f).values("pais").annotate(Sum("porcentaje"))
    porc_total = divisas.aggregate(Sum('porcentaje__sum'))['porcentaje__sum__sum']
    divs2 = []
    pais_es = []
    for d in divisas:
        try:
            d['moneda'] = divisa.objects.get(alias_svs=d['moneda']).nombre
        except divisa.DoesNotExist:
            d['moneda'] = "No especificada"
        try:
            divs2.append([d['moneda'], float(float(d['porcentaje__sum']) * 100 / float(porc_total))])
        except ZeroDivisionError:
            pass
    suma_principales = 0
    for p in paises:
        try:
            p['pais'] = pais.objects.get(pk=p['pais']).nombre
        except pais.DoesNotExist:
            pass
        try:
            if float(float(p['porcentaje__sum']) * 100 / float(porc_total)) > 0.05:
                pais_es.append([p['pais'], float(float(p['porcentaje__sum']) * 100 / float(porc_total))])

            pais_es = sorted(pais_es,key=lambda x: x[1], reverse=True)
            suma_principales = 0
            for index in range(5):
                try:
                    suma_principales += pais_es[index][1]
                except IndexError:
                    continue
            pais_es = sorted(pais_es,key=lambda x: x[1], reverse=True)[:5]
        except ZeroDivisionError:
            pass

    if suma_principales < 99.5 and suma_principales > 0.05:
        pais_es.append(['Otros', float(100 -suma_principales)])
        
    #print(pais_es)

    texto = "{\n\
            chart: {\n\
            type: 'pie',\n\
            backgroundColor: '#E8EAEC',\n\
            renderTo: 'graficodivisas',\n\
            borderColor: '#E8EAEC',\n\
            borderWidth: 0\n\
            },\n\
            plotOptions: {\n\
            pie: {\n\
                borderColor: '#FFFFFF',\n\
            borderWidth: 0,\n\
            center: ['50%', '50%'],\n\
            innerSize: '22%',\n\
            showInLegend: true,\n\
            size: '75%',\n\
            slicedOffset: 5,\n\
            allowPointSelect: false,\n\
            animation: false,\n\
            connectNulls: false,\n\
            cropThreshold: 300,\n\
            cursor: '',\n\
            dashStyle: null,\n\
            dataLabels: {\n\
                enabled: true,\n\
                align: 'left',\n\
                connectorWidth: 1,\n\
                connectorPadding: 5,\n\
                distance: 13,\n\
                enabled: true,\n\
                softConnector: true,\n\
                borderRadius: 0,\n\
                borderWidth: 0,\n\
                connectorColor: 'gray',\n\
                borderRadius: 0,\n\
                borderWidth: 0,\n\
                formatter: function() {\n\
                    return '<span style=\"font:normal 16px Helvetica, Arial, Geneva, sans-serif;\">'+this.point.name +'</span><br/><span style=\"color: '+this.point.color+';font: normal 16px Helvetica, Arial, Geneva, sans-serif;\">'+Highcharts.numberFormat(this.y,1)+'%</span>';\n\
                },\n\
                style:{\n\
                    font: 'normal 16px Helvetica, Arial, Geneva, sans-serif',\n\
                    lineHeight: '16px'\n\
                },\n\
                padding: 1,\n\
                rotation: 0,\n\
                shadow: false,\n\
                x: 0,\n\
                y: 0\n\
            },\n\
            enableMouseTracking: false,\n\
            id: null,\n\
            lineWidth: 2,\n\
            pointStart: 0,\n\
            pointInterval: 1,\n\
            selected: false,\n\
            shadow: false,\n\
            showCheckbox: false,\n\
            stacking: null,\n\
            showInLegend: true,\n\
            stickyTracking: false,\n\
            turboThreshold: 1000,\n\
            visible: true,\n\
            zIndex: null\n\
            }\n\
            },\n\
            title: {\n\
                text: ''\n\
            },\n\
            legend: {\n\
                enabled:false\n\
            },\n\
            credits: {\n\
                enabled: false\n\
            },\n\
            exporting: {\n\
                enabled: false\n\
            },\n\
            colors: ['rgba(61, 70, 76, 1)','rgba(241, 148, 0, 1)','rgba(158, 94, 2, 1)','rgba(227, 203, 135, 1)','rgba(103, 133, 132, 1)','rgba(112, 113, 115, 1)'],\n\
            series: [{\n\
                data: \n\
                        "+str(divs2)+"\n\
                      \n\
            }]\n\
            }"
    arch = open(ruta+str(fond)+"_grafico3.js", "w+", encoding="utf-8")
    arch.write(str(texto))
    arch.close()
    #Se genera el png desde el json
    generar_png(fond, 3)
    salida +="Generado el png del grafico3<br>"

    arch = open(ruta+str(fond)+"_grafico4.js", "w+", encoding="utf-8")
    texto = "{\n\
            chart: {\n\
            type: 'pie',\n\
            backgroundColor: '#FFFFFF',\n\
            renderTo: 'graficopais',\n\
            borderColor: '#FFFFFF',\n\
            borderWidth: 0\n\
            },\n\
            plotOptions: {\n\
            pie: {\n\
                borderColor: '#FFFFFF',\n\
            borderWidth: 0,\n\
            center: ['50%', '50%'],\n\
            innerSize: '22%',\n\
            showInLegend: true,\n\
            size: '75%',\n\
            slicedOffset: 5,\n\
            allowPointSelect: false,\n\
            animation: false,\n\
            connectNulls: false,\n\
            cropThreshold: 300,\n\
            cursor: '',\n\
            dashStyle: null,\n\
            dataLabels: {\n\
                enabled: true,\n\
                align: 'left',\n\
                connectorWidth: 1,\n\
                connectorPadding: 5,\n\
                distance: 13,\n\
                enabled: true,\n\
                softConnector: true,\n\
                borderRadius: 0,\n\
                borderWidth: 0,\n\
                connectorColor: 'gray',\n\
                borderRadius: 0,\n\
                borderWidth: 0,\n\
                formatter: function() {\n\
                    return '<span style=\"font:normal 16px Helvetica, Arial, Geneva, sans-serif;\">'+this.point.name +'</span><br/><span style=\"color: '+this.point.color+';font: normal 16px Helvetica, Arial, Geneva, sans-serif;\">'+Highcharts.numberFormat(this.y,1)+'%</span>';\n\
                },\n\
                style:{\n\
                    font: 'normal 16px Helvetica, Arial, Geneva, sans-serif',\n\
                    lineHeight: '16px'\n\
                },\n\
                padding: 1,\n\
                rotation: 0,\n\
                shadow: false,\n\
                x: 0,\n\
                y: 0\n\
            },\n\
            enableMouseTracking: false,\n\
            id: null,\n\
            lineWidth: 2,\n\
            pointStart: 0,\n\
            pointInterval: 1,\n\
            selected: false,\n\
            shadow: false,\n\
            showCheckbox: false,\n\
            stacking: null,\n\
            showInLegend: true,\n\
            stickyTracking: false,\n\
            turboThreshold: 1000,\n\
            visible: true,\n\
            zIndex: null\n\
            }\n\
            },\n\
            title: {\n\
                text: ''\n\
            },\n\
            legend: {\n\
                enabled:false\n\
            },\n\
            credits: {\n\
                enabled: false\n\
            },\n\
            exporting: {\n\
                enabled: false\n\
            },\n\
            colors: ['rgba(61, 70, 76, 1)','rgba(241, 148, 0, 1)','rgba(158, 94, 2, 1)','rgba(227, 203, 135, 1)','rgba(103, 133, 132, 1)','rgba(112, 113, 115, 1)'],\n\
            series: [{\n\
                data: \n\
                        "+str(pais_es)+"\n\
                      \n\
            }]\n\
            }"

    arch.write(str(texto))
    arch.close()
    generar_png(fond, 4)
    salida +="Generado el png del grafico4<br>"

    return HttpResponse(salida)

def generar_factsheet2(anio, paginador=1, fond=None):
    salida =""
    fondos = fondo.objects.filter(fecha_resolucion__lt = datetime.datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    fondos = fondos.exclude(inicio_operaciones__year__gt=anio)

    #fondos = fondos.filter(admin__razon_social__icontains="xlc")
    #fondos = fondos.filter(pk__contains="9347")

    if fond != None:
        fondos = fondos.filter(runsvs=fond)
    
    #if fond_inicio != None:
    #    fondos = fondos.filter(runsvs__gte =str(fond_inicio))
    #if fond_termino != None:
    #    fondos = fondos.filter(runsvs__lte=str(fond_termino))
    
    filenames = []
    index = 0
    for f in fondos:
        paginador =get_paginador(f)
        if index == 10:
            continue
        else:
            filename =settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(f.runsvs)+'.pdf'
            file = Path(filename)
            
            if not file.exists():
                generar_json(f.runsvs, anio)
                print(settings.URL_BASE+'pdf/'+str(f.runsvs)+'/'+str(anio)+"/"+str(paginador))
                html= HTML(url=settings.URL_BASE+'pdf/'+str(f.runsvs)+'/'+str(anio)+"/"+str(paginador))
                html.write_pdf(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(f.runsvs)+'.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')])
                
                os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(f.runsvs)+"_1.png")
                os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(f.runsvs)+"_2.png")
                os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(f.runsvs)+"_3.png")
                os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(f.runsvs)+"_4.png")

                #escribir_log("Generado el pdf de "+f.runsvs+"<br>","Generar_factsheet")
                ##paginador = str(int(paginador) + 2)
                index += 1
                #merger = PdfFileMerger()
                #merger.append(PdfFileReader(file(filename, 'rb')))
                #merger.write(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/factseets '+str(categ)+' '+str(anio)+'.pdf')


    #for filename in filenames:
    #    os.remove(filename)

    #escribir_log("Generado el pdf completo","Generar_factsheet")

#genera el anuario con todos los factsheet emepezando por el nro paginador
def generar_factsheet(anio, paginador=1, categ=None):

    salida =""
    fondos = fondo.objects.filter(fecha_resolucion__lt = datetime.datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('administrador__razon_social')
    fondos = fondos.exclude(inicio_operaciones__year__gt=anio)

    if categ != None:
        fondos = fondos.filter(categoria__indice=categ)
    else:
        fondos = fondos.filter('administrador__razon_social')

    #if fond_inicio != None:
    #    fondos = fondos.filter(runsvs__gte =str(fond_inicio))
    #if fond_termino != None:
    #    fondos = fondos.filter(runsvs__lte=str(fond_termino))
    
    filenames = []

    for f in fondos:
        paginador = get_paginador(f)
        generar_json(f.runsvs, anio)

        html= HTML(url=settings.URL_BASE+'pdf/'+str(f.runsvs)+'/'+str(anio)+"/"+str(paginador))
        html.write_pdf(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(f.runsvs)+'.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')])
        filenames.append(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(f.runsvs)+'.pdf')
    
        os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(f.runsvs)+"_1.png")
        os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(f.runsvs)+"_2.png")
        os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(f.runsvs)+"_3.png")
        os.remove("ev/anuariofi/acafi/static/tmp/imgs/"+str(f.runsvs)+"_4.png")

        escribir_log("Generado el pdf de "+f.runsvs+"<br>","Generar_factsheet")
        #paginador = str(int(paginador) + 2)
    
    merger = PdfFileMerger()
    for filename in filenames:
        merger.append(PdfFileReader(file(filename, 'rb')))

    merger.write(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/factseets '+str(categ)+' '+str(anio)+'.pdf')

    for filename in filenames:
        os.remove(filename)

    escribir_log("Generado el pdf completo","Generar_factsheet")

#genera anuario con facsheets corfo
def generar_factsheet_corfo(anio, paginador=1):
    salida =""
    categs = categoria.objects.exclude(indice__lt=5000).order_by('orden')
    fondos = fondo_corfo.objects.filter(tipo_fondo__in=categs, periodo ="12-2017").order_by('tipo_fondo','nombre')
    #fondos = fondo_corfo.objects.all().order_by('orden')
    #fondos = fondos.order_by('nombre')
    #fondos = fondos.filter(rut__gte =str(fond_inicio), rut__lte=str(fond_termino))
    filenames = []

    for f in fondos:
        #paginador = get_paginador(f)
        paginador = f.pagina
        html= HTML(url=settings.URL_BASE+'perfil/fondo_corfo/factsheet/'+str(f.rut)+'/'+str(anio)+'/'+str(paginador))
        html.write_pdf(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(f.rut)+'.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')])
        filenames.append(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/'+str(f.rut)+'.pdf')

        escribir_log("Generado el pdf de "+f.rut+"<br>")
        #paginador = str(int(paginador) + 1)
    
    merger = PdfFileMerger()
    for filename in filenames:
        merger.append(PdfFileReader(file(filename, 'rb')))

    merger.write(settings.STATIC_ROOT+settings.STATIC_URL+'/tmp/factsheets corfo '+str(anio)+'.pdf')

    for filename in filenames:
        os.remove(filename)

    escribir_log("Generado el pdf completo de corfo","Generar_factsheet")


def generar_png_series(fond):
    comando =  "sudo phantomjs highcharts-convert.js -infile "
    comando += "ev/anuariofi/acafi/static/series/json/"+str(fond)+"_grafico1.js "
    comando += "-outfile ev/anuariofi/acafi/static/series/imgs/"+str(fond)+"_1.png"
    comando += " -width 750 -height 500 -resources "
    comando += "ev/anuariofi/acafi/static/bower_components/jquery/dist/jquery.js,"
    comando += "ev/anuariofi/acafi/static/bower_components/highcharts/highcharts.js"
    ruta = "ev/anuariofi/acafi/static/series/json/"
    print(ruta+str(fond)+"_grafico1.sh")
    
    arch = open(ruta+str(fond)+"_grafico1.sh", "w+", encoding="utf-8")
    arch.write(str(comando))
    arch.close()


    os.system("sudo sh "+ruta+str(fond)+"_grafico1.sh")
    
    #borramos los js y sh implicados
    #os.remove("ev/anuariofi/acafi/static/series/json/"+str(fond)+"_grafico1.js")
    #os.remove(ruta+str(fond)+"_grafico1.sh")
def generar_json_series(fond, nombre):
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"series/json/"

    datos = []
    label = []
    s = serie.objects.get(fondo__runsvs=fond, nombre=nombre)
    nombre = nombre.replace('/', '')
    cuotas = cuota.objects.filter(serie=s, fecha__lt="2018-01-01").order_by('fecha')

    for c in cuotas:
        c.fecha= c.fecha.strftime('%d/%m/%Y')
        if c.indice == None:
            c.indice = 0
        datos.append( float (round(c.indice, 2)) )
        label.append(str(c.fecha))


    #grafico rentablidad
    texto ="{\n\
                chart: {\n\
                    renderTo: 'graficoRentabilidad',\n\
                    type: 'line',\n\
                    marginRight: 0,\n\
                    marginBottom: 80,\n\
                    backgroundColor: '#FFFFFF',\n\
                    borderColor: '#FFFFFF',\n\
                    borderWidth: 0\n\
                },\n\
                xAxis: {\n\
                    categories: "+str(label)+",\n\
                    lineWidth : 0,\n\
                    gridLineWidth: 0,\n\
                    tickLength: 0,\n\
                    labels: {\n\
                        style: {\n\
                            font: 'normal 9px Helvetica, Arial, Geneva, sans-serif',\n\
                            lineHeight: '9px'\n\
                        }\n\
                    },\n\
                    endOnTick: false,\n\
                    showFirstLabel: true,\n\
                    startOnTick: false\n\
                },\n\
                \n\
                yAxis: {\n\
                    title: {\n\
                        text: ''\n\
                    },\n\
                    tickColor: '#C0D0E0',\n\
                    tickWidth: 1,\n\
                    tickLength: 4,\n\
                    lineWidth : 1,\n\
                    gridLineWidth: 0,\n\
                    startOnTick:false,\n\
                    plotLines: [{\n\
                    value: 0,\n\
                    width: 0,\n\
                    color: '#808080'\n\
                    }],\n\
                    tickPixelInterval: 30,\n\
                    alternateGridColor: 'rgba(112, 113, 115, 0.1)',\n\
                    labels: {\n\
                        formatter: function() {\n\
                            return Highcharts.numberFormat(this.value,0, ',', '.');\n\
                        },\n\
                        style: {\n\
                            font: 'normal 10pt Helvetica, Arial, Geneva, sans-serif'\n\
                        }\n\
                    }\n\
                },\n\
                legend: {\n\
                    layout: 'horizontal',\n\
                    align: 'right',\n\
                    verticalAlign: 'bottom',\n\
                    floating:false,\n\
                    x: 0,\n\
                    y: 15,\n\
                    borderWidth: 1,\n\
                    borderColor: 'rgba(112, 113, 115, 0.2)',\n\
                    symbolWidth: 12,\n\
                    symbolPadding: 2,\n\
                    itemMarginTop: 0,\n\
                    itemMarginBottom: 0,\n\
                    padding: 8,\n\
                    itemStyle: {\n\
                        font: 'normal 11px Helvetica, Arial, Geneva, sans-serif'\n\
                    }\n\
                },\n\
                plotOptions: {\n\
                    series: {\n\
                        lineWidth: 2,\n\
                        shadow: false,\n\
                        animation: false,\n\
                        marker: {\n\
                            enabled: false\n\
                        }\n\
                    }\n\
                },\n\
                credits: {\n\
                    enabled: false\n\
                },\n\
                title: {\n\
                    text: ''\n\
                },\n\
                exporting: {\n\
                    enabled: false\n\
                },\n\
                colors: ['rgba(61, 70, 76, 1)'],\n\
                series: [\n\
                \n\
                {\n\
                    name: 'Rentabilidad Serie',\n\
                    data: "+str(datos)+"    \n\
                }                \n\
                ],\n\
                pointInterval: 24 * 3600 * 1000\n\
                }"
    arch = open(ruta+str(fond)+"_"+str(nombre)+"_grafico1.js", "w+", encoding="utf-8")
    arch.write(str(texto))
    arch.close()
    print("Generado el js "+ruta+str(fond)+"_"+str(nombre)+"_grafico1.js")
   
     #Se genera el png desde el json
    generar_png_series(str(fond)+"_"+str(nombre))
    #return HttpResponse('ok')

def generar_pdf_series(request, fond, nombre=None):
    if nombre == None:
        nombres_series = serie.objects.filter(fondo = fond).values_list('nombre', flat=True)
        if not os.path.exists(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(fond)+"/"):
            os.makedirs(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(fond)+"/")
        for n in nombres_series:
            generar_json_series(fond, n)
            html= HTML(url=settings.URL_BASE+'perfil/series/factsheet/'+str(fond)+'/'+str(n)+'/')
            html.write_pdf(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(fond)+"/"+str(n)+'.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')])
            os.remove("ev/anuariofi/acafi/static/series/imgs/"+str(fond)+"_"+str(n)+"_1.png")
    else:
        generar_json_series(fond, nombre)
        html= HTML(url=settings.URL_BASE+'perfil/series/factsheet/'+str(fond)+'/'+str(nombre)+'/')
        html.write_pdf(settings.STATIC_ROOT+settings.STATIC_URL+'/series/'+str(fond)+" "+str(nombre)+'.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')])
        os.remove("ev/anuariofi/acafi/static/series/imgs/"+str(fond)+"_"+str(nombre)+"_1.png")
    return HttpResponse('ok')    

def generar_factsheets_series():
    admins = administrador.objects.order_by('razon_social')
    #admins = admins.filter(razon_social__istartswith='MONEDA')
    for a in admins:
        #fondos = detalle_fondo.objects.filter(fecha_resolucion__lt = datetime.datetime.now(), vigente = True, admin = a).order_by('runsvs')
        fondos = fondo.objects.filter(inicio_operaciones__lt = '2018-01-01', vigente = True, admin = a).order_by('runsvs')
        
        if fondos == []:
            pass
        else:
            print(unicodedata.normalize('NFKD', str(a.razon_social)).encode('ascii', 'ignore').decode("utf-8"))
            for f in fondos:
                fond = f.runsvs
                try:
                    df = detalle_fondo.objects.get(fondo = f)    

                    if df.serie_antigua != None:
                        n = df.serie_antigua.nombre
                        
                        #nombres_series = serie.objects.filter(fondo = fond).values_list('nombre', flat=True)
                        if not os.path.exists(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(a.razon_social)+"/"):
                            os.makedirs(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(a.razon_social)+"/")
                            
                            shutil.chown(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(a.razon_social)+"/", 'ubuntu', 'ubuntu')
                        razon_social = unicodedata.normalize('NFKD', str(a.razon_social)).encode('ascii', 'ignore').decode("utf-8")
                        f = unicodedata.normalize('NFKD', str(f)).encode('ascii', 'ignore').decode("utf-8")

                        ns = unicodedata.normalize('NFKD', str(n)).encode('ascii', 'ignore').decode("utf-8")
                        ns = ns.replace('/', '')

                        print("Serie: "+str(ns))
                        if not os.path.exists(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(razon_social)+"/"+str(f)+'.pdf'):
                
                            generar_json_series(fond, ns)
                            html= HTML(url=settings.URL_BASE+'perfil/series/factsheet/'+str(fond)+'/'+str(ns)+'/')
                            html.write_pdf(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(razon_social)+"/"+str(f)+'.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')])
                            os.remove("ev/anuariofi/acafi/static/series/imgs/"+str(fond)+"_"+str(ns)+"_1.png" )
    
                except serie.DoesNotExist:
                    pass
                print(fond)
                escribir_log(fond,"Generar_factsheets_series")

    

