from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from acafi.models import resumen

#lista los resumenes
def listar_resumenes(request):
    lista = resumen.objects.all()

    url = ''
    filter_periodo = request.GET.get('filter_periodo')
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    
    

    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        lista = lista.filter(periodo__fondo__runsvs__icontains=filter_rut) | lista.filter(periodo__fondo__nombre__icontains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__icontains=filter_periodo)
        url += 'filter_periodo=' + filter_periodo + '&'

    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(descripcion__icontains=filter_name)
        url += 'filter_name=' + filter_name + '&'

    
    page = request.GET.get('page')
    if page is None:
        page = 1
        
    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_periodo': filter_periodo, 'filter_rut': filter_rut, 'filter_name': filter_name}
    context = {"lista": lista, "count": count, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'resumen/lista.html', merged_dict)

