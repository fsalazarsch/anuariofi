from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime

from acafi.models import serie, fondo_ext, fondoInversion, cuota, fondo
from acafi.deploy import convertir
from acafi.scraping.mini_scrapper import get_indice


def dias_entre(fecha_mayor, fecha_menor):
    date_format = "%d/%m/%Y"
    a = datetime.strptime(fecha_menor, date_format)
    b = datetime.strptime(fecha_mayor, date_format)
    delta = b - a
    return (delta.days)

#lista todas las series
def listar_series(request):
    lista = serie.objects.all()

    url = ''
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    filter_fechaini= request.GET.get('filter_fechaini')
    filter_fechater= request.GET.get('filter_fechater')
    filter_continuadora = request.GET.get('filter_continuadora')
    

    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        lista = lista.filter(fondo__runsvs__contains=filter_rut) | lista.filter(fondo__nombre__icontains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    
    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(nombre__icontains=filter_name)
        url += 'filter_name=' + filter_name + '&'

    if filter_fechaini != None and filter_fechaini != '' and filter_fechaini != 'None':
        lista = lista.filter(fecha_inicio=filter_fechaini)
        url += 'filter_fechaini=' + filter_fechaini + '&'
    
    if filter_fechater != None and filter_fechater != '' and filter_fechater != 'None':
        lista = lista.filter(fecha_termino=filter_fechater)
        url += 'filter_fechater=' + filter_fechater + '&'

    if filter_continuadora != None and filter_continuadora != '' and filter_continuadora != 'None':
        lista = lista.filter(continuadora__nombre__icontains=filter_continuadora)
        url += 'filter_continuadora=' + filter_continuadora + '&'

    page = request.GET.get('page')
    if page is None:
        page = 1


    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]


    for l in lista:
        
        rentablidades = []
        rentablidades2 = []
        rent_anios = []
        ahora = datetime.now()
        anio_actual= int(ahora.year)

        #c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
        #c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
        #c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
        #c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4) #hace 3 años
        #c5 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-6) #hace 5 años
        #ci = cuota.objects.filter(serie= l)[1] #inicio
        
        #=((c1/antigua)^(365,25/(diferencia_dias)))-1
        '''
        try:
            c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
            c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
            if c1.indice== 0 or c2.indice== 0 or c1.indice == None or c2.indice == None:
                rentablidades.append(0)
            else:

                rentablidades.append(float(c1.indice)/float(c2.indice)-1)
                rentablidades2.append(str(c1.indice)+"/"+str(c2.indice)+"-1")
        except cuota.DoesNotExist:
            rentablidades.append('N/A')
        try:
            c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
            c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
            if c3.indice== 0 or c2.indice== 0:
                rentablidades.append(0)
            #rentablidades.append( str(float(c1.indice))+'/'+str(float(c2.indice)) )
            else:
                rentablidades.append(float(c2.indice)/float(c3.indice)-1)
                rentablidades2.append(str(c2.indice)+"/"+str(c3.indice)+"-1")
        except cuota.DoesNotExist:
            rentablidades.append('N/A')
        try:
            c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
            c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4)
            if c3.indice== 0 or c4.indice== 0:
                rentablidades.append(0)
            else:
                rentablidades.append(float(c3.indice)/float(c4.indice)-1)
                rentablidades2.append(str(c3.indice)+"/"+str(c4.indice)+"-1")
        except cuota.DoesNotExist:
            rentablidades.append('N/A')
        
        #hace 3 años
        try:
            c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
            c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4)
            
            if c1.indice== 0 or c4.indice== 0 or c1==None or c4== None:
                rent_anios.append(0)
            else:
                exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), '31/12/'+str(anio_actual-4) ) ))
                rent_anios.append( ((c1.indice/c4.indice)**(exp))-1 )
        except cuota.DoesNotExist:
            rent_anios.append(0)        
        #hace 5 años
        try:
            c5 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-6) #hace 5 años
            c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
            
            if c1.indice== 0 or c5.indice== 0 or c1==None or c5== None:
                rent_anios.append(0)
            else:
                exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), '31/12/'+str(anio_actual-6) ) ))
                rent_anios.append( ((c1.indice/c5.indice)**(exp))-1 )
        except cuota.DoesNotExist:
            rent_anios.append(0)

        #inicio
        try:
            ci = cuota.objects.filter(serie= l).order_by('fecha')[:1] #inicio
            c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
            

            if c1.indice==None:
                rent_anios.append(0)
            else:
                exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), ci[0].fecha.strftime("%d/%m/%Y") ) ))
                print(str(l.nombre)+" =>"+str((c1.indice/100))+"**"+str(exp))
                rent_anios.append( ((c1.indice/100)**(exp))-1 )
        except cuota.DoesNotExist:
            rent_anios.append(0)
        '''
        



    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    try:
        filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name, 'filter_continuadora': filter_continuadora, 'filter_fechaini': filter_fechaini,  'filter_fechater': filter_fechater}
        context = {"rentablidades" : rentablidades, "rentablidades2" : rentablidades2, "rent_anios": rent_anios,  "lista": lista, "count": count, "lista": lista, 'url': url}
    except UnboundLocalError:
        rentablidades = []
        rentablidades2 = []
        rent_anios = []
        ahora = datetime.now()
        anio_actual= int(ahora.year)
        filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name, 'filter_continuadora': filter_continuadora, 'filter_fechaini': filter_fechaini,  'filter_fechater': filter_fechater}
        context = {"rentablidades" : rentablidades, "rentablidades2" : rentablidades2, "rent_anios": rent_anios,  "lista": lista, "count": count, "lista": lista, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'serie/lista.html', merged_dict)

#despliega factsheet pdf
def factsheet_series(request, fond, nombre):

    l = serie.objects.get(fondo__runsvs = fond, nombre=nombre)

    rentablidades = []
    rent_anios = []
    ahora = datetime.now()
    anio_actual= int(ahora.year) -1
    primera_cuota = cuota.objects.filter(serie= l).order_by('fecha').first()
    anio_pivote = int(primera_cuota.fecha.year)
    print(anio_pivote)
    try:

        c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
        if anio_pivote-1 == anio_actual-2:
            c2 = primera_cuota
        else:
            c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
        if c1.indice== 0 or c2.indice== 0 or c1.indice == None or c2.indice == None:
            rentablidades.append(0)
        else:

            rentablidades.append(float(c1.indice)/float(c2.indice)-1)
    except cuota.DoesNotExist:
        rentablidades.append('N/A')
    try:
        if anio_pivote-1 == anio_actual-3:

            c3 = primera_cuota
        else: 
            c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
        c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
        
        if c3.indice== 0 or c2.indice== 0 or c3.indice == None or c2.indice == None:
            rentablidades.append(0)
        #rentablidades.append( str(float(c1.indice))+'/'+str(float(c2.indice)) )
        else:
            rentablidades.append(float(c2.indice)/float(c3.indice)-1)
    except cuota.DoesNotExist:
        rentablidades.append('N/A')
    try:
        c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
        if anio_pivote-1 == anio_actual-4: 
            c4 = primera_cuota
        else:
            c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4)
        if c3.indice== 0 or c4.indice== 0 or c3.indice== None or c4.indice==None:
            rentablidades.append(0)
        else:
            rentablidades.append(float(c3.indice)/float(c4.indice)-1)
    except cuota.DoesNotExist:
        rentablidades.append('N/A')
        
        #hace 3 años
    try:
        c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
        c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4)
            
        if c1.indice== 0 or c4.indice== 0 or c1.indice==None or c4.indice== None:
            rent_anios.append(0)
        else:
            exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), '31/12/'+str(anio_actual-4) ) ))
            rent_anios.append( ((c1.indice/c4.indice)**(exp))-1 )
    except cuota.DoesNotExist:
        rent_anios.append(0)        
    #hace 5 años
    try:
        c5 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-6) #hace 5 años
        c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
            
        if c1.indice== 0 or c5.indice== 0 or c1.indice==None or c5.indice== None or c1==None or c5== None:
            rent_anios.append(0)
        else:
            exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), '31/12/'+str(anio_actual-6) ) ))
            rent_anios.append( ((c1.indice/c5.indice)**(exp))-1 )
    except cuota.DoesNotExist:
        rent_anios.append(0)

    #inicio
    try:
        ci = cuota.objects.filter(serie= l).order_by('fecha')[:1] #inicio
        c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
        
        if c1.indice==None:
            rent_anios.append(0)
        else:
            exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), ci[0].fecha.strftime("%d/%m/%Y") ) ))
            rent_anios.append( ((c1.indice/100)**(exp))-1 )
    except cuota.DoesNotExist:
        rent_anios.append(0)


    context = {"serie": l, "rentablidades" : [i * 100 for i in rentablidades], "rent_anios": [i * 100 for i in rent_anios]}        
    return render(request, "serie/factsheet.html", context)

def recalcular_serie(request, fond, nombre):
    f = fondo.objects.get(pk = fond)
    s = serie.objects.get(fondo= f, nombre=nombre)
    
    cuotas = cuota.objects.filter(serie=s)
    for c in cuotas:
        c.indice = 0
        c.save()
    HttpResponse(fond, nombre, 'ok')

