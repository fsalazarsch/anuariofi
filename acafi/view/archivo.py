from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from os import listdir, path

#lista archivos dentro de static/tmp
def listar_archivos(request):
    mypath = "ev/anuariofi/acafi/static/tmp/"
    aux = [f for f in listdir(mypath) if path.isfile(''.join((mypath, f)))]
    lista = []
    url = ''
    filter_name = request.GET.get('filter_name')
    print( str(filter_name != '') )
    if filter_name != None and filter_name != '' and filter_name != 'None':
        for l in aux:
            if filter_name in l:
                lista.append(l)
        url += 'filter_name=' + filter_name + '&'
    else:
        lista = aux

    
    page = request.GET.get('page')
    if page is None:
        page = 1
 
    count = len(lista)
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_name': filter_name}
    context = {"lista": lista, "count": count, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'archivo/lista.html', merged_dict)