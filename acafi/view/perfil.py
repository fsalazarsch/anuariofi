from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib.auth.models import User
from acafi.models import  perfil, administrador

#lista todos los perfiles no administradores
def listar_perfiles(request):
    lista = perfil.objects.all()

    url = ''
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    filter_vigente = request.GET.get('filter_vigente')
    filter_fi = request.GET.get('filter_fi')

    if filter_rut != None and filter_rut != '' and filter_rut != '':
        lista = lista.filter(usuario_id=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'
        filter_rut = int(filter_rut)

    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(administrador_id=filter_name)
        url += 'filter_name=' + filter_name + '&'

    if filter_vigente != None and filter_vigente != '' and filter_vigente != 'None':
        lista = lista.filter(vigente=filter_vigente)
        url += 'filter_vigente=' + filter_vigente + '&'

    if filter_fi != None and filter_fi != '' and filter_fi != 'None':
        lista = lista.filter(fecha_inscripcion=filter_fi)
        url += 'filter_fi=' + filter_fi + '&'

    page = request.GET.get('page')
    if page is None:
        page = 1

    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    lista_administrador = administrador.objects.all().order_by('razon_social')
    lista_user = User.objects.all().order_by('username').exclude(is_staff=1)

    filtros = {'page': page, 'filter_rut': filter_rut, 'filter_name': filter_name, 'filter_vigente': filter_vigente, "filter_fi":filter_fi}
    context = {"lista": lista, "lista_administrador":lista_administrador, "lista_user":lista_user, "count": count, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'profile/lista.html', merged_dict)

