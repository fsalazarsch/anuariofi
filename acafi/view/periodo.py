from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.functions import Substr
import csv
from acafi.models import periodo

#lista los periodos totales
def listar_periodos(request):
    lista = periodo.objects.all().order_by(Substr('periodo', 3), 'periodo')

    url = ''
    filter_periodo = request.GET.get('filter_periodo')
    filter_rut = request.GET.get('filter_rut')
    filter_vc = request.GET.get('filter_vc')
    if filter_vc != None:
        filter_vc = filter_vc.replace('.','')
        filter_vc = filter_vc.replace(',','.')

    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        lista = lista.filter(fondo__runsvs__icontains=filter_rut) | lista.filter(fondo__nombre__icontains=filter_rut)
        url += 'filter_rut=' + filter_rut + '&'

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__icontains=filter_periodo)
        url += 'filter_periodo=' + filter_periodo + '&'

    if filter_vc != None and filter_vc != '' and filter_vc != 'None':
        lista = lista.filter(valor_obs = filter_vc)
        url += 'filter_vc=' + filter_vc + '&'

    
    page = request.GET.get('page')
    if page is None:
        page = 1
    
   

    count = lista.count()

    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    for l in lista:
        print(l.fondo.inicio_operaciones)


    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_periodo': filter_periodo, 'filter_rut': filter_rut, 'filter_vc': filter_vc}
    context = {"lista": lista, "count": count, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'periodo/lista.html', merged_dict)

