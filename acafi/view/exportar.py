from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.contrib.auth.models import User
from acafi.forms import Corfo_form, Bug_form, Inversiones_corfo_form
from acafi.models import fondo, perfil, administrador, divisa, categoria, fondo_ext, movimiento, exportacion, inversion, periodo, rent, fondo_corfo, resumen
from acafi.view.usuario import enviar_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.safestring import mark_safe
from django.contrib.auth import authenticate, login
from django.template import loader
from django.db.models import Avg, Sum
import json, csv
from datetime import datetime
from django.core.exceptions import ValidationError
from io import BytesIO
import pyexcel as pyexcel
from django.db.models.functions import Substr
from acafi.deploy import get_paginador


def listar_exportaciones(request):
    lista = exportacion.objects.all()

    url = ''
    filter_name = request.GET.get('filter_name')
    
    if filter_name != None and filter_name != '' and filter_name != 'None':
        lista = lista.filter(nombre__icontains=filter_name)
        url += 'filter_name=' + filter_name + '&'

    page = request.GET.get('page')
    if page is None:
        page = 1

    count = lista.count()
    paginator = Paginator(lista, 15)
    lista = lista[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

    try:
        lista = paginator.page(page)
    except PageNotAnInteger:
        lista = paginator.page(1)
    except EmptyPage:
        lista = paginator.page(paginator.num_pages)

    filtros = {'page': page, 'filter_name': filter_name }
    context = {"lista": lista, "count": count, 'url': url}

    merged_dict = dict(list(filtros.items()) + list(context.items()))
    return render(request, 'exportacion/lista.html', merged_dict)

def exportar_datos_fondo(request):#listo

    filter_periodo = request.GET.get('filter_periodo')
    filter_rut = request.GET.get('filter_rut')
    filter_name = request.GET.get('filter_name')
    


    lista  = fondo.objects.all()
    i = 0
    data =[]
        
    

    #writer = csv.writer(response)
    #writer.writerow(['Nombre del Fondo', 'RUT', 'Administradora', 'Categoria', 'Monto Activos', 'Moneda'])

    lista  = fondo.objects.filter(vigente = True)

    if filter_rut != None and filter_rut != '' and filter_rut != 'None':
        lista = lista.filter(periodo__fondo__runsvs__contains=filter_rut) | lista.filter(periodo__fondo__nombre__icontains=filter_rut)

    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    
    i = 0
    for l in lista:

        if l.admin == None:
            admin= '--'
        else:
            admin = l.admin.razon_social
            
            data.append([admin, l.runsvs])
            fila =['Activo', 'Monto']
            data.append(fila)

        if filter_periodo == None:
            filter_periodo = "03-2018"
        
        movs = movimiento.objects.filter(fondo = l, periodo = filter_periodo)
        for m in movs:
            fila = [m.etiqueta_svs, m.monto]
            data.append(fila)

        #if l.inversion

        data.append([])          

        #writer.writerow([l.runsvs, l.nombre, vigente, admin, validado, categ, estr, comision, comentario, divisa, codigo_bolsa, bloomberg, isin, bolsa_offshore, portafolio_manager, riesgo_feller_rate, riesgo_comision, riesgo_fitch_ratings, riesgo_humphreys, riesgo_icr ])
        #i += 1
        #print(str(i)+" "+l.periodo+" "+l.fondo.runsvs)

    sheet = pyexcel.Sheet(data)
    io = BytesIO()
    sheet.save_to_memory("xlsx", io)
    output = HttpResponse(io.getvalue())
    output["Content-Disposition"] = "attachment; filename= \"catastro_activos.xlsx\""
    output["Content-type"] = "text/xlsx"
    return output#catastro

def exportar_problemas_fondo(request):#listo
    
    lista  = fondo.objects.all()
    data =[]
        
    fila = ['Rut del Fondo', 'Nombre del Rut', '2017', '2016 (malo)']
    data.append(fila)
    anios = ['2017', '2016']
    suma = []
    lista  = fondo.objects.filter(inicio_operaciones__lt = datetime.strptime('2018-01-01', '%Y-%m-%d'), inicio_operaciones__isnull=False, vigente = True )
    for l in lista:
        
        for anio in anios:

            per = periodo.objects.filter(periodo="12-2017", fondo=l)[:1]
            listResumen = resumen.objects.filter(periodo_id=per).exclude(nacional=0, extranjero=0)
            movimientos = movimiento.objects.filter(fondo=l, periodo='12-'+str(anio)).order_by('-ap')

            totales_raw = movimiento.objects.filter(fondo=l, periodo="12-"+str(anio), etiqueta_svs__icontains='total')
            totales = {total.etiqueta_svs.title(): total.monto for total in totales_raw}
            

            cartera = listResumen.exclude(descripcion__icontains='total').aggregate(suma=Sum('nacional') + Sum('extranjero'))['suma']
            if cartera == None:
                cartera = 0
            try:
                dic_mov = {
                'Disponible': movimientos.get(etiqueta_svs__icontains='efectivo').monto,
                'Cartera de Inversiones': cartera,
                }
            except movimiento.DoesNotExist:
                
                dic_mov = {
                'Disponible': 0,
                'Cartera de Inversiones': 0,
                }
            try:
                tot = totales['Total Activo']
            except KeyError:
                tot =0 
            suma.append(tot - dic_mov['Disponible'] - dic_mov['Cartera de Inversiones'])

        fila = [l.runsvs , l.nombre, suma[0], 0]
        print([l.runsvs , suma[0], suma[1]])
        suma = []
        '''
        inversiones_fondo = inversion.objects.filter(periodo="12-2017", fondo = l).aggregate(Sum('porcentaje'))
        #print(inversiones_fondo['porcentaje__sum'])
        
        if inversiones_fondo['porcentaje__sum'] != None:
            suma = inversiones_fondo['porcentaje__sum']
        else:
            suma = 0
        print( l.runsvs+" : "+str(suma) )

        if l.tipo_inversion == 0:
            res = "No Rescatable"
        else:
            res = "Rescatable"

        try:
            total2 = movimiento.objects.get(fondo=l, periodo="12-2017", etiqueta_svs='TOTAL ACTIVO')
            total2 = total2.monto
        except movimiento.DoesNotExist:
            total2 = ''
        try:
            total = movimiento.objects.get(fondo=l, periodo="12-2016", etiqueta_svs='TOTAL ACTIVO')
            total = total.monto
        except movimiento.DoesNotExist:
            total = ''

        r = rent.objects.get(periodo="12-2017", fondo= l)
        '''
        #fila = [l.nombre, l.runsvs, suma, res, l.inicio_operaciones, total, total2, r.rentab_anio_antepasado, r.rentab_anio_pasado, r.rentab_anio, r.rentab_3er_anio, r.rentab_5to_anio, r.rentab_ini_anio]
        data.append(fila)
        #writer.writerow([l.runsvs, l.nombre, vigente, admin, validado, categ, estr, comision, comentario, divisa, codigo_bolsa, bloomberg, isin, bolsa_offshore, portafolio_manager, riesgo_feller_rate, riesgo_comision, riesgo_fitch_ratings, riesgo_humphreys, riesgo_icr ])
        #i += 1
        #print(str(i)+" "+l.periodo+" "+l.fondo.runsvs)
        
    sheet = pyexcel.Sheet(data)
    io = BytesIO()
    sheet.save_to_memory("xlsx", io)
    output = HttpResponse(io.getvalue())
    output["Content-Disposition"] = "attachment; filename= \"fondos con otros activos.xlsx\""
    output["Content-type"] = "text/xlsx"
    return output

def exportar_periodos2(request):#listo
    lista  = fondo.objects.all()
    data_cp =[]
    data_vl =[]
    data_div =[]
    data_divtri =[]
    data_indice =[]
    
    header = ['Rut','Fondo'] + lista_periodos()
    #'N° cuotas pagadas (1)', 'Valor Libro Cuota (2)', 'Acumulado Año (3) Miles $',' En el Trimestre (4) Miles $', 'Por cuota (4)/(1) Pesos', 'Valor Libro+Dividendo por Cuota Pesos', 'Indice (incluye dividendos)']
    data_cp.append(header)
    data_vl.append(header)
    data_div.append(header)
    data_divtri.append(header)
    data_indice.append(header)
    

    fondos  = fondo.objects.filter(inicio_operaciones__lt = datetime.strptime('2018-01-01', '%Y-%m-%d'), inicio_operaciones__isnull=False, vigente = True )
    lista = periodo.objects.filter(fondo__in=fondos).order_by('fondo', Substr('periodo', 3), '-periodo')
    
    anterior = None
    for l in lista:

        if l.cuotas_pagadas() == None:
            cuotas_pagadas = 0
        else:
            cuotas_pagadas = l.cuotas_pagadas()

        if l.acum_anio() == None:
            acum_anio = 0
        else:
            acum_anio = l.acum_anio()

        if l.trimestre() == None:
            trimestre = 0
        else:
            trimestre = l.trimestre()

        if l.indice_rentabilidad == None:
            l.indice_rentabilidad = 0
        else:
            indice_rentabilidad = l.indice_rentabilidad()        


        print(l.fondo.runsvs+" - "+l.periodo)

        if anterior == None:
            nva_linea_cp = [l.fondo.runsvs, l.fondo.nombre, cuotas_pagadas]
            nva_linea_vl = [l.fondo.runsvs, l.fondo.nombre, l.valor_obs]
            nva_linea_div = [l.fondo.runsvs, l.fondo.nombre, acum_anio]
            nva_linea_divtri = [l.fondo.runsvs, l.fondo.nombre, trimestre]
            nva_linea_indice = [l.fondo.runsvs, l.fondo.nombre, indice_rentabilidad]
            anterior = l.fondo.runsvs
        elif anterior == l.fondo.runsvs:
            nva_linea_cp.append(cuotas_pagadas)
            nva_linea_vl.append(l.valor_obs)
            nva_linea_div.append(acum_anio)
            nva_linea_divtri.append(trimestre)
            nva_linea_indice.append(indice_rentabilidad)
        else:
            data_cp.append(nva_linea_cp)
            data_vl.append(l.valor_obs)
            data_div.append(acum_anio)
            data_divtri.append(trimestre)
            data_indice.append(indice_rentabilidad)

            nva_linea_cp = [l.fondo.runsvs, l.fondo.nombre, cuotas_pagadas]
            nva_linea_vl = [l.fondo.runsvs, l.fondo.nombre, l.valor_obs]
            nva_linea_div = [l.fondo.runsvs, l.fondo.nombre, acum_anio]
            nva_linea_divtri = [l.fondo.runsvs, l.fondo.nombre, trimestre]
            nva_linea_indice = [l.fondo.runsvs, l.fondo.nombre, indice_rentabilidad]
            anterior = l.fondo.runsvs

        

    content = { 'Nº CUOTAS PAGADAS' : data_cp, 'VALOR LIBRO' : data_vl, 'DIVIDENDO ANUAL' : data_div, 'DIVIDENDO TRIMESTRAL' : data_divtri, 'INDICE' : data_indice  }
    book = pyexcel.get_book(bookdict=content)
    io = BytesIO()
    book.save_to_memory("xlsx", io)
    

    output = HttpResponse(io.getvalue())


    output["Content-Disposition"] = "attachment; filename= \"periodos.xlsx\""
    output["Content-type"] = "text/xlsx"
    return output
#exporta en un .csv todos los periodos (5 mins tiempo)
def exportar_periodos(request):#listo
    

    filter_periodo = request.GET.get('filter_periodo')
    filter_rut = request.GET.get('filter_rut')
    

    lista  = fondo.objects.all()
    data =[]
        
    fila = ['Fondo', 'Run','Periodo','N° cuotas pagadas (1)', 'Valor Libro Cuota (2)', 'Acumulado Año (3) Miles $',' En el Trimestre (4) Miles $', 'Por cuota (4)/(1) Pesos', 'Valor Libro+Dividendo por Cuota Pesos', 'Indice (incluye dividendos)']
    data.append(fila)

    fondos  = fondo.objects.filter(inicio_operaciones__lt = datetime.strptime('2018-01-01', '%Y-%m-%d'), inicio_operaciones__isnull=False, vigente = True )
        
    lista = periodo.objects.filter(fondo__in=fondos).order_by('fondo', Substr('periodo', 3), 'periodo')

    if filter_rut != '' and filter_rut != None and filter_rut != 'None':
        lista = lista.filter(fondo__runsvs__icontains=filter_rut) | lista.filter(fondo__nombre__icontains=filter_rut)

    if filter_periodo != '' and filter_periodo != None and filter_periodo != 'None':
        lista = lista.filter(periodo__icontains=filter_periodo)
        


    i = 0
    for l in lista:

        if l.cuotas_pagadas == None:
            l.cuotas_pagadas = 0
        if l.acum_anio == None:
            l.acum_anio = 0
        if l.trimestre == None:
            l.trimestre = 0
        if l.cuota == None:
            l.cuota = 0
        if l.valorldiv == None:
            l.valorldiv = 0
        if l.rentabilidad == None:
            l.rentabilidad = 0

        print(l.fondo.runsvs+" - "+l.periodo)
        fila =[ l.fondo.nombre, l.fondo.runsvs, l.periodo, l.cuotas_pagadas(), l.valor_obs, l.acum_anio(), l.trimestre(), l.cuota(), l.valorldiv(), l.rentabilidad()]
        data.append(fila)

    sheet = pyexcel.Sheet(data)
    io = BytesIO()
    sheet.save_to_memory("xlsx", io)
    output = HttpResponse(io.getvalue())
    output["Content-Disposition"] = "attachment; filename= \"periodos.xlsx\""
    output["Content-type"] = "text/xlsx"
    return output

def exportar_fondos(request):#listo
    
    filter_name = request.GET.get('filter_name')
    filter_name2 = request.GET.get('filter_name2')
    filter_anio = request.GET.get('filter_anio')
    filter_rut = request.GET.get('filter_rut')
    
    if filter_anio == '' or filter_name == None or filter_name == 'None':
        filter_anio = datetime.today().year
    lista  = fondo.objects.filter(inicio_operaciones__lt = datetime.strptime(str(filter_anio)+'-01-01', '%Y-%m-%d'), inicio_operaciones__isnull=False, vigente = True )
    
    if filter_name != '' and filter_name != None and filter_name != 'None':
        lista = lista.filter(admin__razon_social__icontains = filter_name)

    if filter_name2 != '' and filter_name2 != None and filter_name2 != 'None':
        lista = lista.filter(nombre__icontains = filter_name2)

    if filter_rut != '' and filter_rut != None and filter_rut != 'None':
        lista = lista.filter(runsvs__icontains = filter_rut)

    i = 0
    data =[]
        
    fila =['Run cmf','Nombre del Fondo', 'Administradora',  'Estado de Validación', 'Rescat/No rescat', 'categoria', 'estrategia','comision','comentario','divisa', 'codigo bolsa', 'bloomberg', 'isin', 'bolsa offshore', 'portfolio manager', 'riesgo feller rate', 'riesgo comision', 'riesgo fitch ratings', 'riesgo humphreys', 'riesgo icr']
    data.append(fila)

    i = 0
    for l in lista:

        if l.admin== None:
            admin= '--'
        else:
            admin = l.admin.razon_social

        if l.tipo_inversion == True:
            l.tipo_inversion = 'Rescatable'
        else:
            l.tipo_inversion = 'No rescatable'

        if l.validado() == True:
            validado = 'Validado'
        else:
            validado = 'Pendiente'

        if l.categoria == None:
            categ = 'No'
        else:
            if l.categoria.indice == 0:
                categ = 'No'
            else:
                categ = 'Si'
    

        ext = fondo_ext.objects.get(pk = l.runsvs)
        if ext.estrategia == '' or ext.estrategia == None:
            estr = 'No'
        else:
            estr = 'Si'

        if ext.comision == '' or ext.comision == None:
            comision = 'No'
        else:
            comision = 'Si'

        if ext.comentario == '' or ext.comentario == None:
            comentario = 'No'
        else:
            comentario = 'Si'

        if ext.divisa == None:
            divisa = 'No'
        else:
            if ext.divisa.nombre == '':
                divisa = 'No'
            else:
                divisa = 'Si'

        if ext.codigo_bolsa == '' or ext.codigo_bolsa == None:
            codigo_bolsa = 'No'
        else:
            codigo_bolsa = 'Si'

        if ext.bloomberg == '' or ext.bloomberg == None:
            bloomberg = 'No'
        else:
            bloomberg = 'Si'

        if ext.isin == '' or ext.isin == None:
            isin = 'No'
        else:
            isin = 'Si'

        if ext.bolsa_offshore == '' or ext.bolsa_offshore == None:
            bolsa_offshore = 'No'
        else:
            bolsa_offshore = 'Si'

        if ext.portafolio_manager == '' or ext.portafolio_manager == None:
            portafolio_manager = 'No'
        else:
            portafolio_manager = 'Si'

        if ext.get_riesgo('felier_rate') == '' or ext.get_riesgo('felier_rate') == None:
            riesgo_feller_rate = 'No'
        else:
            riesgo_feller_rate = 'Si'

        if ext.get_riesgo('comision') == '' or ext.get_riesgo('comision') == None:
            riesgo_comision = 'No'
        else:
            riesgo_comision = 'Si'

        if ext.get_riesgo('fitch_ratings') == '' or ext.get_riesgo('fitch_ratings') == None:
            riesgo_fitch_ratings = 'No'
        else:
            riesgo_fitch_ratings = 'Si'

        if ext.get_riesgo('humphreys') == '' or ext.get_riesgo('humphreys') == None:
            riesgo_humphreys = 'No'
        else:
            riesgo_humphreys = 'Si'

        if ext.get_riesgo('icr') == '' or ext.get_riesgo('icr') == None:
            riesgo_icr = 'No'
        else:
            riesgo_icr = 'Si'

        fila = [l.runsvs, l.nombre, admin, validado, l.tipo_inversion, categ, estr, comision, comentario, divisa, codigo_bolsa, bloomberg, isin, bolsa_offshore, portafolio_manager, riesgo_feller_rate, riesgo_comision, riesgo_fitch_ratings, riesgo_humphreys, riesgo_icr ]
        data.append(fila)
        #writer.writerow([l.runsvs, l.nombre, vigente, admin, validado, categ, estr, comision, comentario, divisa, codigo_bolsa, bloomberg, isin, bolsa_offshore, portafolio_manager, riesgo_feller_rate, riesgo_comision, riesgo_fitch_ratings, riesgo_humphreys, riesgo_icr ])
        #i += 1
        #print(str(i)+" "+l.periodo+" "+l.fondo.runsvs)

    sheet = pyexcel.Sheet(data)
    io = BytesIO()
    sheet.save_to_memory("xlsx", io)
    output = HttpResponse(io.getvalue())
    output["Content-Disposition"] = "attachment; filename= \"fondos.xlsx\""
    output["Content-type"] = "text/xlsx"
    return output

def exportar_listado_fondos(request):#listo

    lista  = fondo.objects.filter(inicio_operaciones__lt = datetime.strptime('2018-01-01', '%Y-%m-%d'), inicio_operaciones__isnull=False, vigente = True ).order_by('tipo_inversion')
    i = 0
    data =[]
        
    fila =['Run cmf','Nombre del Fondo', 'Administradora', 'Categoria', 'Rescat/No rescat']
    data.append(fila)

    i = 0
    for l in lista:

        if l.admin== None:
            admin= '--'
        else:
            admin = l.admin.razon_social

        if l.tipo_inversion == True:
            l.tipo_inversion = 'Rescatable'
        else:
            l.tipo_inversion = 'No rescatable'

        if l.categoria == None:
            categ = '---'
        else:
            if l.categoria.indice == 0:
                categ = '---'
            else:
                categ = l.categoria.nombre
    

      

        fila = [l.runsvs, l.nombre, admin, categ, l.tipo_inversion]
        data.append(fila)
        #writer.writerow([l.runsvs, l.nombre, vigente, admin, validado, categ, estr, comision, comentario, divisa, codigo_bolsa, bloomberg, isin, bolsa_offshore, portafolio_manager, riesgo_feller_rate, riesgo_comision, riesgo_fitch_ratings, riesgo_humphreys, riesgo_icr ])
        #i += 1
        #print(str(i)+" "+l.periodo+" "+l.fondo.runsvs)

    sheet = pyexcel.Sheet(data)
    io = BytesIO()
    sheet.save_to_memory("xlsx", io)
    output = HttpResponse(io.getvalue())
    output["Content-Disposition"] = "attachment; filename= \"fondos.xlsx\""
    output["Content-type"] = "text/xlsx"
    return output

def exportar_fondos_corfo(request):
    data =[]
        
    fila = ['RUT','Nombre del Fondo','Vigente','Admin', 'Categoria']
    data.append(fila)

    lista  = fondo_corfo.objects.all()
    i = 0
    for l in lista:
        if l.admin== None:
            admin= '--'
        else:
            admin = l.admin.razon_social

        if l.tipo_fondo== None:
            categoria= '--'
        else:
            categoria = l.tipo_fondo.nombre

        if l.vigente == True:
            l.vigente = 'Vigente'
        else:
            l.vigente = 'Cerrrada'

        #if l.inversion


        fila = [l.rut, l.nombre, l.vigente, admin, categoria]
        data.append(fila)

    sheet = pyexcel.Sheet(data)
    io = BytesIO()
    sheet.save_to_memory("xlsx", io)
    output = HttpResponse(io.getvalue())
    output["Content-Disposition"] = "attachment; filename= \"fondos corfo.xlsx\""
    output["Content-type"] = "text/xlsx"
    return output

def exportar_orden_anuario(request):
    anio=2017
    categs = categoria.objects.exclude(indice=0).order_by('orden')

    lista = fondo.objects.filter(categoria__in=categs, fecha_resolucion__lt = datetime.now(), inicio_operaciones__isnull=False, vigente = True)
    lista = lista.exclude(inicio_operaciones__year__gt=anio).order_by('nombre')
    #lista  = fondo.objects.filter(vigente = True)
    data = []
    data.append(['RUT FONDO','NOMBRE FONDO','ID CATEG','NOMBRE CATEGORIA','N PAG'])
    for l in lista:
        if  l.categoria == None:
            categoria_nombre = None
            categoria_indice = None
        else:
            categoria_nombre = l.categoria.nombre
            categoria_indice = l.categoria.indice
        fila = [l.runsvs, l.nombre, categoria_indice, categoria_nombre, get_paginador(l)]
        data.append(fila)


    lista = fondo_corfo.objects.filter(tipo_fondo__in=categs).order_by('tipo_fondo','nombre')
    for l in lista:
        if  l.tipo_fondo == None:
            categoria_nombre = None
            categoria_indice = None
        else:
            categoria_nombre = l.tipo_fondo.nombre
            categoria_indice = l.tipo_fondo.indice
        fila = [l.rut, l.nombre, categoria_indice, categoria_nombre, get_paginador(l)]
        data.append(fila)    

    sheet = pyexcel.Sheet(data)
    io = BytesIO()
    sheet.save_to_memory("xlsx", io)
    output = HttpResponse(io.getvalue())
    output["Content-Disposition"] = "attachment; filename= \"orden_anuario.xlsx\""
    output["Content-type"] = "text/xlsx"
    return output#catastro  

