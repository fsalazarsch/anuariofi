# Generated by Django 2.0.2 on 2018-08-29 14:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('acafi', '0037_sector'),
    ]

    operations = [
        migrations.CreateModel(
            name='cuota',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(null=True)),
                ('moneda', models.CharField(max_length=5)),
                ('valor_libro', models.DecimalField(decimal_places=4, max_digits=12)),
                ('valor_economico', models.DecimalField(decimal_places=4, max_digits=12)),
                ('patrimonio_neto', models.BigIntegerField()),
                ('activo_total', models.BigIntegerField()),
                ('n_aportantes', models.IntegerField()),
                ('n_aportantes_inst', models.IntegerField()),
                ('agencia', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='serie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=15, null=True)),
                ('caracteristica', models.CharField(max_length=15, null=True)),
                ('fecha_inicio', models.DateField(null=True)),
                ('fecha_termino', models.DateField(null=True)),
                ('valor_inicial', models.DecimalField(decimal_places=4, default=0, max_digits=12)),
                ('continuadora', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='acafi.serie')),
                ('fondo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='acafi.fondo')),
            ],
        ),
        migrations.AddField(
            model_name='cuota',
            name='serie',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='acafi.serie'),
        ),
        migrations.AlterUniqueTogether(
            name='cuota',
            unique_together={('fecha', 'serie')},
        ),
    ]
