# Generated by Django 2.0.2 on 2018-10-17 20:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('acafi', '0054_auto_20181017_1754'),
    ]

    operations = [
        migrations.CreateModel(
            name='fondoInversion2',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('cfi', models.SlugField(verbose_name='Nemotecnico')),
                ('url', models.TextField(blank=True, max_length=255, null=True)),
                ('razon_social', models.TextField(blank=True, max_length=255, null=True)),
                ('ultimo', models.FloatField(blank=True, null=True)),
                ('var', models.FloatField(blank=True, null=True)),
                ('volumen', models.FloatField(blank=True, null=True)),
                ('monto', models.BigIntegerField(blank=True, null=True)),
                ('moneda', models.FloatField(blank=True, null=True)),
                ('precio_compra', models.FloatField(blank=True, null=True)),
                ('precio_venta', models.FloatField(blank=True, null=True)),
                ('serie', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='acafi.serie')),
                ('tipo_moneda', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='acafi.divisa')),
            ],
            options={
                'verbose_name': 'Dividendo2',
                'verbose_name_plural': 'Dividendos2',
            },
        ),
    ]
