# Generated by Django 2.0.2 on 2019-02-24 21:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acafi', '0079_auto_20190224_0148'),
    ]

    operations = [
        migrations.AddField(
            model_name='hoja_catastro',
            name='patrimonio_afi',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='hoja_catastro',
            name='patrimonio_afi_usd',
            field=models.TextField(blank=True, null=True),
        ),
    ]
