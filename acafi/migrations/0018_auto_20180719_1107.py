# Generated by Django 2.0.2 on 2018-07-19 15:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acafi', '0017_auto_20180719_1105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='administrador',
            name='email',
            field=models.EmailField(blank=True, default=None, max_length=254, null=True),
        ),
    ]
