# Generated by Django 2.0.2 on 2019-06-18 15:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acafi', '0096_auto_20190605_1631'),
    ]

    operations = [
        migrations.AddField(
            model_name='indicador',
            name='gbp',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
