# Generated by Django 2.0.2 on 2018-07-24 13:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acafi', '0021_auto_20180719_1348'),
    ]

    operations = [
        migrations.RenameField(
            model_name='fondo',
            old_name='grafico',
            new_name='desactivar_grafico',
        ),
        migrations.AddField(
            model_name='fondo',
            name='desactivar_rentabilidades',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='aportante',
            name='rut',
            field=models.CharField(max_length=15),
        ),
        migrations.AlterField(
            model_name='inversion',
            name='fecha_vencimiento',
            field=models.DateField(blank=True, null=True),
        ),
    ]
