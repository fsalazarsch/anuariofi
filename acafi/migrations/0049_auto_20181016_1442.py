# Generated by Django 2.0.2 on 2018-10-16 17:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acafi', '0048_eventoinversion_divisa'),
    ]

    operations = [
        migrations.CreateModel(
            name='myfondoInversion',
            fields=[
            ],
            options={
                'verbose_name': 'Nemo Run Serie',
                'verbose_name_plural': 'Nemos Run Series',
                'indexes': [],
                'proxy': True,
            },
            bases=('acafi.fondoinversion',),
        ),
        migrations.AlterModelOptions(
            name='fondoinversion',
            options={'verbose_name': 'Dividendo', 'verbose_name_plural': 'Dividendos'},
        ),
        migrations.AlterModelOptions(
            name='serie',
            options={'ordering': ['fondo__nombre']},
        ),
        migrations.AlterField(
            model_name='fondoinversion',
            name='cfi',
            field=models.SlugField(primary_key=True, serialize=False, verbose_name='Nemotecnico'),
        ),
    ]
