# Generated by Django 2.0.2 on 2018-10-17 20:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acafi', '0052_auto_20181017_1750'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fondoinversion',
            name='id',
            field=models.BigAutoField(primary_key=True, serialize=False),
        ),
    ]
