from django import forms
from acafi.models import categoria, fondo, divisa, administrador
from django.contrib.auth.models import User

blank_choice = [('', '----')]

niveles =(('', 'Sin Clasificación'), ('NIVEL 1','NIVEL 1'), ('NIVEL 2','NIVEL 2'), ('NIVEL 3','NIVEL 3'), ('NIVEL 4','NIVEL 4'))
eleccionccr = (('', 'Sin Clasificación'), ('ELEGIBLE', 'ELEGIBLE'), ('REPROBADO', 'REPROBADO'))
estado_corfo =(('', 'No especificada'), ('Vigente','VIGENTE'), ('Cerrada','CERRADA'))


eleccioncat = list()
cats = categoria.objects.filter(seleccionable = 1)
for c in cats:
    if(c.indice == '0'):
        eleccioncat.append((c.indice , str(c.nombre)))
    else:
        eleccioncat.append((c.indice , str(c.indice)+' '+str(c.nombre) ))

eleccionadm = list()
adms = administrador.objects.order_by('razon_social')
for a in adms:
    if(a.tiene_fondos() == True):
        eleccionadm.append((a.rut , str(a.razon_social)))
    

#formulario extendido de fondos (no fondo)
class Fondo_ext_form(forms.Form):
    runsvs = forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': ''}))
    codigo_bolsa = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': ''}))
    bloomberg = forms.SlugField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': ''}))
    isin = forms.SlugField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': ''}))
    bolsa_offshore = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': ''}))
    portafolio_manager = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': ''}))
    tipo_operaciones = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control"}), choices= eleccioncat)
    moneda = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control"}), choices= divisa.objects.values_list("divisa_id", "nombre").order_by("nombre") )
    riesgo_felier_rate = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control input-md"}), choices=niveles )
    riesgo_fitch_ratings = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control input-md"}), choices=niveles)
    riesgo_comision = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control input-md"}), choices=eleccionccr)
    riesgo_icr = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control input-md"}), choices=niveles )
    riesgo_humphreys = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control input-md"}), choices=niveles)
    estrategia = forms.CharField(required=False, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '',  'maxlength' : 600}))
    comision = forms.CharField(required=False, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '',  'maxlength' : 600}))
    comentario = forms.CharField(required=False, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'maxlength' : 600}))
    validado  = forms.BooleanField(required=False)

#asignacion de usaurios a fondo (deprecated)
class Fondo_asignar(forms.Form):
    usuario = forms.ChoiceField(required=True, widget=forms.Select(attrs={"class" : "form-control"}), choices= User.objects.filter(is_staff = 0, is_superuser=0).values_list("id","username") )

#creacion de usuarios nuevos
class Usuario_form(forms.Form):
    nombre = forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Nombre'}))
    apellido = forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Apellido'}))
    password =forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Password', 'type' : 'password'}))
    email = forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Email'}))
    fondo = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control"}), choices= fondo.objects.filter(user = None).values_list("runsvs","nombre") )

#formulario de creacion de bugs en formulario extendido y fondos corfo
class Bug_form(forms.Form):
    adm  = forms.BooleanField(required=False)
    cat  = forms.BooleanField(required=False)
    moneda  = forms.BooleanField(required=False)
    fi  = forms.BooleanField(required=False)
    fer  = forms.BooleanField(required=False)
    fir  = forms.BooleanField(required=False)
    ccr  = forms.BooleanField(required=False)
    icr  = forms.BooleanField(required=False)
    humpr  = forms.BooleanField(required=False)
    fondo = forms.CharField(widget=forms.TextInput())

#formulario corfo
class Corfo_form(forms.Form):
    rut = forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Rut'}))
    periodo = forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Periodo mm-aaaa'}))
    nombre = forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Nombre'}))
    admin = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control"}), choices= administrador.objects.values_list("rut", "razon_social").order_by("razon_social") )
    codigo_bolsa = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Codigo Bolsa'}))
    bloomberg = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Bloomberg'}))
    isin = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'ISIN'}))
    moneda = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control"}), choices= divisa.objects.values_list("divisa_id", "nombre").order_by("nombre") )
    apertura_linea = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'type': 'date'}))
    fecha_vencimiento = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'type': 'date'}))
    tipo_fondo = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control"}), choices= eleccioncat)
    
    tipo_linea = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Tipo Linea Corfo'}))
    area_interes = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Area de Inerés'}))
    patrimonio = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    portfolio_manager = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': ''}))

    estrategia = forms.CharField(required=False, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '',  'maxlength' : 500}))
    comision = forms.CharField(required=False, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '',  'maxlength' : 500}))
    comentario = forms.CharField(required=False, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'maxlength' : 500}))
    
    cifra_monto_us = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    cifra_monto_uf = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))

    cifra_lca_us = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    cifra_lca_uf = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))

    cifra_dc_us = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    cifra_dc_uf = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))

    cifra_dp_us = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    cifra_dp_uf = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))

    cifra_ne = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    cifra_estado = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control input-md"}), choices=estado_corfo )


#formulario de inversiones corfo
class Inversiones_corfo_form(forms.Form):
    inversion_empresa_1 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_1 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_2 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_2 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_3 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_3 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_4 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_4 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_5 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_5 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_6 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_6 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_7 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_7 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_8 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_8 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_9 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_9 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_10 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_10 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_11 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_11 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    inversion_empresa_12 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
    inversion_monto_12 = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'style' :'resize:none', 'placeholder': '', 'type' : 'number'}))
    
#formulario de importacion por excel
class Formimportarlista(forms.Form):
    archivo = forms.FileField(required=True)

class Fondo_exportar_adm(forms.Form):
    adm = forms.ChoiceField(required=True, widget=forms.Select(attrs={"class" : "form-control"}), choices= eleccionadm )
