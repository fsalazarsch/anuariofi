{
                chart: {
                    renderTo: 'graficoRentabilidad',
                    type: 'line',
                    marginRight: 0,
                    marginBottom: 80,
                    backgroundColor: '#FFFFFF',
                    borderColor: '#FFFFFF',
                    borderWidth: 0
                },
                xAxis: {
                    categories: ['11/12/2017', '12/12/2017', '13/12/2017', '14/12/2017', '15/12/2017', '16/12/2017', '17/12/2017', '18/12/2017', '19/12/2017', '20/12/2017', '21/12/2017', '22/12/2017', '23/12/2017', '24/12/2017', '25/12/2017', '26/12/2017', '27/12/2017', '28/12/2017', '29/12/2017', '30/12/2017', '31/12/2017'],
                    lineWidth : 0,
                    gridLineWidth: 0,
                    tickLength: 0,
                    labels: {
                        style: {
                            font: 'normal 9px Helvetica, Arial, Geneva, sans-serif',
                            lineHeight: '9px'
                        }
                    },
                    endOnTick: false,
                    showFirstLabel: true,
                    startOnTick: false
                },
                
                yAxis: {
                    title: {
                        text: ''
                    },
                    tickColor: '#C0D0E0',
                    tickWidth: 1,
                    tickLength: 4,
                    lineWidth : 1,
                    gridLineWidth: 0,
                    startOnTick:false,
                    plotLines: [{
                    value: 0,
                    width: 0,
                    color: '#808080'
                    }],
                    tickPixelInterval: 30,
                    alternateGridColor: 'rgba(112, 113, 115, 0.1)',
                    labels: {
                        formatter: function() {
                            return Highcharts.numberFormat(this.value,0, ',', '.');
                        },
                        style: {
                            font: 'normal 10pt Helvetica, Arial, Geneva, sans-serif'
                        }
                    }
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'bottom',
                    floating:false,
                    x: 0,
                    y: 15,
                    borderWidth: 1,
                    borderColor: 'rgba(112, 113, 115, 0.2)',
                    symbolWidth: 12,
                    symbolPadding: 2,
                    itemMarginTop: 0,
                    itemMarginBottom: 0,
                    padding: 8,
                    itemStyle: {
                        font: 'normal 11px Helvetica, Arial, Geneva, sans-serif'
                    }
                },
                plotOptions: {
                    series: {
                        lineWidth: 2,
                        shadow: false,
                        animation: false,
                        marker: {
                            enabled: false
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                exporting: {
                    enabled: false
                },
                colors: ['rgba(61, 70, 76, 1)'],
                series: [
                
                {
                    name: 'Rentabilidad Serie',
                    data: [100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 99.95, 99.95, 99.93, 99.71, 99.71, 99.71]    
                }                
                ],
                pointInterval: 24 * 3600 * 1000
                }