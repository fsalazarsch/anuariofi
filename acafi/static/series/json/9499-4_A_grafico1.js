{
                chart: {
                    renderTo: 'graficoRentabilidad',
                    type: 'line',
                    marginRight: 0,
                    marginBottom: 80,
                    backgroundColor: '#FFFFFF',
                    borderColor: '#FFFFFF',
                    borderWidth: 0
                },
                xAxis: {
                    categories: ['07/11/2017', '08/11/2017', '09/11/2017', '10/11/2017', '11/11/2017', '12/11/2017', '13/11/2017', '14/11/2017', '15/11/2017', '16/11/2017', '17/11/2017', '18/11/2017', '19/11/2017', '20/11/2017', '21/11/2017', '22/11/2017', '23/11/2017', '24/11/2017', '25/11/2017', '26/11/2017', '27/11/2017', '28/11/2017', '29/11/2017', '30/11/2017', '01/12/2017', '02/12/2017', '03/12/2017', '04/12/2017', '05/12/2017', '06/12/2017', '07/12/2017', '08/12/2017', '09/12/2017', '10/12/2017', '11/12/2017', '12/12/2017', '13/12/2017', '14/12/2017', '15/12/2017', '16/12/2017', '17/12/2017', '18/12/2017', '19/12/2017', '20/12/2017', '21/12/2017', '22/12/2017', '23/12/2017', '24/12/2017', '25/12/2017', '26/12/2017', '27/12/2017', '28/12/2017', '29/12/2017', '30/12/2017', '31/12/2017'],
                    lineWidth : 0,
                    gridLineWidth: 0,
                    tickLength: 0,
                    labels: {
                        style: {
                            font: 'normal 9px Helvetica, Arial, Geneva, sans-serif',
                            lineHeight: '9px'
                        }
                    },
                    endOnTick: false,
                    showFirstLabel: true,
                    startOnTick: false
                },
                
                yAxis: {
                    title: {
                        text: ''
                    },
                    tickColor: '#C0D0E0',
                    tickWidth: 1,
                    tickLength: 4,
                    lineWidth : 1,
                    gridLineWidth: 0,
                    startOnTick:false,
                    plotLines: [{
                    value: 0,
                    width: 0,
                    color: '#808080'
                    }],
                    tickPixelInterval: 30,
                    alternateGridColor: 'rgba(112, 113, 115, 0.1)',
                    labels: {
                        formatter: function() {
                            return Highcharts.numberFormat(this.value,0, ',', '.');
                        },
                        style: {
                            font: 'normal 10pt Helvetica, Arial, Geneva, sans-serif'
                        }
                    }
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'bottom',
                    floating:false,
                    x: 0,
                    y: 15,
                    borderWidth: 1,
                    borderColor: 'rgba(112, 113, 115, 0.2)',
                    symbolWidth: 12,
                    symbolPadding: 2,
                    itemMarginTop: 0,
                    itemMarginBottom: 0,
                    padding: 8,
                    itemStyle: {
                        font: 'normal 11px Helvetica, Arial, Geneva, sans-serif'
                    }
                },
                plotOptions: {
                    series: {
                        lineWidth: 2,
                        shadow: false,
                        animation: false,
                        marker: {
                            enabled: false
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                exporting: {
                    enabled: false
                },
                colors: ['rgba(61, 70, 76, 1)'],
                series: [
                
                {
                    name: 'Rentabilidad Serie',
                    data: [100.0, 99.99, 100.05, 99.8, 99.8, 99.79, 99.33, 99.09, 98.4, 98.63, 99.55, 99.55, 99.54, 100.23, 100.73, 101.68, 101.67, 101.59, 101.58, 101.58, 101.13, 100.78, 100.78, 99.9, 98.89, 98.88, 98.87, 98.75, 98.83, 98.02, 97.51, 97.5, 97.49, 97.48, 98.77, 98.82, 98.81, 99.16, 98.96, 98.96, 98.95, 99.39, 99.72, 99.77, 99.95, 100.32, 100.31, 100.31, 100.31, 100.44, 100.77, 101.23, 101.85, 101.85, 101.84]    
                }                
                ],
                pointInterval: 24 * 3600 * 1000
                }