{
                chart: {
                    renderTo: 'graficoRentabilidad',
                    type: 'line',
                    marginRight: 0,
                    marginBottom: 80,
                    backgroundColor: '#FFFFFF',
                    borderColor: '#FFFFFF',
                    borderWidth: 0
                },
                xAxis: {
                    categories: ['12/09/2016', '13/09/2016', '14/09/2016', '15/09/2016', '16/09/2016', '17/09/2016', '18/09/2016', '19/09/2016', '20/09/2016', '21/09/2016', '22/09/2016', '23/09/2016', '24/09/2016', '25/09/2016', '26/09/2016', '27/09/2016', '28/09/2016', '29/09/2016', '30/09/2016', '01/10/2016', '02/10/2016', '03/10/2016', '04/10/2016', '05/10/2016', '06/10/2016', '07/10/2016', '08/10/2016', '09/10/2016', '10/10/2016', '11/10/2016', '12/10/2016', '13/10/2016', '14/10/2016', '15/10/2016', '16/10/2016', '17/10/2016', '18/10/2016', '19/10/2016', '20/10/2016', '21/10/2016', '22/10/2016', '23/10/2016', '24/10/2016', '25/10/2016', '26/10/2016', '27/10/2016', '28/10/2016', '29/10/2016', '30/10/2016', '31/10/2016', '01/11/2016', '02/11/2016', '03/11/2016', '04/11/2016', '05/11/2016', '06/11/2016', '07/11/2016', '08/11/2016', '09/11/2016', '10/11/2016', '11/11/2016', '12/11/2016', '13/11/2016', '14/11/2016', '15/11/2016', '16/11/2016', '17/11/2016', '18/11/2016', '19/11/2016', '20/11/2016', '21/11/2016', '22/11/2016', '23/11/2016', '24/11/2016', '25/11/2016', '26/11/2016', '27/11/2016', '28/11/2016', '29/11/2016', '30/11/2016', '01/12/2016', '02/12/2016', '03/12/2016', '04/12/2016', '05/12/2016', '06/12/2016', '07/12/2016', '08/12/2016', '09/12/2016', '10/12/2016', '11/12/2016', '12/12/2016', '13/12/2016', '14/12/2016', '15/12/2016', '16/12/2016', '17/12/2016', '18/12/2016', '19/12/2016', '20/12/2016', '21/12/2016', '22/12/2016', '23/12/2016', '24/12/2016', '25/12/2016', '26/12/2016', '27/12/2016', '28/12/2016', '29/12/2016', '30/12/2016', '31/12/2016', '01/01/2017', '02/01/2017', '03/01/2017', '04/01/2017', '05/01/2017', '06/01/2017', '07/01/2017', '08/01/2017', '09/01/2017', '10/01/2017', '11/01/2017', '12/01/2017', '13/01/2017', '14/01/2017', '15/01/2017', '16/01/2017', '17/01/2017', '18/01/2017', '19/01/2017', '20/01/2017', '21/01/2017', '22/01/2017', '23/01/2017', '24/01/2017', '25/01/2017', '26/01/2017', '27/01/2017', '28/01/2017', '29/01/2017', '30/01/2017', '31/01/2017', '01/02/2017', '02/02/2017', '03/02/2017', '04/02/2017', '05/02/2017', '06/02/2017', '07/02/2017', '08/02/2017', '09/02/2017', '10/02/2017', '11/02/2017', '12/02/2017', '13/02/2017', '14/02/2017', '15/02/2017', '16/02/2017', '17/02/2017', '18/02/2017', '19/02/2017', '20/02/2017', '21/02/2017', '22/02/2017', '23/02/2017', '24/02/2017', '25/02/2017', '26/02/2017', '27/02/2017', '28/02/2017', '01/03/2017', '02/03/2017', '03/03/2017', '04/03/2017', '05/03/2017', '06/03/2017', '07/03/2017', '08/03/2017', '09/03/2017', '10/03/2017', '11/03/2017', '12/03/2017', '13/03/2017', '14/03/2017', '15/03/2017', '16/03/2017', '17/03/2017', '18/03/2017', '19/03/2017', '20/03/2017', '21/03/2017', '22/03/2017', '23/03/2017', '24/03/2017', '25/03/2017', '26/03/2017', '27/03/2017', '28/03/2017', '29/03/2017', '30/03/2017', '31/03/2017', '01/04/2017', '02/04/2017', '03/04/2017', '04/04/2017', '05/04/2017', '06/04/2017', '07/04/2017', '08/04/2017', '09/04/2017', '10/04/2017', '11/04/2017', '12/04/2017', '13/04/2017', '14/04/2017', '15/04/2017', '16/04/2017', '17/04/2017', '18/04/2017', '19/04/2017', '20/04/2017', '21/04/2017', '22/04/2017', '23/04/2017', '24/04/2017', '25/04/2017', '26/04/2017', '27/04/2017', '28/04/2017', '29/04/2017', '30/04/2017', '01/05/2017', '02/05/2017', '03/05/2017', '04/05/2017', '05/05/2017', '06/05/2017', '07/05/2017', '08/05/2017', '09/05/2017', '10/05/2017', '11/05/2017', '12/05/2017', '13/05/2017', '14/05/2017', '15/05/2017', '16/05/2017', '17/05/2017', '18/05/2017', '19/05/2017', '20/05/2017', '21/05/2017', '22/05/2017', '23/05/2017', '24/05/2017', '25/05/2017', '26/05/2017', '27/05/2017', '28/05/2017', '29/05/2017', '30/05/2017', '31/05/2017', '01/06/2017', '02/06/2017', '03/06/2017', '04/06/2017', '05/06/2017', '06/06/2017', '07/06/2017', '08/06/2017', '09/06/2017', '10/06/2017', '11/06/2017', '12/06/2017', '13/06/2017', '14/06/2017', '15/06/2017', '16/06/2017', '17/06/2017', '18/06/2017', '19/06/2017', '20/06/2017', '21/06/2017', '22/06/2017', '23/06/2017', '24/06/2017', '25/06/2017', '26/06/2017', '27/06/2017', '28/06/2017', '29/06/2017', '30/06/2017', '01/07/2017', '02/07/2017', '03/07/2017', '04/07/2017', '05/07/2017', '06/07/2017', '07/07/2017', '08/07/2017', '09/07/2017', '10/07/2017', '11/07/2017', '12/07/2017', '13/07/2017', '14/07/2017', '15/07/2017', '16/07/2017', '17/07/2017', '18/07/2017', '19/07/2017', '20/07/2017', '21/07/2017', '22/07/2017', '23/07/2017', '24/07/2017', '25/07/2017', '26/07/2017', '27/07/2017', '28/07/2017', '29/07/2017', '30/07/2017', '31/07/2017', '01/08/2017', '02/08/2017', '03/08/2017', '04/08/2017', '05/08/2017', '06/08/2017', '07/08/2017', '08/08/2017', '09/08/2017', '10/08/2017', '11/08/2017', '12/08/2017', '13/08/2017', '14/08/2017', '15/08/2017', '16/08/2017', '17/08/2017', '18/08/2017', '19/08/2017', '20/08/2017', '21/08/2017', '22/08/2017', '23/08/2017', '24/08/2017', '25/08/2017', '26/08/2017', '27/08/2017', '28/08/2017', '29/08/2017', '30/08/2017', '31/08/2017', '01/09/2017', '02/09/2017', '03/09/2017', '04/09/2017', '05/09/2017', '06/09/2017', '07/09/2017', '08/09/2017', '09/09/2017', '10/09/2017', '11/09/2017', '12/09/2017', '13/09/2017', '14/09/2017', '15/09/2017', '16/09/2017', '17/09/2017', '18/09/2017', '19/09/2017', '20/09/2017', '21/09/2017', '22/09/2017', '23/09/2017', '24/09/2017', '25/09/2017', '26/09/2017', '27/09/2017', '28/09/2017', '29/09/2017', '30/09/2017', '01/10/2017', '02/10/2017', '03/10/2017', '04/10/2017', '05/10/2017', '06/10/2017', '07/10/2017', '08/10/2017', '09/10/2017', '10/10/2017', '11/10/2017', '12/10/2017', '13/10/2017', '14/10/2017', '15/10/2017', '16/10/2017', '17/10/2017', '18/10/2017', '19/10/2017', '20/10/2017', '21/10/2017', '22/10/2017', '23/10/2017', '24/10/2017', '25/10/2017', '26/10/2017', '27/10/2017', '28/10/2017', '29/10/2017', '30/10/2017', '31/10/2017', '01/11/2017', '02/11/2017', '03/11/2017', '04/11/2017', '05/11/2017', '06/11/2017', '07/11/2017', '08/11/2017', '09/11/2017', '10/11/2017', '11/11/2017', '12/11/2017', '13/11/2017', '14/11/2017', '15/11/2017', '16/11/2017', '17/11/2017', '18/11/2017', '19/11/2017', '20/11/2017', '21/11/2017', '22/11/2017', '23/11/2017', '24/11/2017', '25/11/2017', '26/11/2017', '27/11/2017', '28/11/2017', '29/11/2017', '30/11/2017', '01/12/2017', '02/12/2017', '03/12/2017', '04/12/2017', '05/12/2017', '06/12/2017', '07/12/2017', '08/12/2017', '09/12/2017', '10/12/2017', '11/12/2017', '12/12/2017', '13/12/2017', '14/12/2017', '15/12/2017', '16/12/2017', '17/12/2017', '18/12/2017', '19/12/2017', '20/12/2017', '21/12/2017', '22/12/2017', '23/12/2017', '24/12/2017', '25/12/2017', '26/12/2017', '27/12/2017', '28/12/2017', '29/12/2017', '30/12/2017', '31/12/2017'],
                    lineWidth : 0,
                    gridLineWidth: 0,
                    tickLength: 0,
                    labels: {
                        style: {
                            font: 'normal 9px Helvetica, Arial, Geneva, sans-serif',
                            lineHeight: '9px'
                        }
                    },
                    endOnTick: false,
                    showFirstLabel: true,
                    startOnTick: false
                },
                
                yAxis: {
                    title: {
                        text: ''
                    },
                    tickColor: '#C0D0E0',
                    tickWidth: 1,
                    tickLength: 4,
                    lineWidth : 1,
                    gridLineWidth: 0,
                    startOnTick:false,
                    plotLines: [{
                    value: 0,
                    width: 0,
                    color: '#808080'
                    }],
                    tickPixelInterval: 30,
                    alternateGridColor: 'rgba(112, 113, 115, 0.1)',
                    labels: {
                        formatter: function() {
                            return Highcharts.numberFormat(this.value,0, ',', '.');
                        },
                        style: {
                            font: 'normal 10pt Helvetica, Arial, Geneva, sans-serif'
                        }
                    }
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'bottom',
                    floating:false,
                    x: 0,
                    y: 15,
                    borderWidth: 1,
                    borderColor: 'rgba(112, 113, 115, 0.2)',
                    symbolWidth: 12,
                    symbolPadding: 2,
                    itemMarginTop: 0,
                    itemMarginBottom: 0,
                    padding: 8,
                    itemStyle: {
                        font: 'normal 11px Helvetica, Arial, Geneva, sans-serif'
                    }
                },
                plotOptions: {
                    series: {
                        lineWidth: 2,
                        shadow: false,
                        animation: false,
                        marker: {
                            enabled: false
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                exporting: {
                    enabled: false
                },
                colors: ['rgba(61, 70, 76, 1)'],
                series: [
                
                {
                    name: 'Rentabilidad Serie',
                    data: [100.0, 99.49, 99.67, 100.09, 100.23, 100.21, 100.2, 100.19, 99.89, 100.18, 99.34, 99.24, 99.23, 99.22, 98.73, 99.28, 99.8, 98.63, 98.93, 98.92, 98.91, 98.73, 98.86, 99.68, 100.1, 99.75, 99.74, 99.73, 99.72, 99.07, 98.94, 99.16, 99.18, 99.17, 99.16, 98.89, 99.0, 99.15, 98.99, 98.92, 98.92, 98.91, 98.33, 97.08, 96.81, 96.06, 96.01, 96.0, 95.99, 95.99, 95.98, 94.79, 94.15, 94.59, 94.59, 94.58, 96.73, 96.54, 97.59, 98.69, 99.45, 99.45, 99.44, 100.14, 101.06, 101.83, 102.14, 102.53, 102.52, 102.52, 102.68, 102.97, 103.65, 103.57, 103.89, 103.88, 103.88, 103.06, 103.05, 102.54, 102.39, 102.04, 102.04, 102.03, 102.06, 101.46, 101.67, 101.67, 102.33, 102.33, 102.33, 101.56, 102.38, 102.37, 104.07, 104.91, 104.91, 104.9, 106.23, 106.39, 105.49, 105.84, 105.66, 105.66, 105.65, 105.53, 105.76, 104.87, 103.99, 103.89, 103.89, 103.89, 103.89, 105.25, 105.53, 104.75, 105.04, 105.03, 105.03, 105.39, 105.43, 105.85, 104.19, 104.1, 104.1, 104.1, 103.98, 103.25, 103.59, 103.54, 103.51, 103.5, 103.5, 102.46, 103.2, 103.66, 103.23, 103.19, 103.19, 103.19, 102.44, 101.93, 102.12, 101.56, 101.97, 101.97, 101.96, 101.41, 102.47, 102.72, 103.32, 103.05, 103.05, 103.05, 103.63, 103.98, 104.07, 103.85, 104.79, 104.79, 104.79, 104.77, 105.34, 105.03, 104.94, 105.89, 105.88, 105.88, 105.96, 106.3, 108.04, 108.05, 108.23, 108.23, 108.23, 108.5, 108.32, 108.3, 109.04, 109.04, 109.04, 109.04, 109.54, 109.74, 110.62, 109.16, 108.96, 108.96, 108.96, 108.96, 107.04, 107.73, 107.58, 107.63, 107.62, 107.62, 107.68, 108.87, 108.53, 108.73, 108.73, 108.73, 108.73, 107.61, 108.16, 107.1, 107.23, 107.16, 107.16, 107.15, 106.77, 106.45, 106.16, 104.99, 104.99, 104.99, 104.99, 105.35, 105.2, 105.2, 106.0, 105.97, 105.97, 105.97, 107.52, 109.24, 110.03, 109.99, 109.92, 109.92, 109.92, 109.92, 110.43, 110.59, 111.72, 112.21, 112.2, 112.2, 112.4, 112.75, 111.84, 111.41, 111.21, 111.21, 111.21, 111.25, 110.86, 109.35, 110.81, 110.73, 110.73, 110.73, 110.91, 111.86, 112.37, 112.08, 112.27, 112.27, 112.26, 112.9, 112.97, 112.61, 113.32, 113.36, 113.36, 113.36, 112.97, 112.76, 112.74, 112.46, 112.04, 112.03, 112.03, 111.51, 112.11, 111.45, 112.32, 112.02, 112.02, 112.02, 112.6, 112.31, 112.27, 112.08, 111.89, 111.89, 111.89, 111.89, 111.26, 112.13, 111.41, 111.62, 111.62, 111.62, 111.45, 111.69, 112.25, 111.39, 112.1, 112.1, 112.1, 112.19, 112.17, 112.42, 112.18, 112.29, 112.29, 112.29, 112.17, 111.95, 112.15, 111.78, 111.74, 111.74, 111.73, 111.44, 111.42, 111.51, 111.16, 111.76, 111.75, 111.75, 111.75, 111.97, 112.09, 111.56, 111.51, 111.51, 111.5, 111.83, 111.35, 111.5, 109.55, 109.73, 109.73, 109.73, 110.77, 110.77, 110.65, 108.7, 108.93, 108.93, 108.92, 108.32, 109.25, 108.62, 108.21, 107.95, 107.94, 107.94, 106.16, 106.45, 107.1, 107.8, 107.18, 107.18, 107.18, 107.0, 106.09, 106.21, 105.38, 105.62, 105.62, 105.62, 107.31, 108.15, 108.48, 108.64, 108.5, 108.5, 108.5, 108.5, 108.5, 108.16, 108.33, 108.3, 108.3, 108.3, 109.01, 109.79, 110.99, 110.91, 111.47, 111.47, 111.47, 112.36, 111.84, 111.08, 111.31, 111.94, 111.94, 111.94, 111.94, 111.28, 111.09, 110.66, 110.29, 110.28, 110.28, 109.97, 110.46, 111.1, 111.24, 112.39, 112.39, 112.39, 112.26, 112.58, 112.15, 112.01, 112.02, 112.01, 112.01, 113.54, 113.8, 113.8, 113.13, 113.19, 113.18, 113.18, 113.83, 113.87, 113.72, 113.3, 113.01, 113.0, 113.0, 113.4, 112.9, 112.59, 113.16, 112.52, 112.52, 112.52, 114.13, 114.61, 114.36, 114.33, 114.41, 114.41, 114.41, 114.84, 116.85, 117.03, 118.56, 119.03, 119.03, 119.03, 118.62, 118.98, 119.27, 119.98, 119.98, 119.98, 119.98, 121.11, 121.27, 120.2, 118.29, 119.05, 119.05, 119.05, 116.4, 115.47, 115.33, 115.6, 115.81, 115.81, 115.81, 115.81, 115.28, 114.75, 114.52, 113.92, 113.92, 113.92]    
                }                
                ],
                pointInterval: 24 * 3600 * 1000
                }