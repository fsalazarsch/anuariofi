
import datetime, json, xlwt, os, unicodedata, requests, re, csv
from zipfile import ZipFile
from io import BytesIO
from pathlib import Path
from json import JSONDecodeError
from django.db import models
from django.db.models import Sum
from django.contrib import admin, messages
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db.models import Q, F
from django.utils.translation import gettext_lazy as _
from django_mysql.models import JSONField
from django.utils.safestring import mark_safe
from django.contrib import admin
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from django.contrib.admin.models import LogEntry
from django.conf import settings
from weasyprint import HTML, CSS
from bs4 import BeautifulSoup, element, SoupStrainer

def limpiar(llave):
    # deja en minúscula, elimina espacios extra, y elimina símbolos que no permitan usar la llave
    llave = llave.strip().lower().replace(' ', '_')
    llave = unicodedata.normalize('NFKD', llave)
    llave = re.sub(r'\W+', '', llave)
    llave = llave.replace('_', ' ')
    return llave.strip()

def revisar(numero) :
    retorno = str(numero)
    if len(retorno) < 2 :
        retorno = '0' + retorno
    return retorno

def format_time(var):
    #print(var)
    aux = var.split('/')
    return aux[2]+'-'+aux[1]+'-'+aux[0]
def format_num(strnum): #formato español a ingles
    try:
        if '.' in strnum:
            strnum = strnum.replace('.','')
        if ',' in strnum:
            strnum = strnum.replace(',','.')
        #if strnum.count(".") > 1:
        #    strnum = strnum.replace('.','')
            
        return float(strnum)
    except ValueError:
        return 0


#from acafi.view.graficos import generar_json_series

admin.site.register(LogEntry)

def dias_entre(fecha_mayor, fecha_menor):
    date_format = "%d/%m/%Y"
    a = datetime.datetime.strptime(fecha_menor, date_format)
    b = datetime.datetime.strptime(fecha_mayor, date_format)
    delta = b - a
    return (delta.days)

class comuna(models.Model):
    nombre = models.CharField(max_length=50)
    def __str__(self):
        return self.nombre
class comunaAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre']
    search_fields = ['id', 'nombre']
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = 'id', 'nombre'

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        font_style = xlwt.XFStyle()
        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.id, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
        wb.save(response)
        return response
    export_as_csv.short_description = "Exportar"
    
admin.site.register(comuna, comunaAdmin)

class region(models.Model):
    nombre = models.CharField(max_length=50)
    def __str__(self):
        return self.nombre
class regionAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre']
    search_fields = ['id', 'nombre']
    actions = ["export_as_csv"]
    
    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = 'id', 'nombre'

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        font_style = xlwt.XFStyle()
        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.id, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
        wb.save(response)
        return response
    export_as_csv.short_description = "Exportar"
admin.site.register(region, regionAdmin)

class categoriaAntigua(models.Model):
    indice = models.CharField(max_length=100, primary_key=True)
    categoria_madre = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    nombre = models.CharField(max_length=255)
    seleccionable = models.BooleanField(default=True)
    orden = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.indice+" "+self.nombre

    def nombrecodigo(self, obj):
        return obj.indice+" "+obj.nombre

class categoriaAntiguaAdmin(admin.ModelAdmin):
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = 'indice', 'nombre', 'categoria madre indice', 'categoria madre nombre'

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        font_style = xlwt.XFStyle()
        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.indice, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
            if (obj.categoria_madre):
                ws.write(row_num, 2, obj.categoria_madre.indice)
                ws.write(row_num, 3, obj.categoria_madre.nombre)
        wb.save(response)
        return response

    export_as_csv.short_description = "Exportar"
    search_fields = ['indice', 'nombre', 'categoria_madre__indice', 'categoria_madre__nombre']
    list_display = ['indice', 'nombre', 'categoria_madre']
admin.site.register(categoriaAntigua, categoriaAntiguaAdmin)


class categoria(models.Model):
    indice = models.CharField(max_length=100, primary_key=True)
    categoria_madre = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    nombre = models.CharField(max_length=255)
    seleccionable = models.BooleanField(default=True)
    orden = models.IntegerField(null=True, blank=True)
    def nombrecodigo(self, obj):
        return obj.indice+" "+obj.nombre

    def tiene_fondos(self):
        if fondo.objects.filter(categoria=self).exists():
            return True
        else:
            return False

    def __str__(self):
        return self.indice+" "+self.nombre
class categoriaAdmin(admin.ModelAdmin):
    actions = ["export_as_csv"]
    def tiene_fondos(self):
        self.tiene_fondos()

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = 'indice', 'nombre', 'Tiene fondos', 'categoria madre indice', 'categoria madre nombre'

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        font_style = xlwt.XFStyle()
        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.indice, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
            ws.write(row_num, 2, obj.tiene_fondos(), font_style)
            if (obj.categoria_madre):
                ws.write(row_num, 3, obj.categoria_madre.indice)
                ws.write(row_num, 4, obj.categoria_madre.nombre)
        wb.save(response)
        return response

    export_as_csv.short_description = "Exportar"
    search_fields = ['indice', 'nombre', 'categoria_madre__indice', 'categoria_madre__nombre']
    list_display = ['indice', 'nombre', 'categoria_madre']

admin.site.register(categoria, categoriaAdmin)

class pais(models.Model):
    codigo =  models.CharField(max_length=5, primary_key=True)
    nombre =  models.CharField(max_length=100)
    region = models.ForeignKey(region, null=True, on_delete=models.SET_NULL)    
    
    def __str__(self):
        return self.nombre
    class Meta:
        verbose_name_plural = 'Paises'
class paisAdmin(admin.ModelAdmin):
    list_display = ['codigo', 'nombre', 'region']
    search_fields = ['nombre', 'region__nombre']
    actions = ["Exportar"]

    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['codigo', 'nombre', 'region']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.codigo, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
            if obj.region:
                ws.write(row_num, 2, obj.region.nombre, font_style)
        wb.save(response)
        return response
admin.site.register(pais, paisAdmin)

class administrador(models.Model):
    rut = models.CharField(max_length=100, primary_key=True)
    razon_social = models.CharField(max_length=100)     # id est nombre de la empresa
    nombre_fantasia = models.CharField(max_length=100, null=True, blank=True)       # idk sale abajito
    vigente = models.NullBooleanField()
    inscripcion = models.IntegerField(null=True, blank=True)
    fecha_inscripcion = models.DateField(null=True, blank=True)
    fecha_cancelacion = models.DateField(null=True, blank=True)    
    direccion = models.CharField(max_length=100, null=True)
    ciudad = models.CharField(max_length=50, null=True)
    pais = models.CharField(default='CL', max_length=50, null=True)
    telefono = models.IntegerField(null=True)
    email = models.EmailField(null=True, blank=True, default=None)
    website = models.URLField(null=True, blank=True)
    urlsvs = models.CharField(max_length=255, null=True, default=None)
    comuna = models.ForeignKey(comuna, null=True, on_delete=models.SET_NULL)    
    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'

    def __str__(self):
        r = self.rut.split("-")
        pi = "{:,d}".format( int(r[0]) )
        pi = pi.replace(",",".")
        return self.razon_social+" ("+pi+"-"+str(r[1])+")"
    def tiene_fondos(self, fecha=None):
        if fecha is None:
            fecha = datetime.datetime.now()
        if fondo.objects.filter(admin = self, fecha_resolucion__lt = fecha, inicio_operaciones__isnull=False, vigente = True).exists():
            return True
        else:
            False


    def get_fondos(self):
        if self.tiene_fondos() == True:
            fondos = fondo.objects.filter(admin = self, fecha_resolucion__lt = datetime.datetime.now(), inicio_operaciones__isnull=False, vigente = True)
            res = ""
            for f in fondos:
                res += str(f.runsvs)+","
            return res
        else:
            return ''

class administradorAdmin(admin.ModelAdmin):
    actions = ["export_as_csv"]
    def tiene_fondos(self):
         return self.tiene_fondos()

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['rut', 'razón social', 'nombre de fantasía', 'vigente', 'tiene_fondos', 'fecha_inscripcion', 'comuna__nombre']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.rut, font_style)
            ws.write(row_num, 1, obj.razon_social, font_style)
            ws.write(row_num, 2, obj.nombre_fantasia)
            ws.write(row_num, 3, obj.vigente)
            ws.write(row_num, 4, obj.tiene_fondos())
            
            if obj.fecha_inscripcion:
                ws.write(row_num, 5, obj.fecha_inscripcion.strftime('%d/%m/%Y'), font_style)
            if obj.comuna:
                ws.write(row_num, 6, obj.comuna.nombre)
            
        wb.save(response)

        return response


    export_as_csv.short_description = "Exportar"
    list_display = ['rut', 'razon_social', 'nombre_fantasia', 'vigente', 'fecha_inscripcion', 'comuna']
    search_fields = ['rut', 'razon_social']
    list_filter = (
        ('fecha_inscripcion', DateRangeFilter), 'vigente', )
    ordering = ['razon_social']

admin.site.register(administrador, administradorAdmin)

class divisa(models.Model):
    divisa_id = models.CharField(max_length=5, primary_key=True)
    nombre = models.CharField(max_length=100)
    alias_svs = models.CharField(max_length=5, null=True)
    simbologia = models.CharField(max_length=15, null=True)
    plural = models.CharField(max_length=25, null=True)
    equivalencia_pesos = models.FloatField(blank=True, null=True)


    def __str__(self):
        return self.nombre
class divisaAdmin(admin.ModelAdmin):
    list_display = ['divisa_id', 'nombre', 'alias_svs', 'simbologia','plural']
    search_fields = ['divisa_id', 'nombre', 'alias_svs']
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['divisa_id', 'nombre', 'alias svs', 'simbologia', 'plural']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.divisa_id, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
            ws.write(row_num, 2, obj.alias_svs)
            ws.write(row_num, 3, obj.simbologia)
            ws.write(row_num, 4, obj.plural)
        wb.save(response)
        return response


    export_as_csv.short_description = "Exportar"
admin.site.register(divisa, divisaAdmin)

class fondo_ext(models.Model) :
    runsvs = models.SlugField(primary_key=True)
    codigo_bolsa = models.TextField(null=True, blank=True)
    bloomberg = models.TextField(null=True, blank=True)
    isin = models.TextField(null=True, blank=True)
    bolsa_offshore = models.CharField(max_length=100, null=True, blank=True)
    benchmark = models.CharField(max_length=100, null=True, blank=True)
    riesgo = models.TextField(blank=True)
    estrategia = models.TextField()
    comision = models.TextField(null=True)
    comentario = models.TextField(null=True, blank=True, default=None)
    divisa = models.ForeignKey(divisa, null=True, on_delete=models.SET_NULL)    
    patrimonio = models.DecimalField(null=True, blank=True, max_digits=12, decimal_places=4)
    portafolio_manager = models.CharField(max_length=200, null=True, blank=True)
    validado = models.BooleanField(default= False, blank=True)
    fecha_edicion = models.DateField(auto_now_add=True)

    def get_riesgo(obj, llave):
        try:
            riesg = json.loads(obj.riesgo)
            return str(riesg[str(llave)])
        except KeyError:
            return None
        except JSONDecodeError:
            return None

    def get_porcentaje_madre(obj):
        total = fondo_ext.objects.aggregate(Sum('patrimonio'))['patrimonio__sum']
        porc =  float(obj.patrimonio) * 100 /float(total)
        return float(porc)

    class Meta:
        verbose_name = 'Formulario Fondo Inversión'
        verbose_name_plural = 'Formularios Fondos de Inversión'

class fondo_extAdmin(admin.ModelAdmin):
    list_display = ['runsvs', 'codigo_bolsa', 'bloomberg', 'isin', 'benchmark', 'patrimonio', 'divisa', 'riesgo', 'estrategia', 'comision', 'comentario', 'validado']
    search_fields = ['runsvs', 'divisa__nombre']
    def divisa(self, obj):
        return obj.divisa.nombre

admin.site.register(fondo_ext, fondo_extAdmin)

class nemo_inversion(models.Model):
    nemotecnico = models.SlugField(primary_key=True)
    def __str__(self):
        return self.nemotecnico
class nemo_inversionAdmin(admin.ModelAdmin):
    list_display = ['nemotecnico']
    search_fields = ['nemotecnico']
    actions = ["Exportar"]

    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['nemotecnico']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.nemotecnico, font_style)
        wb.save(response)
        return response
admin.site.register(nemo_inversion, nemo_inversionAdmin)     


class fondo(models.Model):
    runsvs = models.SlugField(primary_key=True)
    nombre = models.CharField(max_length=100)
    vigente = models.NullBooleanField()
    tipo_inversion = models.IntegerField(null=True) #RESCATABLE 1 // NO RESCATABLE 0
    #moneda = 
    admin = models.ForeignKey(administrador, on_delete=models.SET_NULL, null=True)
    fecha_resolucion = models.DateField(null=True)
    inicio_operaciones = models.DateField(null=True, blank=True)
    termino_operaciones = models.DateField(null=True, blank=True)
    proxima_renovacion = models.DateField(null=True, blank=True)
    urlsvs = models.CharField(max_length=255, null=True, default=None)
    resolucion = models.IntegerField(null=True, blank=True)
    categoria = models.ForeignKey(categoria, on_delete=models.SET_NULL, null=True, blank=True)
    categoriaAntigua = models.ForeignKey(categoriaAntigua, on_delete=models.SET_NULL, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    desactivar_grafico = models.BooleanField(default=False)
    desactivar_rentabilidades = models.BooleanField(default=False)
    pagina = models.IntegerField(null=True, blank=True)
    def __str__(self):
        return '[' + str(self.runsvs) + '] ' + str(self.nombre)
    def validado(obj):
        return fondo_ext.objects.get(pk = obj.runsvs).validado
    def descargable(self):
        my_file = Path("ev/anuariofi/acafi/static/tmp/"+self.runsvs+".pdf")
        if my_file.exists():
            return True
        else:
            return False
    def anios_totales(self):
        anio_min = 2005
        anio_max = 2018
        if self.inicio_operaciones == None:
            return 0
        if int(self.inicio_operaciones.year) <= anio_min:
            return anio_max -anio_min+1
        else:
            return anio_max -int(self.inicio_operaciones.year)+1
    def nemotecnicos(self):
        fe = fondo_ext.objects.get(pk=self.runsvs)
        if fe != None:
            return fe.codigo_bolsa
        else:
            return None


class fondoAdmin(admin.ModelAdmin):
    list_display = ['runsvs', 'nombre', 'vigente', 'admin', 'categoria', 'inicio_operaciones','termino_operaciones']
    search_fields = ['runsvs', 'nombre']
    actions = ["Exportar"]

    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['runsvs', 'nombre', 'vigente', 'Rescat/No rescat', 'admin', 'categoria', 'inicio operaciones', 'termino operaciones', 'desactivar grafico','desactivar rentabilidades', 'categoria antigua']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.runsvs, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
            ws.write(row_num, 2, obj.vigente)
            if obj.tipo_inversion == 1:
                ws.write(row_num, 3, 'R')
            else:
                ws.write(row_num, 3, 'NR')
            if obj.admin:
                ws.write(row_num, 4, obj.admin.razon_social)
            if obj.categoria:
                ws.write(row_num, 5, obj.categoria.nombre)
            if obj.inicio_operaciones:
                ws.write(row_num, 6, obj.inicio_operaciones.strftime('%d/%m/%Y'), font_style)
            if obj.termino_operaciones:
                ws.write(row_num, 7, obj.termino_operaciones.strftime('%d/%m/%Y'), font_style)

            ws.write(row_num, 8, obj.desactivar_grafico)
            ws.write(row_num, 9, obj.desactivar_rentabilidades)
            if obj.categoriaAntigua:
                ws.write(row_num, 10, obj.categoriaAntigua.nombre)
        wb.save(response)
        return response

admin.site.register(fondo, fondoAdmin)

class perfil(models.Model):
    usuario = models.OneToOneField(User, db_column="user_id", unique=True, primary_key=True, on_delete=models.CASCADE)
    token = models.CharField(max_length=255, null=True, blank=True)
    nivel_acceso = models.IntegerField()
    id_usercreador = models.ForeignKey(User, db_column="id_usercreador", related_name='id_usercreador', on_delete=models.SET_NULL, null=True, blank=True)
    administrador = models.ForeignKey(administrador, on_delete=models.SET_NULL, null=True)
    class Meta:
        verbose_name_plural = 'Perfiles'
    def __str__(self):
        return self.usuario.username
class perfilAdmin(admin.ModelAdmin):
    list_display = ['usuario', 'nivel_acceso', 'administrador', 'token']
    def id_usercreador(self, obj):
        return obj.id_usercreador
        
admin.site.register(perfil, perfilAdmin)

# Series y cotas deprecated (no more)
class serie(models.Model) :
    fondo = models.ForeignKey(fondo, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=15, null=True, blank=True)
    caracteristica = models.CharField(max_length=15, null=True)
    fecha_inicio = models.DateField(null=True)
    fecha_termino = models.DateField(null=True)
    valor_inicial = models.DecimalField(max_digits=12, decimal_places=4, default=0)
    continuadora = models.ForeignKey('self', on_delete=models.SET_NULL, null=True)
    nemotecnico = models.ForeignKey('nemo_inversion', null=True, blank =True, on_delete=models.SET_NULL)
    
    class Meta:
        ordering = ['fondo__nombre']
        unique_together = ('fondo', 'nombre')

    def __str__(self):
        return (str(self.fondo.nombre) + ' ' + str(self.nombre)) if self.fondo.nombre else \
            (str(self.fondo.nombre) + ' ' + str(self.nombre) + ' --> ' +str(self.nemotecnico.nemotecnico))

    #extrae la fecha de origen de su serie si es continuadora
    def fecha_inicio_original(self):
        if self.continuadora == None:
            return self.fecha_inicio
        else:
            fecha_inicio_original(self.continuadora)


    def get_ultimo_patrimonio(self):
        c = cuota.objects.filter(serie = self).order_by('-fecha').first()
        return c.patrimonio_neto


class serieAdmin(admin.ModelAdmin):
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['runsvs', 'nombre', 'caracteristica', 'continuadora', 'categoria indice','nemotecnico', 'fecha_inicio', 'fecha_termino']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.fondo.runsvs, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
            ws.write(row_num, 2, obj.caracteristica)
            if obj.continuadora != None:
                ws.write(row_num, 3, obj.continuadora.nombre)
            if obj.fondo.categoria != None:
                ws.write(row_num, 4, obj.fondo.categoria.indice)
            if obj.nemotecnico != None:
                ws.write(row_num, 5, obj.nemotecnico.nemotecnico)
            if obj.fecha_inicio:
                ws.write(row_num, 6, obj.fecha_inicio.strftime('%d/%m/%Y'), font_style)
            if obj.fecha_termino:
                ws.write(row_num, 7, obj.fecha_termino.strftime('%d/%m/%Y'), font_style)
            
        wb.save(response)

        #writer = csv.writer(response)

        #writer.writerow(['fecha', 'valor_libro', 'dividendo', 'serie', 'rentabilidad', 'indice'])
        #for obj in queryset:
        #    row = writer.writerow([ obj.fecha.strftime('%d/%m/%Y')  , obj.valor_libro, obj.get_dividendo(), obj.serie.nombre, obj.rentab(), obj.indice])

       
        return response


    export_as_csv.short_description = "Exportar"

    def get_nemotecnico(self):
        if self.nemotecnico == None:
            return ''
        return self.nemotecnico.nemotecnico
    list_display = ['id', 'fondo', 'nombre', 'caracteristica','fecha_inicio', 'fecha_termino', 'valor_inicial', 'continuadora', get_nemotecnico]
    search_fields = ['fondo__runsvs', 'nombre']
admin.site.register(serie, serieAdmin)


class cuota(models.Model) :
    fecha = models.DateField(null=True)
    serie = models.ForeignKey(serie, on_delete=models.CASCADE, null=True)
    moneda = models.CharField(max_length=5)
    valor_libro = models.DecimalField(max_digits=12, decimal_places=4, null=True, blank=True)
    valor_economico = models.DecimalField(max_digits=12, decimal_places=4, null=True, blank=True)
    patrimonio_neto = models.BigIntegerField(null=True, blank=True)
    activo_total = models.BigIntegerField(null=True, blank=True)
    n_aportantes = models.IntegerField(null=True, blank=True)
    n_aportantes_inst = models.IntegerField(null=True, blank=True)
    valor_libro_conv = models.DecimalField(max_digits=12, decimal_places=4, null=True, blank=True)
    agencia = models.TextField(null=True, blank=True)
    indice = models.FloatField(null=True)


    def __str__(self):
        return str(self.fecha) + ' : ' + str(self.indice)

    def cuota_anterior(self):
        if isinstance(self.fecha, str):
            self.fecha = datetime.datetime.strptime(self.fecha, '%Y-%m-%d')
        fecha_ant = self.fecha - datetime.timedelta(days=1)
        
        try:
            return cuota.objects.get(serie = self.serie, fecha = fecha_ant)
        except cuota.DoesNotExist:
            return None
    
    def get_divisa_fondo(self):
        f = self.serie.fondo
        fe = fondo_ext.objects.get(runsvs=f.runsvs)
        fe = fe.divisa
        if self.moneda == 'NONE':
            if self.cuota_anterior() == None:
                self.moneda = '$$'
            else:
                self.moneda = self.cuota_anterior().moneda

        #if self.moneda == 'USN':
        #    self.moneda = 'PROM'
        try:
            m = divisa.objects.get(alias_svs= self.moneda)
        except divisa.DoesNotExist:
            m = fe
        if fe != m and fe != None:
            return [m.divisa_id, fe.divisa_id]
        else:
            return None

    def get_dividendo(self):
        dividendo = 0
        try:
            finv = fondoInversion.objects.filter(serie=self.serie)

            for f in finv:
                try:
                    evs = eventoInversion.objects.filter(fondo_inversion= f.cfi, tipo ="DV")



                    evs = evs.filter( Q(fecha_limite= self.fecha, fecha_dividendo__isnull= True) | Q(fecha_dividendo= self.fecha))

                    for e in evs:
                        #determinar cuando se paga el dividendo
                        gdf = fondo_ext.objects.get(pk= self.serie.fondo.runsvs)
                        if gdf.divisa != None:
                            try:
                                dividendo += float(convertir(e.divisa.divisa_id, gdf.divisa.divisa_id, e.valor, self.fecha))
                            except TypeError:
                                dividendo += float( e.valor )
                        else:
                            dividendo += float( e.valor )

                                
                except eventoInversion.DoesNotExist:
                    dividendo += 0
                    
        except fondoInversion.DoesNotExist:
            dividendo = 0

        return dividendo

    def rentab(self):
        if self.cuota_anterior() == None:

            return 0

        dividendo = self.get_dividendo()
        '''
        try:
            dividendo = 0
            finv = fondoInversion.objects.filter(serie=self.serie)
            for f in finv:
                try:
                    evs = eventoInversion.objects.filter(fondo_inversion= f.cfi, fecha_limite=self.fecha, tipo ="DV")
                    for e in evs:
                
                        gdf = fondo_ext.objects.get(pk= self.serie.fondo.runsvs)
                        dividendo += float(convertir(e.divisa.divisa_id, gdf.divisa.divisa_id, e.valor, self.fecha))
                    
                except eventoInversion.DoesNotExist:
                    dividendo += 0
                
    
        except fondoInversion.DoesNotExist:
            dividendo = 0
        '''
        try:
            
            if self.valor_libro_conv == None:
                l1 = self.valor_libro
            else:
                l1 = self.valor_libro_conv

            if self.cuota_anterior().valor_libro_conv == None:
                l2 = self.cuota_anterior().valor_libro
            else:
                l2 = self.cuota_anterior().valor_libro_conv

            rentab = ((float(l1)+float(dividendo)) /float(l2))-1

        except (TypeError, ZeroDivisionError, AttributeError):            
            rentab = 0
        return rentab
    
    def print_rentab(self):
        if self.cuota_anterior() == None:

            return "0"

        dividendo = self.get_dividendo()

        try:
            
            if self.valor_libro_conv == None:
                l1 = self.valor_libro
            else:
                l1 = self.valor_libro_conv

            if self.cuota_anterior().valor_libro_conv == None:
                l2 = self.cuota_anterior().valor_libro
            else:
                l2 = self.cuota_anterior().valor_libro_conv

            rentab = "(("+str(l1)+"+"+str(dividendo)+") /"+str(l2)+")-1"

        except (TypeError, ZeroDivisionError, AttributeError):            
            rentab = 0
        return rentab        
    class Meta :
        unique_together = ('fecha', 'serie')



class cuotaAdmin(admin.ModelAdmin):
    actions = ["export_as_csv"]

    def get_dividendo(self):
        return self.get_dividendo()



    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['runsvs', 'fondo', 'fecha', 'valor_libro', 'dividendo', 'patrimonio_neto','Valor cuota + dividendo', 'rentabilidad',  'indice']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.serie.fondo.runsvs, font_style)
            ws.write(row_num, 1, obj.serie.fondo.nombre, font_style)
            ws.write(row_num, 2, obj.fecha.strftime('%d/%m/%Y'), font_style)
            ws.write(row_num, 3, obj.valor_libro, font_style)
            ws.write(row_num, 4, obj.get_dividendo(), font_style)
            ws.write(row_num, 5, obj.patrimonio_neto, font_style)
            ws.write(row_num, 6, xlwt.Formula('D{0}+E{0})'.format(row_num+1)) , font_style)
            
            ws.write(row_num, 7, obj.rentab(), font_style)
            ws.write(row_num, 8, obj.indice, font_style)

        wb.save(response)

        #writer = csv.writer(response)

        #writer.writerow(['fecha', 'valor_libro', 'dividendo', 'serie', 'rentabilidad', 'indice'])
        #for obj in queryset:
        #    row = writer.writerow([ obj.fecha.strftime('%d/%m/%Y')  , obj.valor_libro, obj.get_dividendo(), obj.serie.nombre, obj.rentab(), obj.indice])

       
        return response


    export_as_csv.short_description = "Exportar"

    def get_fondo_runsvs(obj):
        return obj.serie.fondo
    def rentab(self):
        return self.rentab()
    
    def print_rentab(self):
        return self.print_rentab()


    def valor_divisa(self):
        i = indicador.objects.get(pk=self.fecha)
        return [i.dolar, i.euro, i.uf]

    get_fondo_runsvs.admin_order_field = 'serie.fondo'


    list_display = [get_fondo_runsvs, 'fecha', 'valor_libro', get_dividendo, 'patrimonio_neto', rentab, 'indice', print_rentab]
    search_fields = ['fecha', 'serie__fondo__runsvs' ]
    #list_filter= ['serie__nombre', 'fecha']
    list_filter = (
        ('fecha', DateRangeFilter), 'serie__nombre', )
    actions = ["export_as_csv"]

admin.site.register(cuota, cuotaAdmin)


class aportante(models.Model):
    rut = models.CharField(max_length=15) #Rut
    nombre = models.CharField(primary_key=True, max_length=100)
    tipo_persona = models.CharField(max_length=1)
    class Meta:
        verbose_name = 'Aportante'
        verbose_name_plural = 'Aportantes'
    def __str__(self):
        if self.nombre == None or self.nombre == ' ':
            return "_"
        return self.nombre
    def get_tipo_persona(self):
        if self.tipo_persona == 'B' or self.tipo_persona == 'F':
            return 'EXT'
        else:
            return 'NAC'
    def get_persona(self):
        if self.tipo_persona =='A':
            return 'Persona natural nacional'
        elif self.tipo_persona =='B':
            return 'Persona natural extranjera'
        elif self.tipo_persona == 'C':
            return 'Sociedad anónima abierta nacional'
        elif self.tipo_persona == 'D':
            return 'Estado de Chile (Fisco, Corfo, etc.)'
        elif self.tipo_persona =='E':
            return 'Otro tipo de persona jurídica nacional'
        elif self.tipo_persona =='F':
            return 'Persona jurídica extranjera'
        else:
            return self.tipo_persona

class aportanteAdmin(admin.ModelAdmin):
    def get_tipo_persona(self, obj):
        if obj.tipo_persona == 'B' or obj.tipo_persona == 'F':
            return 'EXT'
        else:
            return 'NAC'
    
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['nombre', 'rut', 'get_tipo_persona']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.nombre, font_style)
            ws.write(row_num, 1, obj.rut, font_style)
            ws.write(row_num, 2, obj.get_tipo_persona())
           
            
        wb.save(response)

        return response


    export_as_csv.short_description = "Exportar"
    list_display = ['nombre', 'rut', 'get_tipo_persona']
    search_fields = ['nombre', 'rut']

    get_tipo_persona.short_description = "Tipo"
    ordering = ['nombre']

admin.site.register(aportante, aportanteAdmin)


class movimiento(models.Model):
    ap = models.BooleanField(default=True) # activo1/pasivo0
    monto = models.BigIntegerField()
    etiqueta_svs = models.CharField(max_length=100)
    periodo = models.CharField(max_length=7)
    fondo = models.ForeignKey(fondo, on_delete=models.CASCADE)
    etiqueta_acafi = models.CharField
    moneda = models.CharField(max_length=50)
    orden_catastro = models.SmallIntegerField(null=True, blank=True)

    def get_porcentaje(obj):
        total = movimiento.objects.filter(ap = obj.ap, fondo = obj.fondo, periodo=obj.periodo).exclude(etiqueta_svs__icontains = 'total').aggregate(Sum('monto'))['monto__sum']
        porc = int(obj.monto) * 100 /total
        return float(porc)

    def monto_real(obj, destino):
        try:
            aux = obj.periodo.split('-')

            moneda_real = obj.moneda.split(' de ')[1]
        except IndexError:
            fe = fondo_ext.objects.get(runsvs = obj.fondo.runsvs)
            moneda_real = fe.divisa.divisa_id         

        if moneda_real.lower() == destino.lower():
            print(obj.fondo.runsvs)
            return obj.monto
        else:
            
            
            i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            if destino == 'dolar':
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                i = indicador.objects.get(pk= nueva_fecha)
            
            if moneda_real.lower() == 'pesos':
                if destino.lower() != 'pesos':
                    while i._meta.get_field( destino.lower()).value_from_object(i) == None:
                        nueva_fecha = i.fecha + datetime.timedelta(days=1)
                        i = indicador.objects.get(pk= nueva_fecha)
                    return obj.monto/i._meta.get_field( destino.lower()).value_from_object(i)*1000
                
            if destino.lower() == 'pesos':
                if moneda_real.lower() != 'pesos':
                    if 'dolar' in moneda_real.lower() or 'dólar' in moneda_real.lower():
                        moneda_real = 'dolar'
                    while i._meta.get_field( moneda_real.lower()).value_from_object(i) == None:
                        nueva_fecha = i.fecha + datetime.timedelta(days=1)
                        i = indicador.objects.get(pk= nueva_fecha)
                    return float(obj.monto*i._meta.get_field( moneda_real.lower()).value_from_object(i) )
                

            if moneda_real.lower() != 'pesos' and destino.lower() != 'pesos':
                while i._meta.get_field( moneda_real.lower()).value_from_object(i) == None or i._meta.get_field( destino.lower()).value_from_object(i) == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                return float(obj.monto*i._meta.get_field( moneda_real.lower()).value_from_object(i)/i._meta.get_field( destino.lower()).value_from_object(i))

    def __str__(self):
        return str(self.fondo)+" "+self.periodo+" "+self.etiqueta_svs


class movimientoAdmin(admin.ModelAdmin):
    list_display = ['id','etiqueta_svs', 'fondo', 'periodo', 'monto', 'ap', 'moneda', 'orden_catastro']
    search_fields = ['etiqueta_svs', 'fondo__runsvs', 'periodo']

    #list_filter= ['serie__nombre', 'fecha']
    list_filter = ['periodo', 'etiqueta_svs']
    actions = ["Exportar"]

    def monto_real(obj, moneda):
        return obj.monto_real(moneda);

    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['etiqueta_svs', 'fondo', 'administradora', 'periodo', 'monto', 'ap', 'moneda', 'monto en pesos']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.etiqueta_svs, font_style)
            if obj.fondo:
                ws.write(row_num, 1, obj.fondo.runsvs)
                if obj.fondo.admin:
                    ws.write(row_num, 2, obj.fondo.admin.rut)
            ws.write(row_num, 3, obj.periodo, font_style)
            ws.write(row_num, 4, obj.monto, font_style)
            ws.write(row_num, 5, obj.ap, font_style)
            ws.write(row_num, 6, obj.moneda, font_style)
            ws.write(row_num, 7, obj.monto_real('pesos'), font_style)
            
        wb.save(response)
        return response

admin.site.register(movimiento, movimientoAdmin)

class periodo(models.Model):
    periodo_id = models.AutoField(primary_key=True)
    fondo = models.ForeignKey(fondo, db_column="fondo_id", on_delete=models.CASCADE)
    periodo = models.CharField(max_length=7) #MM-AAAA
    valor_obs = models.DecimalField('valor de libro de cuota', max_digits=15, decimal_places=4)
    total_aportantes = models.IntegerField()
    datos = models.TextField() #otros datos formato JSON
    class Meta:
        ordering = ['periodo']
    def periodoanterior(obj):
        fondo = obj.fondo
        p = obj.periodo.split('-')
        if str(p[0]) == '03':
            aux = "12-"+str(int(p[1])-1)
        if str(p[0]) == '12':
            aux = "09-"+str(int(p[1]))
        if str(p[0]) == '09':
            aux = "06-"+str(int(p[1]))
        if str(p[0]) == '06':
            aux = "03-"+str(int(p[1]))

        try:
            return periodo.objects.get(fondo=fondo, periodo= aux)
        except periodo.DoesNotExist:
            return None

    def periodosiguiente(obj):
        fondo = obj.fondo
        p = obj.periodo.split('-')
        if str(p[0]) == '03':
            aux = "06-"+str(int(p[1]))
        if str(p[0]) == '06':
            aux = "09-"+str(int(p[1]))
        if str(p[0]) == '09':
            aux = "12-"+str(int(p[1]))
        if str(p[0]) == '12':
            aux = "03-"+str(int(p[1])+1)

        try:
            return periodo.objects.get(fondo=fondo, periodo= aux)
        except periodo.DoesNotExist:
            return None

    '''def periodoanterior(obj):
        fondo = obj.fondo
        fecha_inicio = str(fondo.inicio_operaciones)
        if fondo.termino_operaciones is None:
            fecha_termino = datetime.datetime.today().strftime('%Y-%m-%d')
        #periods = get_periodos(fecha_inicio, fecha_termino)

        fi = fecha_inicio.split('-')
        try:
            ft = fecha_termino.split('-')
        except UnboundLocalError:
            return None
        
        cota_inf = ""
        cota_sup = ""
        periodos = []

        for mes in range(3,13,3):
            if mes >= int(fi[1]):
                cota_inf = str(mes)+'-'+str(fi[0])
                break;
        for mes in range(3,13,3):            
            if mes >= int(ft[1]):
                cota_sup = str(mes)+'-'+str(ft[0])
                break;

        
        cota_inf = cota_inf.split('-')
        cota_sup = cota_sup.split('-')

        for anio in range(int(cota_inf[1]), int(cota_sup[1])+1):
            for mes in range(3,13,3):
                if mes >= int(cota_inf[0]) and anio == int(cota_inf[1]):
                    if mes < 10:
                        periodos.append(str(anio)+"-0"+str(mes))
                    else:
                        periodos.append(str(anio)+"-"+str(mes))
                if anio > int(cota_inf[1]) and anio < int(cota_sup[1])+1:
                    if mes < 10:
                        periodos.append(str(anio)+"-0"+str(mes))
                    else:
                        periodos.append(str(anio)+"-"+str(mes))
                if mes >= int(cota_sup[0]) and anio == int(cota_sup[1])+1:
                    if mes < 10:
                        periodos.append(str(anio)+"-0"+str(mes))
                    else:
                        periodos.append(str(anio)+"-"+str(mes))

           

        if int(cota_sup[0]) < 10:
            cota_sup[0] = "0"+str(cota_sup[0])

        periodos.append(cota_sup[1]+"-"+cota_sup[0])
        
        
        for index, p in enumerate(periodos):
            aux = obj.periodo.split('-')
            print(p)
            if str(aux[1])+'-'+str(aux[0]) == p:
                aux2 = periodos[index-1].split('-')

                try:
                    return periodo.objects.get(periodo=str(aux2[1])+'-'+str(aux2[0]), fondo=obj.fondo)
                    continue
                except periodo.DoesNotExist:
                    return None
                    continue
                
        return None
    '''
    def cuotas_pagadas(obj):
        try:
            d = json.loads(obj.datos)
            if float(d['CUOTAS PAGADAS']) == 0:
                return 0
                #return float(d['CUOTAS EMITIDAS'])
            else:
                return float(d['CUOTAS PAGADAS'])

        except KeyError:
            return ''
    def acum_anio(obj):
        try:
            d = json.loads(obj.datos)
            return float(d['REPARTOS DE DIVIDENDOS'])
        except KeyError:
            return ''
    def trimestre(obj): #En el Trimestre (4) Miles $
        if obj.periodo.startswith('03-') or obj.periodoanterior() == None:
            try:
                d = json.loads(obj.datos)

                return float(abs(d['REPARTOS DE DIVIDENDOS']))
            except KeyError:
                return 0
        else:
            
            anterior =  obj.periodoanterior()
            try:
                ant = json.loads(anterior.datos)
                d = json.loads(obj.datos)
                
                if float(ant['REPARTOS DE DIVIDENDOS']) < float(d['REPARTOS DE DIVIDENDOS']):
                    return 0

                return float(ant['REPARTOS DE DIVIDENDOS']) - float(d['REPARTOS DE DIVIDENDOS']) 

            except KeyError:
                return 0
            except AttributeError:
                return 0
    def cuota(obj): #Por cuota (4)/(1) Pesos
        try:
            d = json.loads(obj.datos)

            if float(d['CUOTAS PAGADAS']) == 0:
                if float(d['CUOTAS EMITIDAS']) == 0:
                    return 0
                else:
                    return (float(obj.trimestre())/float(d['CUOTAS ENITIDAS']))*1000
            return (float(obj.trimestre())/float(d['CUOTAS PAGADAS']))*1000
        except KeyError:
            return 0
    def valorldiv(obj): #  Valor Libro+Dividendo por Cuota Pesos
        return float(obj.cuota())+float(obj.valor_obs)
    def rentabilidad(obj):

        fondo = obj.fondo

        fecha_inicio = str(fondo.inicio_operaciones)
        #Inicio Cambio Hugo
        
        if fecha_inicio != None and fecha_inicio != '' and fecha_inicio != 'None' :
            dt = datetime.datetime.strptime(fecha_inicio, '%Y-%m-%d')
            if obj.periodo == '03-'+str(dt.year):
                return 100
        '''
        try:
            dt = datetime.datetime.strptime(fecha_inicio, '%Y-%m-%d')
            if obj.periodo == '03-'+str(dt.year):
                return 100
        except ValueError:
            return
        '''
        #Fin cambio Hugo

        anterior =  obj.periodoanterior()
        if anterior == None:
            return 100
        try:
            rent = float(obj.valorldiv()) * anterior.rentabilidad()/float(anterior.valor_obs)
        except ZeroDivisionError:
            return 100
        return float(rent)
    def periodos_vacios(obj):
        
        p = obj.periodo.split('-')
        anio = p[1]

        cont = periodo.objects.filter(periodo__endswith= str(anio), fondo=obj.fondo).order_by('periodo')
        flag = True
        cambio = False

        try:
            periodo.objects.get(periodo= '12-'+str(int(anio)), fondo=obj.fondo)
            flag = True
        except periodo.DoesNotExist:
            flag = False
            cambio = True
            pass

        try:
            periodo.objects.get(periodo= '09-'+str(int(anio)), fondo=obj.fondo)
            flag = True
        except periodo.DoesNotExist:
            flag = False
            cambio = True
            pass

        try:
            periodo.objects.get(periodo= '06-'+str(int(anio)), fondo=obj.fondo)
            flag = True
        except periodo.DoesNotExist:
            flag = False
            cambio = True
            pass

        try:
            periodo.objects.get(periodo= '03-'+str(int(anio)), fondo=obj.fondo)
            flag = True
        except periodo.DoesNotExist:
            flag = False
            cambio = True
            pass

        try:
            periodo.objects.get(periodo= str('12-')+str(int(anio)-1), fondo=obj.fondo)
            flag = True
        except periodo.DoesNotExist:
            flag = False
            cambio = True
            pass
        
        if flag == True and cambio == True:
            return True
        else:
            return False     
    def indice_rentabilidad(obj):
        num = obj.rentabilidad()

        p = obj.periodo.split('-')
        anio = p[1]
        
        cont1 = periodo.objects.filter(periodo__endswith= str(anio), fondo=obj.fondo)
        cont = cont1.exclude(periodo='12-'+str(anio)).count()
        cont2 = periodo.objects.filter(periodo= '12-'+str(int(anio)-1), fondo=obj.fondo).count()
        
        if cont2 == 0:
            divisor = 100
        else:
            divisor = periodo.objects.get(periodo= '12-'+str(int(anio)-1), fondo=obj.fondo).rentabilidad()

        #if cont == 4:    
        #    cont -= 1

        cont = cont + cont2
        
        if divisor == 0:
            divisor = 1
        
        #Inicio cambio Hugo
        '''
        try:
            num = obj.rentabilidad()/divisor
        except TypeError:
            num=1
        
        '''
        #Fin cambio Hugo
        num = obj.rentabilidad()/divisor
        
        if obj.periodos_vacios() == True:
            cont = 4 
        if cont != 0:
            aux = num**(4/int(cont))-1
            aux *= 100
        else:
            aux = 0
        return aux
    def detalle_rentabilidad(obj):
        num = obj.rentabilidad()

        p = obj.periodo.split('-')
        anio = p[1]
        
        cont1 = periodo.objects.filter(periodo__endswith= str(anio), fondo=obj.fondo)
        cont = cont1.exclude(periodo='12-'+str(anio)).count()
        cont2 = periodo.objects.filter(periodo= '12-'+str(int(anio)-1), fondo=obj.fondo).count()
        
        if cont2 == 0:
            divisor = 100
        else:
            divisor = periodo.objects.get(periodo= '12-'+str(int(anio)-1), fondo=obj.fondo).rentabilidad()

        #if cont == 4:    
        #    cont -= 1

        cont = cont + cont2
        
        if divisor == 0:
            divisor = 1
        
        #Inicio cambio Hugo
        '''
        try:
            num = obj.rentabilidad()/divisor
        except TypeError:
            num
        
        '''
        #Fin cambio Hugo
        num = obj.rentabilidad()/divisor

        if obj.periodos_vacios() == True:
            cont = 4 
        if cont != 0:
            aux = '(('+str(obj.rentabilidad())+'/'+str(divisor)+')^(4/'+str(cont)+')-1)*100'
        else:
            aux = '0'
        return aux

    def moneda(obj):
        try:
            mov= movimiento.objects.filter(periodo=obj.periodo, fondo=obj.fondo).last()
            if mov == None:
                mov= movimiento.objects.filter(fondo=obj.fondo).last()
        
            if mov == None:
                return 'pesos'  

            return mov.moneda.split(' de ')[1] #retorn dola, peso, mxn
        except IndexError:
            fe = fondo_ext.objects.get(runsvs = obj.fondo.runsvs)
            return fe.divisa.divisa_id 

    def monto_real(obj, dato, destino):
        aux = obj.periodo.split('-')

        moneda_real = obj.moneda()
        
        d = json.loads(obj.datos)
        
        if dato == 'aporte_neto':
            monto = float(d['APORTES']) +float(d['REPARTOS DE PATRIMONIOS'])+float(d['REPARTOS DE DIVIDENDOS'])
        elif dato =='valor_cuota':
            monto = float(obj.valor_obs)
        else:
            try:
                if d[dato]:
                    monto = d[dato]
                else:
                    monto =  0
            except KeyError:
                monto=  0



        if moneda_real.lower() == destino.lower():
            return monto
        else:     
            i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            if destino.lower() == 'dolar':
                nueva_fecha = i.fecha + datetime.timedelta(days=1)
                i = indicador.objects.get(pk= nueva_fecha)
            
            if moneda_real.lower() == 'pesos':
                if destino.lower() != 'pesos':
                    while i._meta.get_field( destino.lower()).value_from_object(i) == None:
                        nueva_fecha = i.fecha + datetime.timedelta(days=1)
                        i = indicador.objects.get(pk= nueva_fecha)
                    return float(float(monto)/i._meta.get_field( destino.lower()).value_from_object(i)*1000 )
                
            if destino.lower() == 'pesos':
                if moneda_real.lower() != 'pesos':
                    if moneda_real == 'dólares':
                        moneda_real = 'dolar'
                    while i._meta.get_field( moneda_real.lower()).value_from_object(i) == None:
                        nueva_fecha = i.fecha + datetime.timedelta(days=1)
                        i = indicador.objects.get(pk= nueva_fecha)
                    return float(float(monto)*i._meta.get_field( moneda_real.lower()).value_from_object(i) )
                

            if moneda_real.lower() != 'pesos' and destino.lower() != 'pesos':
                while i._meta.get_field( moneda_real.lower()).value_from_object(i) == None or i._meta.get_field( destino.lower()).value_from_object(i) == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                    
                return float(float(monto)*i._meta.get_field( moneda_real.lower()).value_from_object(i)/i._meta.get_field( destino.lower()).value_from_object(i))
    def aporte_neto(obj):
        d = json.loads(obj.datos)
        try:
            dividendo =  float(abs(d['REPARTOS DE DIVIDENDOS']))
        except KeyError:
            dividendo = 0
        try:
            patrimonio =  float(abs(d['REPARTOS DE PATRIMONIOS']))
        except KeyError:
            patrimonio = 0
        try:
            aportes =  float(abs(d['APORTES']))
        except KeyError:
            aportes = 0
        return aportes+patrimonio+dividendo

    def __str__(self):
        return self.fondo.runsvs+" "+self.periodo
class periodoAdmin(admin.ModelAdmin):
    
    def get_dividendos(self):
        d = json.loads(self.datos)
        try:
            dividendo =  float(abs(d['REPARTOS DE DIVIDENDOS']))
        except KeyError:
            dividendo = 0
        return dividendo
    def get_patrimonios(self):
        d = json.loads(self.datos)
        try:
            PATRIMONIOS =  float(abs(d['REPARTOS DE PATRIMONIOS']))
        except KeyError:
            PATRIMONIOS = 0
        return PATRIMONIOS
    def get_aportes(self):
        d = json.loads(self.datos)
        try:
            APORTES =  float(abs(d['APORTES']))
        except KeyError:
            APORTES = 0
        return APORTES

    list_display = ['fondo', 'periodo', 'total_aportantes','valor_obs',  get_dividendos, get_patrimonios,  get_aportes]
    search_fields = ['fondo__runsvs', 'periodo']
    list_filter = ["periodo"]
    actions = ["Exportar"]

    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['fondo', 'periodo', 'total_aportantes','valor_obs', 'dividendo', 'patrimonio', 'aportes']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1

            ws.write(row_num, 0, '['+str(obj.fondo.runsvs) +']'+ obj.fondo.nombre, font_style)
            ws.write(row_num, 1, obj.periodo, font_style)
            ws.write(row_num, 2, obj.total_aportantes)
            ws.write(row_num, 3, obj.valor_obs)
            if obj.datos:
                d = json.loads(obj.datos)
                try:
                    dividendo =  float(abs(d['REPARTOS DE DIVIDENDOS']))
                    ws.write(row_num, 4, dividendo)
                except KeyError:
                    pass
                try:
                    dividendo =  float(abs(d['REPARTOS DE PATRIMONIOS']))
                    ws.write(row_num, 5, dividendo)
                except KeyError:
                    pass            
                try:
                    dividendo =  float(abs(d['APORTES']))
                    ws.write(row_num, 6, dividendo)
                except KeyError:
                    pass   


        wb.save(response)
        return response

admin.site.register(periodo, periodoAdmin)

class composicion(models.Model) :  #m -- m de periodo aportante
    periodo = models.ForeignKey(periodo, on_delete=models.CASCADE)
    aportante = models.ForeignKey(aportante, on_delete=models.CASCADE)
    porc_propiedad = models.DecimalField('porcentaje de propiedad', max_digits=7, decimal_places=4)
    class Meta:
        verbose_name_plural = 'Composiciones'
        ordering = ['periodo__fondo']
    def __str__(self):
        return self.aportante.nombre+" "+str(self.periodo)
class composicionAdmin(admin.ModelAdmin):
    list_display = ['get_fondoid', 'get_periodos', 'get_aportante', 'porc_propiedad']
    search_fields = ['periodo__periodo', 'periodo__fondo__runsvs',  'aportante__nombre']
    
    def get_periodos(self, obj):
        return obj.periodo.periodo
    def get_fondoid(self, obj):
        return obj.periodo.fondo_id

    def get_aportante(self, obj):
        return obj.aportante.nombre

    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['periodo', 'fondo runsvs',  'nombre aportante', 'rut aportante']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.periodo.periodo, font_style)
            ws.write(row_num, 1, obj.periodo.fondo.runsvs, font_style)
            ws.write(row_num, 2, obj.aportante.nombre)
            ws.write(row_num, 3, obj.aportante.rut)
        wb.save(response)
        return response
    export_as_csv.short_description = "Exportar"
    list_filter = ['periodo__periodo']

admin.site.register(composicion, composicionAdmin)




class inversion(models.Model):
    fondo = models.ForeignKey(fondo, on_delete=models.CASCADE, null=True)
    empresa = models.CharField(max_length=100)
    codigo = models.CharField(max_length=15, null=True, blank=True, default=None)
    pais = models.ForeignKey('pais' , null=True, blank=True, on_delete=models.SET_NULL, related_name='pais', db_column="pais")
    #models.CharField(max_length=5, null=True)
    moneda = models.ForeignKey('divisa', null=True, blank =True, on_delete=models.SET_NULL, related_name='moneda', db_column="moneda")
    instrumento = models.CharField(max_length=15, null=True,  blank=True, default=None)
    valolizacion = models.SmallIntegerField(null=True, blank=True) # 1 o 3, 1 es deuda y 3 variable
    monto = models.BigIntegerField(null=True, default = 0)
    porcentaje = models.DecimalField(max_digits=7, decimal_places=4, null=True)
    unidades= models.BigIntegerField(null=True, blank=True,)
    periodo = models.CharField(max_length=7) #MM-AAAA
    nemotecnico = models.CharField(max_length=255, null=True, default=None)
    fecha_vencimiento = models.DateField(null=True, blank=True)
    cartera = models.SmallIntegerField(null=True, blank=True, default=None) # 0 si es nacional o extranjero, 1 si es participacion en la empresa
    #2 campos añadidos
    tipo_unidades = models.CharField(max_length=5, null=True)
    cod_pais_transaccion = models.ForeignKey('pais' , null=True, blank=True, on_delete=models.SET_NULL, related_name='cod_pais_transaccion')

    def __str__(self):
        return str(self.fondo)+" "+str(self.empresa)

    class Meta:
        verbose_name_plural = 'Inversiones'

class inversionAdmin(admin.ModelAdmin):

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['rut fondo', 'nombre fondo', 'nombre empresa', 'codigo', 'pais', 'moneda', 'instrumento', 'valolizacion', \
            'monto', 'porcentaje', 'unidades', 'periodo', 'nemotecnico', 'fecha_vencimiento',\
            'tipo_unidades', 'cod_pais_transaccion']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.fondo.runsvs, font_style)
            ws.write(row_num, 1, obj.fondo.nombre, font_style)
            ws.write(row_num, 2, obj.empresa, font_style)
            ws.write(row_num, 3, obj.codigo, font_style)
            if obj.pais:
                ws.write(row_num, 4, obj.pais.nombre, font_style)
            if obj.moneda:
                ws.write(row_num, 5, obj.moneda.nombre, font_style)
            ws.write(row_num, 6, obj.instrumento, font_style)
            ws.write(row_num, 7, obj.valolizacion, font_style)
            ws.write(row_num, 8, obj.monto, font_style)
            ws.write(row_num, 9, obj.porcentaje, font_style)
            ws.write(row_num, 10, obj.unidades, font_style)
            ws.write(row_num, 11, obj.periodo, font_style)
            ws.write(row_num, 12, obj.nemotecnico, font_style)
            if obj.fecha_vencimiento:
                ws.write(row_num, 13, obj.fecha_vencimiento.strftime('%d/%m/%Y'), font_style)
            ws.write(row_num, 14, obj.tipo_unidades)
            if obj.cod_pais_transaccion:
                ws.write(row_num, 15, obj.cod_pais_transaccion.nombre)
        wb.save(response)
        return response

    export_as_csv.short_description = "Exportar"

    def precatastro(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['Rut Fondo', 'Nombre Fondo', 'Rut Administrador', 'Nombre Administrador', 'codigo pais tr', 'pais', 'porcentaje','tipo cartera']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
            row_num += 1
            
        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            ws.write(row_num, 0, obj.fondo.runsvs, font_style)
            ws.write(row_num, 1, obj.fondo.nombre, font_style)
            if obj.fondo.admin:
                ws.write(row_num, 2, obj.fondo.admin.rut, font_style)
                ws.write(row_num, 3, obj.fondo.admin.razon_social, font_style)

            if obj.cod_pais_transaccion:
                ws.write(row_num, 4, obj.cod_pais_transaccion.nombre)
            if obj.pais:
                ws.write(row_num, 5, obj.pais.nombre, font_style)
    
            ws.write(row_num, 6, obj.porcentaje, font_style)
            if  obj.cartera == 1:
                ws.write(row_num, 7, "Metodo part", font_style)
            else:
                ws.write(row_num, 7, "cartera", font_style)
            row_num += 1
            

        wb.save(response)
        return response


    list_display = ['id', 'fondo', 'empresa', 'pais', 'cod_pais_transaccion', 'moneda', 'tipo_unidades', 'instrumento', 'monto', 'porcentaje', 'periodo', 'nemotecnico']
    search_fields = ['fondo__runsvs','empresa', 'periodo']
    list_filter= ['periodo','instrumento']
    actions = ["export_as_csv", "precatastro"]

admin.site.register(inversion, inversionAdmin)

class resumen(models.Model):
    descripcion = models.CharField(max_length=80)
    nacional = models.BigIntegerField(null=True)
    extranjero = models.BigIntegerField(null=True)
    porcentual = models.FloatField(null=True)
    periodo = models.ForeignKey(periodo, on_delete=models.CASCADE, null=True)
    def __str__(self):
        return "("+str(self.periodo.fondo)+" "+str(self.periodo.periodo)+") "+self.descripcion+" -> extr: "+str(self.extranjero)+", nac:"+str(self.nacional)
    def monto(obj):
        return obj.nacional + obj.extranjero

    class Meta:
        verbose_name_plural = 'Resumenes'
    
    '''def getperiod(obj):
        per = periodo.objects.get(pk = obj.periodo)
        return per.periodo    '''


    #periodo = models.ForeignKey(periodo, on_delete=models.CASCADE, null=True)
class resumenAdmin(admin.ModelAdmin):
    list_display = ['periodo', 'descripcion','nacional', 'extranjero']
    search_fields = ['descripcion','periodo__periodo', 'periodo__fondo__runsvs', 'periodo__fondo__nombre', 'extranjero']
    actions = ["Exportar"]
    list_filter= ['periodo__periodo']
    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['runsvs', 'periodo', 'descripcion','nacional', 'extranjero']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            if obj.periodo.fondo:
                ws.write(row_num, 0, obj.periodo.fondo.runsvs)
            ws.write(row_num, 1, obj.periodo.periodo, font_style)
            ws.write(row_num, 2, obj.descripcion, font_style)
            ws.write(row_num, 3, obj.nacional, font_style)
            ws.write(row_num, 4, obj.extranjero, font_style)

        wb.save(response)
        return response
admin.site.register(resumen, resumenAdmin)


class bug(models.Model):
    adm  = models.BooleanField(default=False)
    cat  = models.BooleanField(default=False)
    moneda  = models.BooleanField(default=False)
    fi  = models.BooleanField(default=False)
    fer  = models.BooleanField(default=False)
    fir  = models.BooleanField(default=False)
    ccr  = models.BooleanField(default=False)
    icr  = models.BooleanField(default=False)
    humpr  = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    fondo = models.CharField(max_length=15)
    def __str__(self):
        return self.user.username+" "+self.fondo
class bugAdmin(admin.ModelAdmin):
    list_display = ['id', 'adm', 'cat', 'fir', 'moneda', 'fi', 'user', 'fondo']
admin.site.register(bug, bugAdmin)


class fondo_corfo(models.Model):
    rut = models.SlugField(primary_key=True)
    periodo = models.CharField(max_length=7, null=True, blank =True)
    nombre = models.CharField(max_length=100)
    admin = models.ForeignKey('administrador', null=True, blank =True, on_delete=models.SET_NULL)
    codigo_bolsa = models.CharField(max_length=15, null=True, blank =True)
    bloomberg = models.CharField(max_length=15, null=True, blank =True)
    isin = models.CharField(max_length=15, null=True, blank =True)
    moneda = models.ForeignKey('divisa', null=True, blank =True, on_delete=models.SET_NULL)
    apertura_linea = models.DateField(null=True, blank =True)
    fecha_vencimiento = models.DateField(null=True, blank =True)
    tipo_fondo = models.ForeignKey(categoria, on_delete=models.SET_NULL, default='0', null=True, blank =True)
    tipo_linea = models.CharField(max_length=5, null=True, blank =True)
    area_interes = models.TextField(null=True, blank =True)
    portfolio_manager = models.CharField(max_length=25, null=True, blank =True)
    #secciones 4 - 7
    patrimonio = models.BigIntegerField(null=True, blank =True)
    comision = models.TextField(null=True, blank =True)
    comentario = models.TextField(null=True, blank =True)
    estrategia = models.TextField(null=True, blank =True)

    cifras = models.TextField(default='{}', null=True, blank =True )
    inversiones = models.TextField(default='{}', null=True, blank =True )
    pagina = models.IntegerField(null=True, blank=True)
    def __str__(self):
        return self.rut
    '''def validado(obj):
        flag_cifr = False
        flag_invs = False

        cifr = json.loads(obj.cifras)
        if obj.cifras != '{}':
        invs = json.loads(obj.inversiones)
    '''
    def vigente(obj):
        cifr = json.loads(obj.cifras)
        return  cifr['cifra_estado']
    
    def tabla_inversiones(obj):
        inversiones = json.loads(obj.inversiones)

        tabla = []
        tot = 0
        for i in range(1,12):
            tot += inversiones['inversion_monto_'+str(i)]

        invs = []
        for i in range(1,12):
            if tot == 0:
                aux = 0
            else:
                aux = inversiones['inversion_monto_'+str(i)]*100/tot
            tabla.append([inversiones['inversion_empresa_'+str(i)], inversiones['inversion_monto_'+str(i)], aux ])
                        
        return tabla
    class Meta:
        verbose_name = 'Fondo Corfo'
        verbose_name_plural = 'Fondos Corfo'
class fondo_corfoAdmin(admin.ModelAdmin):
    list_display = ['rut', 'nombre', 'apertura_linea', 'fecha_vencimiento', 'tipo_fondo', 'tipo_linea', 'periodo', 'pagina']
    search_fields = ['rut', 'nombre']
    actions = ["Exportar"]

    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['rut', 'nombre', 'admin', 'apertura_linea', 'fecha_vencimiento', 'tipo_fondo', 'tipo_linea', \
        'codigo_bolsa', 'bloomberg', 'isin', 'moneda', 'apertura_linea', 'portfolio_manager']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.rut, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)
            if obj.admin:
                ws.write(row_num, 2, obj.admin.razon_social)
            ws.write(row_num, 3, obj.apertura_linea, font_style)
            if obj.fecha_vencimiento:
                ws.write(row_num, 4, obj.fecha_vencimiento.strftime('%d/%m/%Y'), font_style)
            if obj.tipo_fondo:
                ws.write(row_num, 5, obj.tipo_fondo, font_style)
            ws.write(row_num, 6, obj.tipo_linea, font_style)
            ws.write(row_num, 7, obj.codigo_bolsa, font_style)
            ws.write(row_num, 8, obj.bloomberg, font_style)
            ws.write(row_num, 9, obj.isin, font_style)
            if obj.moneda:
                ws.write(row_num, 10, obj.moneda.nombre, font_style)
            if obj.apertura_linea:
                ws.write(row_num, 11, obj.apertura_linea.strftime('%d/%m/%Y'), font_style)
            ws.write(row_num, 12, obj.portfolio_manager, font_style)
        wb.save(response)
        return response

admin.site.register(fondo_corfo, fondo_corfoAdmin)

class exportacion(models.Model):
    nombre =  models.CharField(max_length=200, null=True, blank=True)
    ruta = models.CharField(max_length=200, null=True, blank=True)
    def __str__(self):
        return self.nombre
    class Meta:
        verbose_name_plural = 'Exportaciones'

class exportacionAdmin(admin.ModelAdmin):
    list_display = ['ruta', 'nombre']
admin.site.register(exportacion, exportacionAdmin)

class detalle_fondo(models.Model):
    fondo = models.OneToOneField(fondo, primary_key=True, on_delete=models.CASCADE)
    rentabilidad = models.CharField(null=True, blank=True, max_length=1)# D ó T
    serie_antigua = models.ForeignKey(serie, on_delete=models.CASCADE, null=True, blank=True)
class detalle_fondoAdmin(admin.ModelAdmin):

    list_display = ['fondo','rentabilidad', 'serie_antigua']
    search_fields = ['fondo__runsvs','fondo__nombre']
    list_per_page = 20
admin.site.register(detalle_fondo, detalle_fondoAdmin)

class rent(models.Model):
    fondo = models.OneToOneField(fondo, on_delete=models.CASCADE)
    periodo = models.CharField(max_length=7, null=True, blank=True) #MM-AAAA
    rentab_anio =models.FloatField(null=True, blank=True)
    rentab_anio_pasado =models.FloatField(null=True, blank=True)
    rentab_anio_antepasado =models.FloatField(null=True, blank=True)
    rentab_3er_anio =models.FloatField(null=True, blank=True)
    rentab_5to_anio =models.FloatField(null=True, blank=True)
    rentab_ini_anio =models.FloatField(null=True, blank=True)
    diario_rentab_anio =models.FloatField(null=True, blank=True)
    diario_rentab_anio_pasado =models.FloatField(null=True, blank=True)
    diario_rentab_anio_antepasado =models.FloatField(null=True, blank=True)
    diario_rentab_3er_anio =models.FloatField(null=True, blank=True)
    diario_rentab_5to_anio =models.FloatField(null=True, blank=True)
    diario_rentab_ini_anio =models.FloatField(null=True, blank=True)    
    def __str__(self):
        return str(self.fondo)+" "+str(self.periodo)
    class Meta:
        verbose_name = 'Rentabilidad Estática'
        verbose_name_plural = 'Rentabilidades Estáticas'
class rentabilidadAdmin(admin.ModelAdmin):
    actions = ['calcular_diaria', 'export_as_csv']
    def calcular_diaria(self, request, queryset):

        for obj in queryset.order_by('fondo__runsvs'):
            if obj.fondo.inicio_operaciones < datetime.datetime.strptime('2018-01-01', '%Y-%m-%d').date() or obj.diario_rentab_anio == None:
        
                df = detalle_fondo.objects.get(fondo = obj.fondo)
                l = df.serie_antigua
                print(obj.fondo.runsvs)
                if df.rentabilidad == 'D':
                    rentablidades = []
                    rent_anios = []
                    ahora = datetime.datetime.now()
                    anio_actual= int(ahora.year)
                    primera_cuota = cuota.objects.filter(serie= l).order_by('fecha').first()
                    anio_pivote = int(primera_cuota.fecha.year)
                    #print(anio_pivote)
                    try:

                        c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                        if anio_pivote-1 == anio_actual-2:
                            c2 = primera_cuota
                        else:
                            c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
                        if c1.indice== 0 or c2.indice== 0 or c1.indice == None or c2.indice == None:
                            obj.diario_rentab_anio = 0
                        else:
                            obj.diario_rentab_anio =(float(c1.indice)/float(c2.indice)-1)*100
                    except cuota.DoesNotExist:
                        obj.diario_rentab_anio = 0

                    try:
                        if anio_pivote-1 == anio_actual-3:

                            c3 = primera_cuota
                        else: 
                            c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
                        c2 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-2)
                        
                        if c3.indice== 0 or c2.indice== 0 or c3.indice == None or c2.indice == None:
                            obj.diario_rentab_anio_pasado = 0
                        #rentablidades.append( str(float(c1.indice))+'/'+str(float(c2.indice)) )
                        else:
                            obj.diario_rentab_anio_pasado = (float(c2.indice)/float(c3.indice)-1)*100
                    except cuota.DoesNotExist:
                         obj.diario_rentab_anio_pasado = 0

                    try:
                        c3 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-3)
                        if anio_pivote-1 == anio_actual-4: 
                            c4 = primera_cuota
                        else:
                            c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4)
                        if c3.indice== 0 or c4.indice== 0 or c3.indice== None or c4.indice==None:
                            obj.diario_rentab_anio_antepasado = 0
                        else:
                            obj.diario_rentab_anio_antepasado = (float(c3.indice)/float(c4.indice)-1)*100
                    except cuota.DoesNotExist:
                        obj.diario_rentab_anio_antepasado = 0
                        
                        #hace 3 años
                    try:
                        c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                        c4 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-4)
                            
                        if c1.indice== 0 or c4.indice== 0 or c1.indice==None or c4.indice== None:
                            obj.diario_rentab_3er_anio = 0
                        else:
                            exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), '31/12/'+str(anio_actual-4) ) ))
                            obj.diario_rentab_3er_anio = (((c1.indice/c4.indice)**(exp))-1 )*100
                    except cuota.DoesNotExist:
                        obj.diario_rentab_3er_anio = 0      
                    #hace 5 años
                    try:
                        c5 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-6) #hace 5 años
                        c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                            
                        if c1.indice== 0 or c5.indice== 0 or c1.indice==None or c5.indice== None or c1==None or c5== None:
                            obj.diario_rentab_5to_anio = 0
                        else:
                            exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), '31/12/'+str(anio_actual-6) ) ))
                            obj.diario_rentab_5to_anio = (((c1.indice/c5.indice)**(exp))-1 )*100
                    except cuota.DoesNotExist:
                        obj.diario_rentab_5to_anio = 0

                    #inicio
                    try:
                        ci = cuota.objects.filter(serie= l).order_by('fecha')[:1] #inicio
                        c1 = cuota.objects.get(serie= l, fecha__month=12, fecha__day=31, fecha__year=anio_actual-1)
                        
                        if c1.indice==None:
                            obj.diario_rentab_ini_anio = 0
                        else:
                            exp = float(365.25/(dias_entre( '31/12/'+str(anio_actual-1), ci[0].fecha.strftime("%d/%m/%Y") ) ))
                            obj.diario_rentab_ini_anio = (((c1.indice/100)**(exp))-1 )*100
                    except cuota.DoesNotExist:
                        obj.diario_rentab_ini_anio = 0

                    obj.save()
                    #context = {"serie": l, "rentablidades" : [i * 100 for i in rentablidades], "rent_anios": [i * 100 for i in rent_anios]}        
                    #row = writer.writerow([obj.fondo.runsvs, [i * 100 for i in rent_anios], [i * 100 for i in rentablidades]])

            else:
                
                pass
       
        return True
    calcular_diaria.short_description = "Calcular rentab. diaria"


    actions = ["Exportar"]
    def Exportar(self, request, queryset):
        
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        #columns = [' ', ' ','Trimestral', ' ', ' ', ' ', ' ', ' ', 'Diaria']

        #for col_num in range(len(columns)):
        #    ws.write(row_num, col_num, columns[col_num], font_style)
        
        #row_num += 1
        columns = ['fondo', 'serie', 'rentab_anio', 'rentab_anio_pasado', 'rentab_anio_antepasado', 'rentab_3er_anio', 'rentab_5to_anio', 'rentab_ini_anio', \
            'diario_rentab_anio', 'diario_rentab_anio_pasado', 'diario_rentab_anio_antepasado', 'diario_rentab_3er_anio', 'diario_rentab_5to_anio', 'diario_rentab_ini_anio']
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        
        for obj in queryset:

            row_num += 1

            ws.write(row_num, 0, '['+str(obj.fondo.runsvs) +']'+ obj.fondo.nombre, font_style)
            try:
                df = detalle_fondo.objects.get(pk= obj.fondo).serie_antigua
                if df != None:
                    df = df.nombre
                else:
                    df = '---'
            except detalle_fondo.DoesNotExist:
                df = '---'

            ws.write(row_num, 1, df, font_style)
            ws.write(row_num, 2, obj.rentab_anio)
            ws.write(row_num, 3, obj.rentab_anio_pasado)
            ws.write(row_num, 4, obj.rentab_anio_antepasado)
            ws.write(row_num, 5, obj.rentab_3er_anio)
            ws.write(row_num, 6, obj.rentab_5to_anio)
            ws.write(row_num, 7, obj.rentab_ini_anio)

            ws.write(row_num, 8, obj.diario_rentab_anio)
            ws.write(row_num, 9, obj.diario_rentab_anio_pasado)
            ws.write(row_num, 10, obj.diario_rentab_anio_antepasado)
            ws.write(row_num, 11, obj.diario_rentab_3er_anio)
            ws.write(row_num, 12, obj.diario_rentab_5to_anio)
            ws.write(row_num, 13, obj.diario_rentab_ini_anio)

        wb.save(response)
        return response


    list_display = ['fondo', 'periodo', 'diario_rentab_anio', 'diario_rentab_anio_pasado', 'diario_rentab_anio_antepasado', 'diario_rentab_3er_anio', 'diario_rentab_5to_anio', 'diario_rentab_ini_anio']
    search_fields = ['fondo__runsvs', 'periodo']    
admin.site.register(rent, rentabilidadAdmin)


class moneda_periodo(models.Model):
    periodo = models.CharField(max_length=7) #MM-AAAA
    divisa = models.ForeignKey(divisa, null=True, on_delete=models.SET_NULL)
    valor = models.FloatField(null=True, blank=True) 
    def __str__(self):
        return str(self.divisa)+" : "+str(self.periodo)
    class Meta:
        verbose_name = 'Valor Moneda por Período'
        verbose_name_plural = 'Valores Moneda por Período'

class moneda_periodoAdmin(admin.ModelAdmin):
    list_display = ['periodo', 'divisa', 'valor']
admin.site.register(moneda_periodo, moneda_periodoAdmin)

#tablas agregadas al catastro

class resultado(models.Model):#resultados integrales
    ap = models.SmallIntegerField(null=True, blank=True) # 1,2,3
    monto = models.BigIntegerField()
    etiqueta_svs = models.CharField(max_length=255)
    periodo = models.CharField(max_length=7)
    fondo = models.ForeignKey(fondo, on_delete=models.CASCADE)
    etiqueta_acafi = models.CharField
    moneda = models.CharField(max_length=50)
    orden_catastro = models.SmallIntegerField(null=True, blank=True)

    def monto_real(obj, destino):
        aux = obj.periodo.split('-')
        try:
            moneda_real = obj.moneda.split(' de ')[1]
        except IndexError:
            fe = fondo_ext.objects.get(runsvs = obj.fondo.runsvs)
            moneda_real =  fe.divisa.divisa_id 

        if moneda_real.lower() == destino.lower():
            print(obj.fondo.runsvs)
            return obj.monto
        
        
        else:
            
            
            i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
            nueva_fecha = i.fecha + datetime.timedelta(days=1)
            i = indicador.objects.get(pk= nueva_fecha)
            
            if moneda_real.lower() == 'pesos':
                if destino.lower() != 'pesos':
                    while i._meta.get_field( destino.lower()).value_from_object(i) == None:
                        nueva_fecha = i.fecha + datetime.timedelta(days=1)
                        i = indicador.objects.get(pk= nueva_fecha)
                    return obj.monto/i._meta.get_field( destino.lower()).value_from_object(i)*1000
                
            if destino.lower() == 'pesos':
                if moneda_real.lower() != 'pesos':
                    while i._meta.get_field( moneda_real.lower()).value_from_object(i) == None:
                        nueva_fecha = i.fecha + datetime.timedelta(days=1)
                        i = indicador.objects.get(pk= nueva_fecha)
                    return float(obj.monto*i._meta.get_field( moneda_real.lower()).value_from_object(i) )
                

            if moneda_real.lower() != 'pesos' and destino.lower() != 'pesos':
                while i._meta.get_field( moneda_real.lower()).value_from_object(i) == None or i._meta.get_field( destino.lower()).value_from_object(i) == None:
                    nueva_fecha = i.fecha + datetime.timedelta(days=1)
                    i = indicador.objects.get(pk= nueva_fecha)
                return float(obj.monto*i._meta.get_field( moneda_real.lower())/i._meta.get_field( destino.lower()))
    def __str__(self):
        return str(self.fondo)+" "+self.periodo+" "+self.etiqueta_svs




class resultadoAdmin(admin.ModelAdmin):
    list_display = ['etiqueta_svs', 'fondo', 'periodo', 'monto', 'ap', 'moneda', 'orden_catastro']
    search_fields = ['etiqueta_svs', 'fondo__runsvs', 'periodo']
    list_filter = ['periodo', 'etiqueta_svs']
    actions = ["Exportar"]

    def monto_real(obj, moneda):
        return obj.monto_real(moneda);

    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['etiqueta_svs', 'fondo', 'periodo', 'monto', 'ap', 'moneda', 'monto en pesos']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.etiqueta_svs, font_style)
            if obj.fondo:
                ws.write(row_num, 1, obj.fondo.runsvs)
            ws.write(row_num, 2, obj.periodo, font_style)
            ws.write(row_num, 3, obj.monto, font_style)
            ws.write(row_num, 4, obj.ap, font_style)
            ws.write(row_num, 5, obj.moneda, font_style)
            ws.write(row_num, 6, obj.monto_real('pesos'), font_style)
        wb.save(response)
        return response
admin.site.register(resultado, resultadoAdmin)

'''
class accion_nacional(models.Model):
    fondo = models.ForeignKey(fondo, on_delete=models.SET_NULL, null=True, blank=True)
    periodo = models.CharField(max_length=7, null=True, blank=True)
    clasificacion = models.IntegerField(null=True, blank=True)
    nemotecnico = models.CharField(max_length=255, null=True, blank=True)
    rut_emisor = models.CharField(max_length=20, null=True, blank=True)
    cod_pais = models.CharField(max_length=10, null=True, blank=True)
    tipo_instrumento = models.CharField(max_length=25, null=True, blank=True)
    fecha_vencimiento = models.DateField(null=True, blank=True)
    situacion_instrumento = models.IntegerField(null=True, blank=True)
    clasificacion_riesgo = models.CharField(max_length=20, null=True, blank=True)
    grupo_empresarial = models.IntegerField(null=True, blank=True)
    cant_unidades = models.FloatField(null=True, blank=True)    
    tipo_unidades = models.CharField(max_length=10, null=True, blank=True)
    tir_precio = models.FloatField(null=True, blank=True)    
    codigo_valolizacion = models.IntegerField(null=True, blank=True)
    tasa = models.IntegerField(null=True, blank=True)
    tipo_interes = models.CharField(max_length=10, null=True, blank=True)
    valolizacion_cierre = models.IntegerField(null=True, blank=True)
    cod_moneda_liq = models.CharField(max_length=10, null=True, blank=True)
    cod_pais_transaccion = models.CharField(max_length=10, null=True, blank=True)
    cap_emisor = models.FloatField(null=True, blank=True)    
    act_emisor = models.FloatField(null=True, blank=True)    
    activo_fondo = models.FloatField(null=True, blank=True)
    class Meta:
        verbose_name = 'Accion'
        verbose_name_plural = 'Acciones'

    def __str__(self):
        if self.fecha_vencimiento == None:
            fecha_vencimiento = ''
        else:  
            fecha_vencimiento = self.fecha_vencimiento
        return str(self.fondo)+" "+str(self.periodo)+" "+str(self.nemotecnico)+" "+str(fecha_vencimiento)   
class accion_nacionalAdmin(admin.ModelAdmin):
    list_display = ['fondo', 'periodo', 'nemotecnico']
    search_fields = ['fondo__runsvs', 'periodo', 'nemotecnico']
admin.site.register(accion_nacional, accion_nacionalAdmin)    
'''
class sector(models.Model):
    nemotecnico = models.CharField(max_length=50, primary_key=True)
    nombre = models.CharField(max_length=100)
class sectorAdmin(admin.ModelAdmin):
    list_display = ['nemotecnico', 'nombre']
    search_fields = ['nemotecnico', 'nombre']
    actions = ["Exportar"]
    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['nemotecnico', 'nombre']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.nemotecnico, font_style)
            ws.write(row_num, 1, obj.nombre, font_style)

        wb.save(response)
        return response
admin.site.register(sector, sectorAdmin)    


class indicador(models.Model):
    fecha = models.DateField(primary_key=True)
    dolar = models.FloatField(null=True, blank=True)
    uf = models.FloatField(null=True, blank=True)
    euro = models.FloatField(null=True, blank=True)
    mxn = models.FloatField(null=True, blank=True) #peso mexicano
    gbp = models.FloatField(null=True, blank=True) #libra esterlina

    def __str__(self):
        return str(self.fecha)
    class Meta:
        verbose_name_plural = 'Indicadores'

class indicadorAdmin(admin.ModelAdmin):
    list_display = ['fecha', 'dolar', 'uf', 'euro', 'mxn', 'gbp']
    search_fields = ['fecha', 'dolar', 'uf', 'euro', 'mxn', 'gbp']
    list_filter = (
        ('fecha', DateRangeFilter) ,)
    actions = ['exportar']
    def exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = 'fecha', 'dolar', 'uf', 'euro', 'peso mexicano'

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        font_style = xlwt.XFStyle()
        for obj in queryset:
            row_num += 1
            if obj.fecha:
                ws.write(row_num, 0, obj.fecha.strftime('%d/%m/%Y'), font_style)
            ws.write(row_num, 1, obj.dolar, font_style)
            ws.write(row_num, 2, obj.uf, font_style)
            ws.write(row_num, 3, obj.euro, font_style)
            ws.write(row_num, 4, obj.mxn, font_style)
            
        wb.save(response)
        return response
    exportar.short_description = "Exportar"

admin.site.register(indicador, indicadorAdmin)            


#================================
def convertir(origen, destino, cantidad, fecha= datetime.date.today()):
    if origen == destino:
        return cantidad
    if origen== 'Peso Chileno' or origen == 'CLP':
        i = indicador.objects.get(pk=fecha)
        if destino=='dolar' or destino == 'USD' or destino == 'USN':
            fecha= (datetime.datetime.strptime(str(fecha), '%Y-%m-%d') - datetime.timedelta(days=1)).date()
            if i.dolar == None:
                i = indicador.objects.filter(dolar__gt=0, fecha__lt=fecha).last()        
            ind = i.dolar
        

        if destino=='uf' or destino== 'CLF':
            if i.uf == None:
                i = indicador.objects.filter(uf__gt=0, fecha__lt=fecha).last()        
            
            ind = i.uf
        if destino=='euro' or destino == 'EUR':
            if i.euro == None:
                i = indicador.objects.filter(euro__gt=0, fecha__lt=fecha).last()        
        
            ind = i.euro

        
        return float(cantidad)/float(ind)


    if destino == 'Peso Chileno' or destino == 'CLP':
        i = indicador.objects.get(pk=fecha)
        
        if origen=='dolar' or origen == 'USD':
            if i.dolar == None:
                i = indicador.objects.filter(dolar__gt=0, fecha__lt=fecha).last()

            ind = i.dolar
        if origen=='uf' or origen == 'CLF':
            if i.uf == None:
                i = indicador.objects.filter(uf__gt=0, fecha__lt=fecha).last()        
            
            ind = i.uf
        if origen=='euro' or origen== 'EUR' :
            if i.euro == None:
                i = indicador.objects.filter(euro__gt=0, fecha__lt=fecha).last()        
            
            ind = i.euro
        return float(cantidad) * float(ind)
#================================




    

class fondoInversion(models.Model):
    #cfi = models.SlugField(verbose_name= 'Nemotecnico')
    cfi = models.ForeignKey('nemo_inversion', on_delete=models.CASCADE, db_column="cfi")
    url = models.TextField(max_length=255, null=True, blank=True)
    razon_social = models.TextField(max_length=255, null=True, blank=True)
    ultimo = models.FloatField(null=True, blank=True)
    var = models.FloatField(null=True, blank=True)
    volumen = models.FloatField(null=True, blank=True)
    monto = models.BigIntegerField(null=True, blank=True)
    moneda = models.FloatField(null=True, blank=True)
    precio_compra = models.FloatField(null=True, blank=True)
    precio_venta = models.FloatField(null=True, blank=True)
    tipo_moneda = models.ForeignKey('divisa', null=True, blank =True, on_delete=models.SET_NULL)
    #fondo = models.ForeignKey('fondo', null=True, blank =True, on_delete=models.CASCADE)
    serie = models.ForeignKey('serie', null=True, blank =True, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.razon_social+" "+self.cfi.nemotecnico+" "+str(self.serie)    
    def get_cfi(self, obj):
        return obj.cfi.nemotecnico

    def fondo(self, obj):
        if obj.serie == None:
            return None
    
        return obj.serie.fondo.runsvs
    fondo.admin_order_field  = 'fondo'  #Allows column order sorting
    fondo.short_description = 'Fondo'

    
    class Meta:

        unique_together = ('cfi', 'serie')
        verbose_name = 'Inversión (cfi)'
        verbose_name_plural = 'Inversiones (cfi)'

class fondoInversionAdmin(admin.ModelAdmin):
    
    def get_fondo(self, obj):
        if obj.serie == None:
            return None
    
        return obj.serie.fondo.runsvs
    get_fondo.short_description = 'Fondo'

    def get_fondo_nombre(self, obj):
        if obj.serie == None:
            return None
    
        return obj.serie.fondo.nombre
    get_fondo.short_description = 'Nombre Fondo'

    list_display = ['id', 'descr_cfi', 'razon_social', 'tipo_moneda', 'get_fondo', 'get_fondo_nombre', 'serie']
    def descr_cfi(self, obj):
        if obj.cfi:
            return mark_safe("<a href=/admin/acafi/eventoinversion/?q="+str(obj.cfi)+">"+str(obj.cfi)+"</a>")
    
    search_fields = ['cfi__nemotecnico', 'razon_social', 'tipo_moneda__nombre', 'serie__fondo__runsvs', 'serie__fondo__nombre']
    
    actions = ['exportar']
    def exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = ['id', 'descr_cfi', 'razon_social', 'tipo_moneda', 'get_fondo', 'get_fondo_nombre', 'serie']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        font_style = xlwt.XFStyle()
        
        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.id, font_style)
            if obj.cfi:
                ws.write(row_num, 1, obj.cfi.nemotecnico, font_style)
            ws.write(row_num, 2, obj.razon_social, font_style)
            if obj.tipo_moneda:
                ws.write(row_num, 3, obj.tipo_moneda.nombre, font_style)
            if obj.serie:
                ws.write(row_num, 4, obj.serie.fondo.runsvs, font_style)
                ws.write(row_num, 5, obj.serie.fondo.nombre, font_style)
                ws.write(row_num, 6, obj.serie.nombre, font_style)
            ws.write(row_num, 7, obj.ultimo, font_style)
            ws.write(row_num, 8, obj.var, font_style)
            ws.write(row_num, 9, obj.volumen, font_style)
            ws.write(row_num, 10, obj.monto, font_style)
            ws.write(row_num, 11, obj.precio_compra, font_style)
            ws.write(row_num, 12, obj.precio_venta, font_style)


        wb.save(response)
        return response
    exportar.short_description = "Exportar"    

admin.site.register(fondoInversion, fondoInversionAdmin)


class eventoInversion(models.Model):
    fondo_inversion = models.ForeignKey('nemo_inversion', null=True, blank =True, on_delete=models.CASCADE)
    descripcion = models.TextField(max_length=255, null=True, blank=True)
    fecha_limite = models.DateField(null=True, blank=True)
    fecha_pago = models.DateField(null=True, blank=True)
    valor = models.FloatField(null=True, blank=True)
    tipo = models.TextField(max_length=20, null=True, blank=True)
    divisa = models.ForeignKey(divisa, blank=True, null=True, on_delete=models.SET_NULL)    
    fecha_dividendo = models.DateField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.fecha_dividendo:
            self.fecha_dividendo = self.fecha_limite
        super().save(*args, **kwargs)
    class Meta:
        verbose_name = 'Dividendo'
        verbose_name_plural = 'Dividendos'
    def __str__(self):
        return self.descripcion
    def get_moneda_fondo(self):
        fondo = self.fondo_inversion.serie.fondo.runsvs
        fe = fondo_ext.objects.get(runsvs=fondo)
        return fe.divisa.nombre
    def convertir(origen, destino, cantidad, fecha= datetime.date.today()):
        if origen == destino:
            return cantidad
        if origen== 'Peso Chileno' or origen == 'CLP':
            i = indicador.objects.get(pk=fecha)
            if destino=='dolar' or destino == 'USD':
                fecha= (datetime.datetime.strptime(str(fecha), '%Y-%m-%d') - datetime.timedelta(days=1)).date()
                if i.dolar == None:
                    i = indicador.objects.filter(dolar__gt=0, fecha__lt=fecha).last()        
                ind = i.dolar
            

            if destino=='uf' or destino== 'CLF':
                if i.uf == None:
                    i = indicador.objects.filter(uf__gt=0, fecha__lt=fecha).last()        
                
                ind = i.uf
            if destino=='euro' or destino == 'EUR':
                if i.euro == None:
                    i = indicador.objects.filter(euro__gt=0, fecha__lt=fecha).last()        
            
                ind = i.euro

            
            return float(cantidad)/float(ind)


        if destino == 'Peso Chileno' or destino == 'CLP':
            i = indicador.objects.get(pk=fecha)
            
            if origen=='dolar' or origen == 'USD':
                if i.dolar == None:
                    i = indicador.objects.filter(dolar__gt=0, fecha__lt=fecha).last()

                ind = i.dolar
            if origen=='uf' or origen == 'CLF':
                if i.uf == None:
                    i = indicador.objects.filter(uf__gt=0, fecha__lt=fecha).last()        
                
                ind = i.uf
            if origen=='euro' or origen== 'EUR' :
                if i.euro == None:
                    i = indicador.objects.filter(euro__gt=0, fecha__lt=fecha).last()        
                
                ind = i.euro
            return float(cantidad) * float(ind)
    def get_valor_moneda(self):
        
        origen = divisa.nombre
        if 'USD' in origen:
            origen = 'dolar'
        if origen == 'Euro':
            origen = 'euro'

        destino = get_moneda_fondo()
        if 'USD' in destino:
            destino ='dolar'
        if destino == 'Euro':
            destino = 'euro'

        return convertir(origen, destino, valor, fecha_limite)
    


class eventoInversionAdmin(admin.ModelAdmin):

    list_display = ['fondo_inversion', 'descripcion', 'fecha_limite', 'fecha_pago', 'fecha_dividendo', 'valor', 'tipo', 'divisa']
    search_fields = ['fondo_inversion__nemotecnico', 'descripcion', 'fecha_limite', 'fecha_dividendo', 'fecha_pago']
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = 'fondo inversion', 'descripcion', 'fecha_limite', 'fecha_pago', 'fecha_dividendo (extra)', 'valor', 'tipo', 'divisa'

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        font_style = xlwt.XFStyle()
        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.fondo_inversion.nemotecnico, font_style)
            ws.write(row_num, 1, obj.descripcion, font_style)
            if obj.fecha_limite:
                ws.write(row_num, 2, obj.fecha_limite.strftime('%d/%m/%Y'), font_style)
            if obj.fecha_pago:
                ws.write(row_num, 3, obj.fecha_pago.strftime('%d/%m/%Y'), font_style)
            if obj.fecha_dividendo:
                ws.write(row_num, 4, obj.fecha_dividendo.strftime('%d/%m/%Y'), font_style)
            ws.write(row_num, 5, obj.valor, font_style)
            ws.write(row_num, 6, obj.tipo, font_style)
            if obj.divisa:
                ws.write(row_num, 7, obj.divisa.nombre, font_style)
            
        wb.save(response)
        return response
    export_as_csv.short_description = "Exportar"
    
admin.site.register(eventoInversion, eventoInversionAdmin)     


 

class instrumento(models.Model):
    id = models.CharField(max_length=55, primary_key=True)
    descripcion = models.CharField(max_length=155)
    tipo = models.CharField(max_length=55)

class instrumentoAdmin(admin.ModelAdmin):
    list_display = ['id', 'descripcion', 'tipo']
    search_fields = ['id', 'descripcion', 'tipo']
    actions = ['exportar']
    def exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = ['id', 'descripcion', 'tipo']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        font_style = xlwt.XFStyle()
        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.id, font_style)
            ws.write(row_num, 1, obj.descripcion, font_style)
            ws.write(row_num, 2, obj.tipo, font_style)
        wb.save(response)
        return response
    exportar.short_description = "Exportar"

admin.site.register(instrumento, instrumentoAdmin)


class catastro(models.Model): #tabla para guardar json relacionados al catastro
    fondo = models.ForeignKey(fondo, on_delete=models.SET_NULL, null=True, blank=True)
    periodo = models.CharField(max_length=7) #MM-AAAA
    eeff = JSONField(null=True, blank=True)
    parametros_iniciales = JSONField(null=True, blank=True)
    nuevos_fondos = JSONField(null=True, blank=True)
    cartera_inversiones = JSONField(null=True, blank=True)
    acciones_nacionales = JSONField(null=True, blank=True)
    cuotas_aportantes = JSONField(null=True, blank=True)
    clasif_riesgo = JSONField(null=True, blank=True)
    inv_pais = JSONField(null=True, blank=True)
    inv_moneda = JSONField(null=True, blank=True)

class catastroAdmin(admin.ModelAdmin):

    list_display = ['fondo','periodo', 'eeff', 'parametros_iniciales', 'nuevos_fondos', 'cartera_inversiones', 'acciones_nacionales', 'cuotas_aportantes', 'clasif_riesgo', 'inv_pais', 'inv_moneda']
    search_fields = ['fondo__runsvs','fondo__nombre', 'periodo']
admin.site.register(catastro, catastroAdmin)     

class operacion(fondo):
    def __str__(self):
        return '[' + str(self.runsvs) + '] ' + str(self.nombre.encode('utf-8', 'ignore').decode('utf-8'))
        
    objects = fondo()
    class Meta:
        proxy=True
        verbose_name = 'Operación'
        verbose_name_plural = 'Operaciones'

    search_fields = ['runsvs', 'nombre']


#admin.site.register(operacion)

class transaccion_cuota(models.Model):
    nemotecnico = models.ForeignKey(nemo_inversion, on_delete=models.CASCADE)
    fecha = models.DateField(null=True, blank=True)
    negocios = models.IntegerField(null=True, blank=True)
    cuotas_transadas = models.IntegerField(null=True, blank=True)
    monto_transado = models.BigIntegerField(null=True, blank=True)
    class Meta:
        unique_together = ('nemotecnico', 'fecha')
class transaccion_cuotaAdmin(admin.ModelAdmin):

    list_display = ['id', 'nemotecnico', 'fecha', 'negocios', 'cuotas_transadas', 'monto_transado']
    search_fields = ['nemotecnico__nemotecnico', 'fecha']
    actions = ["Exportar"]
    def Exportar(self, request, queryset):
        meta = self.model._meta
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(meta)
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(format(meta))
        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['id', 'nemotecnico', 'fecha', 'negocios', 'cuotas_transadas', 'monto_transado']
    
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        for obj in queryset:
            row_num += 1
            ws.write(row_num, 0, obj.id, font_style)
            ws.write(row_num, 1, obj.nemotecnico.nemotecnico, font_style)
            ws.write(row_num, 2, obj.fecha.strftime('%d/%m/%Y'), font_style)
            ws.write(row_num, 3, obj.negocios, font_style)
            ws.write(row_num, 4, obj.cuotas_transadas, font_style)
            ws.write(row_num, 5, obj.monto_transado, font_style)

        wb.save(response)
        return response
admin.site.register(transaccion_cuota, transaccion_cuotaAdmin) 
    


class categoriaCatastro(models.Model):
    categoria = models.OneToOneField(categoria, primary_key=True, on_delete=models.CASCADE)
    nombre = models.TextField(null=True, blank=True)
class categoriaCatastroAdmin(admin.ModelAdmin):
    list_display = ['categoria', 'nombre']
    search_fields = ['categoria__indice', 'categoria__nombre', 'nombre']
admin.site.register(categoriaCatastro, categoriaCatastroAdmin) 

    



class hoja_catastro(models.Model):
    periodo = models.CharField(max_length=7, primary_key=True) #MM-AAAA
    lista_fondos = models.TextField(null=True, blank=True)
    lista_fondos_nuevos = models.TextField(null=True, blank=True)
    
    fondos_nuevos = models.TextField(null=True, blank=True)
    total_activos = models.TextField(null=True, blank=True)
    activos_fondo = models.TextField(null=True, blank=True)
    activo_por_afi = models.TextField(null=True, blank=True)
    activos_fondos_afi = models.TextField(null=True, blank=True)#
    activos_por_clase = models.TextField(null=True, blank=True)#
    inversiones =  models.TextField(null=True, blank=True)
    
    eeff = models.TextField(null=True, blank=True)
    resumen =  models.TextField(null=True, blank=True)
    tabla_catastro =  models.TextField(null=True, blank=True)
    aportes_netos = models.TextField(null=True, blank=True)
    
    patrimonio_fondo = models.TextField(null=True, blank=True)
    patrimonio_afi = models.TextField(null=True, blank=True)
    patrimonio_clase = models.TextField(null=True, blank=True)

    valor_cuotas = models.TextField(null=True, blank=True)
    cuotas_pagadas = models.TextField(null=True, blank=True)
    volumen_cuotas_mes = models.TextField(null=True, blank=True)
    volumen_monto = models.TextField(null=True, blank=True)
class hoja_catastroAdmin(admin.ModelAdmin):
    list_display = ['periodo']
    search_fields = ['periodo']
admin.site.register(hoja_catastro, hoja_catastroAdmin) 
