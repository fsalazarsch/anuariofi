from django.contrib import admin, messages
from .models import operacion, detalle_fondo, fondo, serie, cuota
from acafi.view.graficos import generar_json
from acafi.view.fondo import regenerar_pdf
from django.conf import settings
from io import BytesIO
from zipfile import ZipFile
import os, unicodedata
from django.http import HttpResponse
from weasyprint import HTML, CSS

# Register your models here.
class operacionAdmin(admin.ModelAdmin):

    actions = ['scrapear', 'generar_indices', 'generar_pdf', 'descargar_pdf', 'regenerar_pdf_catastro', 'descargar_pdf_factsheets']
    search_fields = ['runsvs', 'nombre', 'admin__razon_social', 'admin__rut']
    
    def rent(self):
        df = detalle_fondo.objects.get(pk=self)
        return df.rentabilidad

    def scrapear(self, request, queryset):
        for f in queryset:
            if f.vigente == True:
    
                print(f.runsvs)
                url = f.urlsvs
                url = url[:-1] + '7'
                url = url.replace('auth=&send=&', '')
                url = url.replace('pestania=1', 'pestania=7')
        
                fecha_ini = f.inicio_operaciones
                fecha_fin = f.termino_operaciones

        
                fecha_ini = datetime.datetime.strptime('01/01/2018','%d/%m/%Y').date()

                #hace_20_dias = datetime.date.today() - datetime.timedelta(days=60)
                #fecha_ini = hace_20_dias
                
                '''else:
                    if fecha_ini < hace_20_dias:
                        fecha_ini = hace_20_dias
                '''
        
                if not fecha_fin or not isinstance(fecha_fin, (datetime.date, datetime)):
                    fecha_fin = datetime.datetime.today()

        
                dics = []
                dic = {'dia1' : revisar(fecha_ini.day), 'dia2' : revisar(fecha_fin.day), 'mes1' : revisar(fecha_ini.month),
                       'mes2' : revisar(fecha_fin.month), 'anio1' : str(fecha_ini.year), 'anio2' : str(fecha_fin.year),
                       'enviado' : '1', 'sub_consulta_fi' : 'Consultar'}
                colador = SoupStrainer("tr")

                hdr = {'User-Agent': 'Mozilla/5.0'}
                pedido = requests.post(url, data=dic, headers=hdr)
                
                sopa = BeautifulSoup(pedido.text, 'html.parser', parse_only=colador)
                iterable = sopa.find_all("tr")
                first = True
                llaves = []
                for caldo in iterable:

                    if first:
                        llaves = []
                        for tag in caldo.contents:
                            llave = str(tag.string)
                            llaves.append(limpiar(llave))
                        first = False
                        continue
                    else:
                        valores = [str(tag.string).strip().upper() for tag in caldo.contents]
                        dic = {x : y for x , y in zip(llaves, valores)}
                        string = json.dumps(dic, indent=2)
                        if "SIN INFORMACIÓN" in str(dic) or "SIN INFORMACION" in str(dic):
                            break
                    
                        try:
                            s = serie.objects.get(nombre = dic['serie'], fondo = f, fecha_inicio__isnull=False)
                            try:
                                c = cuota.objects.get(serie = s, fecha =format_time(dic['fecha']))                        
                            except cuota.DoesNotExist:
                                c = cuota(serie = s, fecha =format_time(dic['fecha']))                        
                                c.save()                    
                            
                            c.valor_libro=format_num(dic['valor libro'])
                            c.moneda = dic['moneda']
                            c.valor_economico = format_num(dic['valor economico'])
                            c.patrimonio_neto = format_num(dic['patrimonio neto'])
                            c.activo_total = format_num(dic['activo total'])
                            c.agencia = dic['agencia']
                            c.n_aportantes_inst = format_num(dic['no aportantes institucionales'])
                            c.n_aportantes = format_num(dic['no de aportantes'])
                            c.agencia = dic['agencia']
                            c.save()
                            print(c.fecha, c.patrimonio_neto)
                            
                            if c.get_divisa_fondo() != None:
                                gdf = c.get_divisa_fondo()
                                c.valor_libro_conv = convertir(gdf[0], gdf[1], c.valor_libro, c.fecha)
                                c.save()

                        except serie.DoesNotExist:
                            pass
        message_bit = "Fondos escrapeados"
        self.message_user(request,  message_bit)

    def generar_indices(self, request, queryset):
        for obj in queryset:
            print(obj.runsvs)
            try:
                s = detalle_fondo.objects.get(pk=obj).serie_antigua
                cuotas = cuota.objects.filter(serie = s)
                for c in cuotas:
                    if c.get_divisa_fondo() != None:
                        gdf = c.get_divisa_fondo()
                        #c.valor_libro_conv = convertir(gdf[0], gdf[1], c.valor_libro, c.fecha)
                        c.save()

                    #c.indice = get_indice(c)

                    if c.cuota_anterior() == None or c.cuota_anterior().indice == None:
                        if cuota.objects.filter(serie = c.serie, fecha__lt=c.fecha).exists():
                            aux = cuota.objects.filter(serie = c.serie, fecha__lt=c.fecha).order_by('fecha').last()
                            c.indice = aux.indice
                        else:
                            c.indice = 100
                    else:

                        indice_ant = float(c.cuota_anterior().indice)
                        c.indice = indice_ant*(1+float(c.rentab()))



                    c.save()
                    print(c.fecha)
                print( s.nombre)
            except (detalle_fondo.DoesNotExist, AttributeError):
                pass

        message_bit = "Fondos con índices generados"
        self.message_user(request,  message_bit)
        pass
    def generar_pdf(self, request, queryset):
        for obj in queryset:
            #generar_json_series(obj, ns.nombre)
            ns = detalle_fondo.objects.get(pk=obj).serie_antigua
            ns = ns.nombre
            #nombre serie

            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"series/json/"

            datos = []
            label = []
            s = serie.objects.get(fondo =obj, nombre=ns)
            ns = ns.replace('/', '')
            cuotas = cuota.objects.filter(serie=s, fecha__lt="2018-01-01").order_by('fecha')

            for c in cuotas:
                c.fecha= c.fecha.strftime('%d/%m/%Y')
                if c.indice == None:
                    c.indice = 0
                datos.append( float (round(c.indice, 2)) )
                label.append(str(c.fecha))


            #grafico rentablidad
            texto ="{\n\
                        chart: {\n\
                            renderTo: 'graficoRentabilidad',\n\
                            type: 'line',\n\
                            marginRight: 0,\n\
                            marginBottom: 80,\n\
                            backgroundColor: '#FFFFFF',\n\
                            borderColor: '#FFFFFF',\n\
                            borderWidth: 0\n\
                        },\n\
                        xAxis: {\n\
                            categories: "+str(label)+",\n\
                            lineWidth : 0,\n\
                            gridLineWidth: 0,\n\
                            tickLength: 0,\n\
                            labels: {\n\
                                style: {\n\
                                    font: 'normal 9px Helvetica, Arial, Geneva, sans-serif',\n\
                                    lineHeight: '9px'\n\
                                }\n\
                            },\n\
                            endOnTick: false,\n\
                            showFirstLabel: true,\n\
                            startOnTick: false\n\
                        },\n\
                        \n\
                        yAxis: {\n\
                            title: {\n\
                                text: ''\n\
                            },\n\
                            tickColor: '#C0D0E0',\n\
                            tickWidth: 1,\n\
                            tickLength: 4,\n\
                            lineWidth : 1,\n\
                            gridLineWidth: 0,\n\
                            startOnTick:false,\n\
                            plotLines: [{\n\
                            value: 0,\n\
                            width: 0,\n\
                            color: '#808080'\n\
                            }],\n\
                            tickPixelInterval: 30,\n\
                            alternateGridColor: 'rgba(112, 113, 115, 0.1)',\n\
                            labels: {\n\
                                formatter: function() {\n\
                                    return Highcharts.numberFormat(this.value,0, ',', '.');\n\
                                },\n\
                                style: {\n\
                                    font: 'normal 10pt Helvetica, Arial, Geneva, sans-serif'\n\
                                }\n\
                            }\n\
                        },\n\
                        legend: {\n\
                            layout: 'horizontal',\n\
                            align: 'right',\n\
                            verticalAlign: 'bottom',\n\
                            floating:false,\n\
                            x: 0,\n\
                            y: 15,\n\
                            borderWidth: 1,\n\
                            borderColor: 'rgba(112, 113, 115, 0.2)',\n\
                            symbolWidth: 12,\n\
                            symbolPadding: 2,\n\
                            itemMarginTop: 0,\n\
                            itemMarginBottom: 0,\n\
                            padding: 8,\n\
                            itemStyle: {\n\
                                font: 'normal 11px Helvetica, Arial, Geneva, sans-serif'\n\
                            }\n\
                        },\n\
                        plotOptions: {\n\
                            series: {\n\
                                lineWidth: 2,\n\
                                shadow: false,\n\
                                animation: false,\n\
                                marker: {\n\
                                    enabled: false\n\
                                }\n\
                            }\n\
                        },\n\
                        credits: {\n\
                            enabled: false\n\
                        },\n\
                        title: {\n\
                            text: ''\n\
                        },\n\
                        exporting: {\n\
                            enabled: false\n\
                        },\n\
                        colors: ['rgba(61, 70, 76, 1)'],\n\
                        series: [\n\
                        \n\
                        {\n\
                            name: 'Rentabilidad Serie',\n\
                            data: "+str(datos)+"    \n\
                        }                \n\
                        ],\n\
                        pointInterval: 24 * 3600 * 1000\n\
                        }"
            arch = open(ruta+str(obj.runsvs)+"_"+str(ns)+"_grafico1.js", "w+", encoding="utf-8")
            arch.write(str(texto))
            arch.close()
            print("Generado el js "+ruta+str(obj.runsvs)+"_"+str(ns)+"_grafico1.js")
            #generar_png_series(str(fond)+"_"+str(nombre))

            comando =  "sudo phantomjs highcharts-convert.js -infile "
            comando += ruta+str(obj.runsvs)+"_"+str(ns)+"_grafico1.js "
            comando += "-outfile ev/anuariofi/acafi/static/series/imgs/"+str(obj.runsvs)+"_"+str(ns)+"_1.png"
            comando += " -width 750 -height 500 -resources "
            comando += "ev/anuariofi/acafi/static/bower_components/jquery/dist/jquery.js,"
            comando += "ev/anuariofi/acafi/static/bower_components/highcharts/highcharts.js"
            ruta = settings.STATIC_ROOT+settings.STATIC_URL+"series/json/"
            print(ruta+str(obj.runsvs)+"_grafico1.sh")
            
            arch = open(ruta+str(obj.runsvs)+"_grafico1.sh", "w+", encoding="utf-8")
            arch.write(str(comando))
            arch.close()
            os.system("sudo sh "+ruta+str(obj.runsvs)+"_grafico1.sh")

            
            #=====----   ---=======#
            html= HTML(url=settings.URL_BASE+'perfil/series/factsheet/'+str(obj.runsvs)+'/'+str(ns)+'/')
            html.write_pdf(settings.STATIC_ROOT+settings.STATIC_URL+'series/'+str(obj.admin.razon_social)+"/"+ '[' + str(obj.runsvs) + '] ' + \
            unicodedata.normalize('NFKD', str(obj.nombre)).encode('ascii', 'ignore').decode("utf-8")
            +'.pdf', stylesheets=[CSS(settings.STATIC_ROOT+'/static/dist/css/anuario.css')])
            #os.remove("ev/anuariofi/acafi/static/series/imgs/"+str(obj.runsvs)+"_"+str(ns)+"_1.png" )

        message_bit = "PDF generados"
        self.message_user(request,  message_bit)            
        pass

    def descargar_pdf(self, request,queryset):
        archivos = []
        
        path = settings.STATIC_ROOT+settings.STATIC_URL+'series/'

        in_memory = BytesIO()
        zf = ZipFile(in_memory, mode="w")
        contador = 0

        for obj in queryset:
            print(obj.runsvs)
        


            for root, dirs, files in os.walk(path, topdown=False):
                for name in files:
                    if obj.runsvs in name and '.pdf' in name:
                        contador += 1
                        print(os.path.join(root, name))
                        zf.write(os.path.join(root, name), name)
                    

        

        for file in zf.filelist:
            file.create_system = 0

        zf.close()

        if contador == 0:
            message_bit = 'Los fondos seleccionados no tienen pdf'
            self.message_user(request,  message_bit, level=messages.ERROR)
            pass
        else:
            #message_bit = str(contador) +' fondos comprimidos'
            #self.message_user(request,  message_bit)

            response = HttpResponse(content_type="application/zip")
            response["Content-Disposition"] = "attachment; filename=fondos.zip"
        
            in_memory.seek(0)    
            response.write(in_memory.read())
            return response

    def regenerar_pdf_catastro(self, request, queryset):
        for obj in queryset:
            print(obj.runsvs)
            anio= 2017
            path = settings.STATIC_ROOT+settings.STATIC_URL+'tmp/'+str(anio)+'/'
            if not os.path.isfile(path+obj.runsvs+'.pdf'):
                regenerar_pdf(request, obj.runsvs, anio)
        message_bit = "PDF de factshets de fondos al año "+str(anio)+" generados"
        self.message_user(request,  message_bit)

    def descargar_pdf_factsheets(self, request, queryset):
        archivos = []
        contador = 0
        in_memory = BytesIO()
        zf = ZipFile(in_memory, mode="w")
    
        for obj in queryset:

            print(obj.runsvs)
            anio = 2017

            path = settings.STATIC_ROOT+settings.STATIC_URL+'tmp/'+str(anio)+'/'


            
            if os.path.isfile(path+obj.runsvs+'.pdf'):
                contador += 1
                if obj.categoria == None:
                    categoria_nombre = 'Sin Categoria'
                else:
                    categoria_nombre = obj.categoria.nombre

                zf.write(path+obj.runsvs+'.pdf', categoria_nombre+'/'+obj.runsvs)
                #zf.write(path+obj.runsvs+'.pdf', obj.runsvs+'.pdf')

        zf.close()
        if contador == 0:
            message_bit = 'Los fondos seleccionados no tienen pdf'
                
            self.message_user(request,  message_bit, level=messages.ERROR)
            pass
        else:
            message_bit = str(contador) +' factsheets comprimidos'
            self.message_user(request,  message_bit)

            response = HttpResponse(content_type="application/zip")
            response["Content-Disposition"] = "attachment; filename=factsheets.zip"
            
            in_memory.seek(0)    
            response.write(in_memory.read())
            return response



    scrapear.short_description = "Escrapear"
    generar_indices.short_description = "Generar Índices"
    generar_pdf.short_description = "Generar PDF"
    descargar_pdf.short_description = "Descargar PDF"
    regenerar_pdf_catastro.short_description = "Generar PDF Factsheets"
    descargar_pdf_factsheets.short_description = "Descargar PDF (por categoria)"


    list_display = ['runsvs', 'nombre', 'vigente', 'desactivar_rentabilidades', rent, 'admin', 'inicio_operaciones']
admin.site.register(operacion, operacionAdmin)