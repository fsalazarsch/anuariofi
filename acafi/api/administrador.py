import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import administrador

#API_ADMINISTRADOR
@api_view(['GET'])
def get_administradores(request):
    if request.method == 'GET':

        cats = list(administrador.objects.all().values('rut', 'razon_social', 'vigente', 'fecha_inscripcion'))
        
        for c in cats:
            if c['fecha_inscripcion'] != None:
                c['fecha_inscripcion'] = c['fecha_inscripcion'].strftime('%d/%m/%Y')
            else:
                c['fecha_inscripcion'] = ''
        return Response({"response": "OK", "Codigo" : 200, "administradores" : cats })
    
@api_view(['GET'])
def get_administrador(request, id = None):

    if request.method == 'GET':

        cat = list(administrador.objects.filter(rut=id).values('rut', 'razon_social', 'vigente', 'fecha_inscripcion'))
        if cat[0]['fecha_inscripcion'] != None:
            cat[0]['fecha_inscripcion'] = cat[0]['fecha_inscripcion'].strftime('%d/%m/%Y')
        else:
            cat[0]['fecha_inscripcion'] = ''
        return Response({"response": "OK",  "Codigo": 200, "administrador" : cat })
