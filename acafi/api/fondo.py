import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import fondo, fondo_corfo, inversion
from acafi.scraping.mini_scrapper import get_periodo_actual
from django.db.models import Sum
from django.db.models import F

#API_FONDO
@api_view(['GET', 'POST'])
def get_fondos(request):
    if request.method == 'GET':

        cats = list(fondo.objects.all().values('runsvs', 'nombre', 'admin', 'vigente', 'tipo_inversion'))
        return Response({"response": "OK", "Codigo": 200, "fondos" : cats })
        
    
@api_view(['GET'])
def get_fondo(request, id = None):

    if request.method == 'GET':

        cat = list(fondo.objects.filter(pk=id).values('runsvs', 'nombre', 'admin', 'vigente', 'tipo_inversion'))
        return Response({"response": "OK", "Codigo": 200, "fondo" : cat })

@api_view(['GET', 'POST'])
def get_fondos_corfo(request):
    if request.method == 'GET':

        cats = list(fondo_corfo.objects.all().values('rut', 'nombre', 'cifras', 'tipo_fondo'))
        return Response({"response": "OK", "Codigo": 200, "fondos_corfo" : cats })
        
    
@api_view(['GET'])
def get_fondo_corfo(request, id = None):

    if request.method == 'GET':

        cat = list(fondo_corfo.objects.filter(pk=id).values('rut', 'nombre', 'cifras', 'tipo_fondo'))
        return Response({"response": "OK", "Codigo": 200, "fondo_corfo" : cat })


@api_view(['GET'])
def get_region_exposure(request, runsvs ):
    aux = get_periodo_actual().split('-')
    periodo = str(aux[1])+'-'+str(aux[0])
    try:
        f = fondo.objects.get(pk= runsvs)
        inversiones = inversion.objects.filter(fondo=f, periodo= periodo).values(region=F('cod_pais_transaccion__region__nombre')).annotate(suma_porcentaje = Sum('porcentaje')) \
        .order_by('cod_pais_transaccion__region')

        return Response({"response": "OK", "Codigo": 200, 'inversiones' : list(inversiones) })
    except fondo.DoesNotExist:
        return Response({"Response": "Error", "Codigo" : 400, "Mensaje": "No existe el fondo en la base de datos de Fondos de Inversión"})


    #total = {}
    #for i in inversiones:




