import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from django.http import HttpResponse

#API_USER
@api_view(['GET', 'POST'])
def get_users(request):
    if request.method == 'GET':

        try:
            username = request.query_params['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.query_params["password"]
        except MultiValueDictKeyError:
            passwd = None


        if passwd == "" or passwd == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password"})
        if username == "" or username == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "username"})

        try:
            p = User.objects.get(username=username)
            if p.check_password(passwd):
                    us = list(User.objects.all().values('username', 'first_name', 'last_name', 'email'))
                    return Response({"response": "OK", "Codigo":200, "users" : us })
            else:
                return Response({"response": "Error", "Codigo": 402, "msj" : "Error de autenticacion"})
        except User.DoesNotExist:
            return Response({"response": "Error", "Codigo": 403, "msj" : "El usuario no existe"})


    elif request.method == 'POST':
        try:
            username = request.data['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.data["password"]
        except MultiValueDictKeyError:
            passwd = None

        try:
            nombre_usuario = request.data['nombre_usuario']
        except MultiValueDictKeyError:
            nombre_usuario = None

        try:
            password_usuario = request.data['password_usuario']
        except MultiValueDictKeyError:
            password_usuario = None

        try:
            email_usuario = request.data['email_usuario']
        except MultiValueDictKeyError:
            email_usuario = None

        try:
            first_name = request.data['first_name']
        except MultiValueDictKeyError:
            first_name = None

        try:
            last_name = request.data['last_name']
        except MultiValueDictKeyError:
            last_name = None

        try:
            nivel_acceso = request.data['nivel_acceso']
        except MultiValueDictKeyError:
            nivel_acceso = None

        #return Response(request.data)
        if passwd == "" or passwd == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password usuario autenticacion"})
        if username == "" or username == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre usuario autenticacion"})

        p = User.objects.get(username=username)
        if p.check_password(passwd):
            if(username == nombre_usuario):
                return Response({"response": "Error", "Codigo": 404, "msj" : "Ya existe el usuario"})
            else:

                if nombre_usuario == "" or nombre_usuario == None:
                    return Response({"response": "Error",  "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre de usuario a crear"})
                if password_usuario == "" or password_usuario == None:
                    return Response({"response": "Error",  "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password de usuario a crear"})
                if nivel_acceso == "" or nivel_acceso == None:
                    return Response({"response": "Error",  "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nivel de acceso de usuario a crear"})

                if email == None:
                    email = ""
      
                u = User(username = nombre_usuario, password= make_password(password_usuario), email = email_usuario, is_active = 1, is_staff = 0, is_superuser = 0, date_joined = strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                u.save()
                
                prof = User.objects.latest('id')
                perf = perfil(usuario = prof, nivel_acceso= nivel_acceso, id_usercreador = p)
                perf.save()
                
                return Response({"response": "Ok",  "Codigo": 200, "msj" : "Usuario creado exitosamente"}) 
        else:
            return Response({"response": "Error",  "Codigo": 402, "msj" : "Error de autenticacion"})           

@api_view(['GET', 'PUT'])
def get_user(request, id = None):
    if request.method == 'GET':  
        try:
            username = request.query_params['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.query_params["password"]
        except MultiValueDictKeyError:
            passwd = None

        if passwd == "" or passwd == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password"})
        if username == "" or username == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "username"})

        try:
            p = User.objects.get(username=username)
            if p.check_password(passwd):                
                if type(id) == int:
                    us = list(User.objects.filter(pk = id).values('username', 'password', 'first_name', 'last_name', 'email'))
                else:
                    us = list(User.objects.filter(username = id).values('username', 'password', 'first_name', 'last_name', 'email'))

                
                return Response({"response": "OK", "Codigo": 200, "user" : us })
            else:
                return Response({"response": "Error", "Codigo": 402, "msj" : "Error de autenticacion"})       
        except User.DoesNotExist:
            return Response({"response": "Error", "Codigo": 403, "msj" : "El usuario no existe"})       

