import json, operator
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import fondo, movimiento, serie, transaccion_cuota
from django.db.models import Q, Sum


#************************ API RESUMEN CATASTRO ***************************
@api_view(['GET'])
def get_volumen_cuotas_mes (request, period):
    res = []
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]   
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')

    meses = []
    meses.append('12-2018')
    for i in range(2019, int(aux[1])+1):
        for j in range(1,13):
            if i < int(aux[1]): 
                meses.append(str(j)+"-"+str(i))
            elif i == int(aux[1]) and j <= int(aux[0]):
                meses.append(str(j)+"-"+str(i))

    for l in lista:
        fila = {}
        fila['run_fon'] = l.runsvs.split('-')[0]
        fila['nom_fon'] = l.nombre
        fila['run_adm'] = l.admin.rut
        fila['nom_adm'] = l.admin.razon_social

        if l.categoria == None:
            fila['clase'] = ''
        else:
            fila['clase'] = l.categoria.indice

        for pers in meses:
            aux = pers.split('-')
            # por cada fondo buscar todos sus nemos
            nemos = serie.objects.filter(fondo = l).values_list('nemotecnico', flat=True)
            # de esos nemos sacar la suma  de sus cuotas transadas
            # en virtud de su mes y años
            tc = transaccion_cuota.objects.filter(nemotecnico__in = nemos , fecha__month=str(aux[0]), fecha__year = str(aux[1])).aggregate(Sum('cuotas_transadas'))
            if tc['cuotas_transadas__sum'] == None:
                fila[pers] = 0
            else:
                fila[pers] = tc['cuotas_transadas__sum']

        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "volumen_cuotas_mes" : res })

