import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import categoria

#API_CATEGORIA
@api_view(['GET', 'POST'])
def get_categorias(request):
    if request.method == 'GET':
        cats = list(categoria.objects.all().values('indice', 'nombre', 'categoria_madre'))
        return Response({"response": "OK", "Codigo": 200, "categorias" : cats })
        
    elif request.method == 'POST':
        try:
            username = request.data['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.data["password"]
        except MultiValueDictKeyError:
            passwd = None

        try:
            indice = request.data['indice']
        except MultiValueDictKeyError:
            indice = None

        try:
            nombre = request.data['nombre']
        except MultiValueDictKeyError:
            nombre = None

        try:
            categoria_madre = request.data['categoria_madre']
        except MultiValueDictKeyError:
            categoria_madre = None

        #return Response(request.data)
        if passwd == "" or passwd == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password usuario autenticacion"})
        if username == "" or username == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre usuario autenticacion"})

        try:
            p = User.objects.get(username=username)
            if p.check_password(passwd) and (p.is_superuser == 1):
                if indice == "" or indice == None:
                    return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "indice"})
                if nombre == "" or nombre == None:
                    return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre"})

                cm = categoria.objects.filter(nombre = categoria_madre)
                if not cm:
                    cm = None
                else:
                    cm = cm[0]

                c = categoria(indice = indice.upper(), nombre= nombre, categoria_madre = cm)
                c.save()
                return Response({"response": "Ok", "Codigo": 200, "msj" : "Categoria creada exitosamente"}) 
            else:
                return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para crear una categoria"})
        except User.DoesNotExist:
            return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para eliminar una categoria"})


@api_view(['GET', 'PUT', 'DELETE'])
def get_categoria(request, id = None):

    if request.method == 'GET':
        cat = list(categoria.objects.filter(pk = id).values('indice', 'nombre', 'categoria_madre'))
        return Response({"response": "OK",  "Codigo": 200, "categoria" : cat })

    if request.method == 'PUT':

        try:
            username = request.query_params['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.query_params["password"]
        except MultiValueDictKeyError:
            passwd = None

        if passwd == "" or passwd == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password"})
        if username == "" or username == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "username"})


        try:
            nombre = request.query_params['nombre']
        except MultiValueDictKeyError:
            nombre = None

        try:
            categoria_madre = request.query_params["categoria_madre"]
        except MultiValueDictKeyError:
            categoria_madre = None

        try:
            p = User.objects.get(username=username)
            if p.check_password(passwd) and (p.is_superuser == 1):
                try:
                    cat = categoria.objects.get(pk = id)
                
                    if nombre != "" and nombre != None:
                        cat.nombre = nombre
                        cat.save()

                    if categoria_madre != "" and categoria_madre != None:
                        aux = categoria.objects.get(nombre= categoria_madre)
                        cat.categoria_madre = aux
                        cat.save()

                    return Response({"response": "Ok", "Codigo": 200, "msj" : "Categoria modificada correctamente"})

                except categoria.DoesNotExist:
                    return Response({"response": "Error", "Codigo": 402, "msj" : "La categoria no existe"})
            else:
                return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para modificar una categoria"})
        except User.DoesNotExist:
            return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para eliminar una categoria"})



    if request.method == 'DELETE':

        try:
            username = request.data['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.data["password"]
        except MultiValueDictKeyError:
            passwd = None

        if passwd == "" or passwd == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password usuario autenticacion"})
        if username == "" or username == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre usuario autenticacion"})

        try:
            p = User.objects.get(username=username)
            if p.check_password(passwd) and (p.is_superuser == 1):
                try:
                    cat = categoria.objects.get(pk = id)        
                    cat.delete()
                    return Response({"response": "Ok", "Codigo": 200, "msj" : "Se ha borrado la categoria correctamente"})                
                except categoria.DoesNotExist:
                    return Response({"response": "Error", "Codigo": 402, "msj" : "La categoria no existe"}) 
            else:
                return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para eliminar una categoria"})
        except User.DoesNotExist:
            return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para eliminar una categoria"})

