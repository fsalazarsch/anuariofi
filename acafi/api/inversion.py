import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import inversion

#API_INVERSION
@api_view(['GET'])
def get_inversiones(request, fondo, periodo ):

    if request.method == 'GET':
        cat = list(inversion.objects.filter(periodo=periodo, fondo__runsvs=fondo).values('empresa', 'pais', 'porcentaje'))

        for c in cat:
                c['porcentaje'] =float(c['porcentaje'])
        return Response({"response": "OK", "Codigo": 200, "inversion" : cat })
