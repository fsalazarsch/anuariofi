import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import fondo, movimiento, categoria, administrador, sector, inversion, resumen


#************************ API ACTIVOS FONDOS NUEVOS ***************************
@api_view(['GET'])
def get_activos_fondos_nuevos(request, period):
    res = []

    aux = period.split('-')      
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
    lista = lista.filter(inicio_operaciones__month__gte=int(aux[0])-2)
    lista = lista.exclude(categoria=0).order_by('categoria')


    for l in lista:
        fila = {}
        if l.categoria == None:
            categ = 0
        else:
            categ = l.categoria.indice
            
        if l.tipo_inversion == 1:
            l.tipo_inversion = 'R'
        else:
            l.tipo_inversion = 'NR'

        mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
        if 'dolar' in mov.moneda.lower() or 'dólar' in mov.moneda.lower():
            mon = 'US$'
        elif 'euro' in mov.moneda.lower():
            mon = 'Euro'
        else:
            mon = '$'

        fila['nombre_fondo'] = l.nombre
        fila['administradora'] = l.admin.razon_social
        fila['run_fondo'] = l.runsvs.split('-')[0]
        fila['clase'] = categ
        fila['moneda'] = mon
        fila['tipo'] =  l.tipo_inversion
        fila['activos'] = float(mov.monto_real('pesos'))
        
        res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "activos_fondos_nuevos" : res })

#************************ API TOTAL ACTIVOS ***************************
@api_view(['GET'])
def get_total_activos(request, period):

    res = []
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]   
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria=0).order_by('categoria')
    
    categ= [{"categoria" : "Deuda" , "codigo": [11]},
        {"categoria":"Accionarios","codigo": [12]},
        {"categoria":"Mixtos","codigo": [13]},
        {"categoria":"Otros Mobiliarios","codigo" : [14]},
        {"categoria":"Activos Alternativos","codigo": [20]},
        {"categoria": "Inmobiliarios","codigo": [2111, 2121, 2201]},
        {"categoria":"Capital Privado","codigo": [2112, 2122, 2202]},
        {"categoria":"Fondo de Fondos","codigo" : [2113, 2123, 2203]},
        {"categoria":"Deuda Privada" ,"codigo": [2114, 2124, 2204]},
        {"categoria":"Infraestructura y Energia" ,"codigo": [2115, 2125, 2205]},
        {"categoria":"Otros Alternativos" ,"codigo": [2116, 2126, 2206]}]
    
    total = 0
    total_fondos = 0
    for cat in categ:
        fila = {}
        suma = 0
        num_fondos = 0
        fila['categoria'] = cat.get('categoria')
        aux = cat.get('codigo')
        try:
            for a in aux:
                filtrado_lista = lista.filter(categoria__indice__startswith=a)

                for l in filtrado_lista:
                    try:
                        mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
                        suma += float(mov.monto_real('pesos'))
                        num_fondos +=1
                    except movimiento.DoesNotExist:
                        pass

            fila['monto'] = suma
            fila['cant_fondos'] = num_fondos
            total += suma
            total_fondos += num_fondos
        except (movimiento.DoesNotExist, KeyError):
            pass
        res.append(fila)

    fila={}
    fila['categoria'] = 'TOTAL'
    fila['monto'] = total
    fila['cant_fondos'] = total_fondos 
    res.append(fila)
    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "total_activos" : res })

#************************ API ACTIVOS FONDOS ***************************
@api_view(['GET'])
def get_activos_fondos(request, period):
  
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria =0).order_by('categoria')

    categs = categoria.objects.exclude(pk=0).order_by('indice')
    res = []
    for c in categs:
        fila = {}
        if c.tiene_fondos() == False:
            if int(c.indice) % 100 == 0:
                fila['tipo'] = 'Clase'
                fila['nombre'] = c.nombre
                fila['run'] = ''
                fila['indice'] = c.indice
                fila["activos"]= ''

            else:
                fila['tipo'] = 'Subclase'
                fila['nombre'] = c.nombre
                fila['run'] = ''
                fila['indice'] = c.indice
                fila["activos"]= ''

        else:
            fila['tipo'] = 'Subclase'
            fila['nombre'] = c.nombre
            fila['run'] = ''
            fila['indice'] = c.indice
            fila["activos"]= ''

        res.append(fila)

        for l in lista.filter(categoria=c):
            fila = {}
            if l.categoria == None:
                lcategoriaindice = ''
            else:
                lcategoriaindice = l.categoria.indice

            fila['tipo'] = 'Fondo'
            fila['nombre'] = l.nombre
            fila['run'] = l.runsvs.split('-')[0]
            fila['indice'] = lcategoriaindice
            try:
                mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
                fila["activos"]= float(mov.monto_real('pesos'))
            
            except (movimiento.DoesNotExist, KeyError):
                fila["activos"]= ''
            if fila.get('activos')== '':
                pass
            else:
                res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "activos_fondos" : res })

#************************ API ACTIVOS POR AFI ***************************
@api_view(['GET'])
def get_activos_por_afi(request, period):
    
    res = []
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]   
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria=0).order_by('categoria')
    


    categ= [{"categoria" : "Deuda" , "codigo": [11]},
        {"categoria":"Accionarios","codigo": [12]},
        {"categoria":"Mixtos","codigo": [13]},
        {"categoria":"Otros Mobiliarios","codigo" : [14]},
        {"categoria":"Activos Alternativos","codigo": [20]},
        {"categoria": "Inmobiliarios","codigo": [2111, 2121, 2201]},
        {"categoria":"Capital Privado","codigo": [2112, 2122, 2202]},
        {"categoria":"Fondo de Fondos","codigo" : [2113, 2123, 2203]},
        {"categoria":"Deuda Privada" ,"codigo": [2114, 2124, 2204]},
        {"categoria":"Infraestructura y Energia" ,"codigo": [2115, 2125, 2205]},
        {"categoria":"Otros Alternativos" ,"codigo": [2116, 2126, 2206]}]

    admins = administrador.objects.filter().order_by('razon_social')
    for ad in admins:
        total = 0
        fila={}
        if ad.tiene_fondos(str(anio)+'-'+str(mes)+'-01'):
            fila['run_adm'] = ad.rut
            fila['nombre'] = ad.razon_social
            for cat in categ:
                suma = 0
                nom = cat.get('categoria')
                aux = cat.get('codigo')
                for a in aux:
                    for l in lista.filter(categoria__indice__startswith=a, admin=ad):
                        try:
                            mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
                            suma += float(mov.monto_real('pesos'))
                        except movimiento.DoesNotExist:
                            pass
                fila[nom] = suma
                total+=suma
            fila['total'] = total
            res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "activos_por_afi" : res })

#************************ API ACTIVOS FONDOS POR AFI ***************************
@api_view(['GET'])
def get_activos_fondos_afi(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria =0).order_by('categoria')

    categ= [{"categoria" : "Deuda" , "codigo": [11]},
    {"categoria":"Accionarios","codigo": [12]},
    {"categoria":"Mixtos","codigo": [13]},
    {"categoria":"Otros Mobiliarios","codigo" : [14]},
    {"categoria":"Activos Alternativos","codigo": [20]},
    {"categoria": "Inmobiliarios","codigo": [2111, 2121, 2201]},
    {"categoria":"Capital Privado","codigo": [2112, 2122, 2202]},
    {"categoria":"Fondo de Fondos","codigo" : [2113, 2123, 2203]},
    {"categoria":"Deuda Privada" ,"codigo": [2114, 2124, 2204]},
    {"categoria":"Infraestructura y Energia" ,"codigo": [2115, 2125, 2205]},
    {"categoria":"Otros Alternativos" ,"codigo": [2116, 2126, 2206]}]

    admins = administrador.objects.filter().order_by('razon_social')
    res= []
    cont = 0
    for ad in admins:
        total = 0
        fila={}
        if ad.tiene_fondos(str(anio)+'-'+str(mes)+'-01'):
            
            for l in lista.filter(admin=ad):
                fila = {}
                if l.categoria == None:
                    lcategoriaindice = ''
                else:
                    lcategoriaindice = l.categoria.indice

                fila['run_adm'] = ad.rut
                fila['nombre_adm'] = ad.razon_social
                fila['nombre_fon'] = l.nombre
                fila['run'] = l.runsvs.split('-')[0]
                fila['indice'] = lcategoriaindice
                for cat in categ:
                    aux = cat.get('codigo')
                    for a in aux:
                        if lcategoriaindice.startswith(str(a)):
                            fila['nom_categ'] = cat.get('categoria')

                try:
                    mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
                    fila["activos"]= float(mov.monto_real('pesos'))
                
                except (movimiento.DoesNotExist, KeyError):
                    fila["activos"]= ''
                if fila.get('activos')== '':
                    pass
                else:
                    res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "activos_fondos_afi" : res })

#************************ API ACTIVOS POR AFI ***************************
@api_view(['GET'])
def get_activos_por_clase(request, period):
    
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria =0).order_by('categoria')

    categs = categoria.objects.exclude(pk=0).order_by('indice')
    res = []
    total = 0
    for c in categs:

        fila = {}
        suma = 0
        cat = c.indice
        cat = cat.strip('0')

        for l in lista.filter(categoria__indice__startswith=cat):

            try:
                mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL ACTIVO")
                suma += float(mov.monto_real('pesos'))

            except (movimiento.DoesNotExist, KeyError):
                pass
        if len(cat) == 1:
            total += suma
        fila['nombre_cat'] = c.nombre
        fila['indice'] = c.indice
        fila['monto'] = suma
        res.append(fila)
    fila= {}
    fila['nombre_cat'] = 'TOTAL'
    fila['indice'] = ''
    fila['monto'] = total
    res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "activos_por_clase" : res })

#************************ API INVERSIONES NACIONALES ***************************
@api_view(['GET'])
def get_inversiones_nac(request, period):

    res = []
    sectores  = sector.objects.order_by('nombre')
    inversiones = inversion.objects.filter(periodo=period, instrumento="ACC").values('monto','nemotecnico','periodo')

    categorias = []
    for s in sectores:
        if s.nombre in categorias:
            pass
        else:
            categorias.append(s.nombre)
    
    nemos = []
    for s in sectores:
        if s.nombre != 'Otros':
            nemos.append(s.nemotecnico)

    otros = []
    for s in sectores:
        if s.nombre == 'Otros':
            otros.append(s.nemotecnico)

    categorias.remove('Otros')
    total = 0

    for cat in categorias:
        fila = {}
        suma = 0
        fila['nombre'] = cat
        for s in sectores:
            if cat == s.nombre:
                for inv in inversiones:
                    if s.nemotecnico == inv.get('nemotecnico'):
                        suma += inv.get('monto')

        fila['monto'] = suma
        total+=suma
        res.append(fila)

    suma = 0
    for inv in inversiones:
        if inv.get('nemotecnico') not in nemos or inv.get('nemotecnico') in otros:
            suma += inv.get('monto')

    fila ={}
    fila['nombre'] = 'Otros'
    fila['monto'] = suma

    fila1 ={}
    fila1['nombre']= 'TOTAL'
    fila1['monto']= total+suma

    res.append(fila)
    res.append(fila1)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "inversiones_nac" : res })


#************************ API INVERSIONES EXTRANJERAS ***************************
@api_view(['GET'])
def get_inversiones_ext(request, period):

    res = []
    categorias  = ['Acciones de sociedades anónimas abiertas',
            'Acciones de sociedades anónimas inmobiliarias y concesionarias',
            'Acciones no registradas',
            'Bienes raíces',
            'Cartera de créditos o de cobranzas',
            'Certificados de depósitos de valores (CDV)',
            'Cuotas de fondos de inversión',
            'Cuotas de fondos de inversión privados',
            'Cuotas de fondos mutuos',
            'Depósitos a plazo y otros títulos de bancos e instituciones financieras',
            'Derechos preferentes de suscripción de acciones de sociedades anónimas abiertas',
            'Deuda de operaciones de leasing',
            'Otras inversiones',
            'Otros títulos de deuda',
            'Otros títulos de renta variable',
            'Proyectos en desarrollo',
            'Títulos de deuda no registrados',
            'Títulos emitidos o garantizados por Estados o Bancos Centrales',
            'Títulos que representen productos']

    inversiones = resumen.objects.filter(periodo__periodo=period).values('nacional','extranjero','descripcion')

    total_ext = 0
    total_nac = 0

    for cat in categorias:
        fila = {}
        ext = 0
        nac = 0
        fila['categoria'] = cat
        for inv in inversiones:
            if cat == inv.get('descripcion'):
                ext += float(inv.get('extranjero'))
                nac += float(inv.get('nacional'))
        fila['monto_ext'] = ext
        fila['monto_nac'] = nac
        total_ext += ext
        total_nac += nac
        res.append(fila)

    fila ={}
    fila['nombre']= 'TOTAL'
    fila['monto_ext']= total_ext
    fila['monto_nac']= total_nac

    res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "inversiones_ext" : res })