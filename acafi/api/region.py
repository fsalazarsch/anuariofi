import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import region, pais

#API_PAIS
@api_view(['GET'])
def get_regiones(request):
    if request.method == 'GET':

        cats = list(region.objects.all().values('id', 'nombre'))
        return Response({"response": "OK", "Codigo": 200, "regiones" : cats })
        
    
@api_view(['GET'])
def get_region(request, id = None):
    cats = {}
    if request.method == 'GET':
        regs = region.objects.filter(nombre__icontains=id)
        for r in regs:
            aux = {}
            paises = pais.objects.filter(region=r).values_list('nombre', flat=True)
            aux['nombre'] = r.nombre
            aux['paises'] =  paises
            
            cats[r.id] = aux

        return Response({"response": "OK", "Codigo": 200, "regiones" : cats })
