import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import divisa

#API_DIVISA
@api_view(['GET'])
def get_divisas(request):
    if request.method == 'GET':

        cats = list(divisa.objects.all().values('divisa_id', 'nombre', 'alias_svs'))
        return Response({"response": "OK", "Codigo": 200, "divisas" : cats })
        
    
@api_view(['GET'])
def get_divisa(request, id = None):

    if request.method == 'GET':

        cats = list(divisa.objects.filter(pk=id).values('divisa_id', 'nombre', 'alias_svs'))
        return Response({"response": "OK", "Codigo": 200, "divisa" : cats })
