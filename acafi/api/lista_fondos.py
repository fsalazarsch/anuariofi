import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import fondo, movimiento
from django.db.models import Q


#************************ API LISTAR FONDOS VIGENTES ***************************
@api_view(['GET'])
def get_lista_fondos_vigentes(request, period):

    res = []
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]   
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')

    etiquetas = [{"etiqueta" : "TOTAL ACTIVO" , "obj": 'activo'},
                {"etiqueta":"TOTAL ACTIVO CORRIENTE","obj": 'act_corriente'},
                {"etiqueta":"TOTAL ACTIVO NO CORRIENTE","obj": 'act_no_corriente'},
                {"etiqueta":"TOTAL PASIVO","obj" : 'pasivo'},
                {"etiqueta":"TOTAL PASIVO CORRIENTE","obj": 'pas_corriente'},
                {"etiqueta":"TOTAL PASIVO NO CORRIENTE","obj": 'pas_no_corriente'},
                {"etiqueta":"TOTAL PATRIMONIO NETO  O","obj": 'patrimonio'}]

    for l in lista:
        fila = {}

        fila['run_adm'] = l.admin.rut
        fila['nom_adm'] = l.admin.razon_social
        fila['nom_fon'] = l.nombre
        fila['run_fon'] = l.runsvs

        
        if l.tipo_inversion == 1:
            fila['rescate'] = 'R'
        else:
            fila['rescate'] = 'NR'

        if l.categoria == None:
            fila['clase'] = 0
        else:
            fila['clase'] = l.categoria.indice

        if l.nemotecnicos() == None:
            fila['nemo'] = ''
        else:
            fila['nemo'] = l.nemotecnicos()

        fila['inicio'] = l.inicio_operaciones.strftime('%d-%m-%Y')
        
        if l.termino_operaciones != None:
            fila['termino'] = l.termino_operaciones.strftime('%d-%m-%Y')
        else:
            fila['termino'] = ''

        fila['link'] = l.urlsvs

        for e in etiquetas:
            obj = e.get('obj')
            et = e.get('etiqueta')
            try:
                mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs=et)
                fila[obj] = float(mov.monto_real('pesos'))
            except movimiento.DoesNotExist:
                fila[obj] = ''

        cont = 0
        for e in etiquetas:
            obj = e.get('obj')
            if fila[obj] == '':
                cont += 1

        if cont == 7:
            pass
        else:
            res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "lista_vigentes" : res })

#************************ API LISTAR FONDOS NUEVOS ***************************
@api_view(['GET'])
def get_lista_fondos_nuevos(request, period):

    res = []
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__year=aux[1], inicio_operaciones__month__lte=aux[0])
    lista = lista.filter(inicio_operaciones__month__gte=int(aux[0])-2)
    lista = lista.exclude(categoria=0).order_by('runsvs')

    etiquetas = [{"etiqueta" : "TOTAL ACTIVO" , "obj": 'activo'},
                {"etiqueta":"TOTAL ACTIVO CORRIENTE","obj": 'act_corriente'},
                {"etiqueta":"TOTAL ACTIVO NO CORRIENTE","obj": 'act_no_corriente'},
                {"etiqueta":"TOTAL PASIVO","obj" : 'pasivo'},
                {"etiqueta":"TOTAL PASIVO CORRIENTE","obj": 'pas_corriente'},
                {"etiqueta":"TOTAL PASIVO NO CORRIENTE","obj": 'pas_no_corriente'},
                {"etiqueta":"TOTAL PATRIMONIO NETO  O","obj": 'patrimonio'}]

    for l in lista:
        fila = {}

        fila['run_adm'] = l.admin.rut
        fila['nom_adm'] = l.admin.razon_social
        fila['nom_fon'] = l.nombre
        fila['run_fon'] = l.runsvs

        
        if l.tipo_inversion == 1:
            fila['rescate'] = 'R'
        else:
            fila['rescate'] = 'NR'

        if l.categoria == None:
            fila['clase'] = 0
        else:
            fila['clase'] = l.categoria.indice

        if l.nemotecnicos() == None:
            fila['nemo'] = ''
        else:
            fila['nemo'] = l.nemotecnicos()

        fila['inicio'] = l.inicio_operaciones.strftime('%d-%m-%Y')
        
        if l.termino_operaciones != None:
            fila['termino'] = l.termino_operaciones.strftime('%d-%m-%Y')
        else:
            fila['termino'] = ''

        fila['link'] = l.urlsvs

        for e in etiquetas:
            obj = e.get('obj')
            et = e.get('etiqueta')
            try:
                mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs=et)
                fila[obj] = float(mov.monto_real('pesos'))
            except movimiento.DoesNotExist:
                fila[obj] = ''

        cont = 0
        for e in etiquetas:
            obj = e.get('obj')
            if fila[obj] == '':
                cont += 1

        if cont == 7:
            pass
        else:
            res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "lista_nuevos" : res })