import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import fondo, movimiento, categoria, administrador, periodo


#************************ API PATRIMONIO FONDOS POR AFI ***************************
@api_view(['GET'])
def get_patrimonio_fondos_afi(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria =0).order_by('categoria')

    admins = administrador.objects.filter().order_by('razon_social')
    res= []
    cont = 0
    for ad in admins:
        total = 0
        fila={}
        if ad.tiene_fondos(str(anio)+'-'+str(mes)+'-01'):
            
            for l in lista.filter(admin=ad):
                fila = {}
                if l.categoria == None:
                    lcategoriaindice = ''
                else:
                    lcategoriaindice = l.categoria.indice

                fila['run_adm'] = ad.rut
                fila['nombre_adm'] = ad.razon_social
                fila['nombre_fon'] = l.nombre
                fila['run'] = l.runsvs.split('-')[0]
                fila['indice'] = lcategoriaindice

                try:
                    mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL PATRIMONIO NETO  O")
                    fila["patrimonio"]= float(mov.monto_real('pesos'))
                
                except (movimiento.DoesNotExist, KeyError):
                    fila["patrimonio"]= ''
                if fila.get('patrimonio')== '':
                    pass
                else:
                    res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "patrimonio_fondo" : res })

#************************ API PATRIMONIO POR AFI ***************************
@api_view(['GET'])
def get_patrimonio_afi(request, period):
    
    res = []
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]   
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria=0).order_by('categoria')

    admins = administrador.objects.filter().order_by('razon_social')
    for ad in admins:
        total = 0
        fila={}
        if ad.tiene_fondos(str(anio)+'-'+str(mes)+'-01'):
            fila['run_adm'] = ad.rut
            fila['nombre'] = ad.razon_social

            suma = 0
            for l in lista.filter(admin=ad):
                try:
                    mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL PATRIMONIO NETO  O")
                    suma += float(mov.monto_real('pesos'))
                except movimiento.DoesNotExist:
                    pass
            fila['patrimonio'] = suma
            res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "patrimonio_afi" : res })

#************************ API PATRIMONIO FONDOS POR CLASE ***************************
@api_view(['GET'])
def get_patrimonio_clase(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria =0).order_by('categoria')

    categs = categoria.objects.exclude(pk=0).order_by('indice')

    res = []
    total = 0
    for c in categs:

        fila = {}
        suma = 0
        cat = c.indice
        cat = cat.strip('0')

        for l in lista.filter(categoria__indice__startswith=cat):

            try:
                mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs="TOTAL PATRIMONIO NETO  O")
                suma += float(mov.monto_real('pesos'))

            except (movimiento.DoesNotExist, KeyError):
                pass
        if len(cat) == 1:
            total += suma
        fila['nombre_cat'] = c.nombre
        fila['indice'] = c.indice
        fila['monto'] = suma
        res.append(fila)
    fila= {}
    fila['nombre_cat'] = 'TOTAL'
    fila['indice'] = ''
    fila['monto'] = total
    res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "patrimonio_clase" : res })

#************************ API PATRIMONIO CUOTAS ***************************
@api_view(['GET'])
def get_patrimonio_cuotas(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria =0).order_by('runsvs')

    res = []

    for l in lista:

        fila = {}
        fila['nom_adm'] = l.admin.razon_social
        fila['run_adm'] = l.admin.rut
        fila['nom_fon'] = l.nombre
        fila['run_fon'] = l.runsvs.split('-')[0]

        if l.categoria == None:
            fila['clase'] = ''
        else:
            fila['clase'] = l.categoria.indice

        try:
            p = periodo.objects.get(periodo=period, fondo=l)
            fila['monto'] = p.monto_real('valor_cuota', 'pesos')
        except periodo.DoesNotExist:
            fila['monto'] = ''
        
        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "valor_cuotas" : res })

#************************ API PATRIMONIO CUOTAS PAGADAS ***************************
@api_view(['GET'])
def get_patrimonio_cuotas_pagadas(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.exclude(categoria =0).order_by('runsvs')
    
    res = []
    for l in lista:

        fila = {}
        fila['nom_adm'] = l.admin.razon_social
        fila['run_adm'] = l.admin.rut
        fila['nom_fon'] = l.nombre
        fila['run_fon'] = l.runsvs.split('-')[0]

        if l.categoria == None:
            fila['clase'] = ''
        else:
            fila['clase'] = l.categoria.indice

        try:
            p = periodo.objects.get(periodo=period, fondo=l)
            monto = json.loads(p.datos)
            try:
                fila['cuotas'] = monto['CUOTAS PAGADAS']
            except (KeyError, UnboundLocalError):
                fila['cuotas'] = ''

        except periodo.DoesNotExist:
            fila['cuotas'] = '' 

        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "cuotas_pagadas" : res })
