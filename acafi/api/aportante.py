import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import aportante

#API_APORTANTE
@api_view(['GET'])
def get_aportantes(request):
    if request.method == 'GET':

        cats = list(aportante.objects.all().values('rut', 'nombre', 'tipo_persona'))
        return Response({"response": "OK", "Codigo": 200, "aportantes" : cats })
        
    
@api_view(['GET'])
def get_aportante(request, id = None):

    if request.method == 'GET':

        cat = list(aportante.objects.filter(rut=id).values('rut', 'nombre', 'tipo_persona'))
        return Response({"response": "OK", "Codigo": 200, "aportante" : cat })
