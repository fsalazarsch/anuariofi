import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import movimiento

#API_MOVIMIENTO
@api_view(['GET'])
def get_movimientos(request, fondo, periodo ):

    if request.method == 'GET':
        cat = list(movimiento.objects.filter(periodo=periodo, fondo__runsvs=fondo).values('periodo', 'etiqueta_svs', 'monto', 'moneda','ap'))
        
        for c in cat:
            if c['ap'] == True:
                c['ap'] = 'activo'
            else:
                c['ap'] = 'pasivo'

        return Response({"response": "OK", "Codigo": 200, "movimiento" : cat })
