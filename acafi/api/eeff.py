import json, operator
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import periodo, fondo, categoria, movimiento, resultado
from django.db.models import Q


#************************ API RESUMEN CATASTRO ***************************
@api_view(['GET'])
def get_resumen_catastro(request, period):
    
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('categoria')

    resumen = []
    for l in lista:
        flag = 0
        fila = {}
        if l.categoria == None:
            lcategoriaindice = ''
        else:
            lcategoriaindice = l.categoria.indice
            fila ["run"]= l.runsvs.split('-')[0]
            fila ["fondo"]= l.nombre
            fila ["clase"]= lcategoriaindice
            try:
                p = periodo.objects.get(periodo=str(period), fondo=l)
                datos = json.loads(p.datos)

                fila ["aportes"]= str(float(p.monto_real('APORTES', 'pesos')))
                fila ["reparto"]= str(float(p.monto_real('REPARTOS DE PATRIMONIOS', 'pesos')))
                fila ["dividendos"]= str(float(p.monto_real('REPARTOS DE DIVIDENDOS', 'pesos')))

            except (periodo.DoesNotExist, KeyError):
                flag = 1
                fila ["aportes"]= ''
                fila ["reparto"]= ''
                fila ["dividendos"]= ''
            
            if flag == 0 and movimiento.objects.filter(periodo=period, fondo = l).exists():
                resumen.append(fila)
            else:
                pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "resumen_catastro" : resumen })

#************************ API TABLA CATASTRO ***************************
@api_view(['GET'])
def get_tabla_catastro(request, period):
    
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria =0).order_by('categoria')

    categs = categoria.objects.exclude(pk=0).order_by('indice')
    res = []
    for c in categs:
        fila = {}
        if c.tiene_fondos() == False:
            if int(c.indice) % 100 == 0:
                fila['tipo'] = 'Clase'
                fila['nombre'] = c.nombre
                fila['run'] = ''
                fila['indice'] = c.indice
                fila["aportes"]= ''
                fila["reparto"]= ''
                fila["dividendos"]=''
            else:
                fila['tipo'] = 'Subclase'
                fila['nombre'] = c.nombre
                fila['run'] = ''
                fila['indice'] = c.indice
                fila["aportes"]= ''
                fila["reparto"]= ''
                fila["dividendos"]=''
        else:
            fila['tipo'] = 'Subclase'
            fila['nombre'] = c.nombre
            fila['run'] = ''
            fila['indice'] = c.indice
            fila["aportes"]= ''
            fila["reparto"]= ''
            fila["dividendos"]=''
        res.append(fila)

        for l in lista.filter(categoria=c):
            flag = 0
            fila = {}
            if l.categoria == None:
                lcategoriaindice = ''
            else:
                lcategoriaindice = l.categoria.indice

            fila['tipo'] = 'Fondo'
            fila['nombre'] = l.nombre
            fila['run'] = l.runsvs.split('-')[0]
            fila['indice'] = lcategoriaindice

            try:
                p = periodo.objects.get(periodo=period, fondo=l)
                datos = json.loads(p.datos)
                fila["aportes"]= float(p.monto_real('APORTES', 'pesos'))
                fila["reparto"]= float(p.monto_real('REPARTOS DE PATRIMONIOS', 'pesos'))
                fila["dividendos"]= float(p.monto_real('REPARTOS DE DIVIDENDOS', 'pesos'))
            except (KeyError, AttributeError):
                flag = 1
                fila["aportes"]= ''
                fila["reparto"]= ''
                fila["dividendos"]=''

            if flag == 0 and movimiento.objects.filter(periodo=period, fondo = l).exists():
                res.append(fila)
            else:
                pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "tabla_catastro" : res })

#************************ API APORTES NETO ***************************
@api_view(['GET'])
def get_aportes_netos(request, period):
  
    categ= [{"categoria" : "Deuda" , "codigo": [11]},
            {"categoria":"Accionarios","codigo": [12]},
            {"categoria":"Mixtos","codigo": [13]},
            {"categoria":"Otros Mobiliarios","codigo" : [14]},
            {"categoria":"Activos Alternativos","codigo": [20]},
            {"categoria": "Inmobiliarios","codigo": [2111, 2121, 2201]},
            {"categoria":"Capital Privado","codigo": [2112, 2122, 2202]},
            {"categoria":"Fondo de Fondos","codigo" : [2113, 2123, 2203]},
            {"categoria":"Deuda Privada" ,"codigo": [2114, 2124, 2204]},
            {"categoria":"Infraestructura y Energia" ,"codigo": [2115, 2125, 2205]},
            {"categoria":"Otros Alternativos" ,"codigo": [2116, 2126, 2206]}]
    
    res = []
    total = 0
    for cat in categ:
        fila = {}
        suma = 0
        #si la categoria padre es la misma
        fila['categoria']= cat.get('categoria')
        aux = cat.get('codigo')
        try:
            for a in aux:
                periodos = periodo.objects.filter(periodo=str(period), fondo__categoria__indice__startswith=a)
                for p in periodos:                
                    try:
                        suma +=  float(p.monto_real('aporte_neto', 'pesos'))
                    except (KeyError, AttributeError):
                        pass
            fila['monto']= suma
            total +=suma
        except (periodo.DoesNotExist, KeyError):
            pass

        res.append(fila)
    fila={}
    fila['categoria'] = 'TOTAL'
    fila['monto'] = total
    res.append(fila)
    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "aportes_netos" : res })


#************************ API EEFF ***************************
@api_view(['GET'])
def get_eeff(request, period, tipo):

    res = []
    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]   
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)            
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')

    etiquetas =[{'etiqueta':'01 ACTIVO'},
        {'etiqueta':'02 Activo Corriente'},
        {'etiqueta':'03 AC-Efectivo y efectivo equivalente'},
        {'etiqueta':'04 AC-Activos financieros a valor razonable con efecto en resultados'},
        {'etiqueta':'05 AC-Activos financieros a valor razonable con efecto en otros resultados integrales'},
        {'etiqueta':'06 AC-Activos financieros a valor razonable con efecto en resultados entregados en garantía'},
        {'etiqueta':'07 AC-Activos financieros a costo amortizado'},
        {'etiqueta':'08 AC-Cuentas y documentos por cobrar por operaciones'},
        {'etiqueta':'09 AC-Otros documentos y cuentas por cobrar'},
        {'etiqueta':'10 AC-Otros activos'},
        {'etiqueta':'11 Total Activo Corriente'},
        {'etiqueta':'12 Activo no Corriente'},
        {'etiqueta':'13 ANC-Activos financieros a valor razonable con efecto en resultados'},
        {'etiqueta':'14 ANC-Activos financieros a valor razonable con efecto en otros resultados integrales'},
        {'etiqueta':'15 ANC-Activos financieros a costo amortizado'},
        {'etiqueta':'16 ANC-Cuentas y documentos por cobrar por operaciones'},
        {'etiqueta':'17 ANC-Otros documentos y cuentas por cobrar'},
        {'etiqueta':'18 ANC-Inversiones valorizadas por el método de la participación'},
        {'etiqueta':'19 ANC-Propiedades de Inversión'},
        {'etiqueta':'10 ANC-Otros activos'},
        {'etiqueta':'21 Total Activo No Corriente'},
        {'etiqueta':'22 Total Activo'},
        {'etiqueta':'23 PASIVO'},
        {'etiqueta':'24 Pasivo Corriente'},
        {'etiqueta':'25 PC-Pasivos financieros a valor razonable con efecto en resultados'},
        {'etiqueta':'26 PC-Préstamos'},
        {'etiqueta':'27 PC-Otros Pasivos Financieros'},
        {'etiqueta':'28 PC-Cuentas y documentos por pagar por operaciones'},
        {'etiqueta':'29 PC-Remuneraciones sociedad administradora'},
        {'etiqueta':'30 PC-Otros documentos y cuentas por pagar'},
        {'etiqueta':'31 PC-Ingresos anticipados'},
        {'etiqueta':'32 PC-Otros pasivos'},
        {'etiqueta':'33 Total Pasivo Corriente'},
        {'etiqueta':'34 Pasivo No Corriente'},
        {'etiqueta':'35 PNC-Préstamos'},
        {'etiqueta':'36 PNC-Otros Pasivos Financieros'},
        {'etiqueta':'37 PNC-Cuentas y documentos por pagar por operaciones'},
        {'etiqueta':'38 PNC-Otros documentos y cuentas por pagar'},
        {'etiqueta':'39 PNC-Ingresos anticipados'},
        {'etiqueta':'40 PNC-Otros pasivos'},
        {'etiqueta':'41 Total Pasivo No Corriente'},
        {'etiqueta':'42 PATRIMONIO NETO'},
        {'etiqueta':'43 PN-Aportes'},
        {'etiqueta':'44 PN-Otras Reservas'},
        {'etiqueta':'45 PN-Resultados Acumulados  o'},
        {'etiqueta':'46 PN-Resultado del ejercicio  o'},
        {'etiqueta':'47 PN-Dividendos provisorios'},
        {'etiqueta':'48 Total Patrimonio Neto  o'},
        {'etiqueta':'49 Total Pasivo'},
        {'etiqueta':'50 IPDLO-Intereses y reajustes'},
        {'etiqueta':'51 IPDLO-Ingresos por dividendos'},
        {'etiqueta':'52 IPDLO-Diferencias de cambio netas sobre activos financieros a costo amortizado  o'},
        {'etiqueta':'53 IPDLO-Diferencias de cambio netas sobre efectivo y efectivo equivalente  o'},
        {'etiqueta':'54 IPDLO-Cambios netos en valor razonable de activos financieros y pasivos financieros a valor razonable con efecto en resultados  o'},
        {'etiqueta':'55 IPDLO-Resultado en venta de instrumentos financieros  o'},
        {'etiqueta':'56 IPDLO-Resultados por venta de inmuebles'},
        {'etiqueta':'57 IPDLO-Ingreso por arriendo de bienes raíces'},
        {'etiqueta':'58 IPDLO-Variaciones en valor razonable de propiedades de inversión  o'},
        {'etiqueta':'59 IPDLO-Resultado en inversiones valorizadas por el método de la participación  o'},
        {'etiqueta':'60 IPDLO-Otros  o'},
        {'etiqueta':'61 Total ingresospérdidas netos de la operación  o'},
        {'etiqueta':'62 GASTOS'},
        {'etiqueta':'63 G-Depreciaciones'},
        {'etiqueta':'64 G-Remuneración del Comité de Vigilancia'},
        {'etiqueta':'65 G-Comisión de administración'},
        {'etiqueta':'66 G-Honorarios por custodia y admistración'},
        {'etiqueta':'67 G-Costos de transacción'},
        {'etiqueta':'68 G-Otros gastos de operación'},
        {'etiqueta':'69 Total gastos de operación'},
        {'etiqueta':'70 Utilidadpérdida de la operación  o'},
        {'etiqueta':'71 G-Costos financieros'},
        {'etiqueta':'72 Utilidadpérdida antes de impuesto  o'},
        {'etiqueta':'73 G-Impuesto a las ganancias por inversiones en el exterior'},
        {'etiqueta':'74 Resultado del ejercicio  o'},
        {'etiqueta':'75 ORI-Cobertura de Flujo de Caja'},
        {'etiqueta':'76 ORI-Ajustes por Conversión  o'},
        {'etiqueta':'77 ORI-Ajustes provenientes de inversiones valorizadas por el método de la participación  o'},
        {'etiqueta':'78 ORI-Otros Ajustes al Patrimonio Neto  o'},
        {'etiqueta':'79 Total de otros resultados integrales  o'},
        {'etiqueta':'80 Total Resultado Integral  o'}]

    for l in lista:
        cont = 0
        fila = {}
        fila['run_fon'] = l.runsvs
        fila['nom_fon'] = l.nombre
        fila['run_adm'] = l.admin.rut
        fila['nom_adm'] = l.admin.razon_social

        if l.categoria == None:
            fila['clase'] = 0
        else:
            fila['clase'] = l.categoria.indice

        for e in etiquetas:
            et = e.get('etiqueta')
            obj = et.replace('  o','')
            
            if int(et[:2]) < 50:
                try:
                    aux = et[3:]
                    mov = movimiento.objects.get(periodo=period, fondo = l, etiqueta_svs=aux)
                    if tipo == 'cat':
                        fila[obj] = float(mov.monto_real('pesos'))
                    if tipo == 'pre':
                        obj2 = 'pre '+obj
                        fila[obj] = float(mov.monto_real('pesos'))
                        fila[obj2] = float(mov.monto)

                except movimiento.DoesNotExist:
                    if tipo == 'cat':
                        fila[obj] = ''
                    if tipo == 'pre':
                        obj2 = 'pre '+obj
                        fila[obj] = ''
                        fila[obj2] = ''
                    cont += 1
            else:
                try:
                    aux = et[3:]
                    mov = resultado.objects.get(periodo=period, fondo = l, etiqueta_svs=aux)
                    if tipo == 'cat':
                        fila[obj] = float(mov.monto_real('pesos'))
                    if tipo == 'pre':
                        obj2 = 'pre '+obj
                        fila[obj] = float(mov.monto_real('pesos'))
                        fila[obj2] = float(mov.monto)

                except resultado.DoesNotExist:
                    if tipo == 'cat':
                        fila[obj] = ''
                    if tipo == 'pre':
                        obj2 = 'pre '+obj
                        fila[obj] = ''
                        fila[obj2] = ''
                    cont += 1

        if cont > 79:
            pass
        else:
            res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "eeff" : res })