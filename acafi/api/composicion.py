import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import composicion

#API_COMPOSICION
@api_view(['GET'])
def get_composiciones(request, fondo, periodo ):

    if request.method == 'GET':
        cat = list(composicion.objects.filter(periodo__periodo=periodo, periodo__fondo__runsvs=fondo).values('periodo', 'aportante', 'porc_propiedad'))
        
        for c in cat:
        	c['porc_propiedad'] = float(c['porc_propiedad'])
        return Response({"response": "OK",  "Codigo": 200, "composicion" : cat })
