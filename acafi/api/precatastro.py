import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import fondo, movimiento, periodo, resumen, inversion, sector, composicion, fondo_ext
from django.db.models import Q, Sum

#************************ API LISTAR FONDOS VIGENTES ***************************
@api_view(['GET'])
def get_parametros_iniciales(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]  
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')
    
    res = []
    for l in lista:

        fila = {}

        fila['nombre'] = l.nombre
        fila['admin_rut'] = l.admin.rut
        fila['admin_nombre'] = l.admin.razon_social
        fila['runsvs'] = l.runsvs
        fila['inicio'] = l.inicio_operaciones.strftime('%d-%m-%Y')

        if l.termino_operaciones != None:
            fila['termino'] = l.termino_operaciones.strftime('%d-%m-%Y')
        else:
            fila['termino'] = ''

        if l.categoria == None:
            fila['categoria'] = '-'
        else:
            fila['categoria'] = l.categoria.indice

        if l.tipo_inversion == 0:
            fila['tipo_inversion'] = "No Rescatable"
        if l.tipo_inversion == 1:
            fila['tipo_inversion'] = "Rescatable"

        try:
            m = movimiento.objects.get(fondo=l, periodo=period, etiqueta_svs="Total Activo")
            fila['moneda_activo'] = m.moneda
            fila['total_activo'] = m.monto
        except movimiento.DoesNotExist:
            fila['moneda_activo'] = '-'
            fila['total_activo'] = '-'

        try:
            p = periodo.objects.get(periodo=period, fondo=l)
            aux2 = json.loads(p.datos)
            fila['aportes'] = aux2['APORTES']
            fila['reparto_patrimonio'] = aux2['REPARTOS DE PATRIMONIOS']
            fila['reparto_dividendo']= aux2['REPARTOS DE DIVIDENDOS']
            fila['patrimonio_obs'] = float(p.valor_obs)
        except periodo.DoesNotExist:
            fila['aportes'] = '-'
            fila['reparto_patrimonio'] = '-'
            fila['reparto_dividendo']= '-'
            fila['patrimonio_obs'] = '-'

        try:
            m = movimiento.objects.get(fondo=l, periodo=period, etiqueta_svs__icontains="Total Patrimonio Neto")
            fila['patrimonio_monto'] = m.monto
            
        except movimiento.DoesNotExist:
            fila['patrimonio_monto'] = '-'

        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "parametros_iniciales" : res })

#************************ API CARTERA DE INVERSIONES ***************************
@api_view(['GET'])
def get_cartera_inversiones(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]  
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')

    arr = ["Acciones de sociedades anónimas abiertas",
        "Derechos preferentes de suscripción de acciones de sociedades anónimas abiertas",
        "Cuotas de fondos mutuos",
        "Cuotas de fondos de inversión",
        "Certificados de depósitos de valores (CDV)",
        "Títulos que representen productos",
        "Otros títulos de renta variable",
        "Depósitos a plazo y otros títulos de bancos e instituciones financieras",
        "Cartera de créditos o de cobranzas",
        "Títulos emitidos o garantizados por Estados o Bancos Centrales",
        "Otros títulos de deuda",
        "Acciones no registradas",
        "Cuotas de fondos de inversión privados",
        "Títulos de deuda no registrados",
        "Bienes raíces",
        "Proyectos en desarrollo",
        "Deuda de operaciones de leasing",
        "Acciones de sociedades anónimas inmobiliarias y concesionarias",
        "Otras inversiones"]
    res = []
    for l in lista:

        fila = {}

        fila['nom_fon'] = l.nombre
        fila['run_adm'] = l.admin.rut
        fila['nom_adm'] = l.admin.razon_social
        fila['run_fon'] = l.runsvs

        if l.categoria == None:
            fila['clase'] = '---'
        else:
            fila['clase'] = l.categoria.indice

        if l.tipo_inversion == 0:
            fila['rescate'] = "No Rescatable"
        if l.tipo_inversion == 1:
            fila['rescate'] = "Rescatable"

        try:
            p = periodo.objects.get(fondo=l, periodo=period)
        except periodo.DoesNotExist:
            pass
        aux = {}
        aux2 = {}
        for a in arr:
            
            try:
                r = resumen.objects.get(periodo=p, descripcion=a)
                aux[a] = r.nacional
                aux2[a] = r.extranjero
            except resumen.DoesNotExist:
                aux[a] = 0
                aux2[a] = 0
        fila['nacional'] = aux
        fila['extranjero'] = aux2

        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "cartera_inversiones" : res })

#************************ API ACCIONES NACIONALES ***************************
@api_view(['GET'])
def get_acciones_nacionales(request, period):

    acciones = inversion.objects.filter(periodo=period)

    res = []

    for a in acciones:

        if a.instrumento == "ACC":
            fila = {}

            fila['nom_fon'] = a.fondo.nombre
            fila['run_adm'] = a.fondo.admin.rut
            fila['nom_adm'] = a.fondo.admin.razon_social
            fila['run_fon'] = a.fondo.runsvs

            if a.fondo.categoria == None:
                fila['clase'] = '---'
            else:
                fila['clase'] = a.fondo.categoria.indice
            try:
                s = sector.objects.get(pk=a.nemotecnico)
                fila['sector'] = s.nombre
            except sector.DoesNotExist:
                fila['sector'] = 'Otros'

            fila['empresa'] = a.empresa
            fila['codigo'] = a.codigo
            fila['pais'] = a.pais.nombre
            fila['moneda'] = a.moneda.nombre
            fila['valolizacion'] = a.valolizacion
            fila['monto'] = a.monto
            fila['porcentaje'] = a.porcentaje
            fila['unidades'] = a.unidades
            fila['nemotecnico'] = a.nemotecnico
            fila['fecha_vencimiento'] = a.fecha_vencimiento
            fila['tipo_unidades'] = a.tipo_unidades
            try:
                fila['cod_pais_transaccion'] = a.cod_pais_transaccion.codigo
            except:
                fila['cod_pais_transaccion'] = '---'

            res.append(fila)

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "acciones_nacionales" : res })

#************************ API CUOTAS APORTANTES ***************************
@api_view(['GET'])
def get_cuotas_aportantes(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]  
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')

    res = []
    for l in lista:
        fila = {}

        fila['nom_fon'] = l.nombre
        fila['run_adm'] = l.admin.rut
        fila['nom_adm'] = l.admin.razon_social
        fila['run_fon'] = l.runsvs

        if l.categoria == None:
            fila['clase'] = '---'
        else:
            fila['clase'] = l.categoria.indice

        

        if l.tipo_inversion == 0:
            fila['rescate'] = "No Rescatable"
        if l.tipo_inversion == 1:
            fila['rescate'] = "Rescatable"
        

        p = periodo.objects.get(periodo=period, fondo=l)
        aux = json.loads(p.datos)
        fila['total_aportantes'] = p.total_aportantes
        fila['cuotas_emitidas'] = float(aux['CUOTAS EMITIDAS'])
        fila['cuotas_pagadas'] = float(aux['CUOTAS PAGADAS'])
        
        composiciones = composicion.objects.filter(periodo=p).order_by('-porc_propiedad')
        fila2 = []
        for comp in composiciones:
            aux2 = {}
            aux2['nom_ap'] = comp.aportante.nombre
            aux2['run_ap'] = comp.aportante.rut
            aux2['pers_ap'] = comp.aportante.get_persona()
            aux2['porc_propiedad'] = float(comp.porc_propiedad)
            fila2.append(aux2)
        fila['composiciones'] = fila2

        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "cuotas_aportantes" : res })

#************************ API CLASIFICACION DE RIESGO ***************************
@api_view(['GET'])
def get_clasif_riesgo(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]  
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')

    res = []
    for l in lista:

        fila = {}
        fila['nom_fon'] = l.nombre
        fila['run_fon'] = l.runsvs

        if l.categoria == None:
            fila['clase'] = '---'
        else:
            fila['clase'] = l.categoria.indice

        f = fondo_ext.objects.get(runsvs=l.runsvs)

        try:
            riesg = json.loads(f.riesgo)
            feller= riesg['felier_rate']
            fitch = riesg['fitch_ratings']
            icr = riesg['icr']
            humphreys = riesg['humphreys']

        except json.JSONDecodeError:
            feller = ''
            fitch=''
            icr =''
            humphreys=''

        fila['fitch'] =fitch
        fila['humphreys'] =humphreys
        fila['feller'] =feller
        fila['icr'] =icr

        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "clasif_riesgo" : res })

#************************ API INVERSION PAIS ***************************
@api_view(['GET'])
def get_inv_pais(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]  
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')

    res = []

    for l in lista:

        fila = {}
        fila['nom_fon'] = l.nombre
        fila['run_adm'] = l.admin.rut
        fila['nom_adm'] = l.admin.razon_social
        fila['run_fon'] = l.runsvs

        if l.categoria == None:
            fila['categoria'] = '---'
        else:
            fila['categoria'] = l.categoria.indice

        
        if l.tipo_inversion == 0:
            fila['rescate'] = "No Rescatable"
        if l.tipo_inversion == 1:
            fila['rescate'] = "Rescatable"

        fila['paises'] = {}
        invs = inversion.objects.filter(fondo=l, periodo= period).values('pais').annotate(suma_porcentaje = Sum('porcentaje')).order_by('pais')
        for i in invs:
            try:
                i['suma_porcentaje'] = float(i['suma_porcentaje'])
            except TypeError:
                i['suma_porcentaje'] = 0
        fila['paises'] = list(invs)

        fila['cod_paises_transaccion'] = {}
        invs = inversion.objects.filter(fondo=l, periodo= period).values('cod_pais_transaccion').annotate(suma_porcentaje = Sum('porcentaje')).order_by('cod_pais_transaccion')
        for i in invs:
            try:
                i['suma_porcentaje'] = float(i['suma_porcentaje'])
            except TypeError:
                i['suma_porcentaje'] = 0
        fila['cod_paises_transaccion'] = list(invs)

        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "inv_pais" : res })

#************************ API INVERSION MONEDA ***************************
@api_view(['GET'])
def get_inv_moneda(request, period):

    aux = period.split('-')
    mes = (int(aux[0])+1)%12
    if aux[0] == '12':
        anio = int(aux[1])+1
    else:
        anio = aux[1]  
    lista = fondo.objects.filter(inicio_operaciones__isnull=False)
    lista = lista.filter(inicio_operaciones__lt=str(anio)+'-'+str(mes)+'-01')
    lista = lista.filter(Q(termino_operaciones__isnull=True) | \
    (Q(termino_operaciones__gte=aux[1]+'-'+str(int(aux[0])-2)+'-01')))
    lista = lista.exclude(categoria=0).order_by('runsvs')

    res = []

    for l in lista:

        fila = {}
        fila['nom_fon'] = l.nombre
        fila['run_adm'] = l.admin.rut
        fila['nom_adm'] = l.admin.razon_social
        fila['run_fon'] = l.runsvs

        if l.categoria == None:
            fila['categoria'] = '---'
        else:
            fila['categoria'] = l.categoria.indice

        
        if l.tipo_inversion == 0:
            fila['rescate'] = "No Rescatable"
        if l.tipo_inversion == 1:
            fila['rescate'] = "Rescatable"


        fila['moneda'] = {}
        invs = inversion.objects.filter(fondo=l, periodo= period).values('moneda').annotate(suma_porcentaje = Sum('porcentaje')).order_by('moneda')
        for i in invs:
            try:
                i['suma_porcentaje'] = float(i['suma_porcentaje'])
            except TypeError:
                i['suma_porcentaje'] = 0
        fila['moneda'] = list(invs)

        fila['tipo_unidades'] = {}
        invs = inversion.objects.filter(fondo=l, periodo= period).values('tipo_unidades').annotate(suma_porcentaje = Sum('porcentaje')).order_by('tipo_unidades')
        for i in invs:
            try:
                i['suma_porcentaje'] = float(i['suma_porcentaje'])
            except TypeError:
                i['suma_porcentaje'] = 0
        fila['tipo_unidades'] = list(invs)


        if movimiento.objects.filter(periodo=period, fondo = l).exists():
            res.append(fila)
        else:
            pass

    if request.method == 'GET':
        return Response({"response": "OK", "Codigo": 200, "inv_moneda" : res })