import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import pais

#API_PAIS
@api_view(['GET'])
def get_paises(request):
    if request.method == 'GET':

        cats = list(pais.objects.all().values('codigo', 'nombre', 'region__nombre'))
        return Response({"response": "OK", "Codigo": 200, "paises" : cats })
        
    
@api_view(['GET'])
def get_pais(request, id = None):

    if request.method == 'GET':

        cats = list(pais.objects.filter(pk=id).values('codigo', 'nombre', 'region__nombre'))
        return Response({"response": "OK", "Codigo": 200, "pais" : cats })
