import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import perfil

#API_PERFIL
@api_view(['GET', 'POST'])
def get_perfiles(request):

    if request.method == 'GET':

        try:
            username = request.query_params['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.query_params["password"]
        except MultiValueDictKeyError:
            passwd = None


        #return Response(request.data)
        if passwd == "" or passwd == None:
            return Response({"response": "Error",  "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password usuario autenticacion"})
        if username == "" or username == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre usuario autenticacion"})


        p = User.objects.get(username=username)
        if p.check_password(passwd):
            al = perfil.objects.values_list('nivel_acceso', flat=True).get(usuario =p.id)
            perfs = list(perfil.objects.filter(nivel_acceso__lte = al).values( 'usuario','nivel_acceso', 'id_usercreador'))
            return Response({"response": "OK", "Codigo": 200, "perfiles" : perfs })
        
    elif request.method == 'POST':


        try:
            username = request.data['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.data['password']
        except MultiValueDictKeyError:
            passwd = None

        try:
            nivel_acceso = request.data['nivel_acceso']
        except MultiValueDictKeyError:
            nivel_acceso = None

        try:
            nombre = request.data['nombre']
        except MultiValueDictKeyError:
            nombre = None

        #return Response(request.data)
        if passwd == "" or passwd == None:
            return Response({"response": "Error",  "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password usuario autenticacion"})
        if username == "" or username == None:
            return Response({"response": "Error",  "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre usuario autenticacion"})

        try:
            p = User.objects.get(username=username)
            if p.check_password(passwd) and (p.is_superuser == 1):

                if nivel_acceso == "" or nivel_acceso == None:
                    return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nivel_acceso"})
                if nombre == "" or nombre == None:
                    return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre"})

                id_perfil = User.objects.get(username = nombre)
                if not id_perfil:
                    return Response({"response": "Error", "Codigo": 403, "msj" : "No existe el usuario"})
                else:
                    perf = perfil(usuario = id_perfil, nivel_acceso= nivel_acceso, id_usercreador = p)
                    perf.save()
                    return Response({"response": "Ok",  "Codigo": 200,"msj" : "Perfil creado exitosamente"}) 
            else:
                return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para crear un perfil"})
        except User.DoesNotExist:
            return Response({"response": "Error", "Codigo": 403, "msj" : "El usuario no existe"})


@api_view(['GET', 'PUT', 'DELETE'])
def get_perfil(request, id = None):

    if request.method == 'GET':

        try:
            username = request.query_params['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.query_params["password"]
        except MultiValueDictKeyError:
            passwd = None


        #return Response(request.data)
        if passwd == "" or passwd == None:
            return Response({"response": "Error",  "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password usuario autenticacion"})
        if username == "" or username == None:
            return Response({"response": "Error",  "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "nombre usuario autenticacion"})

        p = User.objects.get(username=username)
        if p.check_password(passwd):
            al = perfil.objects.values_list('nivel_acceso', flat=True).get(usuario =p.id)
            perfs = list(perfil.objects.filter(nivel_acceso__lte = al, pk = id).values( 'usuario','nivel_acceso', 'id_usercreador'))
        return Response({"response": "OK", "Codigo": 200, "perfil" : perfs })

    if request.method == 'PUT':

        try:
            username = request.query_params['username']
        except MultiValueDictKeyError:
            username = None

        try:
            passwd = request.query_params["password"]
        except MultiValueDictKeyError:
            passwd = None

        if passwd == "" or passwd == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "password"})
        if username == "" or username == None:
            return Response({"response": "Error", "Codigo": 401, "msj" : "Falta un campo en la request", "campo" : "username"})

        try:
            nivel_acceso = request.query_params["nivel_acceso"]
        except MultiValueDictKeyError:
            nivel_acceso = None

        try:
            p = User.objects.get(username=username)
            if p.check_password(passwd) and (p.is_superuser == 1):
                try:
                    perf = perfil.objects.get(pk = id)

                    if nivel_acceso != "" and nivel_acceso != None:
                        perf.nivel_acceso = nivel_acceso
                        perf.save()

                    return Response({"response": "Ok", "Codigo": 200, "msj" : "Perfil modificado correctamente"})

                except categoria.DoesNotExist:
                    return Response({"response": "Error", "Codigo": 402, "msj" : "El perfil no existe"})
            else:
                return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para modificar un perfil"})
        except User.DoesNotExist:
            return Response({"response": "Error", "Codigo": 404, "msj" : "No tiene los permisos para eliminar un perfil"})
