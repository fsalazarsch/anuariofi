import json
from time import gmtime, strftime
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.hashers import make_password 
from acafi.models import resumen



#API_RESUMEN
@api_view(['GET'])
def get_resumen(request, fondo, periodo ):

    if request.method == 'GET':
        cat = list(resumen.objects.filter(periodo__periodo=periodo, periodo__fondo__runsvs=fondo).exclude(nacional=0, extranjero=0).values('descripcion', 'nacional', 'extranjero', 'porcentual'))
        return Response({"response": "OK", "Codigo": 200, "resumen" : cat })
