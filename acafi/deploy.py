from django.conf import settings
from datetime import datetime, date
import unicodedata, datetime
import datetime
from acafi.models import periodo, categoria, fondo, fondo_corfo, indicador


#escribe un archivo de log en dd-mm-YY.log
def escribir_log(texto, nombre=None):
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"log/"
    d = datetime.datetime.now()
    if( int(d.month) < 10 ):
        mes = "0"+str(d.month)
    else:
        mes = str(d.month)
    if( int(d.day) < 10 ):
        dia = "0"+str(d.day)
    else:
        dia = str(d.day)

    if( int(d.hour) < 10 ):
        hora = "0"+str(d.hour)
    else:
        hora = str(d.hour)
    if( int(d.minute) < 10):
        minuto = "0"+str(d.minute)
    else:
        minuto = str(d.minute)

    if nombre != None:
        ruta += nombre+" "
    try:
        arch = open(ruta+str(d.year)+"-"+mes+"-"+dia+".log", "a+", encoding="utf-8")
    
        texto = unicodedata.normalize('NFKD', texto).encode('ascii', 'ignore')

        arch.write("["+hora+":"+minuto+"] -- "+str(texto)+"\n")
    except UnicodeEncodeError:
        pass

def escribir_log_puro(texto, nombre=None):
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"log/"
    if nombre != None:
        ruta += nombre+" "
    try:
        arch = open(ruta+".log", "a+", encoding="utf-8")
    
        #texto = unicodedata.normalize('NFKD', texto).encode('ascii', 'ignore')

        arch.write(str(texto)+"\n")
    except UnicodeEncodeError:
        pass


def get_paginador(f):#cambio de 1100 a 1200
    if f.pagina != None and f.pagina != 0:
        return f.pagina
    else:
        paginas_iniciales=42 #cuando sean divisibles por 100 pero no por mil
        sumador =0
        anio=2017
        anterior = None
        categs = categoria.objects.exclude(indice=0).order_by('orden')
        for c in categs:
            if anterior == None:
                anterior = c.indice[0:2]
                print(str(anterior))
            if int(c.indice) < 5000:
                if fondo.objects.filter(categoria=c, fecha_resolucion__lt = datetime.datetime.now(), inicio_operaciones__isnull=False, vigente = True).exclude(inicio_operaciones__year__gt=anio).exists():
                    for fond in fondo.objects.filter(categoria=c, fecha_resolucion__lt = datetime.datetime.now(), inicio_operaciones__isnull=False, vigente = True).exclude(inicio_operaciones__year__gt=anio).order_by('nombre'):
                        if fond == f:
                            sumador += paginas_iniciales
                            return sumador
                        #print("Sumo 2")
                        sumador += 2
                    if c.indice[0:2] != anterior and anterior != None:
                        #print(str(c.indice[0:2])+" -- "+str(anterior))
                        anterior = c.indice[0:2]
                        sumador += 2
                    # /***|***\     /***|***\    /***|
                    # | f1|f1 |  => | I | I | => | I | ...
                    # \ x |x+1/     \x+2|x+3/    \x+4|
            else:#fondos corfo
                
                if fondo_corfo.objects.filter(tipo_fondo=c).order_by('nombre').exists():
                    for fond in fondo_corfo.objects.filter(tipo_fondo=c).order_by('nombre'):
                        if fond == f:
                            sumador += 1
                            return sumador
                        sumador += 1
        

def get_periodos(fondo, fecha_inicio =None, fecha_termino=None):

    if fecha_inicio is None:
        fecha_inicio = str(fondo.inicio_operaciones)
    if fondo.termino_operaciones is None or fecha_termino is None:
        fecha_termino = datetime.datetime.today().strftime('%Y-%m-%d')
    #periods = get_periodos(fecha_inicio, fecha_termino)

    fi = fecha_inicio.split('-')
    ft = fecha_termino.split('-')
    
    cota_inf = ""
    cota_sup = ""
    periodos = []

    #print(fi)
    for mes in range(3,13,3):
        if fi == ['None']:
            continue
        if mes >= int(fi[1]):
            cota_inf = str(mes)+'-'+str(fi[0])
            continue
    for mes in range(3,13,3):            
        if mes >= int(ft[1]):
            cota_sup = str(mes)+'-'+str(ft[0])
            continue

    
    cota_inf = cota_inf.split('-')
    cota_sup = cota_sup.split('-')

    try:
        for anio in range(int(cota_inf[1]), int(cota_sup[1])+1):
            for mes in range(3,13,3):            
                if mes >= int(cota_inf[0]) and anio == int(cota_inf[1]):
                    if mes < 10:
                        periodos.append(str(anio)+"-0"+str(mes))
                    else:
                        periodos.append(str(anio)+"-"+str(mes))
                if anio > int(cota_inf[1]) and anio < int(cota_sup[1])+1:
                    if mes < 10:
                        periodos.append(str(anio)+"-0"+str(mes))
                    else:
                        periodos.append(str(anio)+"-"+str(mes))
                if mes <= int(cota_sup[0]) and anio >= int(cota_sup[1])+1:
                    if mes < 10:
                        periodos.append(str(anio)+"-0"+str(mes))
                    else:
                        periodos.append(str(anio)+"-"+str(mes))
    except IndexError:
        pass   

    #periodos.append(cota_sup[1]+"-"+cota_sup[0])
    return periodos

#retorna los periodos ordenados, incluyendo el actual
def getperiodosanteriores(f, cota_inferior, cota_superior): 
    
    
    periodos = list()
    for per in get_periodos(f, cota_inferior, cota_superior):
        aux = per.split("-")
        if per <= cota_superior:
            periodos.append(str(aux[1])+'-'+str(aux[0]))

    #print(periodos)
    p = periodo.objects.filter(fondo=f, periodo__in= periodos)
    #for auxp in p:
    #    print(auxp.periodo)
    #print(p)
    
    return p

def convertir(origen, destino, cantidad, fecha= datetime.date.today()):
    if origen == destino:
        return cantidad
    if origen== 'Peso Chileno' or origen == 'CLP':
        i = indicador.objects.get(pk=fecha)
        if destino=='dolar' or destino == 'USD':
            fecha= (datetime.datetime.strptime(str(fecha), '%Y-%m-%d') - datetime.timedelta(days=1)).date()
            if i.dolar == None:
                i = indicador.objects.filter(dolar__gt=0, fecha__lt=fecha).last()        
            ind = i.dolar
        

        if destino=='uf' or destino== 'CLF':
            if i.uf == None:
                i = indicador.objects.filter(uf__gt=0, fecha__lt=fecha).last()        
            
            ind = i.uf
        if destino=='euro' or destino == 'EUR':
            if i.euro == None:
                i = indicador.objects.filter(euro__gt=0, fecha__lt=fecha).last()        
        
            ind = i.euro

        
        return float(cantidad)/float(ind)


    if destino == 'Peso Chileno' or destino == 'CLP':
        i = indicador.objects.get(pk=fecha)
        
        if origen=='dolar' or origen == 'USD':
            if i.dolar == None:
                i = indicador.objects.filter(dolar__gt=0, fecha__lt=fecha).last()

            ind = i.dolar
        if origen=='uf' or origen == 'CLF':
            if i.uf == None:
                i = indicador.objects.filter(uf__gt=0, fecha__lt=fecha).last()        
            
            ind = i.uf
        if origen=='euro' or origen== 'EUR' :
            if i.euro == None:
                i = indicador.objects.filter(euro__gt=0, fecha__lt=fecha).last()        
            
            ind = i.euro
        return float(cantidad) * float(ind)

def revisar(numero) :
    retorno = str(numero)
    if len(retorno) < 2 :
        retorno = '0' + retorno
    return retorno

#funciones para catastro
def forfecha(period):
    return datetime.datetime.strptime("01-"+str(period), "%d-%m-%Y").strftime("%b %Y")

def valor_divisa(period, divisa):
    aux = period.split('-')
    
    i = indicador.objects.filter(fecha__month=aux[0], fecha__year=aux[1]).order_by('-fecha').first()
    if divisa == 'dolar':
        nueva_fecha = i.fecha + datetime.timedelta(days=1)
        i = indicador.objects.get(pk= nueva_fecha)
    
    if divisa != 'pesos':
        while i._meta.get_field( divisa.lower()).value_from_object(i) == None:
            nueva_fecha = i.fecha + datetime.timedelta(days=1)
            i = indicador.objects.get(pk= nueva_fecha)
        return i._meta.get_field( divisa.lower()).value_from_object(i)


