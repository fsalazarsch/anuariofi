from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.contrib.auth.models import User
from acafi.models import fondo, perfil, administrador, divisa, categoria, fondo_ext, movimiento, exportacion, inversion, periodo, rent, fondo_corfo, resumen, resultado, accion_nacional, composicion, sector, catastro
from acafi.view.usuario import enviar_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.safestring import mark_safe
from django.contrib.auth import authenticate, login
from django.template import loader
from django.db.models import Count, Avg, Sum, Q, F
from django.conf import settings
import json, csv, requests, decimal, pyexcel
from datetime import datetime
from django.core.exceptions import ValidationError
from io import BytesIO
from openpyxl import Workbook, load_workbook


from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.styles import colors, fills
from openpyxl.cell import Cell

from django.db.models.functions import Substr
from acafi.deploy import escribir_log

filter_periodo = "09-2018"
ruta = settings.STATIC_ROOT+settings.STATIC_URL+'tmp/catastro'+str(filter_periodo)+'.xlsx'    

def get_sector(stri):
    try:
        s = sector.objects.get(pk=stri)
        return s.nombre
    except sector.DoesNotExist:
        return '---'

def crear_archivo():
    try:
        wb = load_workbook(ruta)
    except FileNotFoundError:
        wb = Workbook()
        wb.save(ruta)
    return wb


def crear_hoja(wb, stri):
    if stri not in wb.sheetnames:
        sheet = wb.create_sheet(stri)
    else:
        sheet = wb[str(stri)]
    return sheet

def descargar_excel():
    peraux = filter_periodo.split("-")

    mes_inicio = int(peraux[0])-2
    anio = int(peraux[1])

    if int(peraux[0]) != 12:
        mes_termino = int(peraux[0])+1
    else:
        mes_termino = 1
        anio += 1

    if mes_inicio < 10:
        mes_inicio = "0"+str(mes_inicio)

    fecha_desde = "01/"+str(mes_inicio)+"/"+str(anio)

    if mes_termino < 10:
        mes_termino = "0"+str(mes_termino)

    fecha_hasta = "01/"+str(mes_termino)+"/"+str(anio)

    url="http://www.cmfchile.cl/institucional/inc/clasificaciones_asignadas_excel_fcorte_2.php?clasificadora=0&tipo_emisor=0&emisor=0&tipo_instrumento=0&fecha_desde="+str(fecha_desde)+"&fecha_hasta="+str(fecha_hasta)+"&fecha_corte="+str(fecha_hasta)+"&insc_emisor=0&viginst=0"
    hdr = {'User-Agent': 'Mozilla/5.0'}
    r = requests.post(url, headers=hdr)
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    output = open(ruta+"riesgos.xlsx", 'wb')
    output.write(r.content)
    output.close()

def estados_financieros():
    filter_periodo = "09-2018"

    lista = fondo.objects.filter(inicio_operaciones__lt = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    #lista = fondo.objects.filter(fondo__runsvs__contains= 7008)

    for l in lista:
        print(l.runsvs)

        try:
            c = catastro.objects.get(fondo=l, periodo = filter_periodo)
        except catastro.DoesNotExist:
            c = catastro(fondo=l, periodo=filter_periodo)
            c.save()

        if c.eeff == {}:
            movimientos = movimiento.objects.filter(fondo = l, periodo = filter_periodo)
                
            aux = {}

            movs = movimientos.filter(etiqueta_svs__startswith="AC-")
            for m in movs:
                aux['AC-'] = {}
                aux['AC-'][str(m.etiqueta_svs)] = m.monto

            movs = movimientos.filter(etiqueta_svs__startswith="ANC-")
            for m in movs:
                aux['ANC-'] = {}
                aux['ANC-'][str(m.etiqueta_svs)] = m.monto

            movs = movimientos.filter(etiqueta_svs__startswith="PC-")
            for m in movs:
                aux['PC-'] = {}
                aux['PC-'][str(m.etiqueta_svs)] = m.monto

            movs = movimientos.filter(etiqueta_svs__startswith="PNC-")
            for m in movs:
                aux['PNC-'] = {}
                aux['PNC-'][str(m.etiqueta_svs)] = m.monto

            movs = movimientos.filter(etiqueta_svs__startswith="PN-")
            for m in movs:
                aux['PN-'] = {}
                aux['PN-'][str(m.etiqueta_svs)] = m.monto

            movs = movimientos.filter(etiqueta_svs__startswith="TOTAL")
            for m in movs:
                aux['TOTAL_MOVIMIENTOS'] = {}
                aux['TOTAL_MOVIMIENTOS'][str(m.etiqueta_svs)] = m.monto

            resultados = resultado.objects.filter(fondo = l, periodo = filter_periodo)

            results = resultados.filter(etiqueta_svs__startswith="IPDLO-")
            for r in results:
                aux['IPDLO-'] = {}
                aux['IPDLO-'][str(r.etiqueta_svs)] = r.monto
           
            try:
                results =resultados.get(etiqueta_svs="G-")
                for r in results:
                    aux['G-'] = {}
                    aux['G-'][str(r.etiqueta_svs)] = r.monto
            except resultado.DoesNotExist:
                aux['G-'] = {}

            try:
                results =resultados.get(etiqueta_svs="ORI-")
                for r in results:
                    aux['ORI-'] = {}
                    aux['ORI-'][str(r.etiqueta_svs)] = r.monto
            except resultado.DoesNotExist:
                aux['ORI-'] = {}

            try:
                results =resultados.get(etiqueta_svs="TOTAL")
                for r in results:
                    aux['TOTAL_RESULTADOS'] = {}
                    aux['TOTAL_RESULTADOS'][str(r.etiqueta_svs)] = r.monto
            except resultado.DoesNotExist:
                aux['TOTAL_RESULTADOS'] = {}

            try:
                r =resultados.get(etiqueta_svs__icontains="PERDIDA DE LA OPERACION")
                aux['P-'] = {}
                aux['P-'][str(r.etiqueta_svs)] = r.monto
            except resultado.DoesNotExist:
                aux['P-'] = {}

            try:
                r =resultados.get(etiqueta_svs__icontains="PERDIDA ANTES DE IMPUESTO")
                aux['P-'][str(r.etiqueta_svs)] = r.monto
            except resultado.DoesNotExist:
                aux['P-'] = {}

            try:
                r =resultados.get(etiqueta_svs__icontains="RESULTADO DEL EJERCICIO")
                aux['P-'][str(r.etiqueta_svs)] = r.monto
            except resultado.DoesNotExist:
                aux['P-'] = {}

            c.eeff = aux
            c.save()
        else:
            pass


    #return HttpResponseRedirect('/perfil/')
def parametros_iniciales():#hoja2
    
    lista = fondo.objects.filter(inicio_operaciones__lt = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)

    for l in lista:
        print(l.runsvs)
        aux = {}

        try:
            c = catastro.objects.get(fondo=l, periodo = filter_periodo)
        except catastro.DoesNotExist:
            c = catastro(fondo=l, periodo=filter_periodo)
            c.save()

        if c.parametros_iniciales == {}:
            aux['nombre'] = l.nombre
            aux['admin_rut'] = l.admin.rut
            aux['admin_nombre'] = l.admin.razon_social
            aux['runsvs'] = l.runsvs
           
            if l.categoria == None:
                aux['categoria'] = '---'
            else:
                aux['categoria'] = l.categoria.indice

            if l.tipo_inversion == 0:
                aux['tipo_inversion'] = "No Rescatable"
            if l.tipo_inversion == 1:
                aux['tipo_inversion'] = "Rescatable"

            try:
                m = movimiento.objects.get(fondo=l, periodo=filter_periodo, etiqueta_svs="Total Activo")
                aux['total_activo'] = {}
                aux['total_activo']['moneda'] = m.moneda
                aux['total_activo']['monto'] = m.monto
            except movimiento.DoesNotExist:
                aux['total_activo'] = {}
        
            
            p = periodo.objects.get(periodo=filter_periodo, fondo=l)
            aux2 = json.loads(p.datos)
            aux['periodo'] = {}
            aux['periodo']['APORTES'] = aux2['APORTES']
            aux['periodo']['REPARTO_PATRIMONIOS'] = aux2['REPARTOS DE PATRIMONIOS']
            aux['periodo']['REPARTO_DIVIDENDOS']= aux2['REPARTOS DE DIVIDENDOS']

            try:
                m = movimiento.objects.get(fondo=l, periodo=filter_periodo, etiqueta_svs__icontains="Total Patrimonio Neto")
                aux['total_patrimonio_neto'] = {}
                aux['total_patrimonio_neto']['monto'] = m.monto
                aux['total_patrimonio_neto']['valor_obs'] = float(p.valor_obs)
            except movimiento.DoesNotExist:
                aux['total_patrimonio_neto'] = {}

            c.parametros_iniciales = aux
            c.save()
        else:
            pass

def nuevos_fondos(request):#hoja3
    
    wb = Workbook()
    ws = wb.create_sheet("3-Nuevos Fondos Trimestre", 0) # insert at first position

    ws['C2'] = "Rut Fondo"
    ws['D2'] = "Nombre Fondo"
    ws['E2'] = "Rescatable/no Rescatable"
    ws['F2'] = "Incio operaciones"
    ws['G2'] = "Clasificacion acafi"
    ws['H2'] = "Administradora"
    ws['I2'] = "Tiene datos en la eeff"
    

    #lista  = fondo.objects.filter(vigente = True)
    lista = fondo.objects.filter(inicio_operaciones__lte = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    lista = lista.filter( Q(categoria=0) | Q(categoria=None) )
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)



    col = 3
    fila = 3
    for l in lista:
        print(l.runsvs)
        
        ws.cell(row=fila, column=col, value=l.runsvs)
        ws.cell(row=fila, column=col+1, value=l.nombre)

        if l.tipo_inversion == 0:
            tipo_inversion = "No Rescatable"
        if l.tipo_inversion == 1:
            tipo_inversion = "Rescatable"
        ws.cell(row=fila, column=col+2, value=tipo_inversion)
        ws.cell(row=fila, column=col+3, value=l.inicio_operaciones)

        if l.categoria == None:
            categorianombre = '---'
        else:
            categorianombre = l.categoria.indice
        ws.cell(row=fila, column=col+4, value=categorianombre)
        ws.cell(row=fila, column=col+5, value=l.admin.razon_social)

        if resultado.objects.filter(periodo=filter_periodo, fondo=l).count() > 0:
            res = "Si"
        else:
            res = "No"
        ws.cell(row=fila, column=col+6, value=res)
 
        #Valor Libro de la Cuota
        fila += 1
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"tmp/"
    wb.save(ruta+'hoja3.xlsx')

    return HttpResponseRedirect('/perfil/')

def cartera_inversiones():#hoja4

    arr = ["Acciones de sociedades anónimas abiertas", "Derechos preferentes de suscripción de acciones de sociedades anónimas abiertas", "Cuotas de fondos mutuos", "Cuotas de fondos de inversión", "Certificados de depósitos de valores (CDV)", "Títulos que representen productos", "Otros títulos de renta variable", "Depósitos a plazo y otros títulos de bancos e instituciones financieras", "Cartera de créditos o de cobranzas", "Títulos emitidos o garantizados por Estados o Bancos Centrales", "Otros títulos de deuda", "Acciones no registradas", "Cuotas de fondos de inversión privados", "Títulos de deuda no registrados", "Bienes raíces", "Proyectos en desarrollo", "Deuda de operaciones de leasing", "Acciones de sociedades anónimas inmobiliarias y concesionarias", "Otras inversiones"]
    lista = fondo.objects.filter(inicio_operaciones__lte = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)


    for l in lista:
        print(l.runsvs)
        try:
            c = catastro.objects.get(fondo=l, periodo = filter_periodo)
        except catastro.DoesNotExist:
            c = catastro(fondo=l, periodo=filter_periodo)
            c.save()

        if c.cartera_inversiones == {}:
            aux = {}
            
            aux['fondo'] = {}
            aux['fondo']['nombre'] = l.nombre
            aux['fondo']['admin'] = {}
            aux['fondo']['admin']['rut'] = l.admin.rut
            aux['fondo']['admin']['razon_social'] = l.admin.razon_social
            aux['fondo']['runsvs'] = l.runsvs

            if l.categoria == None:
                categorianombre = '---'
            else:
                categorianombre = l.categoria.indice

            aux['fondo']['categoria'] = categorianombre
            if l.tipo_inversion == 0:
                tipo_inversion = "No Rescatable"
            if l.tipo_inversion == 1:
                tipo_inversion = "Rescatable"

            aux['fondo']['tipo_inversion'] = tipo_inversion

            p = periodo.objects.get(fondo=l, periodo=filter_periodo)
            aux['cartera_inversiones'] = {}

            for a in arr:
                aux['cartera_inversiones'][str(a)] = {}
                try:
                    r = resumen.objects.get(periodo=p, descripcion=a)
                    rnacional = r.nacional
                    rextranjero = r.extranjero
                except resumen.DoesNotExist:
                    rnacional = 0
                    rextranjero = 0

                aux['cartera_inversiones'][str(a)]['nacional'] = rnacional
                aux['cartera_inversiones'][str(a)]['extranjero'] = rextranjero
            c.cartera_inversiones = aux
            c.save()
def acciones_nacionales():#hoja5
    
    lista = fondo.objects.filter(inicio_operaciones__lte = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)
        #lista = lista.filter(pk = "7002-5")
    col = 1
    fila = 6
    for l in lista:
        print(l.runsvs)

        try:
            c = catastro.objects.get(fondo=l, periodo = filter_periodo)
        except catastro.DoesNotExist:
            c = catastro(fondo=l, periodo=filter_periodo)
            c.save()

        if c.acciones_nacionales == {}:

            aux = {}
            
            aux['fondo'] = {}
            aux['fondo']['nombre'] = l.nombre
            aux['fondo']['admin'] = {}
            aux['fondo']['admin']['rut'] = l.admin.rut
            aux['fondo']['admin']['razon_social'] = l.admin.razon_social
            aux['fondo']['runsvs'] = l.runsvs

            if l.categoria == None:
                categorianombre = '---'
            else:
                categorianombre = l.categoria.indice

            aux['fondo']['categoria'] = categorianombre


            acciones = accion_nacional.objects.filter(fondo = l, periodo=filter_periodo)
            
            for a in acciones:
                aux['acciones'] = {}
                if a.tipo_instrumento == "ACC":
                    aux['acciones']['sector'] = get_sector(a.nemotecnico)
                    aux['acciones']['nemotecnico'] = a.nemotecnico
                    aux['acciones']['rut_emisor'] = a.rut_emisor
                    aux['acciones']['cod_pais'] = a.cod_pais
                    aux['acciones']['tipo_instrumento'] = a.tipo_instrumento
                    aux['acciones']['fecha_vencimiento'] = a.fecha_vencimiento
                    aux['acciones']['situacion_instrumento'] = a.situacion_instrumento
                    aux['acciones']['clasificacion_riesgo'] = a.clasificacion_riesgo
                    aux['acciones']['grupo_empresarial'] = a.grupo_empresarial
                    aux['acciones']['cant_unidades'] = a.cant_unidades
                    aux['acciones']['tipo_unidades'] = a.tipo_unidades
                    aux['acciones']['tir_precio'] = a.tir_precio
                    aux['acciones']['codigo_valolizacion'] = a.codigo_valolizacion
                    aux['acciones']['tasa'] = a.tasa
                    aux['acciones']['tipo_interes'] = a.tipo_interes
                    aux['acciones']['valolizacion_cierre'] = a.valolizacion_cierre
                    aux['acciones']['cod_moneda_liq'] = a.cod_moneda_liq
                    aux['acciones']['cod_pais_transaccion'] = a.cod_pais_transaccion
                    aux['acciones']['cap_emisor'] = a.cap_emisor
                    aux['acciones']['act_emisor'] = a.act_emisor
                    aux['acciones']['activo_fondo'] = a.activo_fondo
                    
            c.acciones_nacionales = aux
            c.save()
def cuotas_aportantes():#hoja6
    lista = fondo.objects.filter(inicio_operaciones__lte = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)
        #lista = lista.filter(pk = "7002-5")

    for l in lista:
        print(l.runsvs)

        try:
            c = catastro.objects.get(fondo=l, periodo = filter_periodo)
        except catastro.DoesNotExist:
            c = catastro(fondo=l, periodo=filter_periodo)
            c.save()

        if c.cuotas_aportantes == {}:

            aux = {}
            
            aux['fondo'] = {}
            aux['fondo']['nombre'] = l.nombre
            aux['fondo']['admin'] = {}
            aux['fondo']['admin']['rut'] = l.admin.rut
            aux['fondo']['admin']['razon_social'] = l.admin.razon_social
            aux['fondo']['runsvs'] = l.runsvs

            if l.categoria == None:
                categorianombre = '---'
            else:
                categorianombre = l.categoria.indice

            aux['fondo']['categoria'] = categorianombre

            if l.tipo_inversion == 0:
                tipo_inversion = "No Rescatable"
            if l.tipo_inversion == 1:
                tipo_inversion = "Rescatable"
            aux['fondo']['tipo_inversion'] =tipo_inversion

            aux['periodo'] = {}
            p = periodo.objects.get(periodo=filter_periodo, fondo=l)
            
            aux['periodo']['total_aportantes'] = p.total_aportantes
            
            aux2 = json.loads(p.datos)
            aux['periodo']['CUOTAS_EMITIDAS'] = float(aux2['CUOTAS EMITIDAS'])
            aux['periodo']['CUOTAS_PAGADAS'] = float(aux2['CUOTAS PAGADAS'])
            
            composiciones = composicion.objects.filter(periodo=p).order_by('-porc_propiedad')[:12]
            aux['composiciones'] = {}
            aux3=[]
            for comp in composiciones:
                aux2 = {}
                aux2['aportante_nombre'] = comp.aportante.nombre
                aux2['aportante_rut'] = comp.aportante.rut
                aux2['aportante_persona'] = comp.aportante.get_persona()
                aux2['porc_propiedad'] = float(comp.porc_propiedad)
                aux3.append(aux2)
            aux['composiciones'] = aux3

            c.cuotas_aportantes = aux
            c.save()
def clasif_riesgo():#hoja7

    lista = fondo.objects.filter(inicio_operaciones__lte = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)
        #lista = lista.filter(pk = "7002-5")

    for l in lista:
        print(l.runsvs)
        try:
            c = catastro.objects.get(fondo=l, periodo = filter_periodo)
        except catastro.DoesNotExist:
            c = catastro(fondo=l, periodo=filter_periodo)
            c.save()

        if c.clasif_riesgo == {}:

            aux = {}
            
            aux['fondo'] = {}
            aux['fondo']['nombre'] = l.nombre
            aux['fondo']['admin'] = {}
            aux['fondo']['admin']['rut'] = l.admin.rut
            aux['fondo']['admin']['razon_social'] = l.admin.razon_social
            aux['fondo']['runsvs'] = l.runsvs

            if l.categoria == None:
                categorianombre = '---'
            else:
                categorianombre = l.categoria.indice

            aux['fondo']['categoria'] = categorianombre

            f = fondo_ext.objects.get(runsvs=l.runsvs)
            aux['riesgo'] = {}
            try:
                riesg = json.loads(f.riesgo)
                feller= riesg['felier_rate']
                fitch = riesg['fitch_ratings']
                icr = riesg['icr']
                humphreys = riesg['humphreys']

            except json.JSONDecodeError:
                feller = ''
                fitch=''
                icr =''
                humphreys=''


            aux['riesgo']['fitch'] =fitch
            aux['riesgo']['humphreys'] =humphreys
            aux['riesgo']['feller'] =feller
            aux['riesgo']['icr'] =icr

            c.clasif_riesgo = aux
            c.save()
def inv_pais():#hoja8

    lista = fondo.objects.filter(inicio_operaciones__lte = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)
        
    for l in lista:
        print(l.runsvs)
        try:
            c = catastro.objects.get(fondo=l, periodo = filter_periodo)
        except catastro.DoesNotExist:
            c = catastro(fondo=l, periodo=filter_periodo)
            c.save()

        if c.inv_pais == {}:
            aux = {}
            
            aux['fondo'] = {}
            aux['fondo']['nombre'] = l.nombre
            aux['fondo']['admin'] = {}
            aux['fondo']['admin']['rut'] = l.admin.rut
            aux['fondo']['admin']['razon_social'] = l.admin.razon_social
            aux['fondo']['runsvs'] = l.runsvs

            if l.categoria == None:
                categorianombre = '---'
            else:
                categorianombre = l.categoria.indice

            aux['fondo']['categoria'] = categorianombre
            if l.tipo_inversion == 0:
                tipo_inversion = "No Rescatable"
            if l.tipo_inversion == 1:
                tipo_inversion = "Rescatable"

            aux['fondo']['tipo_inversion'] = tipo_inversion


            aux['paises'] = {}
            invs = inversion.objects.filter(fondo=l, periodo= filter_periodo).values('pais').annotate(suma_porcentaje = Sum('porcentaje')).order_by('pais')
            for i in invs:
                try:
                    i['suma_porcentaje'] = float(i['suma_porcentaje'])
                except TypeError:
                    i['suma_porcentaje'] = 0
            aux['paises'] = list(invs)

            aux['cod_paises_transaccion'] = {}
            invs = inversion.objects.filter(fondo=l, periodo= filter_periodo).values('cod_pais_transaccion').annotate(suma_porcentaje = Sum('porcentaje')).order_by('pais')
            for i in invs:
                try:
                    i['suma_porcentaje'] = float(i['suma_porcentaje'])
                except TypeError:
                    i['suma_porcentaje'] = 0
            aux['cod_paises_transaccion'] = list(invs)


            c.inv_pais = aux
            c.save()
def inv_moneda():#hoja9

    lista = fondo.objects.filter(inicio_operaciones__lte = datetime.now(), inicio_operaciones__isnull=False, vigente = True).order_by('runsvs')
    if filter_periodo != None and filter_periodo != '' and filter_periodo != 'None':
        lista = lista.filter(periodo__periodo__contains=filter_periodo)
        
    for l in lista:
        print(l.runsvs)
        try:
            c = catastro.objects.get(fondo=l, periodo = filter_periodo)
        except catastro.DoesNotExist:
            c = catastro(fondo=l, periodo=filter_periodo)
            c.save()

        if c.inv_moneda == {}:
            aux = {}
            
            aux['fondo'] = {}
            aux['fondo']['nombre'] = l.nombre
            aux['fondo']['admin'] = {}
            aux['fondo']['admin']['rut'] = l.admin.rut
            aux['fondo']['admin']['razon_social'] = l.admin.razon_social
            aux['fondo']['runsvs'] = l.runsvs

            if l.categoria == None:
                categorianombre = '---'
            else:
                categorianombre = l.categoria.indice

            aux['fondo']['categoria'] = categorianombre
            if l.tipo_inversion == 0:
                tipo_inversion = "No Rescatable"
            if l.tipo_inversion == 1:
                tipo_inversion = "Rescatable"

            aux['fondo']['tipo_inversion'] = tipo_inversion


            aux['moneda'] = {}
            invs = inversion.objects.filter(fondo=l, periodo= filter_periodo).values('moneda').annotate(suma_porcentaje = Sum('porcentaje')).order_by('pais')
            for i in invs:
                try:
                    i['suma_porcentaje'] = float(i['suma_porcentaje'])
                except TypeError:
                    i['suma_porcentaje'] = 0
            aux['moneda'] = list(invs)

            aux['tipo_unidades'] = {}
            invs = inversion.objects.filter(fondo=l, periodo= filter_periodo).values('tipo_unidades').annotate(suma_porcentaje = Sum('porcentaje')).order_by('pais')
            for i in invs:
                try:
                    i['suma_porcentaje'] = float(i['suma_porcentaje'])
                except TypeError:
                    i['suma_porcentaje'] = 0
            aux['tipo_unidades'] = list(invs)


            c.inv_moneda = aux
            c.save()

def crear_excel():
    wb = crear_archivo()
    sheet = crear_hoja(wb, "1-EEFF")
    catastros = catastro.objects.filter(periodo=filter_periodo).order_by('fondo__runsvs')
    
    col = 5
    for cat in catastros:
        sheet.cell(row=4, column=col, value=str(cat.fondo.runsvs))
            
        fila = 6
        cat = cat.eeff
        for attribute, value in cat.items():

            sheet.cell(row=fila, column=col, value=str(attribute+str(value)))
            fila += 1
        #ws.cell(row=fila, column=col).number_format = '#,##0'
        col+=1
    wb.save(ruta)

def generar_catastro():
    '''
    estados_financieros()
    print('Hoja 1')
    parametros_iniciales()
    print('Hoja 2')
    cartera_inversiones()
    print('Hoja 4')
    acciones_nacionales()
    print('Hoja 5')
    cuotas_aportantes()
    print('Hoja 6')
    clasif_riesgo()
    print('Hoja 7')
    inv_pais()
    print('Hoja 8')
    inv_moneda()
    print('Hoja 9')
    '''
    crear_excel()

