import requests, re, unicodedata, datetime, json, decimal, csv, io, urllib
from django.shortcuts import render
from bs4 import BeautifulSoup, element, SoupStrainer
from acafi.models import fondo_ext, fondo, administrador, periodo, composicion, aportante, inversion, movimiento, resumen, rent, resultado, accion_nacional, serie, cuota, indicador, fondoInversion, detalle_fondo, instrumento, pais, divisa, eventoInversion, nemo_inversion
from acafi.deploy import escribir_log, escribir_log_puro, convertir, revisar
from django.db.models import Min, Q
from django.db.utils import IntegrityError

import os
from django.conf import settings



def descargarscrap():
    for finv in nemo_inversion.objects.all():
        nemo = finv.nemotecnico
        url = 'http://www.bolsadesantiago.com/mercado/Paginas/Resumen-de-Instrumento.aspx?RequestHistorico=1&Nemo='+nemo
        response = requests.get(url)
        with open(os.path.join(settings.STATIC_ROOT+"/static/csv/"+nemo+".txt"), 'wb') as f:
            f.write(response.content)

def scrap():
    nemo = "CFIPIONERO"
    ruta = os.path.join(settings.STATIC_ROOT+"/static/csv/"+nemo+".txt")
    file = open(ruta, "r")
    cont = 0
    for l in  file.readlines():
        if cont > 2:
            print(l)
        cont += 1


def scrapcsv():
	#descargarscrap()
	#escribir_log("descarga de csv lista")
	scrap()

        

        #ftpstream = urllib.request.urlopen(url)
        #csvfile = csv.reader(ftpstream.read().decode('utf-8'))  # with the appropriate encoding 
        #for row in csvfile:
	    #    escribir_log(str(row))
