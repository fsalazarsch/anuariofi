from django.shortcuts import render
from bs4 import BeautifulSoup, element
from bs4 import SoupStrainer
import requests, re, unicodedata
import urllib3, datetime, json, urllib.request
from acafi.models import fondo_ext, fondo, administrador, periodo, composicion, aportante, inversion, movimiento, resumen, fondo_corfo, comuna, fondoInversion, divisa, eventoInversion, nemo_inversion
from acafi.deploy import get_periodos, escribir_log, revisar



url_base ='http://www.cmfchile.cl/institucional/mercados/'
#fondos de inversion rescatabales
url_fir ='http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=TO&entidad=FIRES'
#fondos de inversion no rescatabales
url_finr ='http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=TO&entidad=FINRE'
#fondos mutuos
#url_fmu = 'http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=TO&entidad=RGFMU'
#fondos para la vivvienda
#url_fpv = 'http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=TO&entidad=RGFVI'
#fondos corfo
url_fc = 'http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=VI&entidad=RAFIP'

urls = [url_fir, url_finr, url_fc]


def limpiar(llave):
    # deja en minúscula, elimina espacios extra, y elimina símbolos que no permitan usar la llave
    llave = llave.strip().lower().replace(' ', '_')
    llave = unicodedata.normalize('NFKD', llave)
    llave = re.sub(r'\W+', '', llave)
    llave = llave.replace('_', ' ')
    return llave.strip()
def verificar(RUT) :
    if not re.match('\d{4,8}(-[\dkK])?', RUT) :
        return RUT
    lista = str(RUT)[::-1]
    secuencia = [2, 3, 4, 5, 6, 7]
    ver = 0
    for i in range(len(lista)) :
        ver += secuencia[i % 6] * int(lista[i])
    ver = 11 - (ver % 11)
    return str(ver) if ver < 10 else ('K' if ver == 10 else '0')

def format_time(var):
    aux = var.split('/')
    return aux[2]+'-'+aux[1]+'-'+aux[0]
def filtro_rut(tag):
    return tag.name == 'table' and tag['border'] == "1"

def format_num(strnum): #formato español a ingles
    if ',' in strnum:
        strnum = strnum.replace('.','')
        strnum = strnum.replace(',','.')
    if strnum.count(".") > 1:
        strnum = strnum.replace('.','')
        
    return float(strnum)


def voltear_fecha(s):
    aux = s.split("-")
    return str(aux[2])+"-"+str(aux[1])+"-"+str(aux[0])

#saca los datos de fondos rescatables y no rescatables
def get_lista(): 
    #print(fondos_objetivo)
    for u in urls:
        print(u)
        hdr = {'User-Agent': 'Mozilla/5.0'}
        page = requests.get(u, headers =hdr) 
        soup = BeautifulSoup(page.content, 'html.parser')
        tabla =soup.find_all('tr')
        tabla.pop(0)

        #primero scraping
        res = []
        links = []
        for row in tabla:
            cols = row.find_all('td')
            for link in cols[0].find_all('a'):
                links.append(link['href'])

            cols = [ele.text.strip() for ele in cols]
            cols.append(link['href'])
            res.append([ele for ele in cols if ele])

        #guardar en bdd
        for r in res:
            if r[2] == 'VI':
                r[2] = True
            else:
                r[2] = False
            
            
            if 'RAFIP' not in u:# and r[0] != "9247-9" and r[0] != "9137-5" or r[0] != "9320-3":     
                try:
                #if fondo.objects.filter(pk = r[0]).exists:
                    adm = fondo.objects.get(runsvs= r[0], nombre= r[1], vigente= r[2], urlsvs= url_base+r[3]+'&control=svs&pestania=1')
                except fondo.DoesNotExist:
                    adm = fondo(runsvs= r[0], nombre= r[1], vigente= r[2], urlsvs= url_base+r[3]+'&control=svs&pestania=1')
                    adm.save()
                #print(r[0])

            if 'RAFIP' in u:
                pass

            else:
                try:
                    fondoext = fondo_ext.objects.get(runsvs= r[0])
                except fondo_ext.DoesNotExist:
                    fondoext = fondo_ext(runsvs= r[0], bolsa_offshore='', portafolio_manager='')
                    fondoext.save()

            get_detalle_fondo(url_base+r[3]+'&control=svs&pestania=1')
            print(r[0])
            #escribir_log(str(adm), 'get_lista')
    escribir_log("get_lista -- terminado")
    #return render(request, "scraping.html",  {'tabla' : salida})
def get_detalle_fondo(url): #scraping de pestaña 1, fondo y administrador

    #url = 'http://www.cmfchile.cl/institucional/mercados/entidad.php?mercado=V&rut=9281&grupo=&tipoentidad=FIRES&row=AAAw%20cAAhAABP4PAAu&vig=VI&control=svs&pestania=1'
    hdr = {'User-Agent': 'Mozilla/5.0'}
    page = requests.get(url, headers =hdr) 

    sopita = BeautifulSoup(page.content, 'html.parser')
    
    izq = sopita.find_all("th")
    der = sopita.find_all("td")
    dic = {x.string.strip(): y.text.strip() for x, y in zip(izq, der)}

    if 'RAFIP' in url:
        rut_raw = dic['RUT'].split('-')
        urlsvsadm = 'http://www.cmfchile.cl/institucional/mercados/entidad.php?auth=&send=&mercado=V&rut='+rut_raw[0]+'&grupo=&tipoentidad=RAFIP&vig=VI&row=AAAwy2ACTAAABzqAAd&control=svs&pestania=1'
        a = administrador(rut = dic['RUT'], urlsvs= urlsvsadm)
        a.razon_social = dic['Razón Social']
        a.save()
    else:

        rut_raw = dic['R.U.T. Administradora'].split('-')

        urlsvsadm = 'http://www.cmfchile.cl/institucional/mercados/entidad.php?auth=&send=&mercado=V&rut='+rut_raw[0]+'&grupo=&tipoentidad=RACRT&vig=VI&row=AAAwy2ACTAAABzvAAM&control=svs&pestania=1'
        a = administrador(rut = dic['R.U.T. Administradora'], razon_social = dic['Razón Social Administradora'], urlsvs= urlsvsadm)
        a.save()

        f = fondo.objects.get(pk = dic['R.U.N. del Fondo'])
        f.admin = a
        if dic['Tipo de Fondo de Inversión:'] == 'Rescatable':
            f.tipo_inversion = 1
        else:
            f.tipo_inversion = 0
        f.fecha_resolucion = format_time(dic['Fecha Resolución de aprobación del reglamento interno del fondo'])
        
        if dic['Número Resolución']:
            f.resolucion = dic['Número Resolución']
        
        if dic['Fecha Inicio Operaciones'] and f.inicio_operaciones == None:
            f.inicio_operaciones = format_time(dic['Fecha Inicio Operaciones'])
        if dic['Fecha Término Operaciones']:
            f.termino_operaciones = format_time(dic['Fecha Término Operaciones'])
        f.save()

    flag = 0
    hdr = {'User-Agent': 'Mozilla/5.0'}
    page = requests.get(urlsvsadm, headers =hdr) 

    sopa_adm = BeautifulSoup(page.content, 'html.parser')
    if sopa_adm.find("h4") is not None:
        flag = 1
        urlsvsadm = urlsvsadm.replace('RACRT', 'RGAFI')
        a.urlsvs = urlsvsadm
        a.save()

        hdr = {'User-Agent': 'Mozilla/5.0'}
        page = requests.get(urlsvsadm, headers =hdr) 
        sopa_adm = BeautifulSoup(page.content, 'html.parser')

    izq = sopa_adm.find_all("th")
    der = sopa_adm.find_all("td")
    dic = {x.string.strip(): y.text.strip() for x, y in zip(izq, der)}

    try:
        if dic['Nombre de Fantasía']:
            a.nombre_fantasia = dic['Nombre de Fantasía']
    except KeyError:
        pass

    try:
        if dic['Vigencia'] == 'Vigente':
            a.vigente = True
        else:
            a.vigente = False
    except KeyError:
        a.vigente = False

    if flag == 0:
        if dic['Número de Inscripción']:
            a.inscripcion = dic['Número de Inscripción']

        if dic['Fecha de Inscripción']:
            a.fecha_inscripcion = format_time(dic['Fecha de Inscripción'])

        if dic['Fecha de Cancelación']:
            a.fecha_cancelacion = format_time(dic['Fecha de Cancelación'])

        if dic['e-mail de contacto']:
            a.email = dic['e-mail de contacto']

        if dic['Sitio web']:
            a.website = dic['Sitio web']

    try:
        if dic['Teléfono']:
            a.telefono = dic['Teléfono']
    except KeyError:
        pass

    try:
        if dic['Domicilio']:
            a.direccion = dic['Domicilio']
    except KeyError:
        pass

    try:
        if dic['Ciudad']:
            a.ciudad = dic['Ciudad']
    except KeyError:
        pass

    try:
        if dic['Comuna']:
            try: 
                c = comuna.objects.get(nombre=dic['Comuna'])
                a.comuna = c
            except comuna.DoesNotExist:
                c = comuna(nombre=dic['Comuna'])
                c.save()
                a.comuna = c

        a.save()
    except KeyError:
        pass
    #print(a)
    #escribir_log(str(a.rut))

#scarping general de aportantes
def get_principales_aportantes(periodos=None):
    fondos = fondo.objects.filter(vigente = True).order_by('runsvs')
    fondos = fondos.filter(categoria__isnull=True)
    #fondos = fondo.objects.filter(pk = "9120-0")
    asd = ""
    aportantes =[]
    res = []
    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=27")

        if periodos == None:
            periods = get_periodos(f)
        else:
            periods = []
            periods.append(periodos)

        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            print(data)
            #escribir_log(str(data), 'get_principales_aportantes')
               
            page = requests.post(url, data=data) 
            sopa = BeautifulSoup(page.content, 'html.parser')
            iterable = sopa.find_all("table")
            #asd += str(iterable)
            
            
            aportantes = iterable[0]
            internable = aportantes.find_all('tr')
            internable.pop(0)
            for i in internable:
                modelo =[]
                contador_periodo = 0
                

                for cols in sopa.findAll("td", {"class" : "fondoOscuro"}):
                    cols = cols.parent

                    celdas = cols.findChildren()
                    #nuevo_periodo = periodo(fondo = f, periodo = p[1]+"-"+p[0], valor_obs = , total_aportantes = , datos = ,)
                    #asd += celdas[0].text+' ->'+celdas[1].text
                    modelo.append(celdas[1].text)
                    contador_periodo += 1
                    
                print(modelo)
                if contador_periodo == 8:
                    contador_periodo = 0
                     
                    if modelo[0].strip() != "":
                        j = {}
                        j['CUOTAS EMITIDAS'] = modelo[1]
                        j['CUOTAS PAGADAS'] = modelo[2]
                        j['CUOTAS SUSCRITAS Y NO PAGADAS'] = modelo[3]
                        j['NUMERO DE CUOTAS CON PROMESA DE SUSCRIPCION Y PAGO']= modelo[4]
                        j['NUMERO DE CONTRATOS DE PROMESAS DE SUSCRIPCION Y PAGO'] = modelo[5]
                        j['NUMERO DE PROMITENTES SUSCRIPTORES DE CUOTAS'] = modelo[6]
                            
                        print(p[1]+"-"+p[0])
                        #escribir_log(str(f), 'get_principales_aportantes')
                        #escribir_log(p[1]+"-"+p[0], 'get_principales_aportantes')

                        try:
                            nuevo_periodo = periodo.objects.get(fondo = f, periodo = p[1]+"-"+p[0], valor_obs = float(modelo[7]), total_aportantes = modelo[0] )

                        except periodo.DoesNotExist:
                            nuevo_periodo = periodo(fondo = f, periodo = p[1]+"-"+p[0], valor_obs = float(modelo[7]), total_aportantes = modelo[0])
                            print(nuevo_periodo)
                            nuevo_periodo.save()

                        nuevo_periodo.datos = json.dumps(j)
                        nuevo_periodo.save()
                        

                    comps = []
                    contador_composicion = 0
                    aps = {}
                    if i.findChild().text != 'No existen datos para desplegar':
                        fila = i.find_all('td')
                        aps['nombre'] = str(fila[1].text).replace(u'\uFFFD', '')
                        aps['tipo_persona'] = str(fila[2].text).replace(u'\uFFFD', '')
                        aps['rut'] = str(fila[3].text).replace(u'\uFFFD', '')
                        try:
                            apo = aportante.objects.get(nombre= aps['nombre'])
                        except aportante.DoesNotExist:
                            apo = aportante(nombre= aps['nombre'])
                            apo.save()

                        apo.tipo_persona= aps['tipo_persona']
                        apo.rut = aps['rut']
                        apo.save()

                        #print (str(apo))

                        comp = {}
                        comp['aps'] = aps
                        comp['porcentaje'] = str(fila[4].text)
                        comps.append(comp)

                        #escribir_log(str(comp), 'get_principales_aportantes')


                        try:
                            c = composicion.objects.get(periodo = nuevo_periodo, aportante = apo, porc_propiedad = comps[contador_composicion]['porcentaje'])
                        except composicion.DoesNotExist:
                            c = composicion(periodo = nuevo_periodo, aportante = apo, porc_propiedad = comps[contador_composicion]['porcentaje'])
                            c.save()


                        contador_composicion += 1
                    
                    
                    modelo = []
    escribir_log("get_principales_aportantes -- terminado")

#scaraping general de inversiones
def get_inversiones(periodos=None):
    carteras = ['nac', 'ext', 'met_part']
    carts = {}
    
    fondos = fondo.objects.filter(vigente = True).order_by('runsvs')
    nro_fondo = "9137"
    fondos = fondos.filter(pk__contains = nro_fondo)
    
    for f in fondos:
        rut = f.runsvs.split("-")
        if periodos == None:
            periods = get_periodos(f)
        else:
            periods = []
            periods.append(periodos)

        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            print(data)
            escribir_log(str(data), "get_inversiones")
                        
            for cartera in carteras:
                url_cartera = 'http://www.cmfchile.cl/institucional/inc/inf_financiera/ifrs_xml/ifrs_cartera_' + cartera + '.php?rut=' + data['rut'] + '&periodo=' + data['aa'] + data['mm']
                pedi2 = requests.post(url_cartera, data=data)
                caldo = BeautifulSoup(pedi2.text, 'html.parser', parse_only=SoupStrainer('table'))
                
                flag= True

                iter_carteras = caldo.find_all('tr')
                print("cartera: "+cartera)
                print("Inversiones a insertar: "+str(len(iter_carteras)-3)) #todos menos total y headers
                     

                try:
                    if cartera == 'nac':
                        c = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =0, pais='CL').count()
                    if cartera == 'ext':
                        c = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =0).exclude(pais='CL').count()
                    if cartera == 'met_part':
                        c = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =1).count()
                        
                    print("Numero de inversiones: "+str(c))
                    if c == len(iter_carteras)-3 or len(iter_carteras)-3 == 0:
                        flag = False
                    else:
                        flag = True
                except inversion.DoesNotExist:
                    flag = True
                
                if flag == True:
                    borrar_inv = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =0)
                    if cartera == 'nac':
                        borrar_inv = borrar_inv.filter(pais='CL', cartera = 0).delete()
                    if cartera == 'ext':
                        borrar_inv = borrar_inv.filter(cartera=0).exclude(pais='CL').delete()
                    if cartera == 'met_part':
                        borrar_inv = borrar_inv.filter(cartera=1).delete()


                    for row in iter_carteras:
                        dic = {}
                        if len(row.find_all('td')) == 1:
                            # saltarse el padding
                            continue

                        if cartera == 'nac' :
                            interprete = (None, 2, 3, 16, 4, 12, 15, 20, 9, 5)
                        elif cartera == 'ext' :
                            interprete = (3, 1, 4, 17, 5, 13, 16, 21, 10, 6)
                        elif cartera == 'met_part':
                             interprete = (1,2,3,12,4,9,8, 14, 6,0)
                        else :
                            interprete = None

                        if interprete is None or len(interprete) < 9:
                            continue

                        tododato = [tag.string for tag in row.find_all('td')]
                        
                            
                        if len(tododato) < 9:# or tododato[1] is None:# or tododato[1].strip() in excepto:
                            continue

                        #print( 'largo '+str(len(tododato))+' --> '+str(tododato))
                        
                        if interprete[0] is None:

                            # raspar nombre de la sii
                            url_rut = 'https://zeus.sii.cl/cvc_cgi/nar/nar_consulta'
                            if tododato[interprete[1]] == None:
                                print(tododato)
                                tododato[interprete[1]] =''

                            run = ''.join(tododato[interprete[1]].split())
                            rut_data = {
                                'RUT' : run,
                                'DV' : verificar(run),
                                'ACEPTAR' : 'Consultar'
                            }
                            pedido_rut = requests.post(url_rut, data=rut_data)
                            sopa_rut = BeautifulSoup(pedido_rut.text, 'html.parser')
                            try :
                                if len(run) < 8:
                                    dic['empresa'] = tododato[1].strip()
                                else:
                                    dic['empresa'] = sopa_rut.find(filtro_rut).find_all('font')[1].string.strip()
                            except AttributeError:
                                if tododato[1] == None:
                                    tododato[1] = ''                            
                                dic['empresa'] = tododato[1].strip()
                            except KeyError:
                                if tododato[1] == None:
                                    tododato[1] = ''
                                dic['empresa'] = tododato[1].strip()
                        else:
                            if tododato[interprete[0]] == None:
                                tododato[interprete[0]] = ''
                            dic['empresa'] = tododato[interprete[0]].strip()
                        if dic["empresa"] == '':
                            pass
                            #9306 es laclave
                            #2,2,3,12,4,9,8, 14, 6,0
                        if cartera == 'nac' or cartera == 'met_part':
                            try:
                                run = ''.join(tododato[interprete[1]].split())
                            except AttributeError:
                                run = '0'
                            
                            if len(run) < 8:
                                dic['codigo'] = run
                            else:
                                dic['codigo'] = run + '-' + verificar(run)
                            if dic['codigo'] == '':
                                continue

                        if cartera == 'ext':
                            if tododato[interprete[1]] == None:
                                tododato[interprete[1]] = 'NA'
                            dic['codigo'] = tododato[interprete[1]]
                            if dic['codigo'] == ' ':
                                continue

                        #print(tododato)
                        dic['pais'] = tododato[interprete[2]]
                        dic['moneda'] = tododato[interprete[3]]
                        dic['instrumento'] = tododato[interprete[4]]
                        # para descifrar significado del tipo de instrumento, ver www.svs.cl/sitio/seil/certificacion_inst.php
                        if cartera == 'met_part':
                            if format_num(tododato[interprete[6]]) == format_num(tododato[interprete[5]]):
                                dic['valolizacion'] = 1
                            else:
                                dic['valolizacion'] = 3
                        else:
                            dic['valolizacion'] = tododato[interprete[5]]
                        dic['monto'] = format_num(tododato[interprete[6]])
                        dic['porcentaje'] = format_num(tododato[interprete[7]])
                        dic['unidades'] = format_num(tododato[interprete[8]])
                        dic['periodo'] = data['mm'] + '-' + data['aa']
                        if tododato[1] == None:
                            dic['mnemotecnico'] = ''
                        else:
                            dic['mnemotecnico'] = tododato[1].strip().replace(u'\uFFFD', '')
                        if cartera != 'met_part':
                            dic['fecha_vencimiento'] = tododato[interprete[9]]
                        else:
                            dic['fecha_vencimiento'] = None
                        if dic['fecha_vencimiento'] != None and dic['fecha_vencimiento'].strip() != '':
                            dic['fecha_vencimiento'] = format_time(dic['fecha_vencimiento'])
                        else:
                            dic['fecha_vencimiento'] = None
                        if cartera == 'met_part':
                            tipo_cartera = 1
                            #if dic['unidades'] == 0:
                            #    continue
                        else:
                            tipo_cartera = 0

                        try:
                            print(dic)
                        except UnicodeEncodeError:
                            print('---')

                        inv = inversion(fondo= f, periodo= dic['periodo'], fecha_vencimiento = dic['fecha_vencimiento'], monto = dic['monto'], cartera=tipo_cartera)
                        inv.save()
                        escribir_log(str(f)+' : '+str(dic['periodo']+' : '+str(dic['mnemotecnico'])), "get_inversiones general")
                        
                        
                        inv.nemotecnico= dic['mnemotecnico']
                        inv.empresa = dic['empresa']
                        inv.codigo = dic['codigo']
                        inv.pais = dic['pais']
                        inv.moneda = dic['moneda']
                        inv.instrumento = dic['instrumento']
                        inv.valolizacion = dic['valolizacion']
                        #inv.monto = dic['monto']
                        inv.porcentaje = dic['porcentaje']
                        inv.unidades = dic['unidades']
                        inv.fecha_vencimiento= dic['fecha_vencimiento']
                        inv.save()
                        #else:
                        #    print(dic)
                        print(inv)
                        carts[tododato[1]] = dic

    escribir_log("get_inversiones -- terminado", "get_inversiones")
'''
def get_series():
    fondos = fondo.objects.filter(fecha_resolucion__lt = datetime.datetime.now(), inicio_operaciones__isnull=False, vigente = True)
    #fondos = fondo.objects.filter(pk = '7184-6')
    
    for f in fondos:
        url = f.urlsvs
        url = url[:-1] + '14'
        url = url.replace('auth=&send=&', '')
        colador = SoupStrainer("tr")
        sopa = BeautifulSoup(requests.get(url).text, 'html.parser', parse_only=colador)
        iterable = sopa.find_all("tr")
        first = True
        llaves = []
        dics = []
        for caldo in iterable:
            if first:
                llaves = [str(tag.string).strip().lower() for tag in caldo.contents]
                first = False
                continue
            else:
                valores = [str(tag.string).strip().upper() for tag in caldo.contents]
                dic = {x: y for x, y in zip(llaves, valores)}
                dics.append(dic)

                if dic['fecha inicio'] != '':
                    dic['fecha inicio'] = format_time(dic['fecha inicio'])
                else:
                    dic['fecha inicio'] = None

                try:
                    s = serie.objects.get(nombre = dic['serie'], fecha_inicio= dic['fecha inicio'], fondo = f)
                except serie.DoesNotExist:
                    s = serie(nombre = dic['serie'], fecha_inicio= dic['fecha inicio'], fondo = f)
                    s.save()

                s.caracteristica = dic['característica']
                if dic['continuadora de serie'] != '' and dic['continuadora de serie'] != 'NO':
                    if dic['continuadora de serie'] == 'SI':
                        s.continuadora_id = serie.objects.latest('nombre')
                    else:
                        s.continuadora_id = serie.objects.get(nombre = dic['continuadora de serie'], fondo= f)
                if dic['fecha término'] != '':
                    s.fecha_termino = format_time(dic['fecha término'])
                if dic['valor inicial cuota'] != '':
                    s.valor_inicial = format_num(dic['valor inicial cuota'])

                s.save()

    escribir_log("get_series -- terminado")

    
        #return dics
    #return render(request, "scraping.html",  {'tabla' : dics})
def get_cuotas():

    fondos = fondo.objects.filter(fecha_resolucion__lt = datetime.datetime.now(), inicio_operaciones__isnull=False, vigente = True)
    #fondos = fondo.objects.filter(pk = '7001-7')
    for f in fondos:
        url = f.urlsvs
        url = url[:-1] + '7'
        url = url.replace('auth=&send=&', '')
        
        fecha_ini = f.inicio_operaciones
        fecha_fin = f.termino_operaciones

        if not fecha_ini:
            fecha_ini = datetime.strptime('01/01/1990', '%d/%m/%Y')
        if not fecha_fin or not isinstance(fecha_fin, (date, datetime)):
            fecha_fin = datetime.datetime.today()
        
        
        dics = []
        dic = {'dia1' : revisar(fecha_ini.day), 'dia2' : revisar(fecha_fin.day), 'mes1' : revisar(fecha_ini.month),
               'mes2' : revisar(fecha_fin.month), 'anio1' : fecha_ini.year, 'anio2' : fecha_fin.year,
               'enviado' : '1', 'sub_consulta_fi' : 'Consultar'}

        colador = SoupStrainer("tr")
        pedido = requests.post(url, data=dic)
        sopa = BeautifulSoup(pedido.text, 'html.parser', parse_only=colador)
        iterable = sopa.find_all("tr")
        first = True
        llaves = []
        for caldo in iterable:
            if first:
                llaves = []
                for tag in caldo.contents:
                    llave = str(tag.string)
                    llaves.append(limpiar(llave))
                first = False
                continue
            else:
                valores = [str(tag.string).strip().upper() for tag in caldo.contents]
                dic = {x : y for x , y in zip(llaves, valores)}
                string = json.dumps(dic, indent=2)
                if "SIN INFORMACIÓN" in str(dic) or "SIN INFORMACION" in str(dic):
                    break
            
                try:
                    #escribir_log(str(dic)+str(f.runsvs), "get_cuotas")
                
                    s = serie.objects.get(nombre = dic['serie'], fondo = f)
                
                    try:
                        c = cuota.objects.get(serie = s, fecha =format_time(dic['fecha']))
                    except cuota.DoesNotExist:
                        c = cuota(serie = s, fecha =format_time(dic['fecha']))
                        c.save()

                    c.moneda = dic['moneda']
                    c.valor_libro = format_num(dic['valor libro'])
                    c.valor_economico = format_num(dic['valor economico'])
                    c.patrimonio_neto = int(format_num(dic['patrimonio neto']))
                    c.activo_total = format_num(dic['activo total'])
                    c.agencia = dic['agencia']
                    c.n_aportantes_inst = format_num(dic['no aportantes institucionales'])
                    c.n_aportantes = format_num(dic['no de aportantes'])
                    c.agencia = dic['agencia']
                    c.save()

                except serie.DoesNotExist:
                    break

                #dics.append(dic)
    escribir_log("get_cuotas -- terminado")
    #return dics
    #return render(request, "scraping.html",  {'tabla' : dics})
'''
#scrapping movimientos llama a scrapemovimientos
def get_movimientos(periodos=None):

    fondos = fondo.objects.filter( vigente = True).order_by('runsvs')
    nro_fondo = "9413"
    fondos = fondo.objects.filter(pk__gte = nro_fondo)
    
    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=29")

        if periodos == None:
            print(get_periodos(f))
            periods = get_periodos(f)
        else:
            periods = []
            periods.append(periodos)

        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            print(data)
            scrapemovimientos(url, data, f)
    escribir_log("get_movimientos -- terminado", "get_movimientos")

#scraping resumen llama a scraperesumen
def get_resumen(periodos=None):
    fondos = fondo.objects.filter( vigente = True).order_by('runsvs')
    
    fondos = fondos.filter(pk__gte = '9600')
    fondos = fondos.filter(pk__lte = '9700')
    
    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=30")
        if periodos == None:
            periods = get_periodos(f)
        else:
            periods = []
            periods.append(periodos)
        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] , 'tipo': 'ECRI', 'periodo' : str(p[0])+str(p[1]), 'enviado' :1 }
            print (data)
            scraperesumen(url, data, f)
    #return render(request, "scraping.html",  {'tabla' : data})
    escribir_log("get_resumen -- terminado")

#get dividendos
def get_dividendos(periodos=None):
    def filtro_finanzas(tag):
        return tag.name == 'table' and len(tag.find_all('tr')) > 1

    fondos = fondo.objects.filter( vigente=True).order_by('runsvs')
    #nro_fondo = "9247"
    #fondos = fondo.objects.filter(pk__startswith = nro_fondo)
    
    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=29")

        if periodos == None:
            periods = get_periodos(f)
        else:
            periods = []
            periods.append(periodos)

        for p in periods:
            p = p.split("-")
            data = {
                'mm': p[1],
                'aa': p[0],
                'rut': rut[0]
            }
            #escribir_log(str(p), 'get_dividendos')
            #escribir_log(str(f), 'get_dividendos')
            print(data)

            pedido = requests.post(url, data=data)
            sopa = BeautifulSoup(pedido.text, 'html5lib')
            iter_finanzas = sopa.find_all(filtro_finanzas)
            dividendos = False
            if iter_finanzas is None:
                return
            for tabla in iter_finanzas:
                row = tabla.find('tr').find_all('th')
                try:
                    if len(row) > 1 and  row[0].string and row[1].string and \
                     'ESTADO DE CAMBIOS EN EL PATRIMONIO NETO' in row[0].string and \
                     data['aa'] in row[1].string:
                        filas = tabla.find_all('tr')
                        
                        contador = 0
                        flag = False

                        for fila in filas:
                            columnas = fila.find_all('td')
      
                            for columna in columnas:
                                bytetd = str(columna.string).encode('ascii','ignore')
                                if flag == True:
                                    if bytetd != b'None' and b'Notas adjuntas' not in bytetd :
                                        dividendos = int(bytetd.replace(b'.',b''))
                                    else:
                                        break;
                                if columna.string != None and b'Repartos de dividendos' in bytetd:
                                    flag = True


                except AttributeError:
                    continue
            try:
                perf = periodo.objects.get(fondo=f, periodo=str(data['mm'])+'-'+str(data['aa']))
                dic = json.loads(perf.datos)

                
                dic['REPARTOS DE DIVIDENDOS'] = dividendos
                perf.datos = json.dumps(dic)
                print(dividendos)
                perf.save()
                escribir_log(str(dic), 'get_dividendos')
                
            except periodo.DoesNotExist:
                pass
                
    escribir_log("get_dividendos -- terminado", 'get_dividendos')

def scrapemovimientos(url, data, fondo):
    def iniciales(frase):
        aux = frase.split()
        ret = ''
        for ini in aux:
            ret += ini[0]
        return ret.upper()

    def filtro_finanzas(tag):
        return tag.name == 'table' and len(tag.find_all('tr')) > 1

    def filtro_restos(tag):
        return tag.name == 'tr' and not tag.find_parent('table')

    def traducir(abbr):
        if abbr == '$$':
            return 'pesos'
        elif abbr == 'PROM':
            return 'dólares'
        elif abbr == 'EUR':
            return 'euro'
        else:
            return abbr

    pedido = requests.post(url, data=data)
    sopa = BeautifulSoup(pedido.text, 'html.parser')
    activo = None
    et_comun = ''
    movs = {}
    moneda = ''
    antiguo = True
    iter_finanzas = sopa.find_all(filtro_finanzas)
    if iter_finanzas is None:
        return
    for tabla in iter_finanzas:
        row = tabla.find('tr')
        try:
            if row.find('th').string and 'ESTADO DE SITUACION FINANCIERA' in row.find('th').string :
                iter_finanzas = tabla
                antiguo = False
                break
        except AttributeError:
            continue
    if antiguo:  # periodo anterior al 2010
        if len(iter_finanzas) <= 1:  # sin informacion
            return
        iter_finanzas = iter_finanzas[1]
        moneda = sopa.find('table').find_all('tr')[3].find('td').string  # '$$' ??
        moneda = 'miles de ' + traducir(moneda)
        iter_finanzas = iter_finanzas.find_all('tr')
        for fila in iter_finanzas:
            columnas = fila.find_all('td')
            if len(columnas) > 3 :
                if 'TOTAL' in columnas[1].string or 'PATRIMONIO' in columnas[1].string:
                    dic = {'fondo': data['rut'],
                           'periodo': data['mm'] + '-' + data['aa'],
                           'moneda': moneda,
                           'etiqueta_svs' : columnas[1].string.strip(),
                           'ap': True if columnas[1].string.strip() == 'TOTAL ACTIVOS' else False,
                           'monto': columnas[2].string.strip()}
                    if dic['etiqueta_svs'][-1] == 'S':
                        dic['etiqueta_svs'] = dic['etiqueta_svs'][:-1]
                    movs[dic['etiqueta_svs']] = dic

                    try:
                        nuevo_movimiento = movimiento.objects.get(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'])
                    except movimiento.DoesNotExist:
                        nuevo_movimiento = movimiento(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'], monto=0)
                        nuevo_movimiento.save()

                    nuevo_movimiento.ap = dic['ap']
                    nuevo_movimiento.moneda = dic['moneda']
                    nuevo_movimiento.monto = dic['monto'].replace('.','')
                    nuevo_movimiento.save()
                    
                    escribir_log( str(fondo)+' '+dic['periodo'], 'get_movimientos' )

        # Como me suele pasar, el scraper me está recolectando una sopa donde la mitad de los <tr> están fuera de las
        # tablas, por lo que necesito escarbar entre los restos

        iter_finanzas = sopa.find_all(filtro_restos)
        for fila in iter_finanzas:
            columnas = fila.find_all('td')
            if len(columnas) > 3:
                if 'TOTAL' in columnas[1].string or 'PATRIMONIO' in columnas[1].string:
                    dic = {'fondo': data['rut'],
                           'periodo': data['mm'] + '-' + data['aa'],
                           'moneda': moneda,
                           'etiqueta_svs': columnas[1].string.strip(),
                           'ap': True if columnas[1].string.strip() == 'TOTAL ACTIVOS' else False,
                           'monto': columnas[2].string.strip()}

                    if dic['etiqueta_svs'][-1] == 'S':
                        dic['etiqueta_svs'] = dic['etiqueta_svs'][:-1]
                    movs[dic['etiqueta_svs']] = dic
                    try:
                        nuevo_movimiento = movimiento.objects.get(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'])
                    except movimiento.DoesNotExist:
                        nuevo_movimiento = movimiento(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'], monto=0)
                        nuevo_movimiento.save()

                    nuevo_movimiento.ap = dic['ap']
                    nuevo_movimiento.moneda = dic['moneda']
                    nuevo_movimiento.monto = dic['monto'].replace('.','')
                    nuevo_movimiento.save()
                    escribir_log( str(fondo)+' '+dic['periodo'], 'get_movimientos' )
                    
        return movs

    iter_finanzas = iter_finanzas.find_all('tr')
    for fila in iter_finanzas:
        dic = {}

        columnas = fila.find_all('td')
        if len(columnas) > 0:
            if len(columnas) == 1:
                # determinar activo o pasivo
                n_fila = fila.next_sibling if isinstance(fila.next_sibling, element.Tag) \
                    else fila.next_sibling.next_sibling
                if columnas[0].find('strong') is not None and columnas[0].find('strong').string is not None:
                    if n_fila is not None and len(n_fila.find_all('td')) == 1:
                        if columnas[0].find('strong').string == 'ACTIVO':
                            activo = True
                        elif columnas[0].find('strong').string == 'PASIVO':
                            activo = False
                    else:
                        # rotula si es corriente o no corriente
                        et_comun = iniciales(columnas[0].find('strong').string)
                        et_comun += '-'
            elif len(columnas) > 1:
                # raspa nombre de movimiento e información
                for columna in columnas:
                    if columna is not None:
                        if 'fondoOscuro' in columna['class']:
                            # Totales de composición financiera
                            if 'nivel4' in columna['class']:
                                dic['etiqueta_svs'] = limpiar(columna.strong.string).upper()
                            else:
                                if len(columna.find_previous_siblings('td')) == 3:
                                    dic['monto'] = columna.strong.string.strip()
                        else:
                            # Movimientos segun SVS
                            if 'nivel6' in columna['class']:
                                dic['etiqueta_svs'] = et_comun + limpiar(columna.string)
                            elif len(columna.find_previous_siblings('td')) == 3:
                                dic['monto'] = columna.string.strip()

        else:  # este corre primero en las iteraciones, pero por la estructura de los condicionales viene al último
            monedas = fila.find_all('th')
            if monedas is not None:
                for dinero in monedas:
                    if dinero is not None:
                        currency = re.search('(?<=ESTADO DE SITUACION FINANCIERA  \("Expresado en ).+(?="\))',
                                            dinero.string)
                        if currency is not None and moneda == '':
                            moneda = currency.group(0).strip()
            continue
        if moneda == '':
            dic['moneda'] = '¿Pesos?'
        else :
            dic['moneda'] = moneda

        dic['fondo'] = data['rut']
        dic['ap'] = activo
        dic['periodo'] = data['mm'] + '-' + data['aa']


        try :
            movs[dic['etiqueta_svs']] = dic
            try:
                nuevo_movimiento = movimiento.objects.get(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'])
            except movimiento.DoesNotExist:
                nuevo_movimiento = movimiento(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'], monto=0, ap=False)
                nuevo_movimiento.save()

            nuevo_movimiento.ap = dic['ap']
            nuevo_movimiento.moneda = dic['moneda']
            nuevo_movimiento.monto = dic['monto'].replace('.','')
            nuevo_movimiento.save() 
            
            print(str(nuevo_movimiento.fondo.nombre.encode('ascii','ignore'))+" -- "+nuevo_movimiento.periodo+" -- "+nuevo_movimiento.etiqueta_svs+" -- "+nuevo_movimiento.monto+" -- "+nuevo_movimiento.moneda)
        except KeyError:
            continue

        escribir_log(str(dic), 'get_movimientos')
   
    return movs

def scraperesumen(url, data, fondo):
    def filtro_totales(tag):
        return tag.name == 'strong' and tag.parent.name == 'td'  # basta con name==strong pero mejor mirar

    pedido = requests.post(url, data=data)
    caldo = BeautifulSoup(pedido.text, 'html.parser', parse_only=SoupStrainer('table'))

    chequeo = caldo.find_all(filtro_totales)
    if chequeo is None or chequeo[3].string.strip() == '0' or chequeo[3].string.strip() == '-':
        return None  # Cartera vacía
    dic = {
        'decripcion': chequeo[0].string.strip(),
        'nacional': chequeo[1].string.strip(),
        'extranjero': chequeo[2].string.strip(),
        'porcentual': chequeo[4].string.strip()
    }
    if dic['nacional'] == '0' and dic['extranjero'] == '0' or dic['porcentual'] == '0':
        #print("error de traspaso??")
        return None  # Cartera vacía o error de traspaso... probablemente... ;;;;;;;
    iterable = caldo.find_all('tr')
    objetos = [dic]
    for row in iterable:
        dic = {}
        if len(row.find_all('td')) < 5:
            continue
        if row.find_all(filtro_totales):
            continue
        datos = [tag.string for tag in row.find_all('td')]
        if len(datos) < 5:
            continue

        dic['decripcion'] = datos[0].strip()
        dic['nacional'] = datos[1].strip().replace('.', '')
        dic['extranjero'] = datos[2].strip().replace('.', '')
        dic['porcentual'] = datos[4].strip().replace(',','.')
        objetos.append(dic)
        
        print(str(dic).encode('ascii','ignore'))

        try:
            period = periodo.objects.get(periodo=data['mm']+'-'+data['aa'], fondo_id= fondo)
            try:
                res = resumen.objects.get(descripcion=dic['decripcion'], periodo=period)
            except resumen.DoesNotExist:
                res = resumen(descripcion=dic['decripcion'], periodo=period)
                res.save()

            if dic['nacional'] == '-':
                res.nacional = 0
            else:
                res.nacional = dic['nacional']
            if dic['extranjero'] == '-':
                res.extranjero = 0
            else:
                res.extranjero = dic['extranjero']
            if dic['porcentual'] == '-':
                res.porcentual = 0
            else:
                res.porcentual = dic['porcentual']
            res.save()
            
        except periodo.DoesNotExist:
            pass



    return {'resumen' : objetos, 'periodo' : data['mm'] + '-' + data['aa']}

def get_periodos_faltantes():
    fondos = fondo.objects.filter(vigente = True).order_by('runsvs')
    #nro_fondo = "7219"
    #fondos = fondo.objects.filter(pk__startswith = nro_fondo)
    for f in fondos:
        periods = get_periodos(f)
        
        for p in periods:
            p = p.split("-")
            if p[0] == '2018':
                continue
            if p[0] > '2005':
                data = {
                    'mm': p[1],
                    'aa': p[0],
                    'rut': f.runsvs
                }
                if not periodo.objects.filter(fondo=f, periodo=str(data['mm'])+'-'+str(data['aa'])).exists():
                    print(data)        

'''
====================================
SCRAPING DE LA BOLSA DE COMERCIO
====================================
'''
def get_lista_fondo_inversion():
    
    #import urllib.request, json 
    i = 1
    u = 'http://www.bolsadesantiago.com/mercado/Paginas/cuotasfondosinversion.aspx?RequestAjax=1&hdnPag='+str(i)
    
    r = requests.get(u)
    #escribir_log(str(), 'get_lista_fondo_inversion')
    result = r.json()

    tot = int(result['TotalPaginas'])
    print("TOTAL PAGINAS:"+str(tot))
    for i in range(1, tot+1):
        u = 'http://www.bolsadesantiago.com/mercado/Paginas/cuotasfondosinversion.aspx?RequestAjax=1&hdnPag='+str(i)
          
        r = requests.get(u)
        result = r.json()    
        for finv in result['cuotasFondosInversion']:
            try:
                niv = nemo_inversion.objects.get(nemotecnico=finv['Nemo'].strip())
            except nemo_inversion.DoesNotExist:
                niv = nemo_inversion(nemotecnico=finv['Nemo'].strip())
                niv.save()
            try:
                fi = fondoInversion.objects.filter(cfi = niv).first()
            except fondoInversion.DoesNotExist:
                fi = fondoInversion(cfi = niv)
                fi.save()
            j = 1
            
            if fi == None:
                print("fONDO NULO:"+str(niv))
            else:
                fi.url = 'http://www.bolsadesantiago.com/_layouts/BCS_SP_RI/json/dividendosJson.aspx?RequestAjaxDiv=1&Nemo='+str(fi.cfi)+'&hdnPag='+str(j)

                fi.razon_social = finv['RazonSocial'].strip()
                fi.ultimo = finv['PrecioCierre']
                fi.var = finv['Variacion']
                fi.monto = finv['Monto']
                fi.volumen = finv['UnidadesTransadas']
                fi.moneda = finv['ValorCuota']

                fi.precio_compra = finv['PrecioCompra']
                fi.precio_Venta = finv['PrecioVenta']
                tm = finv['MonedaVCuota'].strip()
                if tm == 'US$':
                    fi.tipo_moneda = divisa.objects.get(pk='USD')
                elif tm == 'EUR':
                    fi.tipo_moneda = divisa.objects.get(pk='EUR')
                else:
                    fi.tipo_moneda = divisa.objects.get(pk='CLP')
                fi.save()

                

                r2 = requests.get(fi.url)
                result2 = r2.json()  
                tot2 = int(result2['TotalPaginas'])
                for j in range (1, tot2+1):

                    u2 = 'http://www.bolsadesantiago.com/_layouts/BCS_SP_RI/json/dividendosJson.aspx?RequestAjaxDiv=1&Nemo='+str(fi.cfi)+'&hdnPag='+str(j)

                    r2 = requests.get(u2)
                    result2 = r2.json()
                    for evento in result2['ListaDividendos']:
                        descr = evento['Descripcion'].strip()
                        try:
                            ev = eventoInversion.objects.get(fondo_inversion=niv, descripcion=descr)
                            pass
                        except eventoInversion.DoesNotExist:
                            ev = eventoInversion(fondo_inversion=niv, descripcion=descr)
                            ev.save()
                        if evento['FechaLimiteString']:
                            ev.fecha_limite = voltear_fecha(evento['FechaLimiteString'])
                        if evento['FechaPagoString']:
                            ev.fecha_pago = voltear_fecha(evento['FechaPagoString'])
                        ev.valor = evento['Valor']
                        ev.tipo = evento['Tipo']
                        if "DIVIDENDO" in descr and evento['Tipo'] == None:
                            ev.tipo = "DV"
                        #divisa
                        if " US$ " in descr:
                            ev.divisa = divisa.objects.get(pk='USD')
                            if evento['Tipo'] == "DV":
                                try:
                                    val = float(str(descr.split(' US$ ')[1]).replace(',','.'))
                                    print(val)
                                    if val != evento['Valor']:
                                        ev.valor = val
                                        ev.save()
                                    #print(val, evento['Valor'], val == evento['Valor'])
                                except ValueError:
                                    pass


                        elif ' $ ' in descr:
                            ev.divisa = divisa.objects.get(pk='CLP')
                            if evento['Tipo'] == "DV":
                                try:
                                    val = float(str(descr.split(' $ ')[1]).replace(',','.'))
                                    if val != evento['Valor']:
                                        ev.valor = val
                                        ev.save()
                                except ValueError:
                                    pass

                        elif 'EUR' in descr:
                            ev.divisa = divisa.objects.get(pk='EUR')
                            if evento['Tipo'] == "DV":
                                try:
                                    val = float(str(descr.split(' EURO ')[1]).replace(',','.'))
                                    if val != evento['Valor']:
                                        ev.valor = val
                                        ev.save()
                                except ValueError:
                                    pass           
                        else:
                            ev.divisa = divisa.objects.get(pk='CLP')
                        

                    ev.save()
            #print(str(fi.cfi))
        print(i)
