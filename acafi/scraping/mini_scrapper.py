import requests, re, unicodedata, datetime, urllib3, json, decimal
from datetime import timedelta

from django.shortcuts import render
from bs4 import BeautifulSoup, element, SoupStrainer
from acafi.models import fondo_ext, fondo, administrador, periodo, composicion, aportante, inversion, movimiento, resumen, rent, resultado, serie, cuota, indicador, fondoInversion, detalle_fondo, instrumento, pais, divisa, eventoInversion
from acafi.deploy import escribir_log, escribir_log_puro, convertir, revisar
from django.db.models import Min
from django.db.utils import IntegrityError
from django.db.models import Q
#from acafi.scraping.listar_empresas import get_periodos




url_base ='http://www.cmfchile.cl/institucional/mercados/'
#fondos de inversion rescatabales
url_fir ='http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=TO&entidad=FIRES'
#fondos de inversion no rescatabales
url_finr ='http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=TO&entidad=FINRE'
#fondos mutuos
#url_fmu = 'http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=TO&entidad=RGFMU'
#fondos para la vivvienda
#url_fpv = 'http://www.cmfchile.cl/institucional/mercados/consulta.php?mercado=V&Estado=TO&entidad=RGFVI'

def extraer_periodo(fecha):
    anio = int(fecha.year)
    mes = int(fecha.month)
    #print(mes)
    if mes < 3 or mes == 12:
        return str(anio-1)+"-12"
    if mes >= 3 and mes < 6:
        return str(anio)+"-03"
    if mes >= 6 and mes < 9:
        return str(anio)+"-06"
    if mes >= 9 and mes < 12:
        return str(anio)+"-09"

def limpiar_num(num):
    num = num.replace('.','')
    num = num.replace(',','.')
    return float(num)


urls = [url_fir, url_finr]
def get_periodo_actual():
    fecha = datetime.datetime.now()
    anio = int(fecha.year)
    mes = int(fecha.month)
    #print(mes)
    if mes < 3 or mes == 12:
        return str(anio-1)+"-12"
    if mes >= 3 and mes < 6:
        return str(anio)+"-03"
    if mes >= 6 and mes < 9:
        return str(anio)+"-06"
    if mes >= 9 and mes < 12:
        return str(anio)+"-09"
def limpiar(llave):
    # deja en minúscula, elimina espacios extra, y elimina símbolos que no permitan usar la llave
    llave = llave.strip().lower().replace(' ', '_')
    llave = unicodedata.normalize('NFKD', llave)
    llave = re.sub(r'\W+', '', llave)
    llave = llave.replace('_', ' ')
    return llave.strip()
def verificar(RUT) :
    if not re.match('\d{4,8}(-[\dkK])?', RUT) :
        return RUT
    lista = str(RUT)[::-1]
    secuencia = [2, 3, 4, 5, 6, 7]
    ver = 0
    for i in range(len(lista)) :
        ver += secuencia[i % 6] * int(lista[i])
    ver = 11 - (ver % 11)
    return str(ver) if ver < 10 else ('K' if ver == 10 else '0')
def format_time(var):
    #print(var)
    aux = var.split('/')
    return aux[2]+'-'+aux[1]+'-'+aux[0]
def filtro_rut(tag):
    return tag.name == 'table' and tag['border'] == "1"
def format_num(strnum): #formato español a ingles
    try:
        if '.' in str(strnum):
            strnum = str(strnum).replace('.','')
        if ',' in strnum:
            strnum = str(strnum).replace(',','.')
        #if strnum.count(".") > 1:
        #    strnum = strnum.replace('.','')
            
        return float(strnum)
    except ValueError:
        return 0


def get_indice(c):
    if c.cuota_anterior() == None or c.cuota_anterior().indice == None:
        if cuota.objects.filter(serie = c.serie, fecha__lt=c.fecha).exists():
            aux = cuota.objects.filter(serie = c.serie, fecha__lt=c.fecha).order_by('fecha').last()
            return aux.indice
        else:
            return 100
    else:

        indice_ant = float(c.cuota_anterior().indice)
        return indice_ant*(1+float(c.rentab()))

#saca los datos de fondos rescatables y no rescatables
def get_lista(): 
    escribir_log("get_lista -- inicio")

    for u in urls:
        page = requests.get(u) 
        soup = BeautifulSoup(page.content, 'html.parser')
        tabla =soup.find_all('tr')
        tabla.pop(0)

        #primero scraping
        res = []
        links = []
        for row in tabla:
            cols = row.find_all('td')
            for link in cols[0].find_all('a'):
                links.append(link['href'])

            cols = [ele.text.strip() for ele in cols]
            cols.append(link['href'])
            res.append([ele for ele in cols if ele])

        #guardar en bdd
        for r in res:
            if r[2] == 'VI':
                r[2] = True
            else:
                r[2] = False
            

            try:
            #if fondo.objects.filter(pk = r[0]).exists:
                adm = fondo.objects.get(runsvs= r[0], nombre= r[1], vigente= r[2], urlsvs= url_base+r[3]+'&control=svs&pestania=1')
            except fondo.DoesNotExist:
                adm = fondo(runsvs= r[0], nombre= r[1], vigente= r[2], urlsvs= url_base+r[3]+'&control=svs&pestania=1')
                adm.save()

            try:
                fondoext = fondo_ext.objects.get(runsvs= r[0])
            except fondo_ext.DoesNotExist:
                fondoext = fondo_ext(runsvs= r[0], bolsa_offshore='', portafolio_manager='')
                fondoext.save()

            get_detalle_fondo(url_base+r[3]+'&control=svs&pestania=1')

            #escribir_log(str(adm), 'get_lista')
    escribir_log("get_lista -- terminado")
    #return render(request, "scraping.html",  {'tabla' : salida})
def get_detalle_fondo(url): #scraping de pestaña 1, fondo y administrador

    #url = 'http://www.cmfchile.cl/institucional/mercados/entidad.php?mercado=V&rut=9281&grupo=&tipoentidad=FIRES&row=AAAw%20cAAhAABP4PAAu&vig=VI&control=svs&pestania=1'
    
    page = requests.get(url) 
    sopita = BeautifulSoup(page.content, 'html.parser')
    
    izq = sopita.find_all("th")
    der = sopita.find_all("td")
    dic = {x.string.strip(): y.text.strip() for x, y in zip(izq, der)}

    rut_raw = dic['R.U.T. Administradora'].split('-')

    urlsvsadm = 'http://www.cmfchile.cl/institucional/mercados/entidad.php?auth=&send=&mercado=V&rut='+rut_raw[0]+'&grupo=&tipoentidad=RACRT&vig=VI&row=AAAwy2ACTAAABzvAAM&control=svs&pestania=1'
    a = administrador(rut = dic['R.U.T. Administradora'], razon_social = dic['Razón Social Administradora'], urlsvs= urlsvsadm)
    a.save()

    f = fondo.objects.get(pk = dic['R.U.N. del Fondo'])
    f.admin = a
    if dic['Tipo de Fondo de Inversión:'] == 'Rescatable':
        f.tipo_inversion = 1
    else:
        f.tipo_inversion = 0
    f.fecha_resolucion = format_time(dic['Fecha Resolución de aprobación del reglamento interno del fondo'])
    
    if dic['Número Resolución']:
        f.resolucion = dic['Número Resolución']
    
    if dic['Fecha Inicio Operaciones']:
        f.inicio_operaciones = format_time(dic['Fecha Inicio Operaciones'])
    if dic['Fecha Término Operaciones']:
        f.termino_operaciones = format_time(dic['Fecha Término Operaciones'])
    f.save()

    flag = 0
    page = requests.get(urlsvsadm) 
    sopa_adm = BeautifulSoup(page.content, 'html.parser')
    if sopa_adm.find("h4") is not None:
        flag = 1
        urlsvsadm = urlsvsadm.replace('RACRT', 'RGAFI')
        a.urlsvs = urlsvsadm
        a.save()
        page = requests.get(urlsvsadm) 
        sopa_adm = BeautifulSoup(page.content, 'html.parser')

    izq = sopa_adm.find_all("th")
    der = sopa_adm.find_all("td")
    dic = {x.string.strip(): y.text.strip() for x, y in zip(izq, der)}

    if dic['Nombre de Fantasía']:
        a.nombre_fantasia = dic['Nombre de Fantasía']

    if dic['Vigencia'] == 'Vigente':
        a.vigente = True
    else:
        a.vigente = False

    if flag == 0:
        if dic['Número de Inscripción']:
            a.inscripcion = dic['Número de Inscripción']

        if dic['Fecha de Inscripción']:
            a.fecha_inscripcion = format_time(dic['Fecha de Inscripción'])

        if dic['Fecha de Cancelación']:
            a.fecha_cancelacion = format_time(dic['Fecha de Cancelación'])

        if dic['e-mail de contacto']:
            a.email = dic['e-mail de contacto']

        if dic['Sitio web']:
            a.website = dic['Sitio web']

    if dic['Teléfono']:
        a.telefono = dic['Teléfono']

    if dic['Domicilio']:
        a.direccion = dic['Domicilio']

    if dic['Ciudad']:
        a.ciudad = dic['Ciudad']

    a.save()
    #escribir_log(str(a.rut))

#scraping principales aportantes
def get_principales_aportantes(periodos=None, fondos_objetivo=None):
    escribir_log("get_principales_aportantes -- inicio")

    fondos = fondo.objects.all().order_by('runsvs')
    if fondos_objetivo != None:
        #fondos_objetivo = fondos_objetivo.split(',')
        fondos = fondos.filter(runsvs__in= fondos_objetivo)
    #fondos = fondos.filter(runsvs__gte= "7020-0")
    #fondos_objetivo = ['7111-0', '9123-5', '9176-6', '9180-4', '9341-6', '9370-K', '9439-0', '9525-7', '9127-8']
    #fondos = fondos.filter(runsvs__in= fondos_objetivo)
    
    asd = ""
    aportantes =[]
    res = []
    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=27")

        if periodos == None:
            periods = [get_periodo_actual()]
        else:
            periods = []
            aux = periodos.split(',')
            for a in aux:        
                au= a.split('-')
                #objfecha = datetime.strptime(str(au[0])+'-'+str(int(au[1])-2)+str('-01'), "%Y-%m-%d").date()
                if f.inicio_operaciones != None:
                #    if objfecha >= f.inicio_operaciones:
                    periods.append(a)

        for p in periods:

            p = p.split("-")

            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            escribir_log(str(data), 'get_principales_aportantes')
            print(data)
            hdr = {'User-Agent': 'Mozilla/5.0'}
            page = requests.post(url, data=data, headers=hdr)

            #page = requests.post(url, data=data) 
            sopa = BeautifulSoup(page.content, 'html.parser')
            iterable = sopa.find_all("table")
            #asd += str(iterable)
            
            
            aportantes = iterable[0]
            internable = aportantes.find_all('tr')
            internable.pop(0)
            for i in internable:

                modelo =[]
                contador_periodo = 0
                

                for cols in sopa.findAll("td", {"class" : "fondoOscuro"}):
                    cols = cols.parent

                    celdas = cols.findChildren()
                    #nuevo_periodo = periodo(fondo = f, periodo = p[1]+"-"+p[0], valor_obs = , total_aportantes = , datos = ,)
                    #asd += celdas[0].text+' ->'+celdas[1].text
                    modelo.append(celdas[1].text)
                    contador_periodo += 1
                    

                if contador_periodo == 8:
                    contador_periodo = 0
                    
                    if modelo[0].strip() != "":
                        j = {}
                        j['CUOTAS EMITIDAS'] = modelo[1]
                        j['CUOTAS PAGADAS'] = modelo[2]
                        j['CUOTAS SUSCRITAS Y NO PAGADAS'] = modelo[3]
                        j['NUMERO DE CUOTAS CON PROMESA DE SUSCRIPCION Y PAGO']= modelo[4]
                        j['NUMERO DE CONTRATOS DE PROMESAS DE SUSCRIPCION Y PAGO'] = modelo[5]
                        j['NUMERO DE PROMITENTES SUSCRIPTORES DE CUOTAS'] = modelo[6]
                            
                        #print(p[1]+"-"+p[0])
                        #escribir_log(str(f), 'get_principales_aportantes')
                        #escribir_log(p[1]+"-"+p[0], 'get_principales_aportantes')

                        try:
                            nuevo_periodo = periodo.objects.get(fondo = f, periodo = p[1]+"-"+p[0])

                        except periodo.DoesNotExist:
                            total_aportantes = modelo[0]
                            nuevo_periodo = periodo(fondo = f, periodo = p[1]+"-"+p[0], valor_obs=float(modelo[7]), total_aportantes = total_aportantes)
                            nuevo_periodo.save()

                        nuevo_periodo.valor_obs = float(modelo[7])
                        nuevo_periodo.total_aportantes = modelo[0] 
                        nuevo_periodo.datos = json.dumps(j)
                        nuevo_periodo.save()
                        

                    comps = []
                    contador_composicion = 0
                    aps = {}
                    if i.findChild().text != 'No existen datos para desplegar':
                        fila = i.find_all('td')
                        aps['nombre'] = str(fila[1].text).replace(u'\uFFFD', '')
                        aps['tipo_persona'] = str(fila[2].text).replace(u'\uFFFD', '')
                        aps['rut'] = str(fila[3].text).replace(u'\uFFFD', '')
                        try:
                            apo = aportante.objects.get(nombre= aps['nombre'])
                        except aportante.DoesNotExist:
                            apo = aportante(nombre= aps['nombre'])
                            apo.save()

                        apo.tipo_persona= aps['tipo_persona']
                        apo.rut = aps['rut']
                        apo.save()

                        #print (str(apo))

                        comp = {}
                        comp['aps'] = aps
                        comp['porcentaje'] = str(fila[4].text)
                        comps.append(comp)

                        #escribir_log(str(comp), 'get_principales_aportantes')


                        try:
                            c = composicion.objects.get(periodo = nuevo_periodo, aportante = apo, porc_propiedad = comps[contador_composicion]['porcentaje'])
                        except composicion.DoesNotExist:
                            c = composicion(periodo = nuevo_periodo, aportante = apo, porc_propiedad = comps[contador_composicion]['porcentaje'])
                            c.save()


                        contador_composicion += 1
                    
                    
                    modelo = []
    escribir_log("get_principales_aportantes -- terminado")

#scrapping inversiones
def get_inversiones(periodos=None, fondos_objetivo=None):
    escribir_log("get_inversiones -- inicio")

    carteras = ['nac', 'ext', 'met_part']
    carts = {}
    fondos = fondo.objects.all().order_by('runsvs')
    
    if fondos_objetivo != None:
        fondos_objetivo = fondos_objetivo.split(',')
        fondos = fondos.filter(pk__in= fondos_objetivo)
        print(fondos.query)
    #fondos_objetivo = ['7111-0', '9123-5', '9176-6', '9180-4', '9341-6', '9370-K', '9439-0', '9525-7', '9127-8']
    #fondos = fondos.filter(runsvs__in= fondos_objetivo)

    for f in fondos:
        rut = f.runsvs.split("-")
        if periodos == None:
            periods = [get_periodo_actual()]
        else:
            periods = []
            aux = periodos.split(',')
            for a in aux:
                periods.append(a)

        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            print(data)
            escribir_log(str(data), "get_inversiones")
                        
            for cartera in carteras:
                url_cartera = 'http://www.cmfchile.cl/institucional/inc/inf_financiera/ifrs_xml/ifrs_cartera_' + cartera + '.php?rut=' + data['rut'] + '&periodo=' + data['aa'] + data['mm']
                hdr = {'User-Agent': 'Mozilla/5.0'}
                
                pedi2 = requests.post(url_cartera, data=data, headers=hdr)
                caldo = BeautifulSoup(pedi2.text, 'html.parser', parse_only=SoupStrainer('table'))
                
                flag= True

                iter_carteras = caldo.find_all('tr')
                print("cartera: "+cartera)
                print("Inversiones a insertar: "+str(len(iter_carteras)-3)) #todos menos total y headers

                try:
                    if cartera == 'nac':
                        c = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =0, pais='CL').count()
                    if cartera == 'ext':
                        c = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =0).exclude(pais='CL').count()
                    if cartera == 'met_part':
                        c = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =1).count()
                        
                    print("Numero de inversiones: "+str(c))
                    if c == len(iter_carteras)-3 or len(iter_carteras)-3 == 0:
                        flag = False
                    else:
                        flag = True
                except inversion.DoesNotExist:
                    flag = True
                
                if flag == True:
                    borrar_inv = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =0)
                    if cartera == 'nac':
                        borrar_inv = borrar_inv.filter(pais='CL', cartera = 0).delete()
                    if cartera == 'ext':
                        borrar_inv = borrar_inv.filter(cartera=0).exclude(pais='CL').delete()
                    if cartera == 'met_part':
                        borrar_inv = borrar_inv.filter(cartera=1).delete()


                    for row in iter_carteras:
                        dic = {}
                        if len(row.find_all('td')) == 1:
                            # saltarse el padding
                            continue

                        if cartera == 'nac' :
                            interprete = (None, 2, 3, 16, 4, 12, 15, 20, 9, 5, 10, 17)
                        elif cartera == 'ext' :
                            interprete = (3, 1, 4, 17, 5, 13, 16, 21, 10, 6, 11, 18)
                        elif cartera == 'met_part':
                             interprete = (1,2,3,12,4,9,8, 14, 6,0, None, None)
                        else :
                            interprete = None

                        if interprete is None or len(interprete) < 9:
                            continue

                        tododato = [tag.string for tag in row.find_all('td')]
                        
                            
                        if len(tododato) < 9:# or tododato[1] is None:# or tododato[1].strip() in excepto:
                            continue

                        #print( 'largo '+str(len(tododato))+' --> '+str(tododato))
                        

                        if interprete[0] is None:

                            # raspar nombre de la sii
                            url_rut = 'https://zeus.sii.cl/cvc_cgi/nar/nar_consulta'
                            if tododato[interprete[1]] == None:
                                print(tododato)
                                tododato[interprete[1]] =''

                            run = ''.join(tododato[interprete[1]].split())
                            rut_data = {
                                'RUT' : run,
                                'DV' : verificar(run),
                                'ACEPTAR' : 'Consultar'
                            }
                            pedido_rut = requests.post(url_rut, data=rut_data)
                            sopa_rut = BeautifulSoup(pedido_rut.text, 'html.parser')
                            try :
                                if len(run) < 8:
                                    dic['empresa'] = tododato[1].strip()
                                else:
                                    dic['empresa'] = sopa_rut.find(filtro_rut).find_all('font')[1].string.strip()
                            except AttributeError:
                                if tododato[1] == None:
                                    tododato[1] = ''                            
                                dic['empresa'] = tododato[1].strip()
                            except KeyError:
                                if tododato[1] == None:
                                    tododato[1] = ''
                                dic['empresa'] = tododato[1].strip()
                        else:
                            if tododato[interprete[0]] == None:
                                tododato[interprete[0]] = ''
                            dic['empresa'] = tododato[interprete[0]].strip()
                        if dic["empresa"] == '':
                            pass
                            #9306 es laclave
                            #2,2,3,12,4,9,8, 14, 6,0
                        if cartera == 'nac' or cartera == 'met_part':
                            if tododato[interprete[1]] == None:
                                tododato[interprete[1]] = 0
                                dic['codigo'] = ''
                            else:
                                run = ''.join(tododato[interprete[1]].split())
                            
                                if len(run) < 8:
                                    dic['codigo'] = run
                                else:
                                    dic['codigo'] = run + '-' + verificar(run)
                                if dic['codigo'] == '':
                                    continue

                        if cartera == 'ext':
                            if tododato[interprete[1]] == None:
                                tododato[interprete[1]] = 'NA'
                            dic['codigo'] = tododato[interprete[1]]
                            if dic['codigo'] == ' ':
                                continue

                        #print(tododato)
                        dic['pais'] = tododato[interprete[2]]
                        dic['moneda'] = tododato[interprete[3]]
                        dic['instrumento'] = tododato[interprete[4]]
                        # para descifrar significado del tipo de instrumento, ver www.svs.cl/sitio/seil/certificacion_inst.php
                        if cartera == 'met_part':
                            if format_num(tododato[interprete[6]]) == format_num(tododato[interprete[5]]):
                                dic['valolizacion'] = 1
                            else:
                                dic['valolizacion'] = 3
                        else:
                            dic['valolizacion'] = tododato[interprete[5]]
                        dic['monto'] = format_num(tododato[interprete[6]])
                        dic['porcentaje'] = format_num(tododato[interprete[7]])
                        dic['unidades'] = format_num(tododato[interprete[8]])
                        dic['periodo'] = data['mm'] + '-' + data['aa']
                        if tododato[1] == None:
                            dic['mnemotecnico'] = ''
                        else:
                            dic['mnemotecnico'] = tododato[1].strip().replace(u'\uFFFD', '')
                        if cartera != 'met_part':
                            dic['fecha_vencimiento'] = tododato[interprete[9]]
                        else:
                            dic['fecha_vencimiento'] = None
                        if dic['fecha_vencimiento'] != None and dic['fecha_vencimiento'].strip() != '':
                            dic['fecha_vencimiento'] = format_time(dic['fecha_vencimiento'])
                        else:
                            dic['fecha_vencimiento'] = None
                        if cartera == 'met_part':
                            tipo_cartera = 1
                            dic['tipo_unidades'] = None
                            dic['cod_pais_transaccion'] = None

                            if dic['unidades'] == 0:
                                continue
                        else:
                            tipo_cartera = 0
                            dic['tipo_unidades'] = tododato[interprete[10]]
                            dic['cod_pais_transaccion'] = tododato[interprete[11]]

                        try:
                            print(dic['monto'])
                        except UnicodeEncodeError:
                            print('---')

                        inv = inversion(fondo= f, periodo= dic['periodo'], fecha_vencimiento = dic['fecha_vencimiento'], monto = dic['monto'], cartera=tipo_cartera)
                        inv.save()
                        escribir_log(str(f)+' : '+str(dic['periodo']+' : '+str(dic['mnemotecnico'])), "get_inversiones general")
                        try:
                            print(dic)
                        except UnicodeEncodeError:
                            print('---')
                        inv.nemotecnico= dic['mnemotecnico']
                        inv.empresa = dic['empresa']
                        inv.codigo = dic['codigo']
                        inv.pais = pais.objects.get(pk=dic['pais'])
                        inv.moneda = divisa.objects.get(alias_svs=dic['moneda'])
                        inv.instrumento = dic['instrumento']
                        inv.valolizacion = dic['valolizacion']
                        #inv.monto = dic['monto']
                        inv.porcentaje = dic['porcentaje']
                        inv.unidades = dic['unidades']
                        inv.fecha_vencimiento= dic['fecha_vencimiento']

                        inv.tipo_unidades =  dic['tipo_unidades']
                        if dic['cod_pais_transaccion'] != None:
                           inv.cod_pais_transaccion = pais.objects.get(pk=dic['cod_pais_transaccion'])

                        inv.save()
                        
                        #print(inv)
                        carts[tododato[1]] = dic

    escribir_log("get_inversiones -- terminado")

def modif_inversiones(periodos=None):#updatea las inversiones
    escribir_log("modif_inversiones -- inicio")

    carteras = ['nac', 'ext']
    carts = {}
    
    fondos = fondo.objects.filter(vigente = True).order_by('runsvs')
    #nro_fondo = "7001"
    #fondos = fondos.filter(runsvs__gte =str(nro_fondo))
    
    #fondos = fondo.objects.filter(pk__startswith = nro_fondo)
    for f in fondos:
        rut = f.runsvs.split("-")
        if periodos == None:
            periods = [get_periodo_actual()]
        else:
            periods = []
            periods.append(periodos)

        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            print(data)
            escribir_log(str(data), "modif_inversiones")
                        
            for cartera in carteras:
                url_cartera = 'http://www.cmfchile.cl/institucional/inc/inf_financiera/ifrs_xml/ifrs_cartera_' + cartera + '.php?rut=' + data['rut'] + '&periodo=' + data['aa'] + data['mm']
                hdr = {'User-Agent': 'Mozilla/5.0'}
                pedido = requests.post(url_cartera, data=data, headers=hdr)

                #pedi2 = requests.post(url_cartera, data=data)
                caldo = BeautifulSoup(pedido.text, 'html.parser', parse_only=SoupStrainer('table'))
                
                flag= True

                iter_carteras = caldo.find_all('tr')
                #print("cartera: "+cartera)
                #print("Inversiones a insertar: "+str(len(iter_carteras)-3)) #todos menos total y headers

                '''try:
                    if cartera == 'nac':
                        c = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =0, pais='CL').count()
                    if cartera == 'ext':
                        c = inversion.objects.filter(fondo= f,  periodo= str(data['mm'])+"-"+str(data['aa']), cartera =0).exclude(pais='CL').count()
                        
                    print("Numero de inversiones: "+str(c))
                    if c == len(iter_carteras)-3 or len(iter_carteras)-3 == 0:
                        flag = False
                    else:
                        flag = True
                except inversion.DoesNotExist:
                    flag = True
                '''
                if flag == True:
                    
                    for row in iter_carteras:
                        dic = {}
                        if len(row.find_all('td')) == 1:
                            # saltarse el padding
                            continue

                        if cartera == 'nac' :
                            interprete = (None, 2, 3, 16, 4, 12, 15, 20, 9, 5, 10, 17)
                        elif cartera == 'ext' :
                            interprete = (3, 1, 4, 17, 5, 13, 16, 21, 10, 6, 11, 18)
                        else :
                            interprete = None

                        if interprete is None or len(interprete) < 9:
                            continue

                        tododato = [tag.string for tag in row.find_all('td')]
                        
                            
                        if len(tododato) < 9:# or tododato[1] is None:# or tododato[1].strip() in excepto:
                            continue

                        #print( 'largo '+str(len(tododato))+' --> '+str(tododato))
                        

                        if interprete[0] is None:

                            # raspar nombre de la sii
                            url_rut = 'https://zeus.sii.cl/cvc_cgi/nar/nar_consulta'
                            if tododato[interprete[1]] == None:
                                #print(tododato)
                                tododato[interprete[1]] =''

                            run = ''.join(tododato[interprete[1]].split())
                            rut_data = {
                                'RUT' : run,
                                'DV' : verificar(run),
                                'ACEPTAR' : 'Consultar'
                            }
                            pedido_rut = requests.post(url_rut, data=rut_data)
                            sopa_rut = BeautifulSoup(pedido_rut.text, 'html.parser')
                            try :
                                if len(run) < 8:
                                    dic['empresa'] = tododato[1].strip()
                                else:
                                    dic['empresa'] = sopa_rut.find(filtro_rut).find_all('font')[1].string.strip()
                            except AttributeError:
                                if tododato[1] == None:
                                    tododato[1] = ''                            
                                dic['empresa'] = tododato[1].strip()
                            except KeyError:
                                if tododato[1] == None:
                                    tododato[1] = ''
                                dic['empresa'] = tododato[1].strip()
                        else:
                            if tododato[interprete[0]] == None:
                                tododato[interprete[0]] = ''
                            dic['empresa'] = tododato[interprete[0]].strip()
                        if dic["empresa"] == '':
                            pass
                            #9306 es laclave
                            #2,2,3,12,4,9,8, 14, 6,0
                        if cartera == 'nac':
                            run = ''.join(tododato[interprete[1]].split())
                            
                            if len(run) < 8:
                                dic['codigo'] = run
                            else:
                                dic['codigo'] = run + '-' + verificar(run)
                            if dic['codigo'] == '':
                                continue

                        if cartera == 'ext':
                            if tododato[interprete[1]] == None:
                                tododato[interprete[1]] = 'NA'
                            dic['codigo'] = tododato[interprete[1]]
                            if dic['codigo'] == ' ':
                                continue

                        #print(tododato)
                        dic['pais'] = tododato[interprete[2]]
                        dic['moneda'] = tododato[interprete[3]]
                        dic['instrumento'] = tododato[interprete[4]]
                        # para descifrar significado del tipo de instrumento, ver www.svs.cl/sitio/seil/certificacion_inst.php
                        dic['valolizacion'] = tododato[interprete[5]]
                        dic['monto'] = format_num(tododato[interprete[6]])
                        dic['porcentaje'] = format_num(tododato[interprete[7]])
                        dic['unidades'] = format_num(tododato[interprete[8]])
                        dic['periodo'] = data['mm'] + '-' + data['aa']
                        
                        dic['tipo_unidades'] = tododato[interprete[10]]
                        dic['cod_pais_transaccion'] = tododato[interprete[11]]
                        

                        if tododato[1] == None:
                            dic['mnemotecnico'] = ''
                        else:
                            dic['mnemotecnico'] = tododato[1].strip().replace(u'\uFFFD', '')
                        if cartera != 'met_part':
                            dic['fecha_vencimiento'] = tododato[interprete[9]]
                        else:
                            dic['fecha_vencimiento'] = None
                        if dic['fecha_vencimiento'] != None and dic['fecha_vencimiento'].strip() != '':
                            dic['fecha_vencimiento'] = format_time(dic['fecha_vencimiento'])
                        else:
                            dic['fecha_vencimiento'] = None

                        try:
                            inv = inversion.objects.filter(fondo= f, periodo= dic['periodo'], fecha_vencimiento = dic['fecha_vencimiento'], monto = dic['monto'], \
                                nemotecnico=dic['mnemotecnico'], empresa= dic['empresa'],  codigo= dic['codigo'],\
                                pais = dic['pais'], moneda=dic['moneda'], instrumento= dic['instrumento'], valolizacion=dic['valolizacion'], \
                                porcentaje= dic['porcentaje'], unidades= dic['unidades'], cartera=0).first()
                            #print(str(inv.id))
                            if inv.tipo_unidades == None and inv.cod_pais_transaccion ==None:
                                inv.tipo_unidades = dic['tipo_unidades']
                                inv.cod_pais_transaccion = dic['cod_pais_transaccion']
                                inv.save()
                        except inversion.DoesNotExist:
                            pass
                        
                        print(str(inv.id)+", "+inv.tipo_unidades+", "+inv.cod_pais_transaccion)
                        carts[tododato[1]] = dic
                
    escribir_log("modif_inversiones -- terminado")

def get_series(fondos_objetivo=None):
    fondos = fondo.objects.filter(vigente = True)
    
    #if fondos_objetivo != None:
    #    fondos_objetivo = fondos_objetivo.split(',')
    #    fondos = fondo.objects.filter(runsvs__in= fondos_objetivo)
    #    print(fondos.count())
    

    for f in fondos:
        url = f.urlsvs
        print(f.runsvs)
        url = url[:-1] + '14'
        url = url.replace('auth=&send=&', '')
        colador = SoupStrainer("tr")
        hdr = {'User-Agent': 'Mozilla/5.0'}
        
        pedido = requests.post(url, headers=hdr)

        #sopa = BeautifulSoup(pedido.text, 'html5lib')

        sopa = BeautifulSoup(pedido.text, 'html.parser', parse_only=colador)
        iterable = sopa.find_all("tr")
        first = True
        llaves = []
        dics = []
        for caldo in iterable:
            if first:
                llaves = [str(tag.string).strip().lower() for tag in caldo.contents]
                first = False
                continue
            else:
                valores = [str(tag.string).strip().upper() for tag in caldo.contents]
                dic = {x: y for x, y in zip(llaves, valores)}
                dics.append(dic)

                if dic['fecha inicio'] != '':
                    dic['fecha inicio'] = format_time(dic['fecha inicio'])
                else:
                    dic['fecha inicio'] = None

                try:
                    s = serie.objects.get(nombre = dic['serie'], fecha_inicio= dic['fecha inicio'], fondo = f)
                except serie.DoesNotExist:
                    s = serie(nombre = dic['serie'], fecha_inicio= dic['fecha inicio'], fondo = f)
                    s.save()

                s.caracteristica = dic['característica']
                if dic['continuadora de serie'] != '' and dic['continuadora de serie'] != 'NO':
                    if dic['continuadora de serie'] == 'SI':
                        s.continuadora_id = serie.objects.latest('nombre')
                    else:
                        try:
                            s.continuadora_id = serie.objects.get(nombre = dic['continuadora de serie'], fondo= f)
                        except serie.DoesNotExist:
                            pass
                if dic['fecha término'] != '':
                    s.fecha_termino = format_time(dic['fecha término'])
                
                if dic['valor inicial cuota'] != '':
                    s.valor_inicial = float(dic['valor inicial cuota'])

                try:
                    #print(u' '.join(json.dumps(dic)).encode('utf-8').strip())
                    s.save()
                except decimal.InvalidOperation:
                    print(u' '.join(json.dumps(dic)).encode('utf-8').strip())
        
    escribir_log("get_series -- terminado")

def rellenar_cuotas():

    fondos = fondo.objects.filter(vigente = True)
    fondos = fondos.filter(admin__rut="96684990-8" )#moneda
    


    for f in fondos:
        print(f.runsvs)
        url = f.urlsvs
        series = serie.objects.filter(continuadora__isnull=False, fondo=f)
        for s in series:
            #si la serie contnnuadora tenia eventos de inversion, guardar dentro de la serie original una copia de fondos de inversion
            try:
                finvs = fondoInversion.objects.filter(serie=s.continuadora)
                for finv in finvs:
                    cfi = finv.cfi
                    #print(cfi)
                    try:
                        finv2 = fondoInversion.objects.filter(serie=s, cfi=cfi).first()
                    except fondoInversion.DoesNotExist:
                        finv2 = finv
                        finv2.pk=None
                        finv2.serie = s
                        finv2.save()
                    #print(str(cfi) +' : '+str(s.nombre))
            except fondoInversion.DoesNotExist:
                pass


            cuotas = cuota.objects.filter(serie=s).order_by('fecha')

            for c in cuotas:
                #CREAR FONDO Y EVENTO DE INVERSION
                
                try:
                    aux = cuota.objects.get(serie= s, fecha=c.fecha)
                    print(s.nombre, c.fecha)
                    if aux.indice == None:
                        aux.valor_libro = c.valor_libro
                        if c.get_divisa_fondo() != None:
                            gdf = c.get_divisa_fondo()
                            aux.valor_libro_conv = convertir(gdf[0], gdf[1], c.valor_libro, c.fecha)
            
                        aux.save()
                        aux.indice = get_indice(aux)
                        aux.save()

                    pass
                except cuota.DoesNotExist:
                    nueva_cuota = cuota(serie = s, fecha=c.fecha, valor_libro= c.valor_libro, moneda=c.moneda, valor_economico=c.valor_economico,\
                        patrimonio_neto=c.patrimonio_neto, activo_total=c.activo_total, agencia=c.agencia, \
                        n_aportantes_inst=c.n_aportantes_inst, n_aportantes=c.n_aportantes)
                    nueva_cuota.save()

                    if nueva_cuota.indice == None:
                        nueva_cuota.indice = get_indice(nueva_cuota)
                        nueva_cuota.save()

                #print(s.nombre, c.fecha)


    
        #return dics
    #return render(request, "scraping.html",  {'tabla' : dics})

def get_cuotas(fondos_objetivo=None, desde=True):
    
    fondos = fondo.objects.filter(vigente=True)

    #fondos_objetivo = ['7111-0', '9123-5', '9176-6', '9180-4', '9341-6', '9370-K', '9439-0', '9525-7', '9127-8']
    #fondos = fondo.objects.filter(runsvs__in= fondos_objetivo)
    
    if fondos_objetivo != None:
        #fondos_objetivo = fondos_objetivo.split(',')
        fondos = fondos.filter(runsvs__in= fondos_objetivo)

    for f in fondos:
        print(f.runsvs)
        escribir_log("get_cuotas -- "+str(f.runsvs))
        url = f.urlsvs
        url = url[:-1] + '7'
        url = url.replace('auth=&send=&', '')
        url = url.replace('pestania=1', 'pestania=7')
        
        fecha_ini = f.inicio_operaciones
        fecha_fin = f.termino_operaciones

        
        
        hace_20_dias = datetime.date.today() - datetime.timedelta(days=60)
        if desde == True or desde == None:
            fecha_ini = hace_20_dias

        
        '''else:
            if fecha_ini < hace_20_dias:
                fecha_ini = hace_20_dias
        '''
        
        if not fecha_fin or not isinstance(fecha_fin, (datetime.date, datetime)):
            fecha_fin = datetime.datetime.today()
        
        print(fecha_ini)
        print(fecha_fin)

        
        dics = []
        dic = {'dia1' : revisar(fecha_ini.day), 'dia2' : revisar(fecha_fin.day), 'mes1' : revisar(fecha_ini.month),
               'mes2' : revisar(fecha_fin.month), 'anio1' : str(fecha_ini.year), 'anio2' : str(fecha_fin.year),
               'enviado' : '1', 'sub_consulta_fi' : 'Consultar'}
        colador = SoupStrainer("tr")

        hdr = {'User-Agent': 'Mozilla/5.0'}
        pedido = requests.post(url, data=dic, headers=hdr)
        
        sopa = BeautifulSoup(pedido.text, 'html.parser', parse_only=colador)
        iterable = sopa.find_all("tr")
        first = True
        llaves = []
        for caldo in iterable:

            if first:
                llaves = []
                for tag in caldo.contents:
                    llave = str(tag.string)
                    llaves.append(limpiar(llave))
                first = False
                continue
            else:
                valores = [str(tag.string).strip().upper() for tag in caldo.contents]
                dic = {x : y for x , y in zip(llaves, valores)}
                string = json.dumps(dic, indent=2)
                if "SIN INFORMACIÓN" in str(dic) or "SIN INFORMACION" in str(dic):
                    break
            
                try:
                    #escribir_log(str(dic)+str(f.runsvs), "get_cuotas")
                    #print(dic['serie'])

                    s = serie.objects.get(nombre = dic['serie'], fondo = f, fecha_inicio__isnull=False)#, fecha_inicio =format_time(dic['fecha']) )
                    

                    try:
                        c = cuota.objects.get(serie = s, fecha =format_time(dic['fecha']))
                        
                    except cuota.DoesNotExist:
                        c = cuota(serie = s, fecha =format_time(dic['fecha']))
                        
                        c.save()

                    
                    
                    c.valor_libro=format_num(dic['valor libro'])
                    c.moneda = dic['moneda']
                    c.valor_economico = format_num(dic['valor economico'])
                    c.patrimonio_neto = int(format_num(dic['patrimonio neto']))
                    c.activo_total = format_num(dic['activo total'])
                    c.agencia = dic['agencia']
                    c.n_aportantes_inst = format_num(dic['no aportantes institucionales'])
                    c.n_aportantes = format_num(dic['no de aportantes'])
                    c.agencia = dic['agencia']
                    c.save()
                    print(c.patrimonio_neto)
                    
                    if c.get_divisa_fondo() != None:
                        gdf = c.get_divisa_fondo()
                        c.valor_libro_conv = convertir(gdf[0], gdf[1], c.valor_libro, c.fecha)
                        c.save()
            
                    
                    if c.indice == None:
                        c.indice = get_indice(c)
                        c.save()
                    print( c.fecha, c )

                except serie.DoesNotExist:
                    pass
                    #break

                #dics.append(dic)
    escribir_log("get_cuotas -- terminado")
    #return dics
    #return render(request, "scraping.html",  {'tabla' : dics})


#scrapping resultados integrales
def get_resultados_integrales(periodos=None, fondos_objetivo = None):
    escribir_log("get_resultados_integrales -- inicio")

    fondos = fondo.objects.all().order_by('runsvs')
    
    if fondos_objetivo != None:
        #fondos_objetivo = fondos_objetivo.split(',')
        fondos = fondos.filter(runsvs__in= fondos_objetivo)
    #fondos_objetivo = ['7111-0', '9123-5', '9176-6', '9180-4', '9341-6', '9370-K', '9439-0', '9525-7', '9127-8']
    #fondos = fondo.objects.filter(runsvs__in= fondos_objetivo)
    #fondos = fondo.objects.filter(pk ="7280-K")
    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=29")

        if periodos == None:
            periods = [get_periodo_actual()]
        else:
            periods = []
            for per in periodos.split(','):
                periods.append(per)

        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            print(data)
            scraperesultados(url, data, f)
    escribir_log("get_resultados_integrales -- terminado")

#scrapping movimientos llama a scrapemovimientos
def get_movimientos(periodos=None, fondos_objetivo=None):
    escribir_log("get_movimientos -- inicio")

    fondos = fondo.objects.all().order_by('runsvs')
    
    if fondos_objetivo != None:
        #fondos_objetivo = fondos_objetivo.split(',')
        fondos = fondos.filter(runsvs__in= fondos_objetivo)
    #fondos_objetivo = ['7111-0', '9123-5', '9176-6', '9180-4', '9341-6', '9370-K', '9439-0', '9525-7', '9127-8']
    #fondos = fondo.objects.filter(runsvs__contains= "9448")
    #periodos = "2018-12"

    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=29")

        if periodos == None:
            periods = [get_periodo_actual()]
        else:
            periods = []
            for per in periodos.split(','):
                periods.append(per)
            
        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            print(data)
            scrapemovimientos(url, data, f)
    escribir_log("get_movimientos -- terminado")

#scraping resumen llama a scraperesumen
def get_resumen(periodos=None, fondos_objetivo=None):
    escribir_log("get_resumen -- inicio")

    fondos = fondo.objects.all().order_by('runsvs')

    if fondos_objetivo != None:
        fondos_objetivo = fondos_objetivo.split(',')
        fondos = fondos.filter(runsvs__in= fondos_objetivo)
    #fondos = fondos.filter(pk__gte="9495-9")
    #fondos_objetivo = ['7111-0', '9123-5', '9176-6', '9180-4', '9341-6', '9370-K', '9439-0', '9525-7', '9127-8']
    #fondos = fondos.filter(runsvs__in= fondos_objetivo)
    

    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=30")
        if periodos == None:
            periods = [get_periodo_actual()]
        else:
            periods = []
            aux = periodos.split(',')
            for a in aux:
                periods.append(a)

        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] , 'tipo': 'ECRI', 'periodo' : str(p[0])+str(p[1]), 'enviado' :1 }
            
            print(f.runsvs)
            print(p[1]+' '+p[0])

            scraperesumen(url, data, f)
    #return render(request, "scraping.html",  {'tabla' : data})
    escribir_log("get_resumen -- terminado")

#get dividendos
def get_dividendos(periodos=None, fondos_objetivo=None):
    def filtro_finanzas(tag):
        return tag.name == 'table' and len(tag.find_all('tr')) > 1
    escribir_log("get_dividendos -- terminado")

    fondos = fondo.objects.all().order_by('runsvs')

    if fondos_objetivo != None:
        print(fondos_objetivo)
        #fondos_objetivo = fondos_objetivo.split(',')
        fondos = fondos.filter(runsvs__in= fondos_objetivo)
    
    for f in fondos:
        rut = f.runsvs.split("-")
        url = f.urlsvs.replace("pestania=1", "pestania=29")

        if periodos == None:
            periods = [get_periodo_actual()]
        else:
            periods = []
            
            for pers in periodos.split(','):
                periods.append(pers)

        for p in periods:
            p = p.split("-")
            data = {'mm': p[1], 'aa': p[0], 'rut': rut[0]}
            #escribir_log(str(p), 'get_dividendos')
            #escribir_log(str(f), 'get_dividendos')
            print(data)
            hdr = {'User-Agent': 'Mozilla/5.0'}
            pedido = requests.post(url, data=data, headers=hdr)

            sopa = BeautifulSoup(pedido.text, 'html5lib')
            iter_finanzas = sopa.find_all(filtro_finanzas)
            dividendos = False
            patrimonios = False
            aportes = False

            if int(p[0]) > 2010 or (int(p[0]) == 2010 and int(p[1]) == 12):
                if iter_finanzas is None:
                    return
                for tabla in iter_finanzas:
                    row = tabla.find('tr').find_all('th')
                    try:
                        if len(row) > 1 and  row[0].string and row[1].string and \
                        'ESTADO DE CAMBIOS EN EL PATRIMONIO NETO' in row[0].string and \
                        data['aa'] in row[1].string:
                            filas = tabla.find_all('tr')
                            
                            contador = 0
                            flag = False
                            flag2 = False
                            flag3 = False

                            for fila in filas:
                                columnas = fila.find_all('td')
          
                                for columna in columnas:
                                    bytetd = str(columna.string).encode('ascii','ignore')
                                    if flag == True:
                                        if bytetd != b'None' and b'Notas adjuntas' not in bytetd :
                                            dividendos = int(bytetd.replace(b'.',b''))
                                        else:
                                            break;
                                    if columna.string != None and b'Repartos de dividendos' in bytetd:
                                        flag = True
                                for columna in columnas:
                                    bytetd = str(columna.string).encode('ascii','ignore')
                                    if flag2 == True:
                                        if bytetd != b'None' and b'Notas adjuntas' not in bytetd :
                                            patrimonios = int(bytetd.replace(b'.',b''))
                                        else:
                                            break;
                                    if columna.string != None and b'Repartos de patrimonio' in bytetd:
                                        flag2 = True

                                for columna in columnas:
                                    bytetd = str(columna.string).encode('ascii','ignore')
                                    if flag3 == True:
                                        if bytetd != b'None' and b'Notas adjuntas' not in bytetd :
                                            aportes = int(bytetd.replace(b'.',b''))
                                        else:
                                            break;
                                    if columna.string != None and b'Aportes' in bytetd:
                                        flag3 = True
                    except AttributeError:
                        continue
            
            else:
                tabla = sopa.find('div', id="variacion")
                try:
                    row = tabla.find_all('tr')

                    for fila in row:
                        cols = fila.find_all('td')
                        try:
                            if cols[0].get_text() == '4.34.00.00':
                                dividendos = cols[2].get_text()
                            if cols[0].get_text() == '4.32.00.00':
                                aportes = cols[2].get_text()
                            if cols[0].get_text() == '4.33.00.00':
                                patrimonios= cols[2].get_text()
                        except IndexError:
                            pass
                except AttributeError:
                    pass

            try:
                perf = periodo.objects.get(fondo=f, periodo=str(data['mm'])+'-'+str(data['aa']))
                dic = json.loads(perf.datos)

                
                dic['REPARTOS DE DIVIDENDOS'] = dividendos
                dic['REPARTOS DE PATRIMONIOS'] = patrimonios
                dic['APORTES'] = aportes
                perf.datos = json.dumps(dic)
                print(dividendos, patrimonios, aportes)
                perf.save()
                escribir_log(str(dic), 'get_dividendos')
                
            except periodo.DoesNotExist:
                pass
     
    escribir_log("get_dividendos -- terminado")

def scrapemovimientos(url, data, fondo):
    def iniciales(frase):
        aux = frase.split()
        ret = ''
        for ini in aux:
            ret += ini[0]
        return ret.upper()

    def filtro_finanzas(tag):
        return tag.name == 'table' and len(tag.find_all('tr')) > 1

    def filtro_restos(tag):
        return tag.name == 'tr' and not tag.find_parent('table')

    def traducir(abbr):
        if abbr == '$$':
            return 'pesos'
        elif abbr == 'PROM':
            return 'dólares'
        elif abbr == 'EUR':
            return 'euro'
        else:
            return abbr
    
    hdr = {'User-Agent': 'Mozilla/5.0'}
    
    pedido = requests.post(url, data=data, headers=hdr)
    sopa = BeautifulSoup(pedido.text, 'html5lib')
    activo = None
    et_comun = ''
    movs = {}
    moneda = ''
    antiguo = True

    
    iter_finanzas = sopa.find_all(filtro_finanzas)
    if iter_finanzas is None:
        return
    for tabla in iter_finanzas:
        row = tabla.find('tr')
        try:
            if row.find('th').string and 'ESTADO DE SITUACION FINANCIERA' in row.find('th').string :
                iter_finanzas = tabla
                antiguo = False
                break
        except AttributeError:
            continue
    if antiguo:  # periodo anterior al 2010
        if len(iter_finanzas) <= 1:  # sin informacion
            return
        iter_finanzas = iter_finanzas[1]
        moneda = sopa.find('table').find_all('tr')[3].find('td').string  # '$$' ??
        moneda = 'miles de ' + traducir(moneda)
        iter_finanzas = iter_finanzas.find_all('tr')
        for fila in iter_finanzas:
            columnas = fila.find_all('td')
            if len(columnas) > 3 :
                if 'TOTAL' in columnas[1].string or 'PATRIMONIO' in columnas[1].string:
                    dic = {'fondo': data['rut'],
                           'periodo': data['mm'] + '-' + data['aa'],
                           'moneda': moneda,
                           'etiqueta_svs' : columnas[1].string.strip(),
                           'ap': True if columnas[1].string.strip() == 'TOTAL ACTIVOS' else False,
                           'monto': columnas[2].string.strip()}
                    if dic['etiqueta_svs'][-1] == 'S':
                        dic['etiqueta_svs'] = dic['etiqueta_svs'][:-1]
                    movs[dic['etiqueta_svs']] = dic

                    try:
                        nuevo_movimiento = movimiento.objects.get(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'])
                    except movimiento.DoesNotExist:
                        nuevo_movimiento = movimiento(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'], monto=0)
                        nuevo_movimiento.save()

                    nuevo_movimiento.ap = dic['ap']
                    nuevo_movimiento.moneda = dic['moneda']
                    nuevo_movimiento.monto = dic['monto'].replace('.','')
                    nuevo_movimiento.save()
                    
                    escribir_log( str(fondo)+' '+dic['periodo'], 'get_movimientos' )

        # Como me suele pasar, el scraper me está recolectando una sopa donde la mitad de los <tr> están fuera de las
        # tablas, por lo que necesito escarbar entre los restos

        iter_finanzas = sopa.find_all(filtro_restos)
        for fila in iter_finanzas:
            columnas = fila.find_all('td')
            if len(columnas) > 3:
                if 'TOTAL' in columnas[1].string or 'PATRIMONIO' in columnas[1].string:
                    dic = {'fondo': data['rut'],
                           'periodo': data['mm'] + '-' + data['aa'],
                           'moneda': moneda,
                           'etiqueta_svs': columnas[1].string.strip(),
                           'ap': True if columnas[1].string.strip() == 'TOTAL ACTIVOS' else False,
                           'monto': columnas[2].string.strip()}

                    if dic['etiqueta_svs'][-1] == 'S':
                        dic['etiqueta_svs'] = dic['etiqueta_svs'][:-1]
                    movs[dic['etiqueta_svs']] = dic
                    try:
                        nuevo_movimiento = movimiento.objects.get(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'])
                    except movimiento.DoesNotExist:
                        nuevo_movimiento = movimiento(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'], monto=0)
                        nuevo_movimiento.save()

                    nuevo_movimiento.ap = dic['ap']
                    nuevo_movimiento.moneda = dic['moneda']
                    nuevo_movimiento.monto = dic['monto'].replace('.','')
                    nuevo_movimiento.save()
                    escribir_log( str(fondo)+' '+dic['periodo'], 'get_movimientos' )
                    
        return movs

    iter_finanzas = iter_finanzas.find_all('tr')
    for fila in iter_finanzas:
        dic = {}

        columnas = fila.find_all('td')
        if len(columnas) > 0:
            if len(columnas) == 1:
                # determinar activo o pasivo
                n_fila = fila.next_sibling if isinstance(fila.next_sibling, element.Tag) \
                    else fila.next_sibling.next_sibling
                if columnas[0].find('strong') is not None and columnas[0].find('strong').string is not None:
                    if n_fila is not None and len(n_fila.find_all('td')) == 1:
                        if columnas[0].find('strong').string == 'ACTIVO':
                            activo = True
                        elif columnas[0].find('strong').string == 'PASIVO':
                            activo = False
                    else:
                        # rotula si es corriente o no corriente
                        et_comun = iniciales(columnas[0].find('strong').string)
                        et_comun += '-'
            elif len(columnas) > 1:
                # raspa nombre de movimiento e información
                for columna in columnas:
                    if columna is not None:
                        if 'fondoOscuro' in columna['class']:
                            # Totales de composición financiera
                            if 'nivel4' in columna['class']:
                                dic['etiqueta_svs'] = limpiar(columna.strong.string).upper()
                            else:
                                if len(columna.find_previous_siblings('td')) == 3:
                                    dic['monto'] = columna.strong.string.strip()
                        else:
                            # Movimientos segun SVS
                            if 'nivel6' in columna['class']:
                                dic['etiqueta_svs'] = et_comun + limpiar(columna.string)
                            elif len(columna.find_previous_siblings('td')) == 3:
                                dic['monto'] = columna.string.strip()

        else:  # este corre primero en las iteraciones, pero por la estructura de los condicionales viene al último
            monedas = fila.find_all('th')
            if monedas is not None:
                for dinero in monedas:
                    if dinero is not None:
                        currency = re.search('(?<=ESTADO DE SITUACION FINANCIERA  \("Expresado en ).+(?="\))',
                                            dinero.string)
                        if currency is not None and moneda == '':
                            moneda = currency.group(0).strip()
            continue
        if moneda == '':
            dic['moneda'] = '¿Pesos?'
        else :
            dic['moneda'] = moneda

        dic['fondo'] = data['rut']
        dic['ap'] = activo
        dic['periodo'] = data['mm'] + '-' + data['aa']


        try :
            movs[dic['etiqueta_svs']] = dic
            try:
                nuevo_movimiento = movimiento.objects.get(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'])
            except movimiento.DoesNotExist:
                nuevo_movimiento = movimiento(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'], monto=0, ap=False)
                nuevo_movimiento.save()

            nuevo_movimiento.ap = dic['ap']
            nuevo_movimiento.moneda = dic['moneda']
            nuevo_movimiento.monto = dic['monto'].replace('.','')
            nuevo_movimiento.save() 
            try:
                print(nuevo_movimiento.fondo.nombre+" -- "+nuevo_movimiento.periodo+" -- "+nuevo_movimiento.etiqueta_svs+" -- "+nuevo_movimiento.monto+" -- "+nuevo_movimiento.moneda)
            except UnicodeEncodeError:
                print("---")
        except KeyError:
            continue

        escribir_log(str(dic), 'get_movimientos')
   
    return movs

def scraperesumen(url, data, fondo):
    def filtro_totales(tag):
        return tag.name == 'strong' and tag.parent.name == 'td'  # basta con name==strong pero mejor mirar

    hdr = {'User-Agent': 'Mozilla/5.0'}
    pedido = requests.post(url, data=data, headers =hdr)
    caldo = BeautifulSoup(pedido.text, 'html.parser', parse_only=SoupStrainer('table'))

    chequeo = caldo.find_all(filtro_totales)
    if chequeo is None or chequeo[3].string.strip() == '0' or chequeo[3].string.strip() == '-':
        return None  # Cartera vacía
    dic = {
        'decripcion': chequeo[0].string.strip(),
        'nacional': chequeo[1].string.strip(),
        'extranjero': chequeo[2].string.strip(),
        'porcentual': chequeo[4].string.strip()
    }
    if dic['nacional'] == '0' and dic['extranjero'] == '0' or dic['porcentual'] == '0':
        #print("error de traspaso??")
        return None  # Cartera vacía o error de traspaso... probablemente... ;;;;;;;
    iterable = caldo.find_all('tr')
    objetos = [dic]
    for row in iterable:
        dic = {}
        if len(row.find_all('td')) < 5:
            continue
        if row.find_all(filtro_totales):
            continue
        datos = [tag.string for tag in row.find_all('td')]
        if len(datos) < 5:
            continue

        dic['decripcion'] = datos[0].strip()
        dic['nacional'] = datos[1].strip().replace('.', '')
        dic['extranjero'] = datos[2].strip().replace('.', '')
        dic['porcentual'] = datos[4].strip().replace(',','.')
        objetos.append(dic)
        #try:
        #    print(dic)
        #except UnicodeEncodeError:
        #    pass
        try:
            period = periodo.objects.get(periodo=data['mm']+'-'+data['aa'], fondo_id= fondo)
            try:
                res = resumen.objects.get(descripcion=dic['decripcion'], periodo=period)
            except resumen.DoesNotExist:
                res = resumen(descripcion=dic['decripcion'], periodo=period)
                res.save()

            if dic['nacional'] == '-':
                res.nacional = 0
            else:
                res.nacional = dic['nacional']
            if dic['extranjero'] == '-':
                res.extranjero = 0
            else:
                res.extranjero = dic['extranjero']
            if dic['porcentual'] == '-':
                res.porcentual = 0
            else:
                res.porcentual = dic['porcentual']
            res.save()
            
        except periodo.DoesNotExist:
            pass



    return {'resumen' : objetos, 'periodo' : data['mm'] + '-' + data['aa']}

def scraperesultados(url, data, fondo):
    def iniciales(frase):
        aux = frase.split()
        ret = ''
        for ini in aux:
            ret += ini[0]
        return ret.upper()

    def filtro_finanzas(tag):
        return tag.name == 'table' and len(tag.find_all('tr')) > 1

    def filtro_restos(tag):
        return tag.name == 'tr' and not tag.find_parent('table')

    hdr = {'User-Agent': 'Mozilla/5.0'}
    
    pedido = requests.post(url, data=data, headers=hdr)
    sopa = BeautifulSoup(pedido.text, 'html.parser')
    tipo = None
    et_comun = ''
    movs = {}
    moneda = ''
    antiguo = True

    iter_finanzas = sopa.find_all(filtro_finanzas)
    if iter_finanzas is None:
        return
    for tabla in iter_finanzas:

        row = tabla.find('tr')
        try:
            if row.find('th').string and 'ESTADO DE RESULTADOS INTEGRALES' in row.find('th').string :
                iter_finanzas = tabla
                break
        except AttributeError:
            continue
    try:
        iter_finanzas = iter_finanzas.find_all('tr')
    except AttributeError:
            return
    for fila in iter_finanzas:
        dic = {}
        if fila == "\n":
            columnas = ''
        else:
            columnas = fila.find_all('td')
        if len(columnas) > 0:
            if len(columnas) == 1:
                # determinar activo o pasivo
                n_fila = fila.next_sibling if isinstance(fila.next_sibling, element.Tag) \
                    else fila.next_sibling.next_sibling
                if columnas[0].find('strong') is not None and columnas[0].find('strong').string is not None:
                    if n_fila is not None and len(n_fila.find_all('td')) == 1:
                        if columnas[0].find('strong').string == 'INGRESOS/ PERDIDAS DE LA OPERACION':
                            tipo = 1
                        elif columnas[0].find('strong').string == 'GASTOS':
                            tipo = 0
                        elif columnas[0].find('strong').string == 'Otros resultados integrales':
                            tipo = 2
                    else:
                        # rotula si es GASTO INGRESO U OTROS
                        et_comun = iniciales(columnas[0].find('strong').string)
                        if et_comun == 'G':
                            tipo = 0
                        elif et_comun == 'IPDLO':
                            tipo = 1
                        elif et_comun == 'ORI':
                            tipo= 2





                        et_comun += '-'
            elif len(columnas) > 1:
                # raspa nombre de movimiento e información
                for columna in columnas:
                    if columna is not None:
                        if 'fondoOscuro' in columna['class']:
                            # Totales de composición financiera
                            if 'nivel4' in columna['class']:
                                dic['etiqueta_svs'] = limpiar(columna.strong.string).upper()
                            else:
                                if 'nivel2' in columna['class']:
                                    dic['etiqueta_svs'] = limpiar(columna.strong.string).upper()
                                    if len(columna.find_previous_siblings('td')) == 4:
                                        dic['monto'] = columna.strong.string.strip()

                            
                                if len(columna.find_previous_siblings('td')) == 3:
                                    dic['monto'] = columna.strong.string.strip()
                        else:
                            # Movimientos segun SVS
                            if 'nivel6' in columna['class']:
                                dic['etiqueta_svs'] = et_comun + limpiar(columna.string)
                            elif len(columna.find_previous_siblings('td')) == 3:
                                dic['monto'] = columna.string.strip()

        else:  # este corre primero en las iteraciones, pero por la estructura de los condicionales viene al último
            monedas = fila.find_all('th')
            if monedas is not None:
                for dinero in monedas:
                    if dinero is not None:
                        currency = re.search('(?<=ESTADO DE RESULTADOS INTEGRALES \("Expresado en ).+(?="\))',
                                            dinero.string)
                        if currency is not None and moneda == '':
                            moneda = currency.group(0).strip()
            continue
        if moneda == '':
            dic['moneda'] = '¿Pesos?'
        else :
            dic['moneda'] = moneda

        dic['fondo'] = data['rut']
        dic['ap'] = tipo
        dic['periodo'] = data['mm'] + '-' + data['aa']


        try :
            movs[dic['etiqueta_svs']] = dic
            
            try:
                nuevo_resultado = resultado.objects.get(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'])
            except resultado.DoesNotExist:
                nuevo_resultado = resultado(fondo= fondo, periodo= dic['periodo'], etiqueta_svs= dic['etiqueta_svs'], monto=0)
                nuevo_resultado.save()

            nuevo_resultado.ap = dic['ap']
            nuevo_resultado.moneda = dic['moneda']
            nuevo_resultado.monto = dic['monto'].replace('.','')
            nuevo_resultado.save() 
            try:
                print(nuevo_resultado.fondo.nombre+" -- "+nuevo_resultado.periodo+" -- "+nuevo_resultado.etiqueta_svs+" -- "+nuevo_resultado.monto+" -- "+nuevo_resultado.moneda)
            except UnicodeEncodeError:
                print("---")
            
        except KeyError:
            continue

        escribir_log(str(dic), 'get_resultados_integrales')
   
    return movs

def insertar_rentabilidades():

    fondos = fondo.objects.filter(vigente=True).order_by('runsvs')
    anio= '2017'
    for f in fondos:

        try:
            r = rent.objects.get(periodo="12-"+str(anio), fondo= f)
            print(f.runsvs)
        except rent.DoesNotExist:
            r = rent(periodo="12-"+str(anio), fondo= f)
            r.save()

            promedios = []


            promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:3][::-1]
            for pa in promedios_aux:
                paperiodo= pa.periodo.split('-')
                promedios.append([paperiodo[1], round(pa.indice_rentabilidad(), 1)])


            #acotamos a 3 anios
            if len(promedios) == 2:
                promedios.insert(0, [str(int(anio)-2), 0])

            if len(promedios) == 1:
                promedios.insert(0, [str(int(anio)-1), 0])
                promedios.insert(0, [str(int(anio)-2), 0])

            if len(promedios) == 0:
                promedios.insert(0, [str(int(anio)), 0])
                promedios.insert(0, [str(int(anio)-1), 0])
                promedios.insert(0, [str(int(anio)-2), 0])

            r.rentab_anio = float(round(promedios[2][1], 2))
            r.rentab_anio_pasado =float(round(promedios[1][1], 2))
            r.rentab_anio_antepasado =float(round(promedios[0][1], 2))
            
            #DATOS RENTABILIDAD PROMEDIO
            rentabilidades = []
            
            aux= 1
            index = 0
            promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:3][::-1]
            for pa in promedios_aux:
                if pa.indice_rentabilidad() != 0:
                    index += 1
                    #print(pa.indice_rentabilidad()/100+1)
                    aux *= (pa.indice_rentabilidad()/100+1)
                    
            if index < 3:
                rentabilidades.append(0)
            else:
                rentabilidades.append(((aux**(1/index))-1)*100)
                #rentabilidades.append(aux/index)

            aux= 1
            index = 0
            promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:5][::-1]
            for pa in promedios_aux:
                if pa.indice_rentabilidad() != 0:
                    index += 1
                    #aux += pa.indice_rentabilidad()
                    #agregado el 1+ rent
                    aux *= (pa.indice_rentabilidad()/100+1)

            if index < 5:
                rentabilidades.append(0)
            else:
                #rentabilidades.append(aux/index)
                rentabilidades.append( ((aux**(1/index))-1)*100 )

            aux = 1
            index = 0
            anios_totales = int(f.anios_totales())
            promedios_aux = periodo.objects.filter(periodo__startswith="12-", fondo= f).order_by('-periodo')[:anios_totales][::-1]
            for pa in promedios_aux:
                    index += 1
                    #aux += pa.indice_rentabilidad()
                    #agregado el 1+ rent
                    aux *= (pa.indice_rentabilidad()/100+1)
                    #print(pa.indice_rentabilidad()+100)
                    if pa.indice_rentabilidad() == 0:
                        anios_totales -= 1
            if index == 0:
                rentabilidades.append(0)
            else:
                rentabilidades.append( ((aux**(1/anios_totales))-1)*100 )

            print(rentabilidades)
            r.rentab_3er_anio =  float(round(rentabilidades[0], 2))
            r.rentab_5to_anio = float(round(rentabilidades[1], 2))
            r.rentab_ini_anio = float(round(rentabilidades[2], 2))
            r.save()
            print(f.runsvs)

def get_acciones(periodos=None):
    #escribir_log("get_acciones -- inicio")
    '''

    
    fondos = fondo.objects.all().order_by('runsvs')
    fondos = fondos.filter( Q(runsvs__contains= '9494') | Q(runsvs__contains= '9161') )
    
    for f in fondos:
        rut = f.runsvs.split("-")
        if periodos == None:
            periods = [get_periodo_actual()]
        else:
            periods = []
            periods.append(periodos)

        for p in periods:
            p = p.split("-")
            data = { 'mm' : p[1], 'aa' : p[0] , 'rut' : rut[0] }
            print(data)
                        
            url_cartera = 'http://www.cmfchile.cl/institucional/inc/inf_financiera/ifrs_xml/ifrs_cartera_nac.php?rut=' + data['rut'] + '&periodo=' + data['aa'] + data['mm']
            
            hdr = {'User-Agent': 'Mozilla/5.0'}
            pedido = requests.post(url_cartera, data=data, headers=hdr)
            caldo = BeautifulSoup(pedido.text, 'html.parser', parse_only=SoupStrainer('table'))
                
            iter_carteras = caldo.find_all('tr')
            
            print(len(iter_carteras))
            count = accion_nacional.objects.filter(fondo=f, periodo= data['mm']+'-'+data['aa']).count()
            if count != len(iter_carteras)-3:
                accion_nacional.objects.filter(fondo=f, periodo= data['mm']+'-'+data['aa']).delete()

                for row in iter_carteras:
                    dic = {}
                    if len(row.find_all('td')) == 1:
                        continue

                    #interprete = (None, 2, 3, 16, 4, 12, 15, 20, 9, 5)                        

                    tododato = [tag.string for tag in row.find_all('td')]

                    if len(tododato) != 0 and 'TOTAL' not in tododato:
                        #print(tododato)

                        a = accion_nacional(fondo=f, periodo= data['mm']+'-'+data['aa'])
                        a.clasificacion = tododato[0]
                        a.nemotecnico = tododato[1]
                        a.rut_emisor = tododato[2]
                        a.cod_pais = tododato[3]
                        a.tipo_instrumento = tododato[4]
                            
                        if tododato[5] != None and tododato[5] != '':
                            format = '%d/%m/%Y'
                            a.fecha_vencimiento = datetime.datetime.strptime(tododato[5], format)

                            #a.fecha_vencimiento = tododato[5]
                        a.situacion_instrumento = tododato[6]
                        a.clasificacion_riesgo = tododato[7]
                        a.grupo_empresarial = tododato[8]
                        a.cant_unidades = limpiar_num(tododato[9])
                        a.tipo_unidades = tododato[10]
                        a.tir_precio = limpiar_num(tododato[11])
                        a.codigo_valolizacion = tododato[12]
                        a.tasa = tododato[13]
                        a.tipo_interes = tododato[14]
                        a.valolizacion_cierre = int(limpiar_num(tododato[15]))
                        a.cod_moneda_liq = tododato[16]
                        a.cod_pais_transaccion = tododato[17]
                        a.cap_emisor = limpiar_num(tododato[18])
                        a.act_emisor = limpiar_num(tododato[19])
                        a.activo_fondo = limpiar_num(tododato[20])
                        a.save()

                    

                #dic['pais'] = tododato[interprete[2]]
    '''
    pass

def get_indicadores():

    inds = ['dolar', 'euro', 'uf']
    sbif_api_key = '010ef7b25a3f455b5bb993950b769a44f8b5ec8a';

    
    #fecha_min = fondo.objects.all().aggregate(Min('inicio_operaciones'))['inicio_operaciones__min']
    
    fecha = datetime.date.today()
    for i in range(365):
        #fecha = date.today() - timedelta(days=10-i)
        fecha = datetime.date.today() - datetime.timedelta(days=i)
        print(fecha)
        try:
            i = indicador.objects.get(pk= fecha)
        except indicador.DoesNotExist:
            i = indicador(fecha=fecha)
            i.save()

        for ind in inds:
            anio = fecha.year
            mes = fecha.month
            dia = fecha.day         

            if ind == 'dolar' and i.dolar:
                pass
            elif ind == 'euro' and i.euro:
                pass
            elif ind == 'uf' and i.uf:
                pass

            else:
                url = 'https://api.sbif.cl/api-sbifv3/recursos_api/'+ind+'/'+str(anio)+'/'+str(mes)+'/dias/'+str(dia)+'?apikey='+sbif_api_key+'&formato=xml';
                    
                page = requests.get(url)
                soup = BeautifulSoup(page.content, 'html.parser')
                try:
                    aux = soup.find('valor').get_text().replace('.','')
                    aux = aux.replace(',','.')
                except AttributeError:
                    print(url)
                    continue

                if ind == 'dolar':
                    i.dolar = float(aux)
                if ind == 'euro':
                    i.euro = float(aux)
                if ind == 'uf':
                    i.uf = float(aux)
                i.save()

        #CONVERSOR DE OTRAS MONEDAS (GBP y MXN)
        if i.mxn:
            pass
        elif i.gbp:
            pass
        else:
            url = "https://api.exchangeratesapi.io/"+str(anio)+"-"+str(mes)+"-"+str(dia)+"?symbols=MXN,GBP"
            print(url)
            response = requests.get(url)
            data = response.text
            parsed = json.loads(data)
            date = parsed["date"]
             
            gbp_rate = float(1/parsed["rates"]["GBP"]) #numero de euros equivalentes
            mxn_rate = float(1/parsed["rates"]["MXN"])
            if i.euro:
                if not(i.gbp):
                    i.gbp = round(gbp_rate*i.euro, 2)
                if not(i.mxn):
                    i.mxn = round(mxn_rate*i.euro, 2)
                i.save()


def check_discontinua():
    #

    lista = ["9214-2", "7264-8", "7202-8", "7251-6", "7234-6", "7007-6", "9165-0", "9185-5", "9130-8", "7262-1", "7253-2", "7141-2", "9256-8", "9275-4", "9327-0", "9314-9", "9311-4", "9309-2", "9273-8", "9360-2", "9337-8", "9308-4", "9151-0", "7197-8", "7039-4", "7279-6", "7111-0", "7236-2", "9207-K", "9242-8", "7256-7", "9143-K", "9146-4", "7281-8"]
    fonds = fondo.objects.filter(pk__in = lista)

    for f in fonds:
        print(f.runsvs)
        invs = fondoInversion.objects.filter(serie__fondo=f)
        for i in invs:
            evs = eventoInversion.objects.filter(fondo_inversion=i.cfi)
            for e in evs:
                escribir_log_puro( str(f.runsvs)+"\t"+str(i.cfi)+"\t"+str(e.fecha_limite)+"\t"+str(e.descripcion)+"\t"+str(e.tipo)+"\t"+str(e.valor))

    

    print('debug')

def get_indices_cron():
    #fondos = fondo.objects.filter(vigente = True)
    #fondos = fondos.exclude(admin__rut="96684990-8" )#moneda
    fondos = ["9151"]
    #fondos = fondos.filter(pk__in=lista_fondos )#moneda

    for f in fondos:
        print(f)
        escribir_log(f, 'get_indices_cron')
        series = serie.objects.filter(fondo__runsvs__contains = f)#, fecha_inicio =format_time(dic['fecha']) )
        s = detalle_fondo.objects.get(pk__runsvs__contains=f).serie_antigua
        cuotas = cuota.objects.filter(serie = s)
        for c in cuotas:
            if c.get_divisa_fondo() != None:
                gdf = c.get_divisa_fondo()
                c.valor_libro_conv = convertir(gdf[0], gdf[1], c.valor_libro, c.fecha)
                c.save()
            c.indice = get_indice(c)
            c.save()
            print( s.nombre, c.fecha )
                

def scrapeinstrumentos():
    hdr = {'User-Agent': 'Mozilla/5.0'}

    url="https://www.svs.cl/sitio/seil/certificacion_inst.php"

    s=requests.Session()
    s.headers.update(hdr)
    r=s.get(url,  verify=False)

    soupdecaracol = BeautifulSoup(r.content, 'html.parser')
    tablas = soupdecaracol.findAll('table')

    del tablas[0]
    del tablas[-1]


    for tabl in tablas:
        tr = tabl.findAll('tr')
        th = tabl.find('th')
        th = str(th.get_text() ).strip()

        for t in tr:
            td = t.findAll('td')
            for i in range(0, len(td), 2):
                try:
                    cod = str(td[i+1].get_text()).strip()
                    desc = str(td[i].get_text()).strip()
                    
                    try:
                        instrumento.objects.get(id = cod, descripcion=desc, tipo=th)
                    except instrumento.DoesNotExist:
                        instrumento(id = cod, descripcion=desc, tipo=th).save()

                except IndexError:
                    pass