from django import template

register = template.Library()

@register.filter
def rango(value, arg=0):
	if arg -5 >= 1:
		inicio = arg-5
	else:
		inicio = 1

	if value+1 <= arg+5:
		fin = value+1
	else:
		fin = arg+5

	return range(inicio, fin)
	
@register.filter()
def to_int(value):
    return int(value)

@register.filter()
def es_exacto(val):
	val = float(val)
	if val == 0:
		return True
	else:
		return False

@register.filter()
def es_mayor_a(invs):
	tot = 0
	for i in invs:
		tot += float(i['porcentaje'])
	if tot >= 25:
		return True
	else:
		return True

#devuelve el numero de columnas que tiene un array convertido en string 
@register.filter()
def nro_columnas(value):
	if value == None or value == '':
		return 0
	array = eval(value)
	return len(array)
