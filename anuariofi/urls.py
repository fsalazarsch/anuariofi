"""anuariofi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, reverse
from django.conf.urls import url, include, handler404, include
from django.contrib.auth.views import login, logout

from acafi.views import getperfil, recuperar, get_login, addbug, mi_error_404
from acafi.view.fondo import index_fondo, listar_fondos, asignar_fondo, factsheet, factsheetpdf, exportar_fondo_adm, regenerar_pdf, catastro, descargar_catastro, importar_cuotastr
from acafi.view.usuario import index_usuario, listar_usuarios, activar_usuario, importar_usuarios
from acafi.view.aportante import listar_aportantes
from acafi.view.perfil import listar_perfiles
from acafi.view.administrador import listar_administradores, crear_pdf
from acafi.view.resumen import listar_resumenes
from acafi.view.composicion import listar_composiciones
from acafi.view.movimiento import listar_movimientos
from acafi.view.inversion import listar_inversiones
from acafi.view.pais import listar_paises
from acafi.view.categoria import listar_categorias
from acafi.view.divisa import listar_divisas
from acafi.view.periodo import listar_periodos
from acafi.view.corfo import index_fondo_corfo, listar_fondos_corfo, factsheet_corfo, import_corfo
from acafi.view.graficos import generar_factsheet, generar_factsheet_corfo, generar_pdf_series, generar_factsheets_series
from acafi.view.archivo import listar_archivos
from acafi.view.rentabilidad import listar_rentabilidades
from acafi.view.serie import listar_series, factsheet_series, recalcular_serie
from acafi.view.cuota import listar_cuotas, listar_cuotas_div
from acafi.view.exportar import listar_exportaciones,  exportar_datos_fondo, exportar_problemas_fondo, exportar_periodos, exportar_fondos, exportar_fondos_corfo, exportar_listado_fondos, exportar_orden_anuario
from acafi.view.catastro import estados_financieros, parametros_iniciales, nuevos_fondos, cartera_inversiones, acciones_nacionales, cuotas_aportantes, clasif_riesgo, inv_moneda, inv_pais, get_lista_activos

from acafi.api.user import get_users, get_user
from acafi.api.categoria import get_categorias, get_categoria
from acafi.api.fondo import get_fondos, get_fondo, get_fondos_corfo, get_fondo_corfo, get_region_exposure
from acafi.api.aportante import get_aportantes, get_aportante
from acafi.api.administrador import get_administradores, get_administrador
from acafi.api.perfil import get_perfiles, get_perfil
from acafi.api.resumen import get_resumen
from acafi.api.lista_fondos import get_lista_fondos_vigentes, get_lista_fondos_nuevos
from acafi.api.eeff import get_resumen_catastro, get_tabla_catastro, get_aportes_netos, get_eeff
from acafi.api.activos import get_activos_fondos_nuevos, get_total_activos, get_activos_fondos, get_activos_por_afi, get_activos_fondos_afi, get_activos_por_clase, get_inversiones_nac, get_inversiones_ext
from acafi.api.patrimonio import get_patrimonio_fondos_afi, get_patrimonio_clase, get_patrimonio_afi, get_patrimonio_cuotas, get_patrimonio_cuotas_pagadas
from acafi.api.cuotas import get_volumen_cuotas_mes
from acafi.api.precatastro import get_parametros_iniciales, get_cartera_inversiones, get_acciones_nacionales, get_cuotas_aportantes, get_clasif_riesgo, get_inv_pais, get_inv_moneda
from acafi.api.composicion import get_composiciones
from acafi.api.movimiento import get_movimientos
from acafi.api.inversion import get_inversiones
from acafi.api.pais import get_paises, get_pais
from acafi.api.region import get_regiones, get_region
from acafi.api.divisa import get_divisas, get_divisa

 
handler404 = mi_error_404

urlpatterns = [

    path('', get_login, name="login"),
    path('logout/', logout, {'next_page': '/login/'}),
    path('login/<response>', get_login, name="login"),
    path('login/', get_login, name="login"),
    path('recuperar/', recuperar,  name="recuperar"),

    path('activar_usuario/<token>/', activar_usuario),

    path('perfil/', getperfil, name="perfil"),
    path('perfil/<response>', getperfil, name="perfil"),
    path('perfil/fondos/', listar_fondos),
    path('perfil/fondos/<periodo>/', listar_fondos),
    path('perfil/fondos/<periodo>/<opcion>/', listar_fondos),

    path('perfil/fondo/<runsvs>', index_fondo),
    path('perfil/asignar_fondo/<runsvs>', asignar_fondo),
    path('perfil/fondo/exportar_adm/', exportar_fondo_adm),
    path('perfil/fondo/exportar_adm/<id>', exportar_fondo_adm),
    path('perfil/fondo/regenerar/<runsvs>', regenerar_pdf),
    path('perfil/fondo/catastro/', catastro),
    path('perfil/fondo/catastro/importar_cuotas/', importar_cuotastr),
    
    path('perfil/fondo/catastro/<libro>/', catastro),
    
    path('perfil/fondo/catastro/generar/<seccion>/<period>/', catastro),
    path('perfil/fondo/catastro/descargar/<seccion>/<period>/', descargar_catastro),

    path('perfil/fondo/catastro/<libro>/<period>/', catastro),
    


    path('perfil/fondopdf/', crear_pdf),
    
    path('perfil/fondos_corfo/', listar_fondos_corfo),
    path('perfil/fondo_corfo/<runsvs>', index_fondo_corfo),
    path('perfil/fondo_corfo/', index_fondo_corfo),

    path('perfil/usuario/', index_usuario),
    path('perfil/usuario/<id>', index_usuario),
    path('perfil/usuarios/', listar_usuarios, name="usuarios"),
    path('perfil/usuarios/<response>', listar_usuarios, name="usuarios"),
    path('perfil/usuario/importar/', importar_usuarios),
    path('perfil/fondo_corfo/importar/', import_corfo),
    

    path('perfil/categorias/', listar_categorias),
    path('perfil/aportantes/', listar_aportantes),
    path('perfil/administradores/', listar_administradores),
    path('perfil/profile/', listar_perfiles),
    path('perfil/resumen/', listar_resumenes),
    path('perfil/composiciones/', listar_composiciones),
    path('perfil/movimientos/', listar_movimientos),
    path('perfil/inversiones/', listar_inversiones),
    path('perfil/paises/', listar_paises),
    path('perfil/divisas/', listar_divisas),
    path('perfil/periodos/', listar_periodos),
    path('perfil/rentabilidades/', listar_rentabilidades),
    path('perfil/series/', listar_series),
    path('perfil/cuotas/', listar_cuotas),
    path('perfil/cuotas/dividendos/', listar_cuotas_div),


    path('perfil/exportar_periodos/', exportar_periodos),
    path('perfil/exportar_fondos/', exportar_fondos),
    path('perfil/exportar_listado_fondos/', exportar_listado_fondos),
    path('perfil/exportar_fondos_corfo/', exportar_fondos_corfo),
    path('perfil/exportar_catastro_activos/', exportar_datos_fondo),
    path('perfil/exportar_problemas_fondo/', exportar_problemas_fondo),
    path('perfil/exportar_orden_anuario/', exportar_orden_anuario),

    path('perfil/addbug/', addbug),

    path('perfil/fondo/factsheet/<id>/', factsheet),
    path('perfil/fondo/factsheet/<id>/<anio>/', factsheet),
    path('perfil/fondo/factsheet/<id>/<anio>/<mes>/', factsheet),
    

    path('perfil/fondo_corfo/factsheet/<id>/<anio>/', factsheet_corfo),
    path('perfil/fondo_corfo/factsheet/<id>/<anio>/<paginador>', factsheet_corfo),
    path('perfil/series/factsheet/<fond>/<nombre>/', factsheet_series),


    path('perfil/archivos/', listar_archivos),
    path('perfil/exportaciones/', listar_exportaciones),

    path('admin/', admin.site.urls),
    

    path('perfil/catastro/1/', estados_financieros),
    path('perfil/catastro/2/', parametros_iniciales),
    path('perfil/catastro/3/', nuevos_fondos),
    path('perfil/catastro/4/', cartera_inversiones),
    path('perfil/catastro/5/', acciones_nacionales),
    path('perfil/catastro/6/', cuotas_aportantes),
    path('perfil/catastro/6/<filter_periodo>/', cuotas_aportantes),
    path('perfil/catastro/7/', clasif_riesgo),
    path('perfil/catastro/8/', inv_pais),
    path('perfil/catastro/9/', inv_moneda),
    path('perfil/catastro/10/', get_lista_activos),

    
    path('genpdf/<fond>/<nombre>/', generar_pdf_series),
    path('genpdf/<fond>/', generar_pdf_series),
    path('genpdf/', generar_factsheets_series),

    path('recalcular_serie/<fond>/<nombre>/', recalcular_serie),


    #path('docs/', import(rest_framework_docs.urls)),
    
    path('api/user/', get_users),
    path('api/user/<id>', get_user),

    path('api/perfil/', get_perfiles),
    path('api/perfil/<id>', get_perfil),

    path('api/fondo/', get_fondos),
    path('api/fondo/<id>', get_fondo),
    path('api/fondo_corfo/', get_fondos_corfo),
    path('api/fondo_corfo/<id>', get_fondo_corfo),
    

    path('api/categoria/', get_categorias),
    path('api/categoria/<id>', get_categoria),

    path('api/aportante/', get_aportantes),
    path('api/aportante/<id>', get_aportante),

    path('api/administrador/', get_administradores),
    path('api/administrador/<id>', get_administrador),

    path('api/resumen/<fondo>/<periodo>', get_resumen),
    path('api/composicion/<fondo>/<periodo>', get_composiciones),
    path('api/movimiento/<fondo>/<periodo>', get_movimientos),
    path('api/inversion/<fondo>/<periodo>', get_inversiones),

    path('api/lista_vigentes/<period>',get_lista_fondos_vigentes),
    path('api/lista_nuevos/<period>',get_lista_fondos_nuevos),

    path('api/activos_fondos_nuevos/<period>', get_activos_fondos_nuevos),
    path('api/total_activos/<period>', get_total_activos),
    path('api/activos_fondos/<period>', get_activos_fondos),
    path('api/activos_por_afi/<period>',get_activos_por_afi),
    path('api/activos_fondos_afi/<period>', get_activos_fondos_afi),
    path('api/activos_por_clase/<period>', get_activos_por_clase),
    path('api/inversiones_nac/<period>', get_inversiones_nac),
    path('api/inversiones_ext/<period>', get_inversiones_ext),


    path('api/resumen_catastro/<period>', get_resumen_catastro),
    path('api/tabla_catastro/<period>', get_tabla_catastro),
    path('api/aportes_netos/<period>', get_aportes_netos),
    path('api/eeff/<tipo>/<period>', get_eeff),

    path('api/patrimonio_fondo/<period>', get_patrimonio_fondos_afi),
    path('api/patrimonio_clase/<period>', get_patrimonio_clase),
    path('api/patrimonio_afi/<period>', get_patrimonio_afi),
    path('api/valor_cuotas/<period>', get_patrimonio_cuotas),
    path('api/cuotas_pagadas/<period>', get_patrimonio_cuotas_pagadas),

    path('api/volumen_cuotas_mes/<period>', get_volumen_cuotas_mes),

    path('api/parametros_iniciales/<period>', get_parametros_iniciales),
    path('api/cartera_inversiones/<period>', get_cartera_inversiones),
    path('api/acciones_nacionales/<period>', get_acciones_nacionales),
    path('api/cuotas_aportantes/<period>', get_cuotas_aportantes),
    path('api/clasif_riesgo/<period>', get_clasif_riesgo),
    path('api/inv_pais/<period>', get_inv_pais),
    path('api/inv_moneda/<period>', get_inv_moneda),

    path('api/pais/', get_paises),
    path('api/pais/<id>', get_pais),

    path('api/region/', get_regiones),
    path('api/region/<id>', get_region),

    path('api/region_exposure/<runsvs>/', get_region_exposure),

    path('api/divisa/', get_divisas),
    path('api/divisa/<id>', get_divisa),

    path('factsheet/<fond_inicio>/<fond_termino>/<anio>/<paginador>', generar_factsheet),
    path('corfo/<anio>/<paginador>', generar_factsheet_corfo),
    path('pdf/<id>/<anio>/<paginador>', factsheetpdf),

    #url(r'^404/$', django.views.defaults.page_not_found, ),

]

